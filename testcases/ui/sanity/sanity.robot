*** Settings ***
Test Teardown       Close All Browsers
#Library             Selenium2Library
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot

Resource    ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource    ${CURDIR}/../../../resources/testdata/ui/${ENV}/fe/sanity.robot

Resource    ${CURDIR}/../../../resources/keywords/ui/uicommon.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/fe/landing.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/fe/search.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/fe/levelc.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/fe/leveld.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/fe/checkout2.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/fe/checkout3.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/fe/thankyou.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/fe/myaccount.robot

Resource    ${CURDIR}/../../../resources/keywords/ui/backend/login.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/backend/order_detail.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/backend/order_list.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/backend/order_status.robot

Resource    ${CURDIR}/../../../resources/keywords/ui/astra/banner.robot
Resource    ${CURDIR}/../../../resources/keywords/ui/astra/login.robot

*** Variables ***

*** Test Cases ***
TC_001_(Leisure,1Room,2Adults,2Night,CreditCard,PayNow)_Verify able to book and cancel booking for direct hotel.
    [Documentation]   Summary= Leisure,1Room,2Adults,2Night,CreditCard,PayNow   
    ...     Device= Desktop
    ...     TravellingPurpose=  Leisure
    ...     NumberOfRoom= 1 Room
    ...     Traveller= 2 Adults, 0 Child
    ...     NumberOfNight= 2 Night
    ...     PaymentMethod=  CreditCard
    ...     PaymentType= PayNow
    ...     SearchType= Destination
    ...     HotelType= Direct
    ...     RoomType= Room Free Cancellation
    ...     CheckIn= 2
    [Tags]  wip

    ${number_of_room}       Set Variable    1
    ${number_of_guest}      Set Variable    2  
    ${number_of_night}      Set Variable    2
    ${checkin_day}          Set Variable    2
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    
    #Login
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_card_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_card_pass}
    Click 'Login' button   

    #Search
    search.Verify search home page is displayed
    Select travelling purpose    ${leisure_purpose}
    Enter search keyword    ${destination_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_destination}     ${destination_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button

    #LevelC
    Verify search result on level c page display correctly   ${destination_name}
    Verify CheckIn/CheckOut date on level c page display correctly  ${checkin_checkout_text}
    Verify hotel display on level c screen  ${destination_hotel}
    Select Hotel from Level C screen    ${hotel_direct_name}

    #LevelD
    Select Window   NEW
    Verify hotel display on level d screen  ${hotel_direct_name}
    Select number of room from room type name   ${hotel_direct_room_type_free_cancellation}   1
    Click 'Book Now' button

    #Checkout2
    checkout2.Verify 'Travelling For' on 'Booker Information'     ${leisure_purpose}
    checkout2.Verify Booker Email     ${credit_card_user}
    checkout2.Enter Booker First Name     ${booker_firstname}
    checkout2.Enter Booker Last Name  ${booker_lastname}
    checkout2.Enter Booker Moblie No  ${booker_mobile_no}
    checkout2.Enter Guest First Name Using Room and Guest Index     0   0   ${guest_room_one_firstname_credit_card}
    checkout2.Enter Guest Last Name Using Room and Guest Index    0   0   ${guest_room_one_lastname_credit_card}
    checkout2.Enter Guest Email Address Using Room and Guest Index     0   0   ${guest_room_one_email_credit_card}
    checkout2.Select Guest Nationality Using Room and Guest Index    0   0   ${guest_room_one_nationality}
    checkout2.Click 'Continue' button

    #Checkout3
    checkout3.Verify 'Hotel Information' on 'Booking Details'     ${hotel_direct_name}
    ${hotel_direct_room_list}=  Create List
    Append To List  ${hotel_direct_room_list}   ${hotel_direct_room_type_free_cancellation}
    checkout3.Verify 'Room Information' on 'Booking Details'  ${number_of_room}     ${hotel_direct_room_list}

    checkout3.Select payment method       ${payment_method_credit_card}
    checkout3.Enter Credit Card number    ${credit_card_number}
    checkout3.Enter name on credit card   ${name_on_card}
    checkout3.Select Card Expiry Date     ${card_expiry_month}    ${card_expiry_year}
    checkout3.Enter CVV Number    ${credit_card_cvv}
    checkout3.Select Tax Invoice/​Receipt Information by company name     ${tax_invoice_name}
    ${tax_invoice_tax_id}=  checkout3.Get Tax ID by company name  ${tax_invoice_name}
    ${tax_invoice_address}=     checkout3.Get Tax Address by company name     ${tax_invoice_name}
    checkout3.Click 'Continue' button

    #ThankYou
    thankyou.Verify booking status on Thank you page   ${booking_status_credit_card_paynow}
    thankyou.Verify 'Save Voucher' button is displayed
    thankyou.Verify 'Print' button is displayed
    ${booking_id}=  Get Booking Id from Thank you screen 
    thankyou.Verify Booker Name  ${booker_firstname}     ${booker_lastname}
    thankyou.Verify Booker Mobile Number     ${booker_mobile_no}
    thankyou.Verify Booker Email Address     ${credit_card_user} 
    thankyou.Verify Tax Invoice Company Name     ${tax_invoice_name} 
    thankyou.Verify Tax Invoice TaxID/Personal ID    ${tax_invoice_tax_id} 
    thankyou.Verify Tax Invoice Address  ${tax_invoice_address}

    thankyou.Verify Property Name    ${hotel_direct_name} 
    thankyou.Verify Property Address     ${hotel_direct_address} 
    thankyou.Verify number of room   ${number_of_room}
    thankyou.Verify number of guest  ${number_of_guest}
    Close All Browsers


    #Verify booking status in backend
    ${current_date}         Get Current Date
    ${expect_booking_date}    Convert Date To Text In Order List    ${current_date}
    ${expect_payment_and_email_date}    Convert Date Payment Sending Email In Order Detail    ${current_date}
    ${expect_booker_full_name}=    Set Variable    ${booker_firstname}${SPACE}${booker_lastname}
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Maximize Browser Window
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Success
    Verify Order List Column Booking Date      ${expect_booking_date}
    Verify Order List Column Booker Name       ${expect_booker_full_name}
    Verify Order List Column Booker Email      ${credit_card_user}
    Verify Order List Column Traveller Name    ${guest_room_one_firstname_credit_card}${SPACE}${guest_room_one_lastname_credit_card}
    Verify Order List Column Payment Status    Success
    Verify Order List Column Payment Method    Credit Card
    Verify Order List Column Cancel Status     No

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Booking Details On Order Detail    ${expect_booker_full_name}   ${credit_card_user}    ${booker_mobile_no}    ${EMPTY}
    #Verify Tax Invoice Address On Order Detail    ${address_company}    ${address_tax_number}    ${address_address}
    #Verify Payment Details on Order Detail    Credit Card    2c2p    -
    #...    authorize    Pay Later    ${expect_order_detail_payment_date}    ${EMPTY}
    Verify Sending Email Conformation to Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Traveller    Success    ${expect_payment_and_email_date}
    Close All Browsers

    #Login to cancel booking
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_card_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_card_pass}
    Click 'Login' button 

    #Cancel Booking
    search.Click My Account botton 
    search.Click My Booking button 
    Cancel Booking using Booking ID     ${booking_id}
    Click Confirm on cancellation pop-up 
    Verify successfully cancel message is displayed     ${booking_id}
    Close All Browsers

    #Verify cancel status in backend
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Cancel
    Verify Order List Column Cancel Status    Void

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Sending Email Cancel To Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Traveller    Success    ${expect_payment_and_email_date}

TC_002_(Leisure,2Rooms,3Adults,1Night,CreditCard,PayLater)_Verify able to book and cancel booking for direct hotel.
    [Documentation]   Summary= Leisure,2Rooms,3Adults,1Night,CreditCard,PayLater  
    ...     Device= Desktop
    ...     TravellingPurpose=  Leisure
    ...     NumberOfRoom= 2 Rooms (Same Room Type)
    ...     Traveller= 3 Adults, 0 Child
    ...     NumberOfNight= 1 Night
    ...     PaymentMethod=  CreditCard
    ...     PaymentType= PayNow
    ...     SearchType= Hotel
    ...     HotelType= Direct
    ...     RoomType= 5 days cancel
    ...     CheckIn= 10
    [Tags]  wip

    ${number_of_room}       Set Variable    2
    ${number_of_guest}      Set Variable    3  
    ${number_of_night}      Set Variable    2
    ${checkin_day}          Set Variable    10
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    
    #Login
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_card_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_card_pass}
    Click 'Login' button   

    #Search
    search.Verify search home page is displayed
    Select travelling purpose    ${leisure_purpose}
    Enter search keyword    ${hotel_direct_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_hotel}     ${hotel_direct_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button


    #LevelD
    Verify hotel display on level d screen  ${hotel_direct_name}
    Select number of room from room type name   ${hotel_direct_room_type_pay_later}   2
    Click 'Book Now' button

    #Checkout2
    Verify 'Travelling For' on 'Booker Information'     ${leisure_purpose}
    Verify Booker Email     ${credit_card_user}
    Enter Booker First Name     ${booker_firstname}
    Enter Booker Last Name  ${booker_lastname}
    Enter Booker Moblie No  ${booker_mobile_no}
    Enter Guest First Name Using Room and Guest Index     0   0   ${guest_room_one_firstname_credit_card}
    Enter Guest Last Name Using Room and Guest Index    0   0   ${guest_room_one_lastname_credit_card}
    Enter Guest Email Address Using Room and Guest Index     0   0   ${guest_room_one_email_credit_card}
    Select Guest Nationality Using Room and Guest Index    0   0   ${guest_room_one_nationality}

    Enter Guest First Name Using Room and Guest Index     1   0   ${guest_room_two_firstname_credit_card}
    Enter Guest Last Name Using Room and Guest Index    1   0   ${guest_room_two_lastname_credit_card}
    Enter Guest Email Address Using Room and Guest Index     1   0   ${guest_room_two_email_credit_card}
    Select Guest Nationality Using Room and Guest Index    1   0   ${guest_room_two_nationality}
    checkout2.Click 'Continue' button

    #Checkout3
    checkout3.Verify 'Hotel Information' on 'Booking Details'     ${hotel_direct_name}
    ${hotel_direct_room_list}=  Create List
    Append To List  ${hotel_direct_room_list}   ${hotel_direct_room_type_pay_later}
    checkout3.Verify 'Room Information' on 'Booking Details'  ${number_of_room}     ${hotel_direct_room_list}  ${TRUE}

    Select payment method       ${payment_method_credit_card}
    Enter Credit Card number    ${credit_card_number}
    Enter name on credit card   ${name_on_card}
    Select Card Expiry Date     ${card_expiry_month}    ${card_expiry_year}
    Enter CVV Number    ${credit_card_cvv}
    Select Tax Invoice/​Receipt Information by company name     ${tax_invoice_name}
    ${tax_invoice_tax_id}=  Get Tax ID by company name  ${tax_invoice_name}
    ${tax_invoice_address}=     Get Tax Address by company name     ${tax_invoice_name}
    checkout3.Click 'Continue' button

    #ThankYou
    Verify booking status on Thank you page   ${booking_status_credit_card_paylater}
    Verify 'Save Voucher' button is displayed
    Verify 'Print' button is displayed
    ${booking_id}=  Get Booking Id from Thank you screen 
    thankyou.Verify Booker Name  ${booker_firstname}     ${booker_lastname}
    thankyou.Verify Booker Mobile Number     ${booker_mobile_no}
    thankyou.Verify Booker Email Address     ${credit_card_user} 
 
    thankyou.Verify Tax Invoice Company Name     ${tax_invoice_name} 
    thankyou.Verify Tax Invoice TaxID/Personal ID    ${tax_invoice_tax_id} 
    thankyou.Verify Tax Invoice Address  ${tax_invoice_address}

    thankyou.Verify Property Name    ${hotel_direct_name} 
    thankyou.Verify Property Address     ${hotel_direct_address} 
    thankyou.Verify number of room   ${number_of_room}
    thankyou.Verify number of guest  ${number_of_guest}
    Close All Browsers

    #Verify booking status in backend
    ${current_date}         Get Current Date
    ${expect_booking_date}    Convert Date To Text In Order List    ${current_date}
    ${expect_payment_and_email_date}    Convert Date Payment Sending Email In Order Detail    ${current_date}
    ${expect_booker_full_name}=    Set Variable    ${booker_firstname}${SPACE}${booker_lastname}
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Maximize Browser Window
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Success
    Verify Order List Column Booking Date      ${expect_booking_date}
    Verify Order List Column Booker Name       ${expect_booker_full_name}
    Verify Order List Column Booker Email      ${credit_card_user}
    Verify Order List Column Traveller Name    ${guest_room_one_firstname_credit_card}${SPACE}${guest_room_one_lastname_credit_card}
    Verify Order List Column Payment Status    Authorize
    Verify Order List Column Payment Method    Credit Card
    Verify Order List Column Cancel Status     No

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Booking Details On Order Detail    ${expect_booker_full_name}   ${credit_card_user}    ${booker_mobile_no}    ${EMPTY}
    #Verify Tax Invoice Address On Order Detail    ${address_company}    ${address_tax_number}    ${address_address}
    #Verify Payment Details on Order Detail    Credit Card    2c2p    -
    #...    authorize    Pay Later    ${expect_order_detail_payment_date}    ${EMPTY}
    Verify Sending Email Conformation to Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Traveller    Success    ${expect_payment_and_email_date}
    Close All Browsers

    #Login to cancel booking
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_card_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_card_pass}
    Click 'Login' button 

    #Cancel Booking
    search.Click My Account botton 
    search.Click My Booking button 
    Cancel Booking using Booking ID     ${booking_id}
    Click Confirm on cancellation pop-up 
    Verify successfully cancel message is displayed     ${booking_id}
    Close All Browsers

    #Verify cancel status in backend
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Cancel
    Verify Order List Column Cancel Status    Non Refund

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Sending Email Cancel To Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Traveller    Success    ${expect_payment_and_email_date}


TC_003_(Work,1Room,2Adults,1Night,CreditCard,PayNow)_Verify able to book and cancel booking for direct hotel.
    [Documentation]   Summary= Work,1Room,2Adults,1Night,CreditCard,PayNow
    ...     Device= Desktop
    ...     TravellingPurpose=  Work
    ...     NumberOfRoom= 1 Rooms
    ...     Traveller= 2 Adults, 0 Child
    ...     NumberOfNight= 1 Night
    ...     PaymentMethod=  CreditCard
    ...     PaymentType= PayNow
    ...     SearchType= Hotel
    ...     HotelType= Direct
    ...     RoomType= 5 days cancel
    ...     CheckIn= 6
    [Tags]  wip

    ${number_of_room}       Set Variable    1
    ${number_of_guest}      Set Variable    2  
    ${number_of_night}      Set Variable    1
    ${checkin_day}          Set Variable    6
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    
    #Login
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_card_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_card_pass}
    Click 'Login' button   

    #Search
    search.Verify search home page is displayed
    Select travelling purpose    ${work_purpose}
    Enter search keyword    ${hotel_direct_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_hotel}     ${hotel_direct_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button


    #LevelD
    Verify hotel display on level d screen  ${hotel_direct_name}
    Select number of room from room type name   ${hotel_direct_room_type_pay_later}   ${number_of_room}
    Click 'Book Now' button

    #Checkout2
    Verify 'Travelling For' on 'Booker Information'     ${work_purpose}
    Verify Booker Email     ${credit_card_user}
    Enter Booker First Name     ${booker_firstname}
    Enter Booker Last Name  ${booker_lastname}
    Enter Booker Moblie No  ${booker_mobile_no}
    Enter Guest First Name Using Room and Guest Index     0   0   ${guest_room_one_firstname_credit_card}
    Enter Guest Last Name Using Room and Guest Index    0   0   ${guest_room_one_lastname_credit_card}
    Enter Guest Email Address Using Room and Guest Index     0   0   ${guest_room_one_email_credit_card}
    Select Guest Nationality Using Room and Guest Index    0   0   ${guest_room_one_nationality}
    checkout2.Click 'Continue' button

    #Checkout3
    checkout3.Verify 'Hotel Information' on 'Booking Details'     ${hotel_direct_name}
    ${hotel_direct_room_list}=  Create List
    Append To List  ${hotel_direct_room_list}   ${hotel_direct_room_type_pay_later}
    Verify 'Room Information' on 'Booking Details'  ${number_of_room}     ${hotel_direct_room_list}  ${FALSE}

    Select payment method       ${payment_method_credit_card}
    Enter Credit Card number    ${credit_card_number}
    Enter name on credit card   ${name_on_card}
    Select Card Expiry Date     ${card_expiry_month}    ${card_expiry_year}
    Enter CVV Number    ${credit_card_cvv}
    Select Tax Invoice/​Receipt Information by company name     ${tax_invoice_name}
    ${tax_invoice_tax_id}=  Get Tax ID by company name  ${tax_invoice_name}
    ${tax_invoice_address}=     Get Tax Address by company name     ${tax_invoice_name}
    checkout3.Click 'Continue' button

    #ThankYou
    Verify booking status on Thank you page   ${booking_status_credit_card_paynow}
    Verify 'Save Voucher' button is displayed
    Verify 'Print' button is displayed
    ${booking_id}=  Get Booking Id from Thank you screen 
    thankyou.Verify Booker Name  ${booker_firstname}     ${booker_lastname}
    thankyou.Verify Booker Mobile Number     ${booker_mobile_no}
    thankyou.Verify Booker Email Address     ${credit_card_user} 
 
    thankyou.Verify Tax Invoice Company Name     ${tax_invoice_name} 
    thankyou.Verify Tax Invoice TaxID/Personal ID    ${tax_invoice_tax_id} 
    thankyou.Verify Tax Invoice Address  ${tax_invoice_address}

    thankyou.Verify Property Name    ${hotel_direct_name} 
    thankyou.Verify Property Address     ${hotel_direct_address} 
    thankyou.Verify number of room   ${number_of_room}
    thankyou.Verify number of guest  ${number_of_guest}
    Close All Browsers

    #Verify booking status in backend
    ${current_date}         Get Current Date
    ${expect_booking_date}    Convert Date To Text In Order List    ${current_date}
    ${expect_payment_and_email_date}    Convert Date Payment Sending Email In Order Detail    ${current_date}
    ${expect_booker_full_name}=    Set Variable    ${booker_firstname}${SPACE}${booker_lastname}
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Maximize Browser Window
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Success
    Verify Order List Column Booking Date      ${expect_booking_date}
    Verify Order List Column Booker Name       ${expect_booker_full_name}
    Verify Order List Column Booker Email      ${credit_card_user}
    Verify Order List Column Traveller Name    ${guest_room_one_firstname_credit_card}${SPACE}${guest_room_one_lastname_credit_card}
    Verify Order List Column Payment Status    Success
    Verify Order List Column Payment Method    Credit Card
    Verify Order List Column Cancel Status     No

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Booking Details On Order Detail    ${expect_booker_full_name}   ${credit_card_user}    ${booker_mobile_no}    ${EMPTY}
    #Verify Tax Invoice Address On Order Detail    ${address_company}    ${address_tax_number}    ${address_address}
    #Verify Payment Details on Order Detail    Credit Card    2c2p    -
    #...    authorize    Pay Later    ${expect_order_detail_payment_date}    ${EMPTY}
    Verify Sending Email Conformation to Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Traveller    Success    ${expect_payment_and_email_date}
    Close All Browsers

    #Login to cancel booking
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_card_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_card_pass}
    Click 'Login' button 

    #Cancel Booking
    search.Click My Account botton 
    search.Click My Booking button 
    Cancel Booking using Booking ID     ${booking_id}
    Click Confirm on cancellation pop-up 
    Verify successfully cancel message is displayed     ${booking_id}
    Close All Browsers

    #Verify cancel status in backend
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Cancel
    Verify Order List Column Cancel Status    Void

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Sending Email Cancel To Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Traveller    Success    ${expect_payment_and_email_date}
    #Cancel Booking
    


TC_004_(Work,1Room,2Adults,1Night,CreditCard,PayLater)_Verify able to book and cancel booking for direct hotel.
    [Documentation]   Summary= Work,1Room,2Adults,2Night,CreditCard,PayLater
    ...     Device= Desktop
    ...     TravellingPurpose=  Work
    ...     NumberOfRoom= 1 Rooms
    ...     Traveller= 2 Adults, 0 Child
    ...     NumberOfNight= 2 Night
    ...     PaymentMethod=  CreditCard
    ...     PaymentType= PayLater
    ...     SearchType= Hotel
    ...     HotelType= Direct
    ...     RoomType= 5 days cancel
    ...     CheckIn= 10
    [Tags]  wip

    ${number_of_room}       Set Variable    1
    ${number_of_guest}      Set Variable    2  
    ${number_of_night}      Set Variable    1
    ${checkin_day}          Set Variable    10
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    
    #Login
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_card_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_card_pass}
    Click 'Login' button   

    #Search
    search.Verify search home page is displayed
    Select travelling purpose    ${work_purpose}
    Enter search keyword    ${destination_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_destination}     ${destination_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button

    #LevelC
    Verify search result on level c page display correctly   ${destination_name}
    Verify CheckIn/CheckOut date on level c page display correctly  ${checkin_checkout_text}
    Verify hotel display on level c screen  ${destination_hotel}
    Select Hotel from Level C screen    ${hotel_direct_name}

    #LevelD
    Select Window   NEW
    Verify hotel display on level d screen  ${hotel_direct_name}
    Select number of room from room type name   ${hotel_direct_room_type_pay_later}   ${number_of_room}
    Click 'Book Now' button

    #Checkout2
    Verify 'Travelling For' on 'Booker Information'     ${work_purpose}
    Verify Booker Email     ${credit_card_user}
    Enter Booker First Name     ${booker_firstname}
    Enter Booker Last Name  ${booker_lastname}
    Enter Booker Moblie No  ${booker_mobile_no}
    Enter Guest First Name Using Room and Guest Index     0   0   ${guest_room_one_firstname_credit_card}
    Enter Guest Last Name Using Room and Guest Index    0   0   ${guest_room_one_lastname_credit_card}
    Enter Guest Email Address Using Room and Guest Index     0   0   ${guest_room_one_email_credit_card}
    Select Guest Nationality Using Room and Guest Index    0   0   ${guest_room_one_nationality}
    checkout2.Click 'Continue' button

    #Checkout3
    checkout3.Verify 'Hotel Information' on 'Booking Details'     ${hotel_direct_name}
    ${hotel_direct_room_list}=  Create List 
    Append To List  ${hotel_direct_room_list}   ${hotel_direct_room_type_pay_later}
    Verify 'Room Information' on 'Booking Details'  ${number_of_room}  ${hotel_direct_room_list}

    Select payment method       ${payment_method_credit_card}
    Enter Credit Card number    ${credit_card_number}
    Enter name on credit card   ${name_on_card}
    Select Card Expiry Date     ${card_expiry_month}    ${card_expiry_year}
    Enter CVV Number    ${credit_card_cvv}
    Select Tax Invoice/​Receipt Information by company name     ${tax_invoice_name}
    ${tax_invoice_tax_id}=  Get Tax ID by company name  ${tax_invoice_name}
    ${tax_invoice_address}=     Get Tax Address by company name     ${tax_invoice_name}
    checkout3.Click 'Continue' button

    #ThankYou
    Verify booking status on Thank you page   ${booking_status_credit_card_paylater}
    Verify 'Save Voucher' button is displayed
    Verify 'Print' button is displayed
    ${booking_id}=  Get Booking Id from Thank you screen 
    thankyou.Verify Booker Name  ${booker_firstname}     ${booker_lastname}
    thankyou.Verify Booker Mobile Number     ${booker_mobile_no}
    thankyou.Verify Booker Email Address     ${credit_card_user} 
 
    thankyou.Verify Tax Invoice Company Name     ${tax_invoice_name} 
    thankyou.Verify Tax Invoice TaxID/Personal ID    ${tax_invoice_tax_id} 
    thankyou.Verify Tax Invoice Address  ${tax_invoice_address}

    thankyou.Verify Property Name    ${hotel_direct_name} 
    thankyou.Verify Property Address     ${hotel_direct_address} 
    #Verify Check-In date    ${check_in_date} 
    #Verify Check-Out date   ${check_out_date} 
    #thankyou.Verify Room Type    ${hotel_direct_room_type_name}
    thankyou.Verify number of room   ${number_of_room}
    thankyou.Verify number of guest  ${number_of_guest}
    Close All Browsers

    #Verify booking status in backend
    ${current_date}         Get Current Date
    ${expect_booking_date}    Convert Date To Text In Order List    ${current_date}
    ${expect_payment_and_email_date}    Convert Date Payment Sending Email In Order Detail    ${current_date}
    ${expect_booker_full_name}=    Set Variable    ${booker_firstname}${SPACE}${booker_lastname}
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Maximize Browser Window
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Success
    Verify Order List Column Booking Date      ${expect_booking_date}
    Verify Order List Column Booker Name       ${expect_booker_full_name}
    Verify Order List Column Booker Email      ${credit_card_user}
    Verify Order List Column Traveller Name    ${guest_room_one_firstname_credit_card}${SPACE}${guest_room_one_lastname_credit_card}
    Verify Order List Column Payment Status    Authorize
    Verify Order List Column Payment Method    Credit Card
    Verify Order List Column Cancel Status     No

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Booking Details On Order Detail    ${expect_booker_full_name}   ${credit_card_user}    ${booker_mobile_no}    ${EMPTY}
    #Verify Tax Invoice Address On Order Detail    ${address_company}    ${address_tax_number}    ${address_address}
    #Verify Payment Details on Order Detail    Credit Card    2c2p    -
    #...    authorize    Pay Later    ${expect_order_detail_payment_date}    ${EMPTY}
    Verify Sending Email Conformation to Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Traveller    Success    ${expect_payment_and_email_date}
    Close All Browsers

    #Login to cancel booking
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_card_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_card_pass}
    Click 'Login' button 

    #Cancel Booking
    search.Verify search home page is displayed
    search.Click My Account botton 
    search.Click My Booking button 
    Cancel Booking using Booking ID     ${booking_id}
    Click Confirm on cancellation pop-up 
    Verify successfully cancel message is displayed     ${booking_id}
    Close All Browsers

    #Verify cancel status in backend
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Cancel
    Verify Order List Column Cancel Status    Non Refund

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Sending Email Cancel To Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Traveller    Success    ${expect_payment_and_email_date}

TC_005_(Work,2Rooms,4Adults,2Night,CreditTerms,PayNow)_Verify able to book and cancel booking for direct hotel.
    [Documentation]   Summary= Work,2Rooms,4Adults,2Night,CreditTerms,PayNow
    ...     Device= Desktop
    ...     TravellingPurpose=  Work
    ...     NumberOfRoom= 2 Rooms
    ...     Traveller= 4 Adults, 0 Child
    ...     NumberOfNight= 2 Night
    ...     PaymentMethod=  CreditTerms
    ...     PaymentType= PayNow
    ...     SearchType= Hotel
    ...     HotelType= Direct
    ...     RoomType= 5 days cancel,Room Free Cancellation
    ...     CheckIn= 10
    [Tags]  wip1

    ${number_of_room}       Set Variable    2
    ${number_of_guest}      Set Variable    4  
    ${number_of_night}      Set Variable    2
    ${checkin_day}          Set Variable    10
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    
    #Login
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_term_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_term_pass}
    Click 'Login' button   

    #Search
    search.Verify search home page is displayed
    Select travelling purpose    ${work_purpose}
    Enter search keyword    ${hotel_direct_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_hotel}     ${hotel_direct_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button

    #LevelD
    Verify hotel display on level d screen  ${hotel_direct_name}
    Select number of room from room type name   ${hotel_direct_room_type_free_cancellation}   1
    Select number of room from room type name   ${hotel_direct_room_type_pay_later}   1
    Click 'Book Now' button

    #Checkout2
    Verify 'Travelling For' on 'Booker Information'     ${work_purpose}
    Verify Booker Email     ${credit_term_user}
    Enter Booker First Name     ${booker_firstname}
    Enter Booker Last Name  ${booker_lastname}
    Enter Booker Moblie No  ${booker_mobile_no}
    Enter Guest First Name Using Room and Guest Index     0   0   ${guest_room_one_firstname_credit_card}
    Enter Guest Last Name Using Room and Guest Index    0   0   ${guest_room_one_lastname_credit_card}
    Enter Guest Email Address Using Room and Guest Index     0   0   ${guest_room_one_email_credit_card}
    Select Guest Nationality Using Room and Guest Index    0   0   ${guest_room_one_nationality}

    Enter Guest First Name Using Room and Guest Index     1   0   ${guest_room_two_firstname_credit_card}
    Enter Guest Last Name Using Room and Guest Index    1   0   ${guest_room_two_lastname_credit_card}
    Enter Guest Email Address Using Room and Guest Index     1   0   ${guest_room_two_email_credit_card}
    Select Guest Nationality Using Room and Guest Index    1   0   ${guest_room_two_nationality}

    checkout2.Click 'Continue' button

    #Checkout3
    checkout3.Verify 'Hotel Information' on 'Booking Details'     ${hotel_direct_name}
    ${hotel_direct_room_list}=  Create List
    Append To List  ${hotel_direct_room_list}   ${hotel_direct_room_type_free_cancellation}   ${hotel_direct_room_type_pay_later}
    Verify 'Room Information' on 'Booking Details'  ${number_of_room}     ${hotel_direct_room_list}  ${FALSE}

    Select payment method       ${payment_method_credit_terms}
    # Enter Cost Center   ${cost_center}   
    # Enter Cost Center Name  ${cost_center_name}     
    Enter Company Refernece code  ${company_reference_code}   
    Select Tax Invoice/​Receipt Information by company name     ${tax_invoice_name_credit_terms}
    ${tax_invoice_tax_id}=  Get Tax ID by company name for credit terms  ${tax_invoice_name_credit_terms}
    ${tax_invoice_address}=   Get Tax Address by company name for credit terms   ${tax_invoice_name_credit_terms}
    checkout3.Click 'Continue' button

    #ThankYou
    Verify booking status on Thank you page   ${booking_status_credit_terms}
    Verify 'Save Voucher' button is displayed
    Verify 'Print' button is displayed
    ${booking_id}=  Get Booking Id from Thank you screen 
    thankyou.Verify Booker Name  ${booker_firstname}     ${booker_lastname}
    thankyou.Verify Booker Mobile Number     ${booker_mobile_no}
    thankyou.Verify Booker Email Address     ${credit_term_user} 
 
    thankyou.Verify Tax Invoice Company Name     ${tax_invoice_name_credit_terms} 
    thankyou.Verify Tax Invoice TaxID/Personal ID    ${tax_invoice_tax_id} 
    thankyou.Verify Tax Invoice Address  ${tax_invoice_address}

    thankyou.Verify Property Name    ${hotel_direct_name} 
    thankyou.Verify Property Address     ${hotel_direct_address} 
    thankyou.Verify number of room   ${number_of_room}
    thankyou.Verify number of guest  ${number_of_guest}
    Close All Browsers

    #Verify booking status in backend
    ${current_date}         Get Current Date
    ${expect_booking_date}    Convert Date To Text In Order List    ${current_date}
    ${expect_payment_and_email_date}    Convert Date Payment Sending Email In Order Detail    ${current_date}
    ${expect_booker_full_name}=    Set Variable    ${booker_firstname}${SPACE}${booker_lastname}
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Maximize Browser Window
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Success
    Verify Order List Column Booking Date      ${expect_booking_date}
    Verify Order List Column Booker Name       ${expect_booker_full_name}
    Verify Order List Column Booker Email      ${credit_term_user}
    Verify Order List Column Traveller Name    ${guest_room_one_firstname_credit_card}${SPACE}${guest_room_one_lastname_credit_card}
    Verify Order List Column Payment Status    Pending
    Verify Order List Column Payment Method    Credit Terms
    Verify Order List Column Cancel Status     No

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Booking Details On Order Detail    ${expect_booker_full_name}   ${credit_term_user}    ${booker_mobile_no}    ${EMPTY}
    #Verify Tax Invoice Address On Order Detail    ${address_company}    ${address_tax_number}    ${address_address}
    #Verify Payment Details on Order Detail    Credit Card    2c2p    -
    #...    authorize    Pay Later    ${expect_order_detail_payment_date}    ${EMPTY}
    Verify Sending Email Conformation to Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Traveller    Success    ${expect_payment_and_email_date}
    Close All Browsers

    #Login to cancel booking
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_term_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_term_pass}
    Click 'Login' button 

    #Cancel Booking
    search.Click My Account botton 
    search.Click My Booking button 
    Cancel Booking using Booking ID     ${booking_id}
    Click Confirm on cancellation pop-up 
    Verify successfully cancel message is displayed     ${booking_id}
    Close All Browsers

    #Verify cancel status in backend
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Cancel
    Verify Order List Column Cancel Status    Non Refund

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Sending Email Cancel To Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Traveller    Success    ${expect_payment_and_email_date}


TC_006_(HotelBed_POI)_Verify able to change search criteria on level C.
   [Documentation]     Device= Mobile
    ...     TravellingPurpose=  Work to Leisure
    ...     SearchType= POI
    ...     HotelType= HotelBed
    ...     Traveller= 3 Adult and 1 Child
    ...     NumberOfRoom= 1 room
    [Tags]  wip1
    
    ${number_of_night}      Set Variable    1
    ${checkin_day}          Set Variable    2
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    
    #Login
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_term_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_term_pass}
    Click 'Login' button   

    #Search
    search.Verify search home page is displayed
    Select travelling purpose    ${work_purpose}
    Enter search keyword    ${poi_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_poi}     ${poi_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button


    #LevelC
    Verify search result on level c page display correctly   ${poi_name}
    Verify CheckIn/CheckOut date on level c page display correctly  ${checkin_checkout_text}
    Verify hotel display on level c screen  ${poi_hotel_name}

    Click 'Change' link
    ${number_of_night}      Set Variable    2
    ${number_of_room}       Set Variable    1
    ${number_of_adult}      Set Variable    3
    ${number_of_child}      Set Variable    1
    ${number_of_child_age}  Set Variable    12

    Select travelling purpose    ${leisure_purpose}
    Enter search keyword    ${change_poi_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_poi}     ${change_poi_name}
    Click 'Room&Guest' dropdown list
    Select number of Room from dropdown list    ${number_of_room}
    Select number of child  ${number_of_child}
    Select number of child age  ${number_of_child_age}
    Close Room&Guest dropdown list
    levelc.Click 'Search' button

    Verify search result on level c page display correctly   ${change_poi_name}
    Verify CheckIn/CheckOut date on level c page display correctly  ${checkin_checkout_text}
    Verify hotel display on level c screen  ${change_poi_hotel_name}

TC_007_(HotelBed_Hotel)_Verify able to change search criteria on level D.
   [Documentation]     Device= Tablet
    ...     TravellingPurpose=  Leisure to Work
    ...     SearchType= Destination to Hotel
    ...     HotelType= Direct
    ...     Traveller= 5 Adults to 4 Adults
    ...     NumberOfRoom= 2 rooms (different type)
    ...     NumberOfNight= 3 nights to 2 nights
    [Tags]  Done

    ${number_of_night}      Set Variable    3
    ${checkin_day}          Set Variable    1
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    ${number_of_room_and_guest}     Set Variable    1 Room, 2 Adult, 0 Child
    
    #Login
    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_term_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_term_pass}
    Click 'Login' button   

    #Search
    search.Verify search home page is displayed
    Select travelling purpose    ${leisure_purpose}
    Enter search keyword    ${hotel_bed_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_hotel}     ${hotel_bed_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button

    #Level D
    Verify hotel display on level d screen  ${hotel_bed_name}
    
    ${number_of_night}      Set Variable    2
    ${number_of_room}       Set Variable    2
    ${checkin_day}          Set Variable    5
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}

    Click 'Change' link
    Select travelling purpose    ${work_purpose}
    Enter search keyword    ${hotel_bed_change_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_hotel}     ${hotel_bed_change_name}

    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    ${expected_checkin_checkout_text}=   Get CheckIn and CheckOut date
    #search.Close calendar  ${mobile_device}

    Click 'Room&Guest' dropdown list
    Select number of Room from dropdown list    ${number_of_room}
    Close Room&Guest dropdown list
    levelc.Click 'Search' button

    Verify hotel display on level d screen  ${hotel_bed_change_name}

TC_008_Verify able to create/delete active banner.
    [Tags]  

    ${path-img-th}                           Set Variable       ${CURDIR}/../../../resources/testdata/ui/${ENV}/astra/banner/Banner Photo.jpg
    ${path-img-en}                           Set Variable       ${CURDIR}/../../../resources/testdata/ui/${ENV}/astra/banner/Banner Photo.jpg
    ${link}                                  Set Variable       ${URL_Banner}
    ${day_add_0}                             Set Variable       0
    ${day_add_7}                             Set Variable       7
    ${day_add_20}                            Set Variable       20
    ${time_add_0}                            Set Variable       00:00:000
    ${time_add_2}                            Set Variable       00:02:000
    ${margin}                                Set Variable       100
    ${priority}                              Set Variable       999.99

    Go To ASTRA Page And Login      ${USERNAME_ASTRA}       ${PASSWORD_ASTRA}
    Go To Banner List
    Go To Button Create New Banner

    Create Banner/Edit         ${path-img-th}        ${path-img-en}        โรบอท แอคทีฟ แบนเนอร์       Robot Active Banner     โรบอท รายละเอียด แอคทีฟ แบนเนอร์
    ...                 Robot Active Banner Description     ${link}     ${day_add_0}        ${day_add_7}       ${day_add_0}        ${day_add_7}     ${day_add_0}
    ...                 ${time_add_0}        ${day_add_7}       ${time_add_0}        ${margin}      ${priority}

    Go To Save Banner

    Verify Banner List First Row      Robot Active Banner     Active
    Input Search In Banner List       Robot Active Banner
    Verify Banner List First Row      Robot Active Banner     Active

    uicommon.Open Browser     ${FRONTEND_BASE_URL}  ${desktop_device}
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    Click 'Get Started' button
    Enter email address to login   ${credit_term_user}  
    landing.Click 'Continue' button
    Enter password for login    ${credit_term_pass}
    Click 'Login' button

    Verify Banner Frontend First Item       Robot Active Banner
    Verify Hotel Name Level D       ${link}         zdvhml

    Switch To ASTRA
    Delete Banner

    Input Search In Banner List     Robot Active Banner
    Verify No Reccords In Banner List

    Switch To Frontend
    Reload Page
    Verify Banner Frontend First Item Should Not Show        Robot Active Banner

    Close Browser




TC_009_TRUE_(Work,1Room,2Adults,1Night,CreditTerm,PayNow)_Verify able to book and cancel booking for direct hotel.
   [Documentation]   Summary= Work,1Room,2Adults,1Night,CreditTerm,PayNow
    ...     Device= Desktop
    ...     TravellingPurpose=  Work
    ...     NumberOfRoom= 1 Rooms
    ...     Traveller= 2 Adults, 0 Child
    ...     NumberOfNight= 1 Night
    ...     PaymentMethod=  CreditTerms
    ...     PaymentType= PayNow
    ...     SearchType= Hotel
    ...     HotelType= Direct
    ...     RoomType= 5 days cancel,Room Free Cancellation
    ...     CheckIn= 10
    [Tags]  trueja

    ${number_of_room}       Set Variable    1
    ${number_of_guest}      Set Variable    2  
    ${number_of_night}      Set Variable    1
    ${checkin_day}          Set Variable    10
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    ${file_path}    Set Variable    ${CURDIR}${/}..${/}..${/}..${/}resources${/}testdata${/}ui${/}${ENV}${/}fe${/}ascendtravel.png
    
    #Login
    uicommon.Open Browser     ${FRONTEND_TRUE_URL}  ${desktop_device}
    Enter True Username     ${true_user}
    Enter True Password     ${true_pass}
    Click True Login Button 

    #AcceptTermAndConditions
    Verify term and conditions popup is display
    Accept Term and conditions 
    Click 'OK' button from Term and conditions popup 
    Wait until term and condition popup is dismissed
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    search.Verify search home page is displayed

    #Search
    Select travelling purpose    ${work_purpose}
    Enter search keyword    ${hotel_direct_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_hotel}     ${hotel_direct_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button

    #LevelD
    leveld.Verify budget limit banner is displayed
    Verify hotel display on level d screen  ${hotel_direct_name}
    Select number of room from room type name   ${hotel_direct_room_type_free_cancellation}   1
    Click 'Book Now' button

    #Checkout2
    Verify 'Travelling For' on 'Booker Information'     ${work_purpose}
    Verify Booker Email     ${true_email}
    Enter Booker First Name     ${true_firstname}
    Enter Booker Last Name  ${true_lastname}
    Enter Booker Moblie No  ${true_mobile_number}
    Enter Guest First Name Using Room and Guest Index     0   0   ${guest_room_one_firstname_credit_card}
    Enter Guest Last Name Using Room and Guest Index    0   0   ${guest_room_one_lastname_credit_card}
    Enter Guest Email Address Using Room and Guest Index     0   0   ${guest_room_one_email_credit_card}
    Select Guest Nationality Using Room and Guest Index    0   0   ${guest_room_one_nationality}
    checkout2.Click 'Continue' button

    #Checkout3
    Select payment method       ${payment_method_credit_terms}
    checkout3.Verify 'Hotel Information' on 'Booking Details'     ${hotel_direct_name}
    ${hotel_direct_room_list}=  Create List
    Append To List  ${hotel_direct_room_list}   ${hotel_direct_room_type_free_cancellation}
    Verify 'Room Information' on 'Booking Details'  ${number_of_room}     ${hotel_direct_room_list}  ${FALSE}

    Verify cost center  ${true_cost_center}
    Verify cost center name     ${true_cost_center_name}
    Enter Company Refernece code    ${true_ref_code}
    Select Checkout 3 Credit Terms Upload File    ${file_path}
    #Select Tax Invoice/​Receipt Information by company name     ${tax_invoice_name_credit_terms}
    ${true_tax_invoice_tax_id}=  Get Tax ID by company name for credit terms  ${true_tax_invoice_name_credit_terms}
    ${true_tax_invoice_address}=   Get Tax Address by company name for credit terms   ${true_tax_invoice_name_credit_terms}
    checkout3.Click 'Continue' button

    #ThankYou
    Verify booking status on Thank you page   ${booking_status_credit_terms}
    Verify 'Save Voucher' button is displayed
    Verify 'Print' button is displayed
    ${booking_id}=  Get Booking Id from Thank you screen 
    thankyou.Verify Booker Name  ${true_firstname}     ${true_lastname}
    thankyou.Verify Booker Mobile Number     ${true_mobile_number}
    thankyou.Verify Booker Email Address     ${true_email} 
 
    thankyou.Verify Tax Invoice Company Name     ${true_tax_invoice_name_credit_terms} 
    thankyou.Verify Tax Invoice TaxID/Personal ID    ${true_tax_invoice_tax_id} 
    thankyou.Verify Tax Invoice Address  ${true_tax_invoice_address}

    thankyou.Verify Property Name    ${hotel_direct_name} 
    thankyou.Verify Property Address     ${hotel_direct_address} 
    thankyou.Verify number of room   ${number_of_room}
    thankyou.Verify number of guest  ${number_of_guest}
    Close All Browsers

    #Verify booking status in backend
    ${current_date}         Get Current Date
    ${expect_booking_date}    Convert Date To Text In Order List    ${current_date}
    ${expect_payment_and_email_date}    Convert Date Payment Sending Email In Order Detail    ${current_date}
    ${expect_booker_full_name}=    Set Variable    ${true_firstname}${SPACE}${true_lastname}
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Maximize Browser Window
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Success
    Verify Order List Column Booking Date      ${expect_booking_date}
    Verify Order List Column Booker Name       ${expect_booker_full_name}
    Verify Order List Column Booker Email      ${true_email}
    Verify Order List Column Traveller Name    ${guest_room_one_firstname_credit_card}${SPACE}${guest_room_one_lastname_credit_card}
    Verify Order List Column Payment Status    Pending
    Verify Order List Column Payment Method    Credit Terms
    Verify Order List Column Cancel Status     No

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Booking Details On Order Detail    ${expect_booker_full_name}   ${true_email}    ${true_mobile_number}    ${EMPTY}
    #Verify Tax Invoice Address On Order Detail    ${address_company}    ${address_tax_number}    ${address_address}
    #Verify Payment Details on Order Detail    Credit Card    2c2p    -
    #...    authorize    Pay Later    ${expect_order_detail_payment_date}    ${EMPTY}
    Verify Sending Email Conformation to Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Traveller    Success    ${expect_payment_and_email_date}
    Close All Browsers

    #Login to cancel booking
    uicommon.Open Browser     ${FRONTEND_TRUE_URL}  ${desktop_device}
    Enter True Username     ${true_user}
    Enter True Password     ${true_pass}
    Click True Login Button 

    #AcceptTermAndConditions
    Verify term and conditions popup is display
    Accept Term and conditions 
    Click 'OK' button from Term and conditions popup 
    Wait until term and condition popup is dismissed
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    search.Verify search home page is displayed

    #Cancel Booking
    search.Click My Account botton 
    search.Click My Booking button 
    Cancel Booking using Booking ID     ${booking_id}
    Click Confirm on cancellation pop-up 
    Verify successfully cancel message is displayed     ${booking_id}
    Close All Browsers

    #Verify cancel status in backend
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Cancel
    Verify Order List Column Cancel Status    Non Refund

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Sending Email Cancel To Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Traveller    Success    ${expect_payment_and_email_date}

TC_010_SCG_(Work,1Room,2Adults,1Night,CreditTerm,PayNow)_Verify able to book and cancel booking for direct hotel.
   [Documentation]   Summary= Work,1Room,2Adults,1Night,CreditTerm,PayNow
    ...     Device= Desktop
    ...     TravellingPurpose=  Work
    ...     NumberOfRoom= 1 Rooms
    ...     Traveller= 2 Adults, 0 Child
    ...     NumberOfNight= 1 Night
    ...     PaymentMethod=  CreditTerms
    ...     PaymentType= PayNow
    ...     SearchType= Destination
    ...     HotelType= Direct
    ...     RoomType= Room Free Cancellation
    ...     CheckIn= 1
    [Tags]  wip33

    ${number_of_room}       Set Variable    1
    ${number_of_guest}      Set Variable    2  
    ${number_of_night}      Set Variable    1
    ${checkin_day}          Set Variable    1
    ${checkout_day}         Evaluate    ${checkin_day} + ${number_of_night}
    ${file_path}    Set Variable    ${CURDIR}/../../../resources/testdata/ui/${ENV}/fe/ascendtravel.png
    
    #Login
    uicommon.Open Browser     ${FRONTEND_SCG_URL}  ${desktop_device}
    Enter SCG Username     ${scg_username}
    Enter SCG Password     ${scg_password}
    Click SCG Login Button 

    #BudgetLimitNotify
    Verify Budget Limit notification popup is displayed
    Verify Budget Limit Text display correctly  ${scg_budget_limit_txt_th} 
    Click 'OK' button from Budget Limit notification popup 
    Wait until budget limit popup is dismissed
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    search.Verify search home page is displayed

    #Search
    Select travelling purpose    ${work_purpose}
    Enter search keyword    ${destination_name}
    Wait until search suggestions box display
    Select search result from suggestion box    ${seach_type_destination}     ${destination_name}
    Select 'Check-in' date  ${checkin_day}  today
    Select 'Check-Out' date     ${checkout_day}     today
    Close calendar  ${desktop_device}
    ${checkin_checkout_text}=   Get CheckIn and CheckOut date
    search.Click 'Search' button

    #LevelC
    Verify search result on level c page display correctly   ${destination_name}
    levelc.Verify budget limit banner is displayed
    Verify CheckIn/CheckOut date on level c page display correctly  ${checkin_checkout_text}
    Verify hotel display on level c screen  ${destination_hotel}
    Select Hotel from Level C screen    ${destination_hotel}

    #LevelD
    Select Window   NEW
    leveld.Verify budget limit banner is displayed
    Verify hotel display on level d screen  ${hotel_direct_name}
    Select number of room from room type name   ${hotel_direct_room_type_free_cancellation}   1
    Click 'Book Now' button

    #Checkout2
    Verify 'Travelling For' on 'Booker Information'     ${work_purpose}
    Verify Booker Email     ${scg_email}
    Enter Booker First Name     ${scg_booker_first_name}
    Enter Booker Last Name  ${scg_booker_last_name}
    Enter Booker Moblie No  ${scg_mobile_number}
    Enter Guest First Name Using Room and Guest Index     0   0   ${guest_room_one_firstname_credit_card}
    Enter Guest Last Name Using Room and Guest Index    0   0   ${guest_room_one_lastname_credit_card}
    Enter Guest Email Address Using Room and Guest Index     0   0   ${guest_room_one_email_credit_card}
    Select Guest Nationality Using Room and Guest Index    0   0   ${guest_room_one_nationality}
    checkout2.Click 'Continue' button

    #Checkout3
    Select payment method       ${payment_method_credit_terms}
    checkout3.Verify 'Hotel Information' on 'Booking Details'     ${hotel_direct_name}
    ${hotel_direct_room_list}=  Create List
    Append To List  ${hotel_direct_room_list}   ${hotel_direct_room_type_free_cancellation}
    Verify 'Room Information' on 'Booking Details'  ${number_of_room}     ${hotel_direct_room_list}  ${FALSE}
    Verify cost center  ${scg_cost_center}
    Verify cost center name     ${scg_cost_center_name}
    Select Checkout 3 Credit Terms Upload File    ${file_path}

    Verify Tax Invoice/Receipt Information name     ${scg_tax_invoice_name_credit_terms}
    Verify Tax Invoice/Receipt Information id by Tax Invoice Name   ${scg_tax_invoice_name_credit_terms}    ${scg_tax_invoice_id_credit_terms}
    Verify Tax Invoice/Receipt Information address by Tax Invoice Name  ${scg_tax_invoice_name_credit_terms}    ${scg_tax_invoice_address_credit_terms}
    
    Select 'Bill To' information from billing name  ${scg_bill_to_name}
    checkout3.Click 'Continue' button

    #ThankYou
    Verify booking status on Thank you page   ${booking_status_credit_terms}
    Verify 'Save Voucher' button is displayed
    Verify 'Print' button is displayed
    ${booking_id}=  Get Booking Id from Thank you screen 
    thankyou.Verify Booker Name  ${scg_booker_first_name}     ${scg_booker_last_name}
    thankyou.Verify Booker Mobile Number     ${scg_mobile_number}
    thankyou.Verify Booker Email Address     ${scg_email} 
 
    thankyou.Verify Tax Invoice Company Name     ${scg_tax_invoice_name_credit_terms} 
    thankyou.Verify Tax Invoice TaxID/Personal ID    ${scg_tax_invoice_id_credit_terms} 
    thankyou.Verify Tax Invoice Address  ${scg_tax_invoice_address_credit_terms}

    thankyou.Verify Property Name    ${hotel_direct_name} 
    thankyou.Verify Property Address     ${hotel_direct_address} 
    thankyou.Verify number of room   ${number_of_room}
    thankyou.Verify number of guest  ${number_of_guest}
    Close All Browsers

    #Verify booking status in backend
    ${current_date}         Get Current Date
    ${expect_booking_date}    Convert Date To Text In Order List    ${current_date}
    ${expect_payment_and_email_date}    Convert Date Payment Sending Email In Order Detail    ${current_date}
    ${expect_booker_full_name}=    Set Variable    ${scg_booker_first_name}${SPACE}${scg_booker_last_name}
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Maximize Browser Window
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Success
    Verify Order List Column Booking Date      ${expect_booking_date}
    Verify Order List Column Booker Name       ${expect_booker_full_name}
    Verify Order List Column Booker Email      ${scg_email}
    Verify Order List Column Traveller Name    ${guest_room_one_firstname_credit_card}${SPACE}${guest_room_one_lastname_credit_card}
    Verify Order List Column Payment Status    Pending
    Verify Order List Column Payment Method    Credit Terms
    Verify Order List Column Cancel Status     No

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Booking Details On Order Detail    ${expect_booker_full_name}   ${scg_email}    ${scg_mobile_number}    ${EMPTY}
    #Verify Tax Invoice Address On Order Detail    ${address_company}    ${address_tax_number}    ${address_address}
    #Verify Payment Details on Order Detail    Credit Card    2c2p    -
    #...    authorize    Pay Later    ${expect_order_detail_payment_date}    ${EMPTY}
    Verify Sending Email Conformation to Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Conformation to Traveller    Success    ${expect_payment_and_email_date}
    Close All Browsers

    #Login to cancel booking
    uicommon.Open Browser     ${FRONTEND_SCG_URL}  ${desktop_device}
    Enter SCG Username     ${scg_username}
    Enter SCG Password     ${scg_password}
    Click SCG Login Button 

    #BudgetLimitNotify
    Verify Budget Limit notification popup is displayed
    Verify Budget Limit Text display correctly  ${scg_budget_limit_txt_th} 
    Click 'OK' button from Budget Limit notification popup 
    Wait until budget limit popup is dismissed
    Click menu language     ${desktop_device}
    Select language     ${desktop_device}     ${en_lang}
    search.Verify search home page is displayed

    #Cancel Booking
    search.Click My Account botton 
    search.Click My Booking button 
    Cancel Booking using Booking ID     ${booking_id}
    Click Confirm on cancellation pop-up 
    Verify successfully cancel message is displayed     ${booking_id}
    Close All Browsers

    #Verify cancel status in backend
    uicommon.Open Browser     ${BACKEND_BASE_URL}  ${desktop_device}
    Input Username And Password Login Backend   ${USERNAME_BACKEND}     ${PASSWORD_BACKEND}
    Go To Order List Page
    Search Order List By Booking Id    ${booking_id}
    Hide All Column Order List
    Verify Order List Column Booking Status    Cancel
    Verify Order List Column Cancel Status    Non Refund

    Go To Order Detail Page    ${booking_id}
    Sleep    2s
    Verify Sending Email Cancel To Supplier    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Booker    Success    ${expect_payment_and_email_date}
    Verify Sending Email Cancel To Traveller    Success    ${expect_payment_and_email_date}
