*** Settings ***
#Suite Setup         Prepare Test Data
Test Teardown       Close All Browsers      
Library             Selenium2Library
Library             DatabaseLibrary
Library             String
Library             ${CURDIR}/../../../library/excel.py
Resource            ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource            ${CURDIR}/../../../resources/keywords/ui/astra/supplier.robot
Resource            ${CURDIR}/../../../resources/keywords/ui/astra/login.robot
Resource            ${CURDIR}/../../../resources/keywords/ui/astra/pagination.robot
Resource            ${CURDIR}/../../../resources/keywords/db/supplier.robot
Resource            ${CURDIR}/../../../resources/keywords/common/common.robot
Resource            ${CURDIR}/../../../resources/testdata/api/${ENV}/astra/supplier/supplier.robot
### Som - I believe this set of set data can be seperate into UI/API. If so,please do ja.
### If not, please move this file to be under common(common between UI/API) folder

# Resource            ${CURDIR}/../../../resources/testData/${ENV}/common/common.robot
*** Variables ***
${path_folder}              ${CURDIR}/../../../resources/TestData/${ENV}/Astra/Supplier/supplier.xlsx
${sheet_name}               suppliertestdata
${page_size_default}        50
${page_size_s}              100
${page_size_m}              150
${page_size_l}              200
${sort_asc}                 ASC
${sort_desc}                DESC
${active_status}            active
${inactive_status}          inactive

*** Keywords ***
Prepare Test Data
    Log To Console    ...
    Log To Console    Preparing Suppliers Test Data
    ${col_name}=      Create List    A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   P 
    # Please set DB_NAME to 'alpha_act'
    # Clear Test Data 
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    :FOR    ${index}    IN RANGE    2   7   
    \   ${country_id}       get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[0]}${index}
    \   ${supplier_type}    get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[1]}${index}
    \   ${name}             get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[2]}${index}
    \   ${address}          get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[3]}${index}
    \   ${tax_id}           get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[4]}${index} 
    \   ${tax_name}         get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[5]}${index} 
    \   ${tax_address}      get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[6]}${index}     
    \   ${followup_tax}     get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[7]}${index}
    \   ${email}            get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[8]}${index}
    \   ${markup}           get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[9]}${index}
    \   ${incl_vat}         get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[10]}${index}
    \   ${vat}              get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[11]}${index}
    \   ${status}           get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[12]}${index}
    \   ${active}           get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[13]}${index}
    \   ${sapcode}          get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[14]}${index}
    \   ${alt_sapcode}      get_value_excel_by_column    ${path_folder}    ${sheet_name}    ${col_name[15]}${index}
    \   ${supplier_id}      Insert Supplier Data Into Supplier Table   ${country_id}     ${supplier_type}   ${name}     ${address}    ${tax_id}   ${tax_name}   ${tax_address}   ${followup_tax}  ${email}     ${markup}     ${incl_vat}    ${vat}    ${status}   ${active}
    \   Insert Supplier MultiCurrency Into SupplierMultiCurrency Table      ${supplier_id}      ${sapcode}
    \   Run Keyword If  '${alt_sapcode}' != 'None'  Insert Supplier MultiCurrency Into SupplierMultiCurrency Table    ${supplier_id}    ${alt_sapcode}  ${alt_currency_id}   ${main_sap_code_flag}
    
    # Get total number of active suppliers from DB
    ${total_supplier}=  Count Total Supplier
    ${total_supplier}   Set Variable    ${total_supplier[0][0]}
    Set Suite Variable    ${total_supplier}
    Log To Console  TOTAL SUPPLIER = ${total_supplier}
    Disconnect From Database

Clear Test Data  
    Log To Console    ...
    Log To Console    Clearing Suppliers Test Data
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier Data From DB   ${sup_name_1}
    Delete Supplier Data From DB   ${sup_name_2}
    Delete Supplier Data From DB   ${sup_name_3}
    Delete Supplier Data From DB   ${sup_name_4}
    Delete Supplier Data From DB   ${sup_name_5}
    Disconnect From Database

*** Test Cases ***
Desktop - To Verify That Supplier List Page Displays Element Correctly
    [Tags]      TC_ACT_06638    supplierList
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    Verify Supplier List Page Display Static Elements Correctly
    Verify Supplier Main Sap Code Column Display Sort Icon Correctly    ${sort_asc}     ${inactive_status}
    Verify Supplier Main Sap Code Column Display Sort Icon Correctly    ${sort_desc}    ${inactive_status}
    Verify Supplier Name Column Display Sort Icon Correctly    ${sort_asc}    ${active_status}
    Verify Supplier Name Column Display Sort Icon Correctly    ${sort_desc}   ${inactive_status}
    Verify That Supplier List Page Display Selected Page Size Correctly    ${page_size_default} 
    Verify Supplier List Display Items Per Page As Expected     ${page_size_default}
    Run Keyword If   ${total_supplier} == 0     Verify No Search Result Found 
    ...   ELSE       Verify Supplier Search Result Row Display Correctly By Main Sap Code   ${main_sap_code_4}    ${sup_name_4}   ${active_status}
    Run Keyword If   ${total_supplier} >= ${page_size_default}      Verify Pagination Details     1   ${page_size_default}    ${total_supplier}
    ...   ELSE IF    ${total_supplier} == 0     Verify Pagination Details Should Not Display
    ...   ELSE       Verify Pagination Details     1   ${total_supplier}    ${total_supplier} 
    Verify Previous Button Should Not Display
    ${total_page_no}=   Get Total Page Number   ${total_supplier}   ${page_size_default}
    Run Keyword If    ${total_page_no} > 1      Verify Next Button Should Display
    ...   ELSE IF     ${total_page_no} <= 1     Verify Page Number Button Should Not Display

Desktop - To Verify That Supplier List Page Displays Number of Rows Per Page Correctly
    [Tags]      TC_ACT_06639    supplierList
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    # Default 50 rows per page
    Verify That Supplier List Page Display Selected Page Size Correctly    ${page_size_default} 
    Verify Supplier List Display Items Per Page As Expected     ${page_size_default}
    Run Keyword If   ${total_supplier} >= ${page_size_default}      Verify Pagination Details     1   ${page_size_default}    ${total_supplier}
    ...     ELSE    Verify Pagination Details     1   ${total_supplier}    ${total_supplier}
    # 100 rows per page
    Select Supplier List Page Size From Drop Dowm List    ${page_size_s}
    Verify That Supplier List Page Display Selected Page Size Correctly     ${page_size_s}
    Verify Supplier List Display Items Per Page As Expected     ${page_size_s}
    Run Keyword If   ${total_supplier} >= ${page_size_s}      Verify Pagination Details     1   ${page_size_s}    ${total_supplier}
    ...     ELSE    Verify Pagination Details     1   ${total_supplier}    ${total_supplier}
    # 150 rows per page
    Select Supplier List Page Size From Drop Dowm List    ${page_size_m}
    Verify That Supplier List Page Display Selected Page Size Correctly     ${page_size_m}
    Verify Supplier List Display Items Per Page As Expected     ${page_size_m}
    Run Keyword If   ${total_supplier} >= ${page_size_m}      Verify Pagination Details     1   ${page_size_m}    ${total_supplier}
    ...     ELSE    Verify Pagination Details     1   ${total_supplier}    ${total_supplier}
    # 200 rows per page
    Select Supplier List Page Size From Drop Dowm List    ${page_size_l}
    Verify That Supplier List Page Display Selected Page Size Correctly     ${page_size_l}
    Verify Supplier List Display Items Per Page As Expected     ${page_size_l}
    Run Keyword If   ${total_supplier} >= ${page_size_l}      Verify Pagination Details     1   ${page_size_l}    ${total_supplier}
    ...     ELSE    Verify Pagination Details     1   ${total_supplier}    ${total_supplier}
    # 50 rows per page
    Select Supplier List Page Size From Drop Dowm List    ${page_size_default}
    Verify That Supplier List Page Display Selected Page Size Correctly     ${page_size_default}
    Verify Supplier List Display Items Per Page As Expected     ${page_size_default}
    Run Keyword If   ${total_supplier} >= ${page_size_default}      Verify Pagination Details     1   ${page_size_default}    ${total_supplier}
    ...     ELSE    Verify Pagination Details     1   ${total_supplier}    ${total_supplier}

Desktop - To Verify That Can Go To Create Supplier Page From Supplier List Page Successfully
    [Tags]      TC_ACT_06640        CreateSupplier
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    Click Create Supplier Button

Desktop - To Verify That Can Go To Supplier Details Page Successfully
    [Tags]      TC_ACT_06641    TC_ACT_06643    supplierList
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    Click Supplier Main Sap Code Hyperlink   ${main_sap_code_1}
    Go To Supplier List
    Click View Supplier Button By Main Sap Code    ${main_sap_code_1}

Desktop - To Verify That Can Update Supplier Status Correctly
    [Tags]      TC_ACT_06642    supplierList
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    Search Supplier     ${main_sap_code_5}
    Change Supplier Status By Main Sap Code   ${main_sap_code_5}  
    Verify Supplier Status Display Correctly By Main Sap Code   ${main_sap_code_5}   ${inactive_status}
    Sleep   2s
    Change Supplier Status By Main Sap Code   ${main_sap_code_5}
    Verify Supplier Status Display Correctly By Main Sap Code   ${main_sap_code_5}   ${active_status}

Desktop - To Verify That Can Go To Edit Supplier Page Successfully
    [Tags]      TC_ACT_06644    supplierList
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    Click Edit Supplier Button By Main Sap Code     ${main_sap_code_1}

Desktop - To Verify That Can Sort Supplier List Correctly
    [Tags]      TC_ACT_06645    supplierList
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    # Sort Supplier Name Column
    Connect ACT Database    ${DB_NAME_V2}
    ${exp_name_sorted_asc}    Select Supplier Name Order By Name   ${sort_asc}
    ${exp_name_sorted_desc}   Select Supplier Name Order By Name   ${sort_desc}
    Verify Supplier Name Column Display Sort Icon Correctly    ${sort_asc}    ${active_status}
    Verify Supplier Name Column Display Sort Icon Correctly    ${sort_desc}   ${inactive_status}
    Verify Supplier Name Sorting Correctly   ${exp_name_sorted_asc}
    Click Sort Supplier Name List Button
    Verify Supplier Name Column Display Sort Icon Correctly    ${sort_asc}    ${inactive_status}
    Verify Supplier Name Column Display Sort Icon Correctly    ${sort_desc}   ${active_status}
    Verify Supplier Name Sorting Correctly   ${exp_name_sorted_desc}
    
    # Sort Main Sap Code Column
    ${exp_sap_code_sorted_asc}    Select Supplier Main Sap Code Order By Sap Code    ${sort_asc}
    ${exp_sap_code_sorted_desc}   Select Supplier Main Sap Code Order By Sap Code    ${sort_desc}
    Click Sort Supplier Main Sap Code List Button 
    Verify Supplier Main Sap Code Column Display Sort Icon Correctly    ${sort_asc}     ${active_status}
    Verify Supplier Main Sap Code Sorting Correctly   ${exp_sap_code_sorted_asc}
    Click Sort Supplier Main Sap Code List Button
    Verify Supplier Main Sap Code Column Display Sort Icon Correctly    ${sort_asc}     ${inactive_status}
    Verify Supplier Main Sap Code Column Display Sort Icon Correctly    ${sort_desc}    ${active_status}
    Verify Supplier Main Sap Code Sorting Correctly   ${exp_sap_code_sorted_desc}
    Disconnect From Database

Desktop - To Verify That Search Result Display Correctly
    [Tags]      TC_ACT_06646    supplierList
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    # Search with full keywords matched main sap code
    Search Supplier     ${main_sap_code_1}
    Verify Search Result in Main Sap Code Column Matched with Search Keyword  ${main_sap_code_1}
    # Search with full keywords matched supplier name
    Clear Search Supplier Keywords  
    Search Supplier     ${sup_name_1}
    Verify Search Result in Supplier Name Column Matched with Search Keyword    ${sup_name_1}
    # Search wirh full keywords matched other sap code
    Clear Search Supplier Keywords
    Search Supplier     ${alt_sap_code_2}
    Verify Supplier Search Result Row Display Correctly By Main Sap Code   ${main_sap_code_4}    ${sup_name_4}   ${active_status}
    # Search with partial keywords matched main sap code, other sap code and supplier name
    Clear Search Supplier Keywords
    Search Supplier     ${keyword_1}
    Verify Supplier Search Result Row Display Correctly By Main Sap Code   ${main_sap_code_4}    ${sup_name_4}   ${active_status}
    Verify Supplier Search Result Row Display Correctly By Main Sap Code   ${main_sap_code_1}    ${sup_name_1}   ${active_status}
    # Search with partial keywords matched supplier name
    Clear Search Supplier Keywords
    Search Supplier     ${keyword_2}
    Verify Search Result in Supplier Name Column Matched with Search Keyword    ${keyword_2}
    # Search with special characters keywords
    Clear Search Supplier Keywords
    Search Supplier     ${keyword_3}
    Verify Search Result in Supplier Name Column Matched with Search Keyword     ${keyword_3}
    # Search with keywords that not matched any results
    Clear Search Supplier Keywords
    Search Supplier     ${keyword_4}
    Verify No Search Result Found
    Verify Pagination Details Should Not Display
    Verify Page Number Button Should Not Display
    # Search with long keywords (255) that not matched any results
    Clear Search Supplier Keywords
    Search Supplier     ${keyword_5}
    Verify No Search Result Found
    Verify Pagination Details Should Not Display
    Verify Page Number Button Should Not Display  

Desktop - To Verify That Can Click Delete Supplier Successfully
    [Tags]      TC_ACT_06648    supplierList
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Supplier List
    Search Supplier     ${main_sap_code_5}
    Click Delete Supplier Button By Main Sap Code    ${main_sap_code_5}
    Verify Confirm To Delete Popup Display
    Click Cancel to Delete Supplier Button
    Verify Supplier Search Result Row Display Correctly By Main Sap Code   ${main_sap_code_5}    ${sup_name_5}   ${active_status}
    # Click Delete Supplier Button By Main Sap Code    ${main_sap_code_5}
    # Verify Confirm To Delete Popup Display
    # Click Confirm To Delete Supplier Button
    # Verify No Search Result Found

Desktop - To Verify That Can Go To Create Supplier From Supplier Menu Successfully
    [Tags]      CreateSupplier      T1
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Create Supplier

Desktop - To Verify That Create Supplier Page Displays Element Correctly
    [Tags]      CreateSupplier      T2
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Go To Create Supplier
    Verify Create Supplier Page Display Static Elements Correctly
    # Verify Supplier Main Sap Code Column Display Sort Icon Correctly    ${sort_asc}     ${inactive_status}
    # Verify Supplier Main Sap Code Column Display Sort Icon Correctly    ${sort_desc}    ${inactive_status}
    # Verify Supplier Name Column Display Sort Icon Correctly    ${sort_asc}    ${active_status}
    # Verify Supplier Name Column Display Sort Icon Correctly    ${sort_desc}   ${inactive_status}
    # Verify That Supplier List Page Display Selected Page Size Correctly    ${page_size_default} 
    # Verify Supplier List Display Items Per Page As Expected     ${page_size_default}
    # Run Keyword If   ${total_supplier} == 0     Verify No Search Result Found 
    # ...   ELSE       Verify Supplier Search Result Row Display Correctly By Main Sap Code   ${main_sap_code_4}    ${sup_name_4}   ${active_status}
    # Run Keyword If   ${total_supplier} >= ${page_size_default}      Verify Pagination Details     1   ${page_size_default}    ${total_supplier}
    # ...   ELSE IF    ${total_supplier} == 0     Verify Pagination Details Should Not Display
    # ...   ELSE       Verify Pagination Details     1   ${total_supplier}    ${total_supplier} 
    # Verify Previous Button Should Not Display
    # ${total_page_no}=   Get Total Page Number   ${total_supplier}   ${page_size_default}
    # Run Keyword If    ${total_page_no} > 1      Verify Next Button Should Display
    # ...   ELSE IF     ${total_page_no} <= 1     Verify Page Number Button Should Not Display