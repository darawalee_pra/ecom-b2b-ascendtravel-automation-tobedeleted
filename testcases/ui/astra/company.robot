*** Settings ***
#Suite Setup         Prepare Test Data
Test Teardown       Close All Browsers      
Library             Selenium2Library
Library             DatabaseLibrary
Library             String
Library             ${CURDIR}/../../../library/excel.py
Resource            ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource            ${CURDIR}/../../../resources/keywords/ui/astra/company.robot
Resource            ${CURDIR}/../../../resources/keywords/ui/astra/login.robot
Resource            ${CURDIR}/../../../resources/keywords/ui/astra/pagination.robot
# Resource            ${CURDIR}/../../../resources/keywords/db/company.robot
Resource            ${CURDIR}/../../../resources/keywords/common/common.robot

*** Variables ***
${page_size_default}        50
${page_size_s}              100
${page_size_m}              150
${page_size_l}              200
${sort_asc}                 ASC
${sort_desc}                DESC
${active_status}            active
${inactive_status}          inactive

*** Keywords ***
Prepare Test Data
    Log To Console    ...
    Log To Console    Preparing Company Test Data
    
    # Get total number of active suppliers from DB
    ${total_company}=  Count Total Supplier
    ${total_company}   Set Variable    ${total_company[0][0]}
    Set Suite Variable    ${total_company}
    Log To Console  TOTAL COMPANY = ${total_company}
    Disconnect From Database

Clear Test Data  
    Log To Console    ...
    Log To Console    Clearing Company Test Data
    Connect ACT Database    ${DB_NAME_COMPANY}
    # Delete Supplier Data From DB   ${sup_name_1}
    Disconnect From Database

*** Test Cases ***
Desktop - To Verify That Can Go To Create Company Page From Create Comapny Side Bar Menu Successfully
    [Tags]        CreateCompany
    Login To ASTRA      ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Click Create Supplier Button