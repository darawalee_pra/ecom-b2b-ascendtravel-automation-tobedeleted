*** Settings ***
#Test Teardown       Close All Browsers   
Library             Selenium2Library
Resource            ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource            ${CURDIR}/../../../resources/keywords/common/common.robot
Resource            ${CURDIR}/../../../resources/keywords/ui/astra/product.robot
Resource            ${CURDIR}/../../../resources/keywords/ui/astra/login.robot

#Note: list of days
# everyday, monday, tuesday, wednesday, thursday, friday, saturday, sunday

#Note: list of hotel facilities
# 100% Non-Smoking, 24-hour check-in, 24-hour fitness center, 24-hour front desk, 24-hour room service, 24-hour security, Additional fee applied for pets, Airport transfer,		
# ATM/cash machine on site, Babysitting, Bar, BBQ facilities, Beach, Bicycle rental, Breakfast buffet, Buzzer/wireless intercom, Car hire, Car park, Car park charges applied,	
# Car park for truck, Car park for van, Car park free of charge, Car park nearby, Car park onsite, Car power charging station, Casino, Cats allowed, Chapel, Coffee shop, Concierge,		
# Continental breakfast, Convenience store, Currency exchange, Daily housekeeping, Designated smoking area, Dogs allowed, Doorman, Dry cleaning, Elevator, Express check-in/check-out,	
# Facilities for disabled guests, Family room, Fax or photo copying in business center, Fireplace, Fitness center with additional charge, Free breakfast, Free fitness center, Free Wi-Fi in all rooms,	
# Gift/souvenir shop, Grocery deliveries, Halal restaurant, Infirmary, Kitchen, Kosher restaurant, Laundromat, Laundry service, Library, Lockers, Luggage storage, Newspapers, Nightclub,		
# Pets allowed, Poolside bar, Portable wi-fi rental, Postal service, Private check in/check out, Restaurant, Room service, Safety deposit boxes, Salon, Self-parking, Shared kitchen,
# Shared lounge/TV area, Shops, Shrine, Shuttle service, Taxi service, Ticket service, Tours, Valet parking, Vending machine, Wheelchair accessible, Wi-Fi in public areas, Wi-Fi with additional charge, Wired internet

#Note: list of staff languages

#Note: list of sport and recreation (hobby)
# Badminton court, Billiards, Boats, Bowling alley, Canoe, Children's playground, Dart board, Diving, Fishing, Fitness center, Games room, Garden, Golf course (on site), Golf course (within 3 km), 	
# Hiking trails, Horse riding, Hot spring bath, Hot tub, Indoor pool, Karaoke, Kids club, Massage, Mini golf course, Outdoor pool, Pool (kids), Private beach, Sauna, Ski equipment rentals, Ski lessons,
# Skiing, Snorkeling, Solarium, Spa, Squash courts, Steamroom, Surfing lessons, Table tennis, Tennis courts, Theme park, Water park, Water sports (motorized), Water sports (non-motorized), 	
# Watersports equipment rentals, Wind surfing, Yoga room

#Note: list of position
#Contracting, General Manager, Sale, Other


*** Test Cases ***
To Verify That Can Create Product With Non-Refundable Successfully
    [Tags]    T1

    ${catagory}                              Set Variable       Hotel
    ${search_destination_name}               Set Variable       Produ
    ${value_destination}                     Set Variable       0
    ${value_hotel_star}                      Set Variable       3.5
    ${checkin_time}                          Set Variable       1200
    ${checkout_time}                         Set Variable       1400
    ${search_supplier_name}                  Set Variable       Produ
    ${value_supplier}                        Set Variable       1
    ${cancel_before_time}                    Set Variable       0930
    ${hotel_content_th}                      Set Variable       โรบอทโปรดักซ์คอนเท็น ภาษาไทย 1234567890 ^w^/
    ${hotel_content_en}                      Set Variable       Robot Product Content Eng 1234567890 :):P<3<3
    ${photo_base_path}                       Set Variable       ${CURDIR}/../../../resources/testdata/ui/${ENV}/astra/product/image
    ${keyword_list}                          Create List        test  testKa
    ${list_day}=    Create List   everyday  monday  sunday
    ${list_facility}=    Create List
    ${list_hobby}=    Create List
    ${contact_position}                      Set Variable       General Manager
    
    Login To ASTRA               ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Click Product Menu
    Go To Create Product
    Verify Product Catagory Is Hotel    ${catagory}
    
    Input Product Information    testProductTH    ${EMPTY}    123 TH        123 EN
    ...                          0.00             0.00        testSearch    02-12345678
    ...                          3.5              2           1200          0000
    ...                          Allotment Set    ${keyword_list}
    
    # Select Supplier Markup     ${search_supplier_name}    ${value_supplier}
    # Default Cancellation - Non-Refundable
    # Add Specific Cancellation Policy
    # Set Specific Cancellation Policy Period From    0
    # Set Specific Cancellation Policy Period To      60
    # Select Days To Specific     ${list_day}
    # Set Specific Days Except On By Period From    25
    # Set Specific Days Except On By Period To      30
    # Set Specific Days Except On By Date           2
    # Specific Cancellation - Non-Refundable
    # Click Add To Save Specific Cancellation
    # Click To Close Specific Details
    # Product Content            ${hotel_content_th}    ${hotel_content_en}
    # ${photo_base_path}  get_canonical_path    ${photo_base_path}
    # Browse Hotel Photo  ${photo_base_path}

    ${contact_locator_list}=    Create List    ${txt_contact_name}   ${txt_contact_email}    ${txt_contact_phone}
    ${contact_name_list}=     Create List    Name1    Name2    Name3
    ${contact_email_list}=    Create List    Email1    Email2    Email3
    ${contact_phone_list}=    Create List    Phone1    Phone2    Phone3
    # ${product_contact_value_list}=    Create List    Name    Email@mail.com    088-888-8888

    Contracting Manager Owner    ${contact_locator_list}    ${contact_name_list}    ${contact_email_list}    ${contact_phone_list}    ${contact_position}

To Verify That Can Create Product With Cancel Before Checkin Date - Charge
    [Tags]    T2
    
    ${catagory}                                Set Variable       Hotel
    ${search_destination_name}                 Set Variable       Produ
    ${value_destination}                       Set Variable       0
    ${value_hotel_star}                        Set Variable       3.5
    ${checkin_time}                            Set Variable       1200
    ${checkout_time}                           Set Variable       1400
    ${search_supplier_name}                    Set Variable       Produ
    ${value_supplier}                          Set Variable       1
    ${cancel_before_time}                      Set Variable       0930
    
    #change login
    Login To ASTRA               ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Click Product Menu
    Go To Create Product
    Verify Product Catagory Is Hotel    ${catagory}
    Input Product Information
    Select Destination            ${search_destination_name}    ${value_destination}
    Select Hotel Star             ${value_hotel_star}
    Input Priority
    Input Checkin Time After      ${checkin_time}
    Input Checkout Time Before    ${checkout_time}
    Select Allotment Type Free Sell
    Input Search Keyword
    Select Product Status
    Select Supplier Markup     ${search_supplier_name}    ${value_supplier}        
    Default Cancellation - Before Checkin Date - Charge

To Verify That Can Create Product With Cancel Before Checkin Date - Free Of Charge
    [Tags]    T3
    
    ${catagory}                                Set Variable       Hotel
    ${search_destination_name}                 Set Variable       Produ
    ${value_destination}                       Set Variable       0
    ${value_hotel_star}                        Set Variable       3.5
    ${checkin_time}                            Set Variable       1200
    ${checkout_time}                           Set Variable       1400
    ${search_supplier_name}                    Set Variable       Produ
    ${value_supplier}                          Set Variable       1
    ${cancel_before_time}                      Set Variable       0930

    Login To ASTRA               ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Click Product Menu
    Go To Create Product
    Verify Product Catagory Is Hotel    ${catagory}
    Input Product Information
    Select Destination            ${search_destination_name}    ${value_destination}
    Select Hotel Star             ${value_hotel_star}
    Input Priority
    Input Checkin Time After      ${checkin_time}
    Input Checkout Time Before    ${checkout_time}
    Select Allotment Type Allotment Set
    Input Search Keyword
    Select Product Status
    Select Supplier Markup     ${search_supplier_name}    ${value_supplier}     
    Default Cancellation - Before Checkin Date - Is Free Of Charge

To Verify That Can Create Product With Cancel On Checkin Date - Charge
    [Tags]    T4
    
    ${catagory}                                Set Variable       Hotel
    ${search_destination_name}                 Set Variable       Produ
    ${value_destination}                       Set Variable       0
    ${value_hotel_star}                        Set Variable       3.5
    ${checkin_time}                            Set Variable       1200
    ${checkout_time}                           Set Variable       1400
    ${search_supplier_name}                    Set Variable       Produ
    ${value_supplier}                          Set Variable       1
    ${cancel_before_time}                      Set Variable       0930

    Login To ASTRA               ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Click Product Menu
    Go To Create Product
    Verify Product Catagory Is Hotel    ${catagory}
    Input Product Information
    Select Destination            ${search_destination_name}    ${value_destination}
    Select Hotel Star             ${value_hotel_star}
    Input Priority
    Input Checkin Time After      ${checkin_time}
    Input Checkout Time Before    ${checkout_time}
    Select Allotment Type Free Sell
    Input Search Keyword
    Select Product Status
    Select Supplier Markup     ${search_supplier_name}    ${value_supplier}     
    Default Cancellation - On Checkin Date - Charge    ${cancel_before_time}

To Verify That Can Create Product With Cancel On Checkin Date - Free Of Charge
    [Tags]    T5

    ${catagory}                                Set Variable       Hotel
    ${search_destination_name}                 Set Variable       Produ
    ${value_destination}                       Set Variable       0
    ${value_hotel_star}                        Set Variable       3.5
    ${checkin_time}                            Set Variable       1200
    ${checkout_time}                           Set Variable       1400
    ${search_supplier_name}                    Set Variable       Produ
    ${value_supplier}                          Set Variable       1
    ${cancel_before_time}                      Set Variable       0930

    Login To ASTRA               ${USERNAME_ASTRA}    ${PASSWORD_ASTRA}   ${ASTRA_BASE_URL_V2}
    Click Product Menu
    Go To Create Product
    Verify Product Catagory Is Hotel    ${catagory}
    Input Product Information
    Select Destination            ${search_destination_name}    ${value_destination}
    Select Hotel Star             ${value_hotel_star}
    Input Priority
    Input Checkin Time After      ${checkin_time}
    Input Checkout Time Before    ${checkout_time}
    Select Allotment Type Allotment Set
    Input Search Keyword
    Select Product Status
    Select Supplier Markup     ${search_supplier_name}    ${value_supplier}     
    Default Cancellation - On Checkin Date - Is Free Of Charge      ${cancel_before_time}



