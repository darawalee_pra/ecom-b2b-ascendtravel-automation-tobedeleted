*** Settings ***
#Suite Setup         Prepare Test Data
# Suite Teardown      Clear Test Data
Test Setup          Test Setup
Library             Selenium2Library
Library             DatabaseLibrary
Library             String
Library             OperatingSystem
Library             JSONLibrary
Library             ${CURDIR}/../../../library/excel.py
Library             ${CURDIR}/../../../library/common.py
Library             ${CURDIR}/../../../library/requestLibrary.py
Resource            ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/company.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/login.robot
Resource            ${CURDIR}/../../../resources/keywords/db/company.robot
Resource            ${CURDIR}/../../../resources/keywords/common/common.robot
Resource            ${CURDIR}/../../../resources/testdata/api/${ENV}/astra/company/company.robot
Resource            ${CURDIR}/../../../resources/keywords/api/apicommon.robot

*** Variables ***
${path_folder}              ${CURDIR}/../../../resources/testdata/api/${ENV}/astra/company
${page_size_default}        50
${sort_asc}                 ASC
${sort_desc}                DESC

*** Keywords ***
Test Setup
    ${access_token}  ${refresh_token}=     POST Astra Login API
    Set Suite Variable  ${access_token}
    Set Suite Variable  ${refresh_token}

Prepare Data
    Log To Console    ...
    Log To Console    Preparing Suppliers Test Data
    Connect ACT Database    ${DB_NAME_COMPANY}
    ${total_company}=  Count Total Company
    ${total_company}   Set Variable    ${total_company[0][0]}
    Set Suite Variable    ${total_company}
    Log To Console  TOTAL COMPANY = ${total_company}
    Disconnect From Database

*** Test Cases ***
To Verify Able To Get Company List By Default Parameter Successfully
    [Tags]    company    api_get_company_list
    Prepare Data
    ${total_page_no}   Get Total Page Number   ${total_company}   ${page_size_default}

    ${resp}    GET Company List API   ${access_token}     ${refresh_token}
    Response Status Should Be 200 OK     ${resp}

    Run Keyword If   ${total_company} >= ${page_size_default}  Verify Get Company List API Response Number of Items As Expected   ${resp}     ${page_size_default}
    ...    ELSE IF   ${page_size_default} > ${total_company}   Verify Get Company List API Response Number of Items As Expected   ${resp}     ${total_company}
    Verify Get Company List Return Item From Value As Expected      ${resp}     1
    Run Keyword If   ${total_company} >= ${page_size_default}  Verify Get Company List Return Item To Value As Expected  ${resp}   ${page_size_default}
    ...    ELSE IF   ${page_size_default} > ${total_company}   Verify Get Company List Return Item To Value As Expected  ${resp}   ${total_company}
    Verify Get Company List Return Total Item Value As Expected     ${resp}     ${total_company}
    Verify Get Company List Return Total Page Value As Expected     ${resp}     ${total_page_no}

To Verify That Able To Get Company List By Sorting Company Name Correctly
    [Tags]      company   api_get_company_list
    Connect ACT Database    ${DB_NAME_COMPANY}
    ${exp_name_sorted_asc}    Select Company Name Order By Name   ${sort_asc}
    ${exp_name_sorted_desc}   Select Company Name Order By Name   ${sort_desc}
    Disconnect From Database
    ## Sort expected val to be ASCII Sort Order
    ${exp_name_sorted_asc}    common.convert_array_to_list  ${exp_name_sorted_asc}
    ${exp_name_sorted_asc}    common.sorting_ascii_asc   ${exp_name_sorted_asc}
    ${exp_name_sorted_desc}   common.convert_array_to_list  ${exp_name_sorted_desc}
    ${exp_name_sorted_desc}   common.sorting_ascii_desc   ${exp_name_sorted_desc}

    ${resp}=    GET Company List API   ${access_token}     ${refresh_token}    ${EMPTY}    1   ${page_size_default}    company_name   ${sort_asc}
    Verify Get Company List API Sorting Result By Comapny Name As Expected    ${resp}    ${exp_name_sorted_asc}

    ${resp}=    GET Company List API   ${access_token}     ${refresh_token}    ${EMPTY}    1   ${page_size_default}    company_name   ${sort_desc}
    Verify Get Company List API Sorting Result By Comapny Name As Expected    ${resp}    ${exp_name_sorted_desc}

To Verify That Can Create Company With No Document Successfully
    [Tags]    company    api_create_company
    [Documentation]  ParentCompany  B2B    SystemIntegration    noDocument      singleCompanyContact   singleAccountManagerOwner 

    ## Prepare Test Data ##
    ${companyName}      Generate Random Text
    ${json}             Load JSON From File    ${path_folder}/${create_company_json}
    ${reqs_obj}         Update Value To Json   ${json}  $.generalInformation.nameTh   ${companyName}
    ${reqs_obj}         Update Value To Json   ${json}  $.generalInformation.nameEn   ${companyName}
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${companyName}.json   ${new_obj}   UTF-8

    ## Test Service ##
    ${resp}          POST Create Company API    ${access_token}  ${refresh_token}   ${path_folder}/${companyName}.json
    Response Status Should Be 200 OK     ${resp}
    ${company_id}   Get Value From Json     ${resp.json()}  $.data.companyId

    ## Clear Test Data ##
    # ${supplier_payment_id}  Get Value From Json     ${resp.json()}  $..data.supplierPaymentMethod[0].supplierPaymentId
    # DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    # Connect ACT Database    ${DB_NAME_SUPPLIER}
    # Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    # Delete Supplier Bank By Supplier Payment Id    ${supplier_payment_id[0]}
    # Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    # Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    # Delete Supplier By Supplier Id  ${supplier_id[0]}
    # Disconnect From Database
    Remove File     ${path_folder}/${companyName}.json