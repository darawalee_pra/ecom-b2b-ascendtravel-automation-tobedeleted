*** Settings ***
#Suite Setup         Prepare Test Data
#Suite Teardown      Clear Test Data    
Library             Selenium2Library
Library             DatabaseLibrary
Library             String
Library             OperatingSystem

Library             ${CURDIR}/../../../Library/convertExcelToJson.py
Resource            ${CURDIR}/../../../Resource/Config/${ENV}/config.robot
Resource            ${CURDIR}/../../../Resource/Keywords/API/Astra/product.robot
Resource            ${CURDIR}/../../../Resource/Keywords/DB/supplier.robot
Resource            ${CURDIR}/../../../Resource/Keywords/Common/common.robot
Resource            ${CURDIR}/../../../Resource/TestData/${ENV}/Common/common.robot

*** Variables ***
${path_folder}                  ${CURDIR}/../../../Resource/TestData/${ENV}/Astra/Product
${product_data_file}            create_product.xlsx
${product_url}                  /hotel/api/v1/hotel


*** Test Cases ***
To Verify That System Can Create New Product Successfully
    [Tags]  test

    ${access_token}  ${refresh_token}=     POST Astra Login API
    ${headers}=      Create Dictionary    Content-Type=application/json   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}

    ${workbook}=     open_excel           ${path_folder}/${product_data_file}
    ${sheet}=        get_sheet            ${workbook}  Product Information
    ${length}=       get_excel_max_row    ${sheet}

    :FOR  ${index}  IN RANGE  1  ${length}
    \  ${flag}=          get_value_by_key               ${sheet}          ${index}    A    D
    \  Continue For Loop If    '${flag}' != 'T'
    \  ${data_list}=     Get JSON Create Product        ${workbook}       ${index}   
    \  Log to console    ${data_list}
    \  ${resp}           POST Request  astra_session    ${product_url}    headers=${headers}  data=${data_list}
    \  Should Be Equal As Strings  ${resp.status_code}  200


    

    
   