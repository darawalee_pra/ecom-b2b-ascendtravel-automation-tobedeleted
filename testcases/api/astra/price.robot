*** Settings ***
#Suite Setup         Prepare Test Data
#Suite Teardown      Clear Test Data
Test Setup          Test Setup
Library             Selenium2Library
Library             DatabaseLibrary
Library             String
Library             OperatingSystem
Library             ${CURDIR}/../../../library/common.py
Library             ${CURDIR}/../../../library/requestLibrary.py
Resource            ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/price.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/login.robot
Resource            ${CURDIR}/../../../resources/keywords/common/common.robot
Resource            ${CURDIR}/../../../resources/keywords/api/apicommon.robot

*** Variables ***
${path_folder}              ${CURDIR}/../../../resources/testdata/api/${ENV}/astra/price/
${room_type_id}             20
${room_condition_id}        1
${default_percent_margin}   999.99
${currency_code}            THB

*** Keywords ***
Test Setup
    ${access_token}     ${refresh_token}=     POST Astra Login API
    Set Suite Variable  ${access_token}
    Set Suite Variable  ${refresh_token}
    ${date} =	Get Current Date    result_format=%Y-%m-%d
    Set Suite Variable  ${date}

*** Test Cases ***
Prepare Data
    ## Create Hotel ##
    #TO DO - Create Hotel (Product) not ready to use

    ## Create Supplier ##
    #TO DO - To be added after able to create room type and room condition

    ## Create Room Type ##
    #TO DO - Create room type not ready to use

    ## Create Room Condition ##
    #TO DO - Create room condition not ready to use

To Verify That API Can Set Allotment For Selected Single Day Properly
    [Tags]      price   setAllotment   getAllotmentAndRate 
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##

    ## Test Service ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{allotment_dict}   Create Dictionary   from=${date}    to=${date}   allotment=1    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${date}    ${date}    ${EMPTY}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${date}     ${allotment_dict.allotment}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Set Allotment For Selected Period Properly
    [Tags]      price   setAllotment   getAllotmentAndRate      
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##
    ${from} =   Add Time To Date	${date}     1 days      result_format=%Y-%m-%d
    ${between} =     Add Time To Date	    ${from}     1 days      result_format=%Y-%m-%d
    ${to} =     Add Time To Date	${from}     2 days      result_format=%Y-%m-%d

    ## Test Service ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{allotment_dict}   Create Dictionary   from=${from}    to=${to}   allotment=2    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${from}    ${to}    ${EMPTY}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${from}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${between}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${to}     ${allotment_dict.allotment}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Set Allotment For Selected Period With Specific Days Properly
    [Tags]      price   setAllotment   getAllotmentAndRate
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##
    # Set period = 5 days. Day1 = ${from}. Day5 = ${to}
    ${from} =   Add Time To Date	${date}     4 days      result_format=%Y-%m-%d
    ${day2} =   Add Time To Date	${from}     1 days      result_format=%Y-%m-%d
    ${day3} =   Add Time To Date	${from}     2 days      result_format=%Y-%m-%d
    ${day4} =   Add Time To Date	${from}     3 days      result_format=%Y-%m-%d
    ${to} =     Add Time To Date	${from}     4 days      result_format=%Y-%m-%d
    ${period_day_start}=    Convert Date    ${from}    result_format=%A
    ${period_day_start}=    Convert To Lowercase    ${period_day_start}
    ${period_day_end}=    Convert Date    ${to}    result_format=%A
    ${period_day_end}=    Convert To Lowercase    ${period_day_end}

    ## Test Service ##
    @{except_by_date_list}      Create List   ${day4}
    ${except_by_period_item}    Create Dictionary   from=${day2}    to=${day3}
    @{except_by_period_list}    Create List   ${except_by_period_item}         
    @{specific_day_list}        Create List   ${period_day_start}   ${period_day_end}  
    &{allotment_dict}   Create Dictionary   from=${from}    to=${to}   allotment=3    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}
    
    ## Verify Allotment result ##
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${from}    ${to}    ${EMPTY}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${from}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${to}       ${allotment_dict.allotment}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Update Existing Allotment For Selected Single Day Properly
    [Tags]      price   setAllotment   getAllotmentAndRate 
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##

    ## Test Service ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{allotment_dict}   Create Dictionary   from=${date}    to=${date}   allotment=1    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}

    ## Edit Allotment ##
    &{allotment_dict}   Create Dictionary   from=${date}    to=${date}   allotment=2    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${date}    ${date}    ${EMPTY}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${date}     ${allotment_dict.allotment}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Update Existing Allotment For Selected Period Properly
    [Tags]      price   setAllotment   getAllotmentAndRate      
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##
    ${from} =   Add Time To Date	${date}     1 days      result_format=%Y-%m-%d
    ${between} =     Add Time To Date	    ${from}     1 days      result_format=%Y-%m-%d
    ${to} =     Add Time To Date	${from}     2 days      result_format=%Y-%m-%d

    ## Set Allotment ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{allotment_dict}   Create Dictionary   from=${from}    to=${to}   allotment=2    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}

    ## Edit Allotment ##
    &{allotment_dict}   Create Dictionary   from=${from}    to=${to}   allotment=4    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${from}    ${to}    ${EMPTY}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${from}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${between}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${to}     ${allotment_dict.allotment}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Update Existing Allotment For Selected Period With Specific Days Properly
    [Tags]      price   setAllotment   getAllotmentAndRate
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##
    # Set period = 5 days. Day1 = ${from}. Day5 = ${to}
    ${from} =   Add Time To Date	${date}     4 days      result_format=%Y-%m-%d
    ${day2} =   Add Time To Date	${from}     1 days      result_format=%Y-%m-%d
    ${day3} =   Add Time To Date	${from}     2 days      result_format=%Y-%m-%d
    ${day4} =   Add Time To Date	${from}     3 days      result_format=%Y-%m-%d
    ${to} =     Add Time To Date	${from}     4 days      result_format=%Y-%m-%d
    ${period_day_start}=    Convert Date    ${from}    result_format=%A
    ${period_day_start}=    Convert To Lowercase    ${period_day_start}
    ${period_day_end}=    Convert Date    ${to}    result_format=%A
    ${period_day_end}=    Convert To Lowercase    ${period_day_end}

    ## Test Service ##
    @{except_by_date_list}      Create List   ${day4}
    ${except_by_period_item}    Create Dictionary   from=${day2}    to=${day3}
    @{except_by_period_list}    Create List   ${except_by_period_item}         
    @{specific_day_list}        Create List   ${period_day_start}   ${period_day_end}  
    &{allotment_dict}   Create Dictionary   from=${from}    to=${to}   allotment=3    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}

    ## Edit Allotment ##    
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{allotment_dict}   Create Dictionary   from=${from}    to=${to}   allotment=6    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Allotment API    ${access_token}  ${refresh_token}    ${room_type_id}     ${allotment_dict}
    Response Status Should Be 200 OK     ${resp}
    
    ## Verify Allotment result ##
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${from}    ${to}    ${EMPTY}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${from}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${day2}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${day3}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${day4}     ${allotment_dict.allotment}
    Verify Get Allotment API Return Allotment By Date As Expected   ${resp}     ${to}       ${allotment_dict.allotment}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Set Rate For Selected Single Day Properly
    [Tags]      price   setRate   getAllotmentAndRate
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##

    ## Test Service ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{rate_dict}   Create Dictionary   from=${date}    to=${date}   sellingPrice=1250.0     cost=1000.0      percentMargin=20.0     rackPrice=1200.0     promoCodeRef=RobotCodeRef    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${date}    ${date}    ${room_condition_id}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Rate API Return Currency Code As Expected    ${resp}     ${currency_code}
    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${date}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${date}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${date}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${date}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${date}     ${rate_dict.promoCodeRef}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Set Rate For Selected Period Properly
    [Tags]      price   setRate   getAllotmentAndRate       
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##
    ${from} =   Add Time To Date	${date}     1 days      result_format=%Y-%m-%d
    ${between} =     Add Time To Date	    ${from}     1 days      result_format=%Y-%m-%d
    ${to} =     Add Time To Date	${from}     2 days      result_format=%Y-%m-%d

    ## Test Service ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{rate_dict}   Create Dictionary   from=${from}    to=${to}   sellingPrice=1250.0     cost=1000.0      percentMargin=20.0     rackPrice=1200.0     promoCodeRef=RobotCodeRef    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${from}    ${to}    ${room_condition_id}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Rate API Return Currency Code As Expected    ${resp}     ${currency_code}
    
    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${from}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${from}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${from}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${from}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${from}     ${rate_dict.promoCodeRef}
    
    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${between}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${between}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${between}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${between}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${between}     ${rate_dict.promoCodeRef}

    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${to}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${to}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${to}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${to}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${to}     ${rate_dict.promoCodeRef}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Set Rate For Selected Period Properly With Specific Days Properly
    [Tags]      price   setRate   getAllotmentAndRate       T1
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##
    # Set period = 5 days. Day1 = ${from}. Day5 = ${to}
    ${from} =   Add Time To Date	${date}     4 days      result_format=%Y-%m-%d
    ${day2} =   Add Time To Date	${from}     1 days      result_format=%Y-%m-%d
    ${day3} =   Add Time To Date	${from}     2 days      result_format=%Y-%m-%d
    ${day4} =   Add Time To Date	${from}     3 days      result_format=%Y-%m-%d
    ${to} =     Add Time To Date	${from}     4 days      result_format=%Y-%m-%d
    ${period_day_start}=    Convert Date    ${from}    result_format=%A
    ${period_day_start}=    Convert To Lowercase    ${period_day_start}
    ${period_day_end}=    Convert Date    ${to}    result_format=%A
    ${period_day_end}=    Convert To Lowercase    ${period_day_end}

    ## Test Service ##
    @{except_by_date_list}      Create List   ${day4}
    ${except_by_period_item}    Create Dictionary   from=${day2}    to=${day3}
    @{except_by_period_list}    Create List   ${except_by_period_item}         
    @{specific_day_list}        Create List   ${period_day_start}   ${period_day_end}  
    &{rate_dict}   Create Dictionary   from=${from}    to=${to}   sellingPrice=1250.0     cost=1000.0      percentMargin=20.0     rackPrice=1200.0     promoCodeRef=RobotCodeRef    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${from}    ${to}    ${room_condition_id}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Rate API Return Currency Code As Expected    ${resp}     ${currency_code}
    
    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${from}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${from}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${from}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${from}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${from}     ${rate_dict.promoCodeRef}

    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${to}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${to}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${to}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${to}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${to}     ${rate_dict.promoCodeRef}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Update Existing Rate For Selected Single Day Properly
    [Tags]      price   setRate   getAllotmentAndRate
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##

    ## Test Service ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{rate_dict}   Create Dictionary   from=${date}    to=${date}   sellingPrice=1250.0     cost=1000.0      percentMargin=20.0     rackPrice=1200.0     promoCodeRef=RobotCodeRef    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}

    ## Edit Rate ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{rate_dict}   Create Dictionary   from=${date}    to=${date}   sellingPrice=2000.0     cost=1000.0      percentMargin=50.0     rackPrice=1000.0     promoCodeRef=RobotCodeRefEdit    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${date}    ${date}    ${room_condition_id}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Rate API Return Currency Code As Expected    ${resp}     ${currency_code}
    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${date}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${date}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${date}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${date}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${date}     ${rate_dict.promoCodeRef}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Update Existing Rate For Selected Period Properly
    [Tags]      price   setRate   getAllotmentAndRate       
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##
    ${from} =   Add Time To Date	${date}     1 days      result_format=%Y-%m-%d
    ${between} =     Add Time To Date	    ${from}     1 days      result_format=%Y-%m-%d
    ${to} =     Add Time To Date	${from}     2 days      result_format=%Y-%m-%d

    ## Test Service ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{rate_dict}   Create Dictionary   from=${from}    to=${to}   sellingPrice=1250.0     cost=1000.0      percentMargin=20.0     rackPrice=1200.0     promoCodeRef=RobotCodeRef    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}

    ## Edit Rate ##
    @{except_by_date_list}    Create List     
    @{except_by_period_list}  Create List     
    @{specific_day_list}      Create List     monday   tuesday   wednesday   thursday   friday   saturday   sunday
    &{rate_dict}   Create Dictionary   from=${from}    to=${to}   sellingPrice=2000.0     cost=1000.0      percentMargin=50.0     rackPrice=1000.0     promoCodeRef=RobotCodeRefEdit    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${from}    ${to}    ${room_condition_id}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Rate API Return Currency Code As Expected    ${resp}     ${currency_code}
    
    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${from}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${from}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${from}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${from}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${from}     ${rate_dict.promoCodeRef}
    
    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${between}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${between}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${between}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${between}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${between}     ${rate_dict.promoCodeRef}

    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${to}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${to}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${to}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${to}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${to}     ${rate_dict.promoCodeRef}

    ## Clear test data ##
   # TBA after prepare data section is completed

To Verify That API Can Update Existing Rate For Selected Period Properly With Specific Days Properly
    [Tags]      price   setRate   getAllotmentAndRate       T1
    # Prepare Data
    ## Define hotelId, supplierId, roomTypeId, roomConditionId ##
    # Set period = 5 days. Day1 = ${from}. Day5 = ${to}
    ${from} =   Add Time To Date	${date}     4 days      result_format=%Y-%m-%d
    ${day2} =   Add Time To Date	${from}     1 days      result_format=%Y-%m-%d
    ${day3} =   Add Time To Date	${from}     2 days      result_format=%Y-%m-%d
    ${day4} =   Add Time To Date	${from}     3 days      result_format=%Y-%m-%d
    ${to} =     Add Time To Date	${from}     4 days      result_format=%Y-%m-%d
    ${period_day_start}=    Convert Date    ${from}    result_format=%A
    ${period_day_start}=    Convert To Lowercase    ${period_day_start}
    ${period_day_end}=    Convert Date    ${to}    result_format=%A
    ${period_day_end}=    Convert To Lowercase    ${period_day_end}

    ## Test Service ##
    @{except_by_date_list}      Create List   ${day4}
    ${except_by_period_item}    Create Dictionary   from=${day2}    to=${day3}
    @{except_by_period_list}    Create List   ${except_by_period_item}         
    @{specific_day_list}        Create List   ${period_day_start}   ${period_day_end}  
    &{rate_dict}   Create Dictionary   from=${from}    to=${to}   sellingPrice=1250.0     cost=1000.0      percentMargin=20.0     rackPrice=1200.0     promoCodeRef=RobotCodeRef    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}

    ## Edit Rate ##
    @{except_by_date_list}      Create List   ${day4}
    ${except_by_period_item}    Create Dictionary   from=${day2}    to=${day3}
    @{except_by_period_list}    Create List   ${except_by_period_item} 
    &{rate_dict}   Create Dictionary   from=${from}    to=${to}   sellingPrice=2000.0     cost=1000.0      percentMargin=50.0     rackPrice=1000.0     promoCodeRef=RobotCodeRefEdit    exceptByDate=${except_by_date_list}    exceptByPeriod=${except_by_period_list}   specificDay=${specific_day_list}
    ${resp}   POST Set Rate API    ${access_token}  ${refresh_token}    ${room_condition_id}     ${rate_dict}
    Response Status Should Be 200 OK     ${resp}
    
    # Verify Allotment result #
    ${resp}      GET Allotment And Rate API      ${access_token}     ${refresh_token}    ${room_type_id}     ${from}    ${to}    ${room_condition_id}
    Verify Get Allotment API Return Default Percent Margin As Expected     ${resp}      ${default_percent_margin}
    Verify Get Rate API Return Currency Code As Expected    ${resp}     ${currency_code}
    
    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${from}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${from}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${from}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${from}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${from}     ${rate_dict.promoCodeRef}

    Verify Get Rate API Return Selling Price By Date As Expected   ${resp}     ${to}     ${rate_dict.sellingPrice}
    Verify Get Rate API Return Cost By Date As Expected   ${resp}     ${to}     ${rate_dict.cost}
    Verify Get Rate API Return Percent Margin By Date As Expected   ${resp}     ${to}     ${rate_dict.percentMargin}
    Verify Get Rate API Return Rack Price By Date As Expected   ${resp}     ${to}     ${rate_dict.rackPrice}
    Verify Get Rate API Return Promo Code Ref By Date As Expected   ${resp}     ${to}     ${rate_dict.promoCodeRef}

    ## Clear test data ##
   # TBA after prepare data section is completed
