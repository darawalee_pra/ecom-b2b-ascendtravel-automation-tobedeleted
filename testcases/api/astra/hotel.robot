*** Settings ***
#Suite Setup         Prepare Test Data
#Suite Teardown      Clear Test Data
Test Setup  Test Setup
Library             Selenium2Library
Library             DatabaseLibrary
Library             String
Library             OperatingSystem
Library             ${CURDIR}/../../../library/common.py
Library             ${CURDIR}/../../../library/requestLibrary.py
Resource            ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/hotel.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/login.robot
Resource            ${CURDIR}/../../../resources/keywords/common/common.robot
Resource            ${CURDIR}/../../../resources/keywords/api/apicommon.robot

*** Variables ***
#Temp hotel id
${hotel_id}                51
${hotel_title}             aow jing la na1, abashiri
${free_sale}               false
${supplier_id}             45
${supplier_name}           Robot Supplier Edit
${room_type_id}            20
${room_type_name}          Original room
${room_condition_id}       1
${room_condition_title}    Include Breakfast

*** Keywords ***
Test Setup
    ${access_token}     ${refresh_token}=     POST Astra Login API
    Set Suite Variable  ${access_token}
    Set Suite Variable  ${refresh_token}

*** Test Cases ***
To Verify That API Can Get Supplier, Room Type And Room Conditions For Active Allotment-Sale Product Successfully
    [Tags]  hotel   api_get_supplier_roomType_roomCondition
    ## Create Hotel ##
    #TO DO - Create Hotel (Product) not ready to use

    ## Create Supplier ##
    #TO DO - To be added after able to create room type and room condition

    ## Create Room Type ##
    #TO DO - Create room type not ready to use

    ## Create Room Condition ##
    #TO DO - Create room condition not ready to use

    ## Call GET Supplier, RoomType, RoomCondition API ##
    ${resp}          GET Supplier RoomType RoomCondition API        ${access_token}  ${refresh_token}   ${hotel_id}
    Response Status Should Be 200 OK     ${resp}

    ## Verify Supplier RoomType RoomCondition Detail ##
    Verify Get Supplier RoomType RoomCondition API Return Hotel Id As Expected   ${resp}     ${hotel_id}
    Verify Get Supplier RoomType RoomCondition API Return Hotel Title As Expected   ${resp}     ${hotel_title}
    Verify Get Supplier RoomType RoomCondition API Return Free Sale As Expected   ${resp}     ${free_sale}
    Verify Get Supplier RoomType RoomCondition API Return Supplier Hotel - Supplier Name As Expected   ${resp}     ${supplier_id}     ${supplier_name}
    Verify Get Supplier RoomType RoomCondition API Return Supplier Hotel - Room Type Name As Expected   ${resp}     ${room_type_id}     ${room_type_name}
    Verify Get Supplier RoomType RoomCondition API Return Supplier Hotel - Room Condition Title As Expected   ${resp}     ${room_condition_id}      ${room_condition_title}

    ## Clear Data ##
    # TBA