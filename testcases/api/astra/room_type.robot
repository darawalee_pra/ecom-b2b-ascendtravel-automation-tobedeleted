*** Settings ***
Test Setup          Test Setup  
Library             Selenium2Library
Library             DatabaseLibrary
Library             String
Library             OperatingSystem
Library             JSONLibrary
Library             ${CURDIR}/../../../library/common.py
Library             ${CURDIR}/../../../library/excel.py
Resource            ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/supplier.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/product.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/room_type.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/login.robot
Resource            ${CURDIR}/../../../resources/keywords/api/apicommon.robot
Resource            ${CURDIR}/../../../resources/keywords/db/room_type.robot
Resource            ${CURDIR}/../../../resources/keywords/common/common.robot
Resource            ${CURDIR}/../../../resources/testdata/api/${ENV}/astra/room_type/room_type.robot

*** Variables ***
${path_folder}      ${CURDIR}/../../../resources/testdata/api/${ENV}/astra/room_type/
${file_name}        create_room_type.xlsx
${col_key}          B
${col_val}          C
${col_type}         D

*** Keywords ***
Test Setup
    ## login to astra
    ${access_token}   ${refresh_token}=     POST Astra Login API
    Set Suite Variable   ${access_token}   
    Set Suite Variable   ${refresh_token}

*** Test Cases ***
To Verify That Able To Get Hotel General Data Successfully
    [Tags]  room_type    api_get_room_type_general_data
    ## prepare expected results
    Connect ACT Database    ${DB_NAME_V2}
    ${count_roomviews}   Count Total Room View
    ${expected_roomviews}   Select Room View Order By Name EN
    Disconnect From Database
    :FOR    ${index}    IN RANGE    0   ${count_roomviews}
    \   Append To List  ${roomview_name_th}    ${expected_roomviews[${index}][1]}
    \   Append To List  ${roomview_name_en}    ${expected_roomviews[${index}][2]}
    ${supplier_ids}     Get Dictionary Keys     ${supplier_dict_hotel_1}
    ${supplier_names}   Get Dictionary Values   ${supplier_dict_hotel_1}   
    ${count_suppliers}  Get Length   ${supplier_ids}

    ## get hotel that have full information
    ${resp}      GET Room Type General Data API   ${access_token}     ${refresh_token}    ${hotel_id_1} 
    Response Status Should Be 200 OK  ${resp}

    ## verify test result
    Verify Get Room Type General Data API Return Hotel Id As Expected   ${resp}     ${hotel_id_1}
    Verify Get Room Type General Data API Return Hotel Name TH As Expected   ${resp}    ${hotel_name_th}
    Verify Get Room Type General Data API Return Hotel Name EN As Expected   ${resp}    ${hotel_name_en}
    Verify Get Room Type General Data API Return Hotel Address TH As Expected   ${resp}    ${hotel_address_th}
    Verify Get Room Type General Data API Return Hotel Address EN As Expected   ${resp}    ${hotel_address_en}
    # Verify Get Room Type General Data API Return Hotel Image As Expected     ${resp}    ${hotel_image}
    :FOR    ${index}   IN RANGE   0   ${count_suppliers}
    \   Verify Get Room Type General Data API Return Supplier Name By Supplier Id As Expected   ${resp}     ${supplier_ids[${index}]}   ${supplier_names[${index}]}
    ${supplier_names_sort_asc}   Sorting Ascii Asc    ${supplier_names}
    Verify Get Room Type General Data API Return Supplier Name List As Expected    ${resp}   ${supplier_names_sort_asc}
    Verify Get Room Type General Data API Return Room View Name TH List As Expected  ${resp}     ${roomview_name_th}   
    Verify Get Room Type General Data API Return Room View Name EN List As Expected  ${resp}     ${roomview_name_en}   
    Verify Get Room Type General Data API Return Number of Supplier Items As Expected   ${resp}     ${count_suppliers}
    Verify Get Room Type General Data API Return Number of Room View Items As Expected  ${resp}     ${count_roomviews}
    Verify Get Room Type General Data API Return Bed Type List As Expected  ${resp}     ${bed_types_list}

To Verify That Able To Get Hotel General Data As Emtpy Value When Data Not Found
    [Tags]  room_type    api_get_room_type_general_data     BB
    ## prepare expected results
    Connect ACT Database    ${DB_NAME_V2}
    ${count_roomviews}   Count Total Room View
    Disconnect From Database

    ## get hotel that has no nameTH, nameEN, addressTH, addressEN, image and supplier
    ${resp}      GET Room Type General Data API   ${access_token}     ${refresh_token}    ${hotel_id_2} 
    Response Status Should Be 200 OK  ${resp}   

    ## verify test result
    Verify Get Room Type General Data API Return Hotel Id As Expected   ${resp}     ${hotel_id_2}
    Verify Get Room Type General Data API Should Not Return Hotel Name TH   ${resp}
    Verify Get Room Type General Data API Should Not Return Hotel Name EN   ${resp}
    Verify Get Room Type General Data API Should Not Return Hotel Address TH   ${resp}
    Verify Get Room Type General Data API Should Not Return Hotel Address EN   ${resp}
    Verify Get Room Type General Data API Should Not Return Hotel Image   ${resp}
    Verify Get Room Type General Data API Return Number of Supplier Items As Expected   ${resp}     0
    Verify Get Room Type General Data API Return Number of Room View Items As Expected  ${resp}     ${count_roomviews}

To Verify That Able To Get Hotel General Data With Long Hotel Information Successfully
    [Tags]  room_type    api_get_room_type_general_data
    ## get hotel that have long information
    ${resp}      GET Room Type General Data API   ${access_token}     ${refresh_token}    ${hotel_id_3} 
    Response Status Should Be 200 OK  ${resp}

    ## verify test result
    Verify Get Room Type General Data API Return Hotel Id As Expected   ${resp}     ${hotel_id_3}
    Verify Get Room Type General Data API Return Hotel Name TH As Expected   ${resp}    ${hotel_name_th_long}
    Verify Get Room Type General Data API Return Hotel Name EN As Expected   ${resp}    ${hotel_name_en_long}
    Verify Get Room Type General Data API Return Hotel Address TH As Expected   ${resp}    ${hotel_address_th_long}
    Verify Get Room Type General Data API Return Hotel Address EN As Expected   ${resp}    ${hotel_address_en_long}

To Verify That Able to Create Room Type With Standard Room - Single Option - Single Bed Type - No Image - No Facilities 
    [Tags]   room_type    api_create_room_type  TC1
    [Documentation]    Condition:
    ...     - beach view 
    ...     - standard room 
    ...     - single option 
    ...     - single bed type 
    ...     - no image 
    ...     - no facilities

    ${room_type_dict}   Gen Dict From Excel By Columns  ${path_folder}  ${file_name}  TC1  ${col_key}  ${col_val}  ${col_type}

    ${resp}     POST Create Room Type API     ${access_token}     ${refresh_token}    ${room_type_dict}
    Log     ${resp.content}
    Response Status Should Be 200 OK  ${resp}

    ## TODO
    ## to verify test result
    ## to delete test data

To Verify That Able to Create Room Type With Standard Room - Multi Options - Multi Bed Types - No Image - No Facilities 
    [Tags]   room_type    api_create_room_type  TC2
    [Documentation]    Condition:
    ...     - city view
    ...     - standard room
    ...     - multi options
    ...     - multi bed types
    ...     - no image
    ...     - no facilities

    ${room_type_dict}   Gen Dict From Excel By Columns  ${path_folder}  ${file_name}  TC2  ${col_key}  ${col_val}  ${col_type}

    ${resp}     POST Create Room Type API     ${access_token}     ${refresh_token}    ${room_type_dict}
    Response Status Should Be 200 OK  ${resp}

    ## TODO
    ## to verify test result
    ## to delete test data

To Verify That Able to Create Room Type With Seperate Room - Single Room - Single Option - Single Bed Type - No Image - No Facilities 
    [Tags]   room_type    api_create_room_type  TC3
    [Documentation]    Condition:
    ...     - country view
    ...     - seperate room
    ...     - single room
    ...     - single option
    ...     - single bed type
    ...     - no image
    ...     - no facilities

    ${room_type_dict}   Gen Dict From Excel By Columns  ${path_folder}  ${file_name}  TC3  ${col_key}  ${col_val}  ${col_type}

    ${resp}     POST Create Room Type API     ${access_token}     ${refresh_token}    ${room_type_dict}
    Response Status Should Be 200 OK  ${resp}

    ## TODO
    ## to verify test result
    ## to delete test data

To Verify That Able to Create Room Type With Seperate Room - Multi Rooms - Multi Options - Multi Bed Types - No Image - No Facilities 
    [Tags]   room_type    api_create_room_type  TC4
    [Documentation]    Condition:
    ...     - courtyard view
    ...     - seperate room
    ...     - multi rooms
    ...     - multi options
    ...     - multi bed types
    ...     - no image
    ...     - no facilities

    ${room_type_dict}   Gen Dict From Excel By Columns  ${path_folder}  ${file_name}  TC4  ${col_key}  ${col_val}  ${col_type}

    ${resp}     POST Create Room Type API     ${access_token}     ${refresh_token}    ${room_type_dict}
    Response Status Should Be 200 OK  ${resp}

    ## TODO
    ## to verify test result
    ## to delete test data