*** Settings ***
#Suite Setup         Prepare Test Data
# Suite Teardown      Clear Test Data
Test Setup  Test Setup
Library             Selenium2Library
Library             DatabaseLibrary
Library             String
Library             OperatingSystem
Library             JSONLibrary
Library             ${CURDIR}/../../../library/excel.py
Library             ${CURDIR}/../../../library/common.py
Library             ${CURDIR}/../../../library/requestLibrary.py
Resource            ${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/supplier.robot
Resource            ${CURDIR}/../../../resources/keywords/api/astra/login.robot
Resource            ${CURDIR}/../../../resources/keywords/db/supplier.robot
Resource            ${CURDIR}/../../../resources/keywords/common/common.robot
Resource            ${CURDIR}/../../../resources/testdata/api/${ENV}/astra/supplier/supplier.robot
Resource            ${CURDIR}/../../../resources/keywords/api/apicommon.robot

*** Variables ***
${path_folder}                  ${CURDIR}/../../../resources/testdata/api/${ENV}/astra/supplier




*** Keywords ***
Prepare Test Data
    ${col_name}=      Create List    A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   P
    # Please set DB_NAME to 'alpha_supplier'
    # Clear Test Data
    Log To Console    ...
    Log To Console    Preparing Suppliers Test Data

    # :FOR    ${index}    IN RANGE    2   7
    # \   ${name}         get_value_excel_by_column    ${path_folder}/${supplier_data_file}   ${sheet_name}    ${col_name[2]}${index}
    # \   ${sap_code}     get_value_excel_by_column    ${path_folder}/${supplier_data_file}   ${sheet_name}    ${col_name[14]}${index}
    # \   ${alt_sapcode}  get_value_excel_by_column    ${path_folder}/${supplier_data_file}   ${sheet_name}    ${col_name[15]}${index}
    # \   ${json}         Load JSON From File    ${path_folder}/${create_supplier_json}
    # \   ${reqs_obj}     Update Value To Json   ${json}  $.supplierInformation.supplierName   ${name}
    # \   ${reqs_obj}     Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    # \   ${dict} 	    Run Keyword If  '${alt_sapcode}' != 'None'   Create Dictionary	    sapCode=${alt_sapcode}	currencyId=1    mainCurrency=false
    # \   ${reqs_obj}     Run Keyword If  '${alt_sapcode}' != 'None'   Add Object To Json	${json}	 $.supplierMultiCurrency   ${dict}
    # \   ${new_obj}      Convert JSON To String    ${reqs_obj}
    # \   Create File     ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    # \   ${access_token}  ${refresh_token}=     POST Astra Login API
    # \   ${resp}   POST Create Update Supplier API   ${access_token}    ${refresh_token}    ${path_folder}/${sap_code}.json
    # \   Log To Console  ${resp.content}
    # \   Response Status Should Be 200 OK     ${resp}
    # \   Remove File     ${path_folder}/${sap_code}.json

    ## Get total number of active suppliers from DB
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    ${total_supplier}=  Count Total Supplier
    ${total_supplier}   Set Variable    ${total_supplier[0][0]}
    Set Suite Variable    ${total_supplier}
    Log To Console  TOTAL SUPPLIER = ${total_supplier}
    Disconnect From Database

Clear Test Data
    ## TODO - To delete by API
    Log To Console    ...
    Log To Console    Clearing Suppliers Test Data
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier Data From DB   ${sup_name_1}
    Delete Supplier Data From DB   ${sup_name_2}
    Delete Supplier Data From DB   ${sup_name_3}
    Delete Supplier Data From DB   ${sup_name_4}
    Delete Supplier Data From DB   ${sup_name_5}
    Disconnect From Database

Test Setup
    ${access_token}  ${refresh_token}=     POST Astra Login API
    Set Suite Variable  ${access_token}
    Set Suite Variable  ${refresh_token}

*** Test Cases ***
To Verify That Able To Get Supplier List By Default Values Correctly
    [Tags]      supplier   api_get_supplier_list    T1
    ${access_token}  ${refresh_token}=     POST Astra Login API
    ${total_page_no}   Get Total Page Number   ${total_supplier}   ${page_size_default}

    ${resp}    GET Supplier List API   ${access_token}     ${refresh_token}
    Response Status Should Be 200 OK     ${resp}
    Run Keyword If   ${total_supplier} >= ${page_size_default}  Verify Get Supplier List API Resposne Number of Items As Expected   ${resp}     ${page_size_default}
    ...    ELSE IF   ${page_size_default} > ${total_supplier}   Verify Get Supplier List API Resposne Number of Items As Expected   ${resp}     ${total_supplier}
    Verify Get Supplier List Return Sort By Value As Expected        ${resp}     supplier_name
    Verify Get Supplier List Return Sort Type Value As Expected      ${resp}     ${sort_asc}
    Verify Get Supplier List Return From Item Value As Expected      ${resp}     1
    Run Keyword If   ${total_supplier} >= ${page_size_default}  Verify Get Supplier List Return To Item Value As Expected  ${resp}   ${page_size_default}
    ...    ELSE IF   ${page_size_default} > ${total_supplier}   Verify Get Supplier List Return To Item Value As Expected  ${resp}   ${total_supplier}
    Verify Get Supplier List Return Current Page Value As Expected   ${resp}     1
    Verify Get Supplier List Return Total Item Value As Expected     ${resp}     ${total_supplier}
    Verify Get Supplier List Return Total Page Value As Expected     ${resp}     ${total_page_no}

To Verify That Able To Get Supplier List By Limit Rows Number Per Page as 50, 100, 150 and 200 Correctly
    [Tags]      supplier  api_get_supplier_list
    ${access_token}  ${refresh_token}=     POST Astra Login API
    ${total_page_no_default}    Get Total Page Number   ${total_supplier}   ${page_size_default}
    ${total_page_no_s}          Get Total Page Number   ${total_supplier}   ${page_size_s}
    ${total_page_no_m}          Get Total Page Number   ${total_supplier}   ${page_size_m}
    ${total_page_no_l}          Get Total Page Number   ${total_supplier}   ${page_size_l}

    ${resp}    GET Supplier List API   ${access_token}     ${refresh_token}     ${EMPTY}    1   ${page_size_s}    supplier_name   ${sort_asc}
    Run Keyword If   ${total_supplier} >= ${page_size_s}   Verify Get Supplier List API Resposne Number of Items As Expected   ${resp}   ${page_size_s}
    ...    ELSE IF   ${page_size_s} > ${total_supplier}    Verify Get Supplier List API Resposne Number of Items As Expected   ${resp}   ${total_supplier}
    Run Keyword If   ${total_supplier} >= ${page_size_s}   Verify Get Supplier List Return To Item Value As Expected   ${resp}  ${page_size_s}
    ...    ELSE IF   ${page_size_s} > ${total_supplier}    Verify Get Supplier List Return To Item Value As Expected   ${resp}  ${total_supplier}
    Verify Get Supplier List Return Total Page Value As Expected     ${resp}     ${total_page_no_s}

    ${resp}    GET Supplier List API   ${access_token}     ${refresh_token}     ${EMPTY}    1   ${page_size_m}    supplier_name   ${sort_asc}
    Run Keyword If   ${total_supplier} >= ${page_size_m}   Verify Get Supplier List API Resposne Number of Items As Expected    ${resp}   ${page_size_m}
    ...    ELSE IF   ${page_size_m} > ${total_supplier}    Verify Get Supplier List API Resposne Number of Items As Expected    ${resp}   ${total_supplier}
    Run Keyword If   ${total_supplier} >= ${page_size_m}   Verify Get Supplier List Return To Item Value As Expected   ${resp}  ${page_size_m}
    ...    ELSE IF   ${page_size_m} > ${total_supplier}    Verify Get Supplier List Return To Item Value As Expected   ${resp}  ${total_supplier}
    Verify Get Supplier List Return Total Page Value As Expected     ${resp}     ${total_page_no_m}

    ${resp}    GET Supplier List API   ${access_token}     ${refresh_token}     ${EMPTY}    1   ${page_size_l}    supplier_name   ${sort_asc}
    Run Keyword If   ${total_supplier} >= ${page_size_l}   Verify Get Supplier List API Resposne Number of Items As Expected    ${resp}   ${page_size_l}
    ...    ELSE IF   ${page_size_l} > ${total_supplier}    Verify Get Supplier List API Resposne Number of Items As Expected    ${resp}   ${total_supplier}
    Run Keyword If   ${total_supplier} >= ${page_size_l}   Verify Get Supplier List Return To Item Value As Expected   ${resp}  ${page_size_l}
    ...    ELSE IF   ${page_size_l} > ${total_supplier}    Verify Get Supplier List Return To Item Value As Expected   ${resp}  ${total_supplier}
    Verify Get Supplier List Return Total Page Value As Expected     ${resp}     ${total_page_no_l}

    ${total_page_no}    Evaluate    ${total_page_no_default} + 1
    :FOR  ${page}  IN RANGE     1   ${total_page_no}
    \   ${resp}    GET Supplier List API   ${access_token}    ${refresh_token}    ${EMPTY}  ${page}   ${page_size_default}   supplier_name   ${sort_asc}
    \   ${to_item}     Run Keyword If   ${page} == ${total_page_no_default}     Set Variable    ${total_supplier}
    \   ...   ELSE     Evaluate   ${page_size_default} * ${page}
    \   ${from_item}   Evaluate    (${page_size_default} * (${page}-1)) + 1
    \   Verify Get Supplier List Return From Item Value As Expected      ${resp}     ${from_item}
    \   Verify Get Supplier List Return To Item Value As Expected        ${resp}     ${to_item}
    \   Verify Get Supplier List Return Current Page Value As Expected   ${resp}     ${page}
    \   Verify Get Supplier List Return Total Item Value As Expected     ${resp}     ${total_supplier}
    \   Verify Get Supplier List Return Total Page Value As Expected     ${resp}     ${total_page_no_default}

To Verify That Able To Get Supplier List By Sorting Main Sap Code Correctly
    [Tags]      supplier   api_get_supplier_list  BB
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    ${exp_sap_code_sorted_asc}    Select Supplier Main Sap Code Order By Sap Code    ${sort_asc}
    ${exp_sap_code_sorted_desc}   Select Supplier Main Sap Code Order By Sap Code    ${sort_desc}
    Disconnect From Database
    ## Sort expected val to be ASCII Sort Order
    ${exp_sap_code_sorted_asc}    common.convert_array_to_list  ${exp_sap_code_sorted_asc}
    ${exp_sap_code_sorted_asc}    common.sorting_ascii_asc   ${exp_sap_code_sorted_asc}
    ${exp_sap_code_sorted_desc}   common.convert_array_to_list  ${exp_sap_code_sorted_desc}
    ${exp_sap_code_sorted_desc}   common.sorting_ascii_desc   ${exp_sap_code_sorted_desc}
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${EMPTY}    1   ${page_size_default}    sap_code   ${sort_asc}
    Verify Get Supplier List API Sorting Result By Supplier Main Sap Code As Expected    ${resp}    ${exp_sap_code_sorted_asc}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${EMPTY}    1   ${page_size_default}    sap_code   ${sort_desc}
    Verify Get Supplier List API Sorting Result By Supplier Main Sap Code As Expected    ${resp}    ${exp_sap_code_sorted_desc}

To Verify That Able To Get Supplier List By Sorting Supplier Name Correctly
    [Tags]      supplier   api_get_supplier_list
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    ${exp_name_sorted_asc}    Select Supplier Name Order By Name   ${sort_asc}
    ${exp_name_sorted_desc}   Select Supplier Name Order By Name   ${sort_desc}
    Disconnect From Database
    ## Sort expected val to be ASCII Sort Order
    ${exp_name_sorted_asc}    common.convert_array_to_list  ${exp_name_sorted_asc}
    ${exp_name_sorted_asc}    common.sorting_ascii_asc   ${exp_name_sorted_asc}
    ${exp_name_sorted_desc}   common.convert_array_to_list  ${exp_name_sorted_desc}
    ${exp_name_sorted_desc}   common.sorting_ascii_desc   ${exp_name_sorted_desc}

    ${access_token}  ${refresh_token}=     POST Astra Login API

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${EMPTY}    1   ${page_size_default}    supplier_name   ${sort_asc}
    Verify Get Supplier List API Sorting Result By Supplier Name As Expected    ${resp}    ${exp_name_sorted_asc}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${EMPTY}    1   ${page_size_default}    supplier_name   ${sort_desc}
    Verify Get Supplier List API Sorting Result By Supplier Name As Expected    ${resp}    ${exp_name_sorted_desc}

To Verify That Able To Get Supplier List By Filter Keywords Correctly
    [Tags]      supplier  api_get_supplier_list
    ${access_token}  ${refresh_token}=     POST Astra Login API
    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${main_sap_code_1}
    Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected  ${resp}  ${main_sap_code_1}   ${sup_name_1}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${sup_name_2}
    Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected  ${resp}  ${main_sap_code_2}   ${sup_name_2}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${alt_sap_code_2}
    Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected  ${resp}  ${main_sap_code_4}   ${sup_name_4}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${keyword_1}
    Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected  ${resp}  ${main_sap_code_4}   ${sup_name_4}
    Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected  ${resp}  ${main_sap_code_1}   ${sup_name_1}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${keyword_2}
    Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected  ${resp}  ${main_sap_code_2}   ${sup_name_2}
    Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected  ${resp}  ${main_sap_code_3}   ${sup_name_3}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${keyword_3}
    Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected  ${resp}  ${main_sap_code_4}   ${sup_name_4}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${keyword_4}
    Verify Get Supplier List API Return Empty Supplier List     ${resp}

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${keyword_5}
    Verify Get Supplier List API Return Empty Supplier List     ${resp}

To Verify That Able To Update Supplier Status Correctly
    [Tags]      supplier  api_update_supplier_status
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${main_sap_code_1}
    ${supplier_id}=    Get Supplier Id By Supplier Main Sap Code    ${resp}     ${main_sap_code_1}
    Log To Console  supplier id=${supplier_id}

    ${resp}=    PUT Supplier Status API    ${access_token}     ${refresh_token}     ${supplier_id}   ${active_status}
    Verify Update Supplier Status API Return Supplier Status As Expected    ${resp}  ${active_status}
    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${main_sap_code_1}
    Verify Get Supplier List API Return Supplier Status By Main Sap Code As Expected    ${resp}  ${main_sap_code_1}  ${active_status}

    ${resp}=    PUT Supplier Status API    ${access_token}     ${refresh_token}     ${supplier_id}   ${inactive_status}
    Verify Update Supplier Status API Return Supplier Status As Expected    ${resp}  ${inactive_status}
    ${resp}=    GET Supplier List API   ${access_token}     ${refresh_token}    ${main_sap_code_1}
    Verify Get Supplier List API Return Supplier Status By Main Sap Code As Expected    ${resp}  ${main_sap_code_1}  ${inactive_status}

To Verify That Able To Get Supplier General Data Correctly
    [Tags]      supplier  api_get_supplier_general_data
    ${access_token}  ${refresh_token}=     POST Astra Login API
    Connect ACT Database    ${DB_NAME_SUPPLIER}

    ${supplier_type}    Select Supplier Type List
    ${count}   Get Length     ${supplier_type}
    ${resp}    GET Supplier General Data API   ${access_token}   ${refresh_token}   supplier_type
    Response Status Should Be 200 OK     ${resp}
    :FOR  ${index}  IN RANGE    0   ${count}
    \   Verify Get Supplier General Data Return Supplier Type Title As Expected     ${resp}     ${supplier_type[${index}][0]}   ${supplier_type[${index}][1]}
    ${supplier_type_sorted}    Select Supplier Type Order By ID   ${sort_asc}
    Verify Supplier Type Sorting As Expected    ${resp}     ${supplier_type_sorted}

    ${supplier_payment_type}  Select Supplier Payment Type List
    ${count}   Get Length     ${supplier_payment_type}
    ${resp}    GET Supplier General Data API   ${access_token}   ${refresh_token}   supplier_payment_type
    :FOR  ${index}  IN RANGE    0   ${count}
    \   Verify Get Supplier General Data Return Supplier Payment Type As Expected   ${resp}     ${supplier_payment_type[${index}][0]}   ${supplier_payment_type[${index}][1]}
    ${supplier_payment_type_sorted}    Select Supplier Payment Type Order By ID   ${sort_asc}
    Verify Supplier Payment Type Sorting As Expected    ${resp}     ${supplier_payment_type_sorted}

    ${bank}     Select Bank List
    ${count}    Get Length  ${bank}
    ${resp}     GET Supplier General Data API   ${access_token}   ${refresh_token}   bank
    :FOR  ${index}  IN RANGE    0   ${count}
    \   Verify Get Supplier General Data Return Bank As Expected    ${resp}     ${bank[${index}][0]}   ${bank[${index}][1]}
    ${supplier_bank_sorted}    Select Bank List Order By Name   ${sort_asc}
    Verify Bank List Sorting As Expected    ${resp}     ${supplier_bank_sorted}

    ${currency}    Select Currency List
    ${count}       Get Length  ${currency}
    ${resp}        GET Supplier General Data API   ${access_token}   ${refresh_token}   currency
    :FOR  ${index}  IN RANGE    0   ${count}
    \   Verify Get Supplier General Data Return Currency As Expected   ${resp}  ${currency[${index}][0]}   ${currency[${index}][1]}   ${currency[${index}][2]}
    ${supplier_currency_sorted}    Select Currency List Order By Code   ${sort_asc}
    Verify Currency List Sorting As Expected    ${resp}     ${supplier_currency_sorted}

    Disconnect From Database

    Connect ACT Database    ${DB_NAME_V2}
    ${country}  Select Country List
    ${count}    Get Length   ${country}
    ${resp}     GET Supplier General Data API   ${access_token}   ${refresh_token}   country
    :FOR  ${index}  IN RANGE  0   ${count}
    \   Verify Get Supplier General Data Return Country As Expected  ${resp}  ${country[${index}][0]}   ${country[${index}][3]}     ${country[${index}][4]}
    ${supplier_country_sorted}    Select Country List Order By Name EN   ${sort_asc}
    Verify Country List Sorting As Expected    ${resp}     ${supplier_country_sorted}
    Disconnect From Database

To Verify That Can Create Supplier With Payment Method Petty Cash - Non Credit Successfully
    [Tags]    supplier    api_create_supplier
    [Documentation]  notFollowUpTaxInvoice=true  includeVat=true  supplierStatus=1  paymentMethod=PettyCash-NonCredit  singleSAPcode  singleSupplierContact  noDocument

    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.credit       false
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}          POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    Response Status Should Be 200 OK     ${resp}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Response Status Should Be 200 OK     ${resp}

    ## Verify Supplier Detail ##
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    ${def_supplier_name}
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  ${def_address}
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  ${def_tax_name}
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  ${def_tax_id}
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  ${def_tax_address}
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  ${def_not_follow_invoice}
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    ${def_mark_up}
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   ${def_include_vat}
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   ${def_status}
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}  ${def_payment_type_id}    ${def_payment_title}
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  ${def_payment_type_id}    ${def_payment_days}
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  ${def_payment_type_id}     false
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  ${def_payment_type_id}    ${def_payment_active}
    Verify Get Supplier Detail API Return Supplier Payment Method - Account Name By Type Id As Expected   ${resp}  ${def_payment_type_id}   ${def_account_name}
    Verify Get Supplier Detail API Return Supplier Payment Method - Account Number By Type Id As Expected   ${resp}  ${def_payment_type_id}   ${def_account_number}
    Verify Get Supplier Detail API Return Supplier Payment Method - Bank Id By Type Id As Expected   ${resp}   ${def_payment_type_id}   ${def_bank_id}
    Verify Get Supplier Detail API Return Supplier Payment Method - Bank Title By Type Id As Expected   ${resp}   ${def_payment_type_id}   ${def_bank_title}
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_id}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_title}
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_phone}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${def_document_list}

    ## Clear Test Data ##
    ${supplier_payment_id}  Get Value From Json     ${resp.json()}  $..data.supplierPaymentMethod[0].supplierPaymentId
    DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Bank By Supplier Payment Id    ${supplier_payment_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify That Can Create Supplier With Payment Method Credit Card - Non Credit Successfully
    [Tags]    supplier    api_create_supplier
    [Documentation]  notFollowUpTaxInvoice=false  includeVat=false  supplierStatus=0  paymentMethod=CreditCard-NonCredit  multipleSAPcode  multipleSupplierContact  singleDocument
   
    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${other_sap_code}     Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierInformation.notFollowUpTaxInvoice    false
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierInformation.includeVat       false
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierInformation.supplierStatus   false
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     2
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.credit       false
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountName
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountNumber
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.bankId
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.note
    ${dict}          Create Dictionary      sapCode=${other_sap_code}   currencyId=1    mainCurrency=false  
    ${reqs_obj}      Add Object To Json     ${json}  $.supplierMultiCurrency        ${dict}
    ${dict}          Create Dictionary      name=robotcontact   position=Reservation    email=robot@robot.com   phone=0123456789
    ${reqs_obj}      Add Object To Json     ${json}  $.supplierContact  ${dict}
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}                 POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json    ${path_folder}/${pdf_file}
    @{def_document_list}    Create List      ${path_folder}/${pdf_file}
    ${supplier_id}          Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}

    # Verify Supplier Detail
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    ${def_supplier_name}
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  ${def_address}
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  ${def_tax_name}
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  ${def_tax_id}
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  ${def_tax_address}
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  false
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    ${def_mark_up}
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   false
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   false
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}     2    Credit Card
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  2    ${def_payment_days}
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  2     false
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  2    ${def_payment_active}
    ## Verify SAP code
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_id}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_title}
    ## Verify another SAP code
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${other_sap_code}   false
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${other_sap_code}   1
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${other_sap_code}   THB
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${other_sap_code}   Thai Baht
    ## Verify supplier contact
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_phone}
    ## Verify another supplier contact
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${dict.name}  ${dict.position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${dict.name}  ${dict.email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${dict.name}  ${dict.phone}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${def_document_list}

    ## Clear Test Data ##
    DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier Document By Supplier Id   ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify That Can Create Supplier With Payment Method Link (Corporate Card) - Non Credit Successfully
    [Tags]    supplier    api_create_supplier
    [Documentation]  notFollowUpTaxInvoice=true  includeVat=false  supplierStatus=1  paymentMethod=Link(CorporateCard)-NonCredit  singleSAPcode  singleSupplierContact  multipleDocument

    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierInformation.includeVat       false
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     3
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.credit       false
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountName
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountNumber
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.bankId
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.note
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${doc_list}      Create List     ${path_folder}/${pdf_file}  ${path_folder}/${png_file}  ${path_folder}/${jpg_file}
    # ${doc_list_name}    Create List     ${pdf_file}     ${png_file}     ${jpg_file}
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}          POST Create Update Supplier With Multiple Documents API   ${access_token}   ${refresh_token}    ${path_folder}/${sap_code}.json  ${doc_list}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}

    ### Verify Supplier Detail ##
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    ${def_supplier_name}
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  ${def_address}
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  ${def_tax_name}
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  ${def_tax_id}
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  ${def_tax_address}
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  ${def_not_follow_invoice}
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    ${def_mark_up}
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   false
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   ${def_status}
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}     3    Link (Corporate Card)
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  3    ${def_payment_days}
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  3     false
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  3    ${def_payment_active}
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_id}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_title}
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_phone}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${pdf_file}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${png_file}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${jpg_file}

    ## Clear Test Data ##
    DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier Document By Supplier Id   ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify That Can Create Supplier With Payment Method Petty Cash - Credit Terms Successfully
    [Tags]    supplier    api_create_supplier
    [Documentation]  notFollowUpTaxInvoice=true  includeVat=true  supplierStatus=1  paymentMethod=PettyCash-Credit  singleSAPcode  singleSupplierContact  noDocument

    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}          POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    Response Status Should Be 200 OK     ${resp}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Response Status Should Be 200 OK     ${resp}

    ### Verify Supplier Detail ##
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    ${def_supplier_name}
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  ${def_address}
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  ${def_tax_name}
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  ${def_tax_id}
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  ${def_tax_address}
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  ${def_not_follow_invoice}
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    ${def_mark_up}
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   ${def_include_vat}
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   ${def_status}
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}  ${def_payment_type_id}    ${def_payment_title}
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  ${def_payment_type_id}    ${def_payment_days}
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  ${def_payment_type_id}     ${def_payment_credit}
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  ${def_payment_type_id}    ${def_payment_active}
    Verify Get Supplier Detail API Return Supplier Payment Method - Note By Type Id As Expected     ${resp}  ${def_payment_type_id}    ${def_payment_note}
    Verify Get Supplier Detail API Return Supplier Payment Method - Account Name By Type Id As Expected   ${resp}  ${def_payment_type_id}   ${def_account_name}
    Verify Get Supplier Detail API Return Supplier Payment Method - Account Number By Type Id As Expected   ${resp}  ${def_payment_type_id}   ${def_account_number}
    Verify Get Supplier Detail API Return Supplier Payment Method - Bank Id By Type Id As Expected   ${resp}   ${def_payment_type_id}   ${def_bank_id}
    Verify Get Supplier Detail API Return Supplier Payment Method - Bank Title By Type Id As Expected   ${resp}   ${def_payment_type_id}   ${def_bank_title}
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_id}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_title}
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_phone}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${def_document_list}

    ## Clear Test Data ##
    ${supplier_payment_id}  Get Value From Json     ${resp.json()}   $..data.supplierPaymentMethod[0].supplierPaymentId
    DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Bank By Supplier Payment Id    ${supplier_payment_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify That Can Create Supplier With Payment Method Credit Card - Credit Terms Successfully
    [Tags]    supplier    api_create_supplier
    [Documentation]  notFollowUpTaxInvoice=true  includeVat=true  supplierStatus=1  paymentMethod=CreditCard-NonCredit  singleSAPcode  singleSupplierContact  noDocument
 
    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     2
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountName
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountNumber
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.bankId
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}          POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    Response Status Should Be 200 OK     ${resp}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Response Status Should Be 200 OK     ${resp}

    ## Verify Supplier Detail ##
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    ${def_supplier_name}
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  ${def_address}
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  ${def_tax_name}
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  ${def_tax_id}
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  ${def_tax_address}
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  ${def_not_follow_invoice}
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    ${def_mark_up}
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   ${def_include_vat}
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   ${def_status}
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}     2    Credit Card
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  2    ${def_payment_days}
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  2     ${def_payment_credit}
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  2    ${def_payment_active}
    Verify Get Supplier Detail API Return Supplier Payment Method - Note By Type Id As Expected     ${resp}  2    ${def_payment_note}
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_id}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_title}
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_phone}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${def_document_list}

    ## Clear Test Data ##
    DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify That Can Create Supplier With Payment Method Link (Corporate Card) - Credit Terms Successfully
    [Tags]    supplier    api_create_supplier
    [Documentation]  notFollowUpTaxInvoice=true  includeVat=true  supplierStatus=1  paymentMethod=Link(CorporateCard)-Credit  singleSAPcode  singleSupplierContact  noDocument

    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     3
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountName
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountNumber
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.bankId
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.receiver
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}          POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    Response Status Should Be 200 OK     ${resp}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Response Status Should Be 200 OK     ${resp}

    ### Verify Supplier Detail ##
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    ${def_supplier_name}
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  ${def_address}
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  ${def_tax_name}
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  ${def_tax_id}
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  ${def_tax_address}
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  ${def_not_follow_invoice}
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    ${def_mark_up}
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   ${def_include_vat}
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   ${def_status}
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}     3    Link (Corporate Card)
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  3    ${def_payment_days}
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  3     ${def_payment_credit}
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  3    ${def_payment_active}
    Verify Get Supplier Detail API Return Supplier Payment Method - Note By Type Id As Expected     ${resp}  3    ${def_payment_note}
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_id}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_title}
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_phone}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${def_document_list}

    ## Clear Test Data ##
    DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify That Can Create Supplier With Payment Method Cheque - Credit Terms Successfully
    [Tags]    supplier    api_create_supplier
    [Documentation]  notFollowUpTaxInvoice=true  includeVat=true  supplierStatus=1  paymentMethod=Cheque-Credit  singleSAPcode  singleSupplierContact  noDocument

    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     4
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountName
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountNumber
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.bankId
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}          POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    Response Status Should Be 200 OK     ${resp}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Response Status Should Be 200 OK     ${resp}

    ### Verify Supplier Detail ##
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    ${def_supplier_name}
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  ${def_address}
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  ${def_tax_name}
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  ${def_tax_id}
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  ${def_tax_address}
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  ${def_not_follow_invoice}
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    ${def_mark_up}
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   ${def_include_vat}
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   ${def_status}
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}     4    Cheque
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  4    ${def_payment_days}
    Verify Get Supplier Detail API Return Supplier Payment Method - Receiver By Type Id As Expected   ${resp}  4     ${def_payment_receiver}
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  4     ${def_payment_credit}
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  4    ${def_payment_active}
    Verify Get Supplier Detail API Return Supplier Payment Method - Note By Type Id As Expected     ${resp}  4    ${def_payment_note}
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_id}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_title}
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_phone}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${def_document_list}

    # ## Clear Test Data ##
    # DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    # Connect ACT Database    ${DB_NAME_SUPPLIER}
    # Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    # Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    # Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    # Delete Supplier By Supplier Id  ${supplier_id[0]}
    # Disconnect From Database
    # Remove File     ${path_folder}/${sap_code}.json

To Verify That Can Create Supplier With Payment Method Account Transfer - Credit Terms Successfully
    [Tags]    supplier    api_create_supplier       T1
    [Documentation]  notFollowUpTaxInvoice=true  includeVat=true  supplierStatus=1  paymentMethod=AccountTransfer-Credit  singleSAPcode  singleSupplierContact  noDocument
    
    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     5
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.receiver
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}          POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    Response Status Should Be 200 OK     ${resp}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Response Status Should Be 200 OK     ${resp}

    ### Verify Supplier Detail ##
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    ${def_supplier_name}
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  ${def_address}
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  ${def_tax_name}
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  ${def_tax_id}
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  ${def_tax_address}
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  ${def_not_follow_invoice}
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    ${def_mark_up}
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   ${def_include_vat}
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   ${def_status}
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}  5    Account Transfer
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  5    ${def_payment_days}
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  5     ${def_payment_credit}
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  5    ${def_payment_active}
    Verify Get Supplier Detail API Return Supplier Payment Method - Note By Type Id As Expected     ${resp}  5    ${def_payment_note}
    Verify Get Supplier Detail API Return Supplier Payment Method - Account Name By Type Id As Expected   ${resp}  5   ${def_account_name}
    Verify Get Supplier Detail API Return Supplier Payment Method - Account Number By Type Id As Expected   ${resp}  5   ${def_account_number}
    Verify Get Supplier Detail API Return Supplier Payment Method - Bank Id By Type Id As Expected   ${resp}   5   ${def_bank_id}
    Verify Get Supplier Detail API Return Supplier Payment Method - Bank Title By Type Id As Expected   ${resp}   5   ${def_bank_title}
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_id}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${sap_code}   ${def_currency_title}
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_position}
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_email}
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${def_contact_name}  ${def_contact_phone}
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${def_document_list}

    ## Clear Test Data ##
    ${supplier_payment_id}  Get Value From Json     ${resp.json()}  $..data.supplierPaymentMethod[0].supplierPaymentId
    DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Bank By Supplier Payment Id    ${supplier_payment_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify That Can Edit Supplier Successfully
    [Tags]    supplier    api_edit_supplier
    [Documentation]  notFollowUpTaxInvoice=true  includeVat=true  supplierStatus=1  paymentMethod=PettyCash-NonCredit  singleSAPcode  singleSupplierContact  noDocument
    
    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.credit       false
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service ##
    ${resp}          POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    Response Status Should Be 200 OK     ${resp}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Response Status Should Be 200 OK     ${resp}

    [Documentation]  notFollowUpTaxInvoice=false  includeVat=false  supplierStatus=0  paymentMethod=CreditCard-Credit  singleSAPcode  multipleSupplierContact  sigleDocument
    ## Prepare Data Edit ##
    ${other_sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${edit_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${other_sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.credit       ${def_payment_credit}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     2
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierInformation.supplierId       ${supplier_id[0]}
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${other_sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API

    ## Test Service - Edit ##
    ${resp}          POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${other_sap_code}.json   ${path_folder}/${pdf_file}
    Response Status Should Be 200 OK     ${resp}
    ${supplier_id}   Get Value From Json     ${resp.json()}  $.data.supplierId

    ## Verify Test Result ##
    ${resp}          GET Supplier Detail API        ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Response Status Should Be 200 OK     ${resp}

    ## Verify Supplier Detail ##
    Verify Get Supplier Detail API Return Supplier Type Id As Expected   ${resp}     ${def_type_id}
    Verify Get Supplier Detail API Return Supplier Type Title As Expected    ${resp}    ${def_type_title}
    Verify Get Supplier Detail API Return Country Id As Expected   ${resp}   ${def_country_id}
    Verify Get Supplier Detail API Return Country Title As Expected   ${resp}    ${def_country_title}
    Verify Get Supplier Detail API Return Supplier Name As Expected   ${resp}    Robot Supplier Edit
    Verify Get Supplier Detail API Return Supplier Address As Expected   ${resp}  TEST EDIT2
    Verify Get Supplier Detail API Return Supplier Tax Name As Expected  ${resp}  TEST EDIT2
    Verify Get Supplier Detail API Return Supplier Tax Id As Expected    ${resp}  1234567890123
    Verify Get Supplier Detail API Return Supplier Tax Address As Expected   ${resp}  TEST EDIT2
    Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected  ${resp}  false
    ${def_email_reserve_list}   Create List     thortestuserccc@gmail.com       aaa@aaa.ooo
    Verify Get Supplier Detail API Return Email Reservation List As Expected   ${resp}    ${def_email_reserve_list}
    Verify Get Supplier Detail API Return Default Markup As Expected   ${resp}    4.0
    Verify Get Supplier Detail API Return Include Vat As Expected   ${resp}   false
    Verify Get Supplier Detail API Return Supplier Status As Expected   ${resp}   false
    Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected   ${resp}     2    Credit Card
    Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected   ${resp}  2    6
    Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected   ${resp}  2     ${def_payment_credit}
    Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected   ${resp}  2    ${def_payment_active}
    Verify Get Supplier Detail API Return Supplier Payment Method - Note By Type Id As Expected     ${resp}  2    TEST EDIT credit2
    Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected   ${resp}  ${other_sap_code}   ${def_is_main_sap_code}
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected   ${resp}  ${other_sap_code}   2
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected   ${resp}  ${other_sap_code}   USD
    Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected   ${resp}  ${other_sap_code}   US Dollar
    ## Verify supplier contact
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${edit_contact_name1}  Sales
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${edit_contact_name1}  robot@edit1.co
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${edit_contact_name1}  0801234567
    ## Verify another supplier contact
    Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected   ${resp}  ${edit_contact_name2}  Owner
    Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected   ${resp}  ${edit_contact_name2}  robot@edit2.co
    Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected   ${resp}  ${edit_contact_name2}  08107654321
    Verify Get Supplier Detail API Return Supplier Document or Not as Expected  ${reqs_obj}     ${resp}     ${pdf_file}

    ## Clear Test Data ##
    ${resp_del}      DELETE Supplier API    ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Should Be Equal As Strings  ${resp_del.status_code}  200
    Remove File     ${path_folder}/${sap_code}.json
    Remove File     ${path_folder}/${other_sap_code}.json

To Verify That Can Delete Supplier Successfully
    [Tags]    supplier    api_delete_supplier
    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     3
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountName
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountNumber
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.bankId
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API
    ${resp_create}   POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    ${supplier_id}   Get Value From Json    ${resp_create.json()}  $.data.supplierId

    ## Test Service ##
    ${resp_del}      DELETE Supplier API    ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Should Be Equal As Strings  ${resp_del.status_code}  200

    ## Verify Test Result ##
    ${resp_get_list}     GET Supplier List API  ${access_token}  ${refresh_token}   ${sap_code}
    Verify Get Supplier List API Return Empty Supplier List  ${resp_get_list}

    ${resp_get_detail}   GET Supplier Detail API  ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Should Be Equal As Strings  ${resp_get_detail.status_code}  404
    Verify Get Supplier Detail API Return Error Code As Expected    ${resp_get_detail}  4003
    Verify Get Supplier Detail API Return Error Message As Expected     ${resp_get_detail}  Supplier ${supplier_id[0]} not found
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Verify Supplier Active Flag Store In DB Correctly   ${supplier_id[0]}   False

    ## Clear Test Data ##
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify That Can View And Download Supplier Document Successfully
    [Tags]    supplier    api_get_supplier_document
    ## Prepare Test Data ##
    ${sap_code}      Generate Random Number
    ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     3
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountName
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountNumber
    ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.bankId
    ${new_obj}       Convert JSON To String    ${reqs_obj}
    Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    ${access_token}  ${refresh_token}=     POST Astra Login API
    ${doc_list}      Create List   ${path_folder}/${pdf_file}     ${path_folder}/${png_file}     ${path_folder}/${jpg_file}
    ${resp_create}   POST Create Update Supplier With Multiple Documents API   ${access_token}   ${refresh_token}    ${path_folder}/${sap_code}.json  ${doc_list}
    ${supplier_id}   Get Value From Json    ${resp_create.json()}  $.data.supplierId
    ${resp_detail}   GET Supplier Detail API  ${access_token}  ${refresh_token}   ${supplier_id[0]}

    ## Test Service & Verify Test Result ##
    ${count}         Get Length   ${doc_list}
    :FOR    ${index}    IN RANGE    0   ${count}
    \   ${document_id}   Get Value From Json    ${resp_detail.json()}  $.data.supplierDocument[${index}].documentId
    \   ${original_file_name}   Get Value From Json    ${resp_detail.json()}  $.data.supplierDocument[${index}].originalName
    \   ${resp_view}     GET Supplier Document API     ${access_token}  ${refresh_token}   ${document_id[0]}  view
    \   Should Be Equal As Strings  ${resp_view.status_code}  200
    \   Verify API Return Header Content-Type As Expected  ${resp_view}   application/octet-stream
    \   Verify API Return Header Content-Disposition As Expected   ${resp_view}    form-data; name="inline"; filename="${original_file_name[0]}"
    \   Should Not Be Empty    ${resp_view.content}
    \   ${resp_download}   GET Supplier Document API    ${access_token}  ${refresh_token}   ${document_id[0]}  download
    \   Should Be Equal As Strings  ${resp_download.status_code}  200
    \   Verify API Return Header Content-Type As Expected  ${resp_download}   application/octet-stream
    \   Verify API Return Header Content-Disposition As Expected   ${resp_download}    form-data; name="attachment"; filename="${original_file_name[0]}"
    \   Should Not Be Empty    ${resp_download.content}

    ## Clear Test Data ##
    DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_id[0]}
    Connect ACT Database    ${DB_NAME_SUPPLIER}
    Delete Supplier MultiCurrency By Supplier Id   ${supplier_id[0]}
    Delete Supplier Payment By Supplier Id  ${supplier_id[0]}
    Delete Supplier Contact By Supplier Id  ${supplier_id[0]}
    Delete Supplier Document By Supplier Id   ${supplier_id[0]}
    Delete Supplier By Supplier Id  ${supplier_id[0]}
    Disconnect From Database
    Remove File     ${path_folder}/${sap_code}.json

To Verify Able To Get Supplier List By Send List Of Supplier Id Successfully
    [Tags]      supplier    api_get_supplier_list_by_ids

    ## Prepare Test Data ##
    ${round}            Set Variable    5
    ${supplier_name}    Set Variable    Robot Test Suppliers
    &{supplier_dict}    Create Dictionary
    Log To Console  Prepare Test Data
    :FOR    ${index}    IN RANGE    0     ${round}
    \   ${sap_code}      Generate Random Number
    \   ${json}          Load JSON From File    ${path_folder}/${create_supplier_json}
    \   ${reqs_obj}      Update Value To Json   ${json}  $.supplierMultiCurrency[0].sapCode   ${sap_code}
    \   ${reqs_obj}      Update Value To Json   ${json}  $.supplierInformation.supplierName   ${supplier_name}${sap_code}
    \   ${reqs_obj}      Update Value To Json   ${json}  $.supplierPaymentMethod.paymentTypeId     3
    \   ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountName
    \   ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.accountNumber
    \   ${reqs_obj}      Delete Object From Json   ${json}      $.supplierPaymentMethod.bankId
    \   ${new_obj}       Convert JSON To String    ${reqs_obj}
    \   Create File      ${path_folder}/${sap_code}.json   ${new_obj}   UTF-8
    \   ${resp_create}   POST Create Update Supplier API    ${access_token}  ${refresh_token}   ${path_folder}/${sap_code}.json
    \   ${supplier_id}   Get Value From Json    ${resp_create.json()}  $.data.supplierId
    \   Log To Console   Supplier_id=${supplier_id[0]}
    \   Set To Dictionary    ${supplier_dict}   ${supplier_id[0]}    ${supplier_name}${sap_code}
    \   Remove File     ${path_folder}/${sap_code}.json
    ${supplier_ids}      Get Dictionary Keys     ${supplier_dict}
    ${supplier_names}    Get Dictionary Values   ${supplier_dict}

    ## Test Service and verify test result ##
    ${count}    Get Length   ${supplier_ids}
    Log To Console  Verify Test Result
    :FOR    ${index}   IN RANGE   0   ${count}
    \  ${expected_supplier_name}    Get From Dictionary    ${supplier_dict}  ${supplier_ids[${index}]}
    \  ${resp}=      POST Supplier Ids API     ${access_token}   ${refresh_token}   ${supplier_ids}
    \  Response Status Should Be 200 OK   ${resp}
    \  Verify POST Supplier Ids API Return Supplier Name By Supplier Id As Expected    ${resp}      ${supplier_ids[${index}]}      ${expected_supplier_name}

    ## Sorting Data ##
    ${supplier_names_sort_asc}     common.sorting_ascii_asc   ${supplier_names}
    Verify Supplier Ids API Return Supplier Name List As Expected    ${resp}        ${supplier_names_sort_asc}

    ## Test Clear Data ##
    Log To Console  Clearing Test Data
    :FOR    ${index}   IN RANGE   0   ${count}
    \  DELETE Supplier API     ${access_token}  ${refresh_token}   ${supplier_ids[${index}]}
    \  Connect ACT Database    ${DB_NAME_SUPPLIER}
    \  Delete Supplier Contact By Supplier Id  ${supplier_ids[${index}]}
    \  Delete Supplier MultiCurrency By Supplier Id   ${supplier_ids[${index}]}
    \  Delete Supplier Payment By Supplier Id  ${supplier_ids[${index}]}
    \  Delete Supplier By Supplier Id  ${supplier_ids[${index}]}
    \  Disconnect From Database