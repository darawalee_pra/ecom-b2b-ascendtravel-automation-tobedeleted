import csv


class csvLibraryNoOfRows(object):
    def csv_length(self, filename):

        '''This creates a keyword named "CSV Length"

        This keyword takes one argument, which is a path to a .csv file. It
        returns a list of rows, with each row being a list of the data in
        each column.
        '''
        length = 0
        with open(filename, 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                length += 1
        return length
