import json
import openpyxl

from openpyxl import load_workbook

def open_excel(path):
    workbook = load_workbook(filename = path)
    return workbook

def get_sheet(workbook, sheet_name):
	return    workbook[sheet_name]

def find_row_index(sheet, target_id):
	for i in range (2,sheet.max_row+1):
		if str(target_id) == str(sheet["A" + str(i)].value):
			return i



def get_value_by_row(sheet, target_id, start_col, end_col):
	
	start_cell = start_col + str(target_id)
	end_cell= end_col + str(target_id)
	val_list=['tmp']

	for cellObj in sheet[start_cell:end_cell]:
		for cell in cellObj:
			val_list.append(cell.value)

	val_list.remove("tmp")

	return val_list

def get_value_by_column(sheet, cell):
	return sheet[cell].value

def get_value_by_key(sheet, key, lookup_column, target_column):

	for i in range (2, sheet.max_row + 1):
		if str(key) == str(sheet[lookup_column + str(i)].value):
			return str(sheet[target_column + str(i)].value) 

def get_excel_max_row(sheet):
	return sheet.max_row

def convert_list_to_dict(key, value):
	return dict(zip(key,value))

def get_header_excel(sheet,start_col,last_col):
	return get_value_by_row(sheet,1,start_col,last_col)

def get_Obj_from_excel(sheet, target_id, start_col, last_col):
	key= get_header_excel(sheet, start_col, last_col)
	row_index= find_row_index(sheet,target_id)
	value= get_value_by_row(sheet, row_index, start_col, last_col)
	obj = convert_list_to_dict(key,value)

	return obj

def get_ArrObj_from_excel(sheet, target_id, start_col, last_col):
	
	key= get_header_excel(sheet, start_col, last_col)
	arr=["tmp"]

	for i in range (2,sheet.max_row+1):
		if int(target_id) == int(sheet["A" + str(i)].value):
			value= get_value_by_row(sheet, i, start_col, last_col)
			obj = convert_list_to_dict(key,value)
			arr.append(obj)
	arr.remove("tmp")

	return arr

def serialize_JSON(obj):
	return json.dumps(obj,indent=4)

