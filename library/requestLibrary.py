import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder

def post_multipart(url, files, data, headers):
    '''
    Example of object that receive from param

    - type file
    files = {'document': ('test.png', open('test.png', 'rb'), 'application/png', {'Expires': '0'})}

    - other form data
    data = {'type':'supplier', 'typeId':813}

    - header
    headers = {
        'x-act-refreshtoken':'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJkbm0iOiJrb3JuIiwidXNyIjoiMzkzNzkiLCJ0eXAiOiJzdGFmZiIsImV4cCI6MTUzMzAyOTE5MH0.eEzNzLSY4tImHJJvSwgyXXG8tGLEtWl7qY6moGQbf6HgOwDb_XlVwMi4fQgOFpi4jZzmnPzJq01ZjkrHzpPoVmC2Sx_PeKIrGunRfARCHRC3drVc-JxPQ3Dy7zBeIEsqH2gDRV8ij-l5CC7TDy3jWZA522DXqnB6HQmRlzklvsg',
        'x-act-accesstoken':'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJkbm0iOiJrb3JuIiwidXNyIjoiMzkzNzkiLCJ0eXAiOiJzdGFmZiIsImV4cCI6MTUzMzAyOTE5MH0.eEzNzLSY4tImHJJvSwgyXXG8tGLEtWl7qY6moGQbf6HgOwDb_XlVwMi4fQgOFpi4jZzmnPzJq01ZjkrHzpPoVmC2Sx_PeKIrGunRfARCHRC3drVc-JxPQ3Dy7zBeIEsqH2gDRV8ij-l5CC7TDy3jWZA522DXqnB6HQmRlzklvsg'
        }
    '''
    return requests.post(url=url, data=data, files=files, headers=headers)

def post_multipart_formdata(url, access_token, refresh_token, fields):
    '''
    Required - pip install requests_toolbelt 

    Example of object that receive from param
    fields={
             'hotelId': '21', 
             'supplierId': '252',
             'roomTypeNameTh': 'test ja',
             'roomTypeNameEn': 'test ja',
             'numberOfRoom': '100',
             'maxAdult': '2',
             'allowChild': 'true',
             'maxChild': '10',
             'babyCot': '0',
             'roomSize': '30.31',
             'roomViewId': '1',
             'bathroomAmount': '10',
             'status': 'true',
             'standardRoom': 'true',
             'roomOption[0].beddingOption[0].bedding[0].numberOfBeds': '100',
             'roomOption[0].beddingOption[0].bedding[0].bedType': 'Single Bed'
             'file': ('file.py', open('file.zip', 'rb'), 'text/plain')
            }
    '''
    multipart_data = MultipartEncoder(fields=fields)
  
    # print  multipart_data.to_string()
    return requests.post(url=url, data=multipart_data, headers={'Content-Type': multipart_data.content_type, 'x-act-accesstoken': access_token, 'x-act-refreshtoken': refresh_token})