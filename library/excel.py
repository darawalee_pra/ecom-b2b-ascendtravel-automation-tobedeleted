import xlrd
import openpyxl
import mimetypes
import os
from robot.libraries.BuiltIn import BuiltIn


import xlsxwriter
from helper import *

from openpyxl import load_workbook

def open_excel(path):
    workbook = load_workbook(filename = path)
    return workbook

def get_value_excel_by_column(path, sheet_name, cell_name):
    workbook = open_excel(path)
    sheet_ranges = workbook[sheet_name]
    return sheet_ranges[cell_name].value

# def find_number_row_excel(path, sheet_name , coulumn, value):
#     workbook = open_excel(path)
#     sheet_ranges = workbook[sheet_name]

#     for i in range(1, 100000):
#         coulumn_number = coulumn + str(i)
#         # print type(sheet_ranges[coulumn_number].value)
#         if sheet_ranges[coulumn_number].value is not None :
#             # print "bbbbbbbbbbbbbbbbb" + str(sheet_ranges[coulumn_number].value)
#             if str(sheet_ranges[coulumn_number].value) == str(value) :
#                 # print "aaaaaaaaaaaaaaa" + str(sheet_ranges[coulumn_number].value)
#                 return str(i)
#             # else: print "======================================" + str(value)
#         else:
#             break

def find_number_row_excel(path, sheet_name , coulumn, value):
    workbook = open_excel(path)
    sheet_ranges = workbook[sheet_name]

    for i in range(1, 100000):
        coulumn_number = coulumn + str(i)
        if type(sheet_ranges[coulumn_number].value) == float :
            if sheet_ranges[coulumn_number].value is not None :
                if int(sheet_ranges[coulumn_number].value) == int(value) :
                    return int(i)
        else :
            if sheet_ranges[coulumn_number].value is not None :
                if str(sheet_ranges[coulumn_number].value) == str(value) :
                    return str(i)

def find_multiple_number_row_excel(path, sheet_name , coulumn, value):
    workbook = open_excel(path)
    sheet_ranges = workbook[sheet_name]

    result = []

    for i in range(1, 100000):
        coulumn_number = coulumn + str(i)
        
        if sheet_ranges[coulumn_number].value is not None :
            if str(sheet_ranges[coulumn_number].value) == str(value) :
                result.append(str(i))
        else:
            break

    return result

def get_sheet_names(path):
        """Returns the names of the sheets of the workbook provided.
        Example:
        | ${workbook} | Open Excel File | Book1.xls |
        | ${sheet_names} | Get Sheet Names | ${workbook} |
        Given Book1.xls has three sheets with names Sheet1, Sheet2 and Sheet3:
        
        | Log | ${sheet_names} | # ['Sheet1', 'Sheet2', 'Sheet3'] |
        | Log | ${sheet_names[0]} | # Sheet1 |
        | Log | ${sheet_names[1]} | # Sheet2 |
        | Log | ${sheet_names[2]} | # Sheet3 |
        """
        workbook = open_excel(path)
        sheet_names = workbook.get_sheet_names()
        return sheet_names

def get_sheet_by_name(workbook, sheet_name):
    return workbook[sheet_name]

def get_value_from_cell(sheet, cell):
    return sheet[cell].value

def count_max_row_excel(sheet):
	return sheet.max_row

def gen_dict_from_excel_by_columns(path, file_name, sheet_name, col_key, col_val, col_type):
    dictionary = {}
    wb = open_excel(path+file_name)
    sheet = get_sheet_by_name(wb, sheet_name)
    row_index = count_max_row_excel(sheet)
    row_index = row_index + 1

    for i in range(2, row_index):
        key = get_value_from_cell(sheet, col_key + str(i))
        value = get_value_from_cell(sheet, col_val + str(i))
        val_type  = get_value_from_cell(sheet, col_type + str(i))

        if ("file" == val_type):
            value = (value, open(path+value,'rb'),(mimetypes.MimeTypes().guess_type(value)[0]))
    
        dictionary.update({key:value})
    
    print  dictionary
    return dictionary