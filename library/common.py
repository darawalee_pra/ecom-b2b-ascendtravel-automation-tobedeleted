import os.path
import datetime
import math
import numpy as np
from robot.libraries.BuiltIn import BuiltIn
from array import *

def get_canonical_path(path):
    return os.path.abspath(path)

def write_name_to_console(s):
   BuiltIn().log_to_console('\033[92m'+s)

def AddMonths(d,x):
    newmonth = ((( d.month - 1) + x ) % 12 ) + 1
    newyear  = d.year + ((( d.month - 1) + x ) / 12 ) 
    return datetime.date( newyear, newmonth, d.day)

def get_previous_six_month():
	return AddMonths(datetime.date.today(), -6)

def change_format_date(param):
	return datetime.date.strftime(param, "%d/%m/%Y")

def get_next_six_month():
	return AddMonths(datetime.date.today(), 6)

def get_ceil_number(number):
	return math.ceil( number )

def alphacmp(x,y):
    return cmp(x.lower(),y.lower())

def sorting_ascii_asc(list):
    list.sort()
    return  list

def sorting_ascii_desc(list):
    list.sort(reverse=True)
    return  list

def convert_array_to_list(array_num):
    print("Original array: "+str(array_num))
    num_list = np.array(array_num).tolist()
    print("Convert the said array to an ordinary list with the same items:")
    print(num_list)
    return  num_list

def choose_multiple_files(locator, basePath=""):
    fileList = []
    for file in os.listdir(basePath):
        path_to_image = os.path.join(basePath, file)
        print("Path to img" + str(path_to_image))
        if not os.path.isfile(path_to_image):
            raise AssertionError("File '%s' does not exist on the local file system" % path_to_image)
        fileList.append(path_to_image)
    selenium2Lib = BuiltIn().get_library_instance('Selenium2Library')
    selenium2Lib.find_element(locator).send_keys("\n".join(fileList))
