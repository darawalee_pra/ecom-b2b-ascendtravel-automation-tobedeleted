import json
import math, time, datetime
import hashlib
import requests
from dateutil import parser
import os
def getHotel(checkInDays, checkOutDays, payType):
    checkInDays=int(checkInDays)
    checkOutDays=int(checkOutDays)
    
    ## Reading environment parameter from file
    json_data = open('../../python_library/getHotel/htb_test_environment.json').read()
    env_data = json.loads(json_data)
    serverUrl = env_data['values'][0]['value']
    # apikey1 = env_data['values'][1]['value']
    sharedsecret = env_data['values'][2]['value']
    apikey2 = env_data['values'][3]['value']
    checkInDate = datetime.datetime.now() + datetime.timedelta(days=checkInDays)
    checkInDate1 = checkInDate.strftime("%Y-%m-%d")
    checkOutDate = datetime.datetime.now() + datetime.timedelta(days=checkOutDays)
    checkOutDate1 = checkOutDate.strftime("%Y-%m-%d")

    print "Today is %s" % datetime.datetime.now().strftime("%Y-%m-%d")
    print "Check-in Date = %s" % checkInDate1
    print "Check-out Date = %s" % checkOutDate1
    print "Payment Type = %s" % payType

    ## Building signature
    utcdate = math.floor(time.time())
    assemble = apikey2 + sharedsecret + str(utcdate).rstrip("0").rstrip(".")
    hash_object = hashlib.sha256(assemble).hexdigest()
    xsignature = hash_object

    ## Reading data and building payload
    json_data = open('../../python_library/getHotel/data.json').read()
    payload_data = json.loads(json_data)
    payload_data["stay"]["checkIn"] = checkInDate1
    payload_data["stay"]["checkOut"] = checkOutDate1
    payload = json.dumps(payload_data)

    url = "https://" + serverUrl + "/hotel-api/1.0/hotels"
    headers = {
        'Accept': "application/json",
        'Content-Type': "application/json",
        'Api-Key': "%s" % apikey2,
        'X-Signature': "%s" % xsignature
    }

    ## Send request to HotelBed
    response = requests.request("POST", url, data=payload, headers=headers)
    htb_response = json.loads(response.text)

    # Write raw response from HTB to file
    file_name = '../../python_library/getHotel/responses/response-' + datetime.datetime.now().strftime("%y%m%d%H%M%S") + ".json"
    f = open(file_name, 'w')
    json.dump(htb_response, f, sort_keys=True, indent=4)
    f.close()
    print "\nResponse file = " + file_name + "\n"

    ## Extract data from response and produce output
    todayOrdinal = datetime.datetime.now().toordinal()

    if payType == "paynow":
        result_data = []
        numberOfHotels = len(htb_response["hotels"]["hotels"])
        for i in range(0, numberOfHotels):
            numberOfRooms = len(htb_response["hotels"]["hotels"][i]["rooms"])
            # print htb_response["hotels"]["hotels"][i]["name"]
            for j in range(0, numberOfRooms):
                numberOfRates = len(htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"])
                for k in range(0, numberOfRates):
                    if htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["rateType"] == "BOOKABLE":
                        numberOfCancelPolicy = len(
                            htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["cancellationPolicies"])
                        for m in range(0, numberOfCancelPolicy):
                            cancelDate = \
                                htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["cancellationPolicies"][m][
                                    "from"]

                        cancelDate = parser.parse(cancelDate)
                        cancelDateOrdinal = cancelDate.toordinal()
                        # print cancelDateOrdinal
                        todayPlusTwo = datetime.datetime.now() + datetime.timedelta(days=2)
                        # todayPlusTwoOrdinal = todayPlusTwo.toordinal()
                        # print todayPlusTwoOrdinal

                        if cancelDateOrdinal <= todayOrdinal:
                            concatRoomName = "%s (%s)" % ((htb_response["hotels"]["hotels"][i]["rooms"][j]["name"]).title(), (htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["boardName"]).title())
                            data = {
                                "hotelname": htb_response["hotels"]["hotels"][i]["name"],
                                "roomname": htb_response["hotels"]["hotels"][i]["rooms"][j]["name"],
                                "boardname": htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["boardName"],
                                "ratekey": htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["rateKey"],
                                "newroomname": "%s" % concatRoomName,
                                "canceldate": "%s" % cancelDate
                            }
                            result_data.append(data)
        return json.dumps(result_data)
        
    elif payType == "paylater":
        result_data = []
        numberOfHotels = len(htb_response["hotels"]["hotels"])
        for i in range(0, numberOfHotels):
            numberOfRooms = len(htb_response["hotels"]["hotels"][i]["rooms"])
            # print htb_response["hotels"]["hotels"][i]["name"]
            for j in range(0, numberOfRooms):
                numberOfRates = len(htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"])
                for k in range(0, numberOfRates):
                    if htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["rateType"] == "BOOKABLE":
                        numberOfCancelPolicy = len(
                            htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["cancellationPolicies"])
                        for m in range(0, numberOfCancelPolicy):
                            cancelDate = \
                            htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["cancellationPolicies"][m]["from"]

                            cancelDate = parser.parse(cancelDate)
                            cancelDateOrdinal = cancelDate.toordinal()
                            # print cancelDateOrdinal
                            todayPlusTwo = datetime.datetime.now() + datetime.timedelta(days=2)
                            todayPlusTwoOrdinal = todayPlusTwo.toordinal()
                            # print todayPlusTwoOrdinal

                            if cancelDateOrdinal >= todayPlusTwoOrdinal:
                                concatRoomName = "%s (%s)" % ((htb_response["hotels"]["hotels"][i]["rooms"][j]["name"]).title(),(htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["boardName"]).title())
                                data = {
                                    "hotelname": htb_response["hotels"]["hotels"][i]["name"],
                                    "roomname": htb_response["hotels"]["hotels"][i]["rooms"][j]["name"],
                                    "boardname": htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["boardName"],
                                    "ratekey": htb_response["hotels"]["hotels"][i]["rooms"][j]["rates"][k]["rateKey"],
                                    "newroomname": "%s" % concatRoomName,
                                    "canceldate": "%s" % cancelDate
                                }
                                result_data.append(data)
        return json.dumps(result_data)


# getHotel(10,13,"paynow")
# getHotel(10,13,"paylater")