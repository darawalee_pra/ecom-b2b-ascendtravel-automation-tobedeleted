*** Variables ***
#alpha
### Site Information ###
${chrome_browser}	Chrome
${desktop_device}   Desktop 
${tablet_device}    Tablet 
${mobile_device}    Mobile 
${en_lang}  EN 
${th_lang}  TH

#************************** Common Variables ******************************
${BROWSER}             		  	Chrome
${Delay}               		  	0.1
${TIMEOUT}    					15
${SEARCH_TIMEOUT}               60
${BOOKING_TIMEOUT}              60
${FRONTEND_BASE_URL}            https://alpha-www.ascendtravel-dev.com

${BACKEND_BASE_URL}    	        https://poc-backend.ascendtravel-dev.com
${USERNAME_BACKEND}    			ascend
${PASSWORD_BACKEND}    			Xv7pGQH4gFJXHRys

${Ascendtravel-API}     	  	poc-www.ascendtravel-dev.com
${COMPANY_SERVICE_API}    	    poc-service.ascendtravel-dev.com/company
${Ascendtravel-SERVICE-API}     poc-service.ascendtravel-dev.com

#DB
${DB_HOSTNAME}         			act-th-test-db.c3pr0f3gzjzp.ap-northeast-1.rds.amazonaws.com
${DB_USERNAME}         			sa
${DB_PASSWORD}         			Passw0rd*
${DB_NAME}             			alpha_ascend_travel
${DB_NAME_V2}             	    alpha_act
${DB_NAME_SUPPLIER}             alpha_supplier
${DB_NAME_COMPANY}              alpha_company
${DB_NAME_AAD_AUTHN}   			alpha_aad_authn
${DB_NAME_AAD_AUTHZ}   			alpha_aad_authz
${DB_CHARSET}          			utf8
${DB_PORT}             			1433

${BIG_COMPANY_USERNAME}         foammm@grr.la
${BIG_COMPANY_PASSWORD}         P@ssw0rd
${BIG_COMPANY_USERNAME_2}       foamlee@grr.la
${BIG_COMPANY_PASSWORD_2}       kkkkkkkk
${BIG_COMPANY_USERNAME_3}       apinya.phu@ascendcorp.com
${BIG_COMPANY_PASSWORD_3}       Ascender@052018
${SMALL_COMPANY_USERNAME}       actqa-alpha@guerrillamail.org
${SMALL_COMPANY_PASSWORD}       P@ssw0rd


${CREDIT_CARD_USER2}            woramon.tha@ascendcorp.com
${CREDIT_CARD_PASS2}            12345678Goyo  
${SCG_COMPANY_USERNAME}         ascendtr@scg.com
${SCG_COMPANY_PASSWORD}         Passw0rd*1234


${AUTHZ_API_URL}       			poc-service.ascendtravel-dev.com
${USERNAME_FRONTEND_CHANGE_PASSWORD}    pasathorn.soo@ascendcorp.com
${PASSWORD_FRONTEND_CHANGE_PASSWORD}    gchd&6hy
${TRAVELLER_EMAIL}                      pasathorn.soo@gmail.com
${TRAVELLER_EMAIL_PASSWORD}             gchd&6hy
${ORDER_LIST_PAGE_URL}        ${BACKEND_BASE_URL}/admin/order/


${ASTRA_BASE_URL}           https://alpha-astra.ascendtravel-dev.com/
${ASTRA_BASE_URL_V2}        https://alpha-v2-astra.ascendtravel-dev.com/
${USERNAME_ASTRA}          astratest@ascendcorp.com
${PASSWORD_ASTRA}          12345678

# Mass Upload Payin
${Booking_ID_1}                         802642
${Booking_ID_2}                         802644
${Booking_ID_Not_System}                999999
${Booking_ID_Not_Credit_Terms}          802629

# IMAGE
${IMAGE_PATH}           https://alpha-images.ascendtravel-dev.com
${SEARCHSYNC_API}       poc-service.ascendtravel-dev.com/elasticsearch/searchsync/searchsync-api

####### new config support mobile responsive design
# ${res_FRONTEND_BASE_URL}      https://alpha-mobile.ascendtravel-dev.com/landing

${ACT_Bypasstoken_Key}      x-act-bypasstoken
${ACT_Bypasstoken_Value}    thorthor

# BANNER
${URL_Banner}            https://alpha-www.ascendtravel-dev.com/hotel/detail/zdvhml?destination=eyJpZCI6OTk3MDkyLCJzbHVnIjoiemR2aG1sIiwic3VnZ2VzdENhdCI6ImhvdGVsIiwidGl0bGUiOiJ6ZHZobWwifQ%3D%3D&purpose=work&checkIn=23-05-2018&checkOut=24-05-2018&guests=W3siYWR1bHQiOjIsImNoaWxkcmVuIjpbXX1d&lang=en

# API
${SERVICE_API}                  https://poc-service.ascendtravel-dev.com
${LEGACY_SERVICE_API}           https://poc-www.ascendtravel-dev.com  
${ASTRA_LOGIN_API}              /account/api/1.0/staff/login          
${SEARCH_SYNC}                  /searchsync-api
${SYNC_RATING_API}              /api/sync/rating
${REVIEW_API}                   /review/api/ratings
${CHECKIN_API}                  /api/CheckInDateStamp
${PAYLATER_API}                 /control/api/v1/cron/payLater