*** Variables ***
#production
### Site Information ###
${url_ascend_travel}    https://www.ascendtravel.com/ 
${chrome_browser}	Chrome
${desktop_device}   Desktop 
${tablet_device}    Tablet 
${mobile_device}    Mobile 
${en_lang}  EN 
${th_lang}  TH

#************************** Common Variables ******************************
${BROWSER}             		  	Chrome
${Delay}               		  	0.1
${TIMEOUT}    				    30
${SEARCH_TIMEOUT}               60
${BOOKING_TIMEOUT}              60
${FRONTEND_BASE_URL}            https://www.ascendtravel.com
${FRONTEND_TRUE_URL}            ${FRONTEND_BASE_URL}/company/true

${FRONTEND_SCG_URL}             ${FRONTEND_BASE_URL}/company/scg

${BACKEND_BASE_URL}    	        https://backend.ascendtravel.com
${USERNAME_BACKEND}    act_qa
${PASSWORD_BACKEND}    Act@dmin

${ORDER_LIST_PAGE_URL}        ${BACKEND_BASE_URL}/admin/order/

${ASTRA_BASE_URL}   https://astra.ascendtravel.com/
${USERNAME_ASTRA}   astratest@ascendcorp.com
${PASSWORD_ASTRA}   12345678

${Ascendtravel-API}     	    www.ascendtravel-dev.com


# ${USERNAME_BACKEND}    g.nongluck
# ${PASSWORD_BACKEND}    GeingAcc4



# BANNER
 ${URL_Banner}           https://www.ascendtravel.com/hotel/detail/zdvhml?destination=eyJpZCI6MjE0MzMsInNsdWciOiJ6ZHZobWwiLCJzdWdnZXN0Q2F0IjoiaG90ZWwiLCJ0aXRsZSI6InpkdmhtbCJ9&purpose=leisure&checkIn=23-05-2018&checkOut=24-05-2018&guests=W3siYWR1bHQiOjIsImNoaWxkcmVuIjpbXX1d&lang=en

# ${COMPANY_SERVICE_API}    	  service.ascendtravel.com/company
# ${Ascendtravel-SERVICE-API}   service.ascendtravel.com
# ${DB_HOSTNAME}         			  act-th-test-db.c3pr0f3gzjzp.ap-northeast-1.rds.amazonaws.com
# ${DB_USERNAME}         			  sa
# ${DB_PASSWORD}         			  Passw0rd*
# ${DB_NAME}             			  alpha_ascend_travel
# ${DB_NAME_AAD_AUTHN}   			  alpha_aad_authn
# ${DB_NAME_AAD_AUTHZ}   			  alpha_aad_authz
# ${DB_CHARSET}          			  utf8
# ${DB_PORT}             			  1433
# ${BIG_COMPANY_USERNAME}       foammm@grr.la
# ${BIG_COMPANY_PASSWORD}       P@ssw0rd
# ${SMALL_COMPANY_USERNAME}     actqa-alpha@guerrillamail.org
# ${SMALL_COMPANY_PASSWORD}    	P@ssw0rd
# ${SCG_COMPANY_USERNAME}       ascendtr
# ${SCG_COMPANY_PASSWORD}       Passw0rd**
# ${Robot_Username}      			  sivavut.von
# ${Robot_Password}      			  12345678
# ${USERNAME_FRONTEND}   			  pisuna.put@ascendcorp.com
# ${PASSWORD_FRONTEND}   			  12345678

# ${AUTHZ_API_URL}       			  poc-service.ascendtravel-dev.com
# ${USERNAME_FRONTEND_CHANGE_PASSWORD}    pasathorn.soo@ascendcorp.com
# ${PASSWORD_FRONTEND_CHANGE_PASSWORD}    gchd&6hy
# ${TRAVELLER_EMAIL}                      pasathorn.soo@gmail.com
# ${TRAVELLER_EMAIL_PASSWORD}             gchd&6hy


# # Mass Upload Payin
# ${Booking_ID_1}                         802642
# ${Booking_ID_2}                         802644
# ${Booking_ID_Not_System}                999999
# ${Booking_ID_Not_Credit_Terms}          802629

# # IMAGE
# ${IMAGE_PATH}           https://alpha-images.ascendtravel-dev.com
# ${SEARCHSYNC_API}       poc-service.ascendtravel-dev.com/elasticsearch/searchsync/searchsync-api

# ####### new config support mobile responsive design
# # ${res_FRONTEND_BASE_URL}      https://alpha-mobile.ascendtravel-dev.com/landing

