*** Variables ***
${Timeout}                          50
${Company_List_Page_URL}            ${BACKEND_BASE_URL}/admin/company/
${Edit_Company_page_URL}            ${BACKEND_BASE_URL}/admin/company/#/update/
${Create_Company_page_URL}          ${BACKEND_BASE_URL}/admin/company/#/create
${ERROR_MSG_DUPLICATE_BRANCH}                               branch number is duplicate
${ERROR_MSG_INVALID_BRANCH}                                 branch number must be 1-99999
${ERROR_MSG_DUPLICATE_COMPANY}                              company name is duplicate
${Popup_Error_on_Edit_Commpany_page}                        //DIV[@class='bootbox-body']
${Close_button_on_Popup_Error}                              //BUTTON[@data-bb-handler='main'][text()='Close']
${Upload_Document_tab_Locator}                              id=tab_document_uploads
${Upload_Members_tab_Locator}                               id=tab_upload_members
${Button_Create_New_Company_onCompanyListPage_Locator}      id=button_new_company
${Select_Parent_Company_Locator}                            id=selectedCompany
${Select_Is_Small_Company_Locator}                          id=isSmall
${Select_Company_Type_Locator}                              id=selectedCompanyType
${Input_Company_Domain_Locator}                             id=domain
${Select_Company_Country_Locator}                           id=selectedCountry
${Select_Company_Location_Locator}                          id=selectedLocation
${Select_Company_Prefix_local_Locator}                      id=selectedPrefixLocal
${Input_Company_name_local_Locator}                         id=company_name
${Select_Company_Prefix_translate_Locator}                  id=selectedPrefixTranslation
${Input_Company_name_translate_Locator}                     id=company_name_translation
####### Company Branch :: 0=notidentify 1=headOffice 2=branch
${Select_Company_branch_0}                                  id=company_branch_type_0
${Select_Company_branch_1}                                  id=company_branch_type_1
${Select_Company_branch_2}                                  id=company_branch_type_2
${Input_Branch_Number_locator}                              id=branch_number
${Select_Company_branch_var}                                id=company_branch_type_replace_me
${Select_Company_branch_Locator}                            formModel.company_branch_type
${Input_Address_Local_Locator}                              id=address
${Input_Address_Translation_Locator}                        id=address_translation
${Input_Phone_Locator}                                      id=phone
${Input_Fax_No_Locator}                                     id=fax
${Link_Add_other_company_contact_Locator}                   id=add_other_company_contact
# #########${company_contact_Locator}                         id=company-contact-group
${Input_contact_name_Locator}                               id=contact_name_
${Input_contact_tel_Locator}                                id=contact_tel_
${Input_contact_email_Locator}                              id=contact_email_
${Delete_contact_Locator}                                   id=contact_remove_
${Button_create_company_Locator}                            id=button_create_company
${Button_delete_company_Locator}                            id=button_delete_company
${Button_edit_company_Locator}                              id=button_update_company
# ${Upload_file_Locator}                                      id=upload-multifile
${Upload_file_Locator}                                       //*[@class="dz-hidden-input"]
${Button_Delete_CompanyDoc_Locator}                         id=delete_document_
${Company_name_local_error_required}                        Company Name Local field is required.
${Company_name_trans_error_required}                        Company Name Translation field is required.
${Company_prefix_local_error_required}                      Company Prefix Local field is required.
${Company_prefix_trans_error_required}                      Company Prefix Translation field is required.
${Company_domain_error_required}                            Company Domain field is required.
${Branch_number_error_required}                             Company Branch field is required.
###################################### Upload Member
# ${Button_Browse_Import_Member_Locator}                      id=import_member/div[4]/input
${Button_Browse_Import_Member_Locator}                      //INPUT[@class='btn cta gray']
${Button_Upload_Member_Locator}                             id=button_upload_members
${Button_Close_Upload_Member_Locator}                       id=button_close_members
${Table_Upload_Member_Success_Locator}                      id=uploadMembersSuccess
${Table_Upload_Member_Fail_Locator}                         id=uploadMembersErrors
${Txt_Company_Domain_Locator}                               id=domainText
##################################### View
${View_Company_page_URL}                                    ${BACKEND_BASE_URL}/admin/company/Default.aspx#/view/
${BackEnd_CompanyPage_GenInfo_Btn_Locator}                  //*[@id="btn_Login"]
${View_Upload_Document_tab_Locator}                         id=tab_view_document_uploads
${Button_Edit_Company_on_View_Page_Locator}                 id=button_edit_company
###################################### Billing Info
${Payment_Method_tab_Locator}                               id=tab_payment_method
${Edit_Billing_Info_tab_Locator}                            id=tab_billing_upload
${View_Billing_Info_tab_Locator}                            id=tab_view_billing_upload
${Button_Browse_Import_Billing_Info_Locator}                id=file_billing
${Button_Upload_Billing_Info_Locator}                       //*[@class="btn btn-primary"]
${Billing_Info_Section_Locator}                             id=viewBillingInformation
${Company_SAPcode_Value_Locator}                            id=companySapCode
${taxCompanyNameLocal_Value_Locator}                        id=taxCompanyNameLocal
${taxCompanyNameTrans_Value_Locator}                        id=taxCompanyNameTrans
${taxNumber_Value_Locator}                                  id=taxNumber
${taxAddressLocal_Value_Locator}                            id=taxAddressLocal
${taxAddressTrans_Value_Locator}                            id=taxAddressTrans
${billingCompanyNameLocal_Value_Locator}                    id=billingCompanyNameLocal
${billingCompanyNameTrans_Value_Locator}                    id=billingCompanyNameTrans
${billingAddressLocal_Value_Locator}                        id=billingAddressLocal
${billingAddressTrans_Value_Locator}                        id=billingAddressTrans
${creditTerm_Value_Locator}                                 id=creditTerm
${billingInfoNote_Value_Locator}                            id=billingInfoNote

${payment_method_tab}             id=tab_payment_method
${payment_method_tab_view}             id=tab_view_payment_method
${payment_method_credit_term}     id=tabCreditTerm
${payment_method_credit_term_view}     id=tab_view_payment_method_credit_term
${creditTermAllow}                id=cerditTermAllow
${creditTermNotAllow}             id=cerditTermNotAllow
${limitPerson}                    id=limitPerson
${allowEveryone}                  id=allowEveryone
${approvalFlowRequire}            id=approvalFlowRequire
${approvalFlowNotRequire}         id=approvalFlowNotRequire
${approvalExceptionHavePerson}    id=approvalExceptionHavePerson
${approvalExceptionNoPerson}      id=approvalExceptionNoPerson
${Button_Browse_Identify_Limit_Person}                     id=limitPersonFile
${Button_Browse_Except_Approval_Flow}                      id=exceptionPerson
${Button_Save_Credit_Term}                      id=buttonUpdateCreditTerm
${creditTermAllow_view}           id=creditTermAllow
${limitPerson_view}               id=creditTermLimitPersonToSee
${creditTermRequireApproval_view}      id=creditTermRequireApproval
${creditTermApprovalException_view}    id=creditTermApprovalException
${creditTermAllow_groupname}           formModelPaymentMethodCreditTerm.creditTermAllow
${limitPerson_groupname}               formModelPaymentMethodCreditTerm.creditTermLimitPersonToSee
${approvalFlowRequire_groupname}       formModelPaymentMethodCreditTerm.creditTermRequireApproval
${approvalException_groupname}         formModelPaymentMethodCreditTerm.creditTermApprovalException
${errorPopup}         //DIV[@class='bootbox-body']
${buttonCancelEditCreditterm}          id=buttonCancelCreditTerm
${Link_Download_Latest_User}           id=