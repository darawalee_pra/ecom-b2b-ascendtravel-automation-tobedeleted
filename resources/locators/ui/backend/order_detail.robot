*** Variables ***
${lbl_booker_details_name}                              id=bookerDetailsName
${lbl_booker_details_email}                             id=bookerDetailsEmail
${lbl_booker_details_contact_no}                        id=bookerDetailsContactNo
${lbl_booker_details_nationality}                       id=bookerDetailsNationnality

${lbl_tax_invoice_address_name_company_name}            id=taxInvoiceAddressNameCompanyName
${lbl_tax_invoice_address_thaiid_tax_id}                id=taxInvoiceAddressThaiIDTaxID
${lbl_tax_invoice_address_address}                      id=taxInvoiceAddressAddress

${lbl_booking_requirement_check_in}                     id=bookingRequirementCheckIn
${lbl_booking_requirement_check_out}                    id=bookingRequirementCheckOut

${lbl_payment_details_payment_method}                   id=paymentDetailsPaymentMethod
${lbl_Payment_Details_Payment_Gateway_Locator}          id=paymentDetailsPaymentGateway
${lbl_Payment_Details_Payment_Card_Issuer_Locator}          id=paymentDetailsCardIssuer
${lbl_Payment_Details_Payment_Payment_Status_Locator}       id=paymentDetailsPaymentStatus
${lbl_Payment_Details_Payment_Payment_Type_Locator}         id=paymentDetailsPaymentType
${lbl_Payment_Details_Payment_Payment_Date_Locator}         id=paymentDetailsPaymentDate
${lbl_Payment_Details_Payment_Payment_ID_Locator}           id=paymentDetailsPaymentID

#${Device_Information_OS_Locator}                    id=deviceInformationOS
#${Device_Information_OS_Version_Locator}            id=deviceInformationOSVersion
#${Device_Information_Browser_Locator}               id=deviceInformationBrowser
#${Device_Information_Browser_Version_Locator}       id=deviceInformationBrwoserVersion
#${Booking_Requirement_Check_In_Locator}             id=bookingRequirementCheckIn
#${Booking_Requirement_Check_Out_Locator}            id=bookingRequirementCheckOut
#${Booking_Requirement_Guest_Locator}                id=bookingRequirementGuest
#${Payment_Details_Payment_Method_Locator}           id=paymentDetailsPaymentMethod
#${Payment_Details_Payment_Gateway_Locator}          id=paymentDetailsPaymentGateway
#${Payment_Details_Payment_Card_Issuer_Locator}      id=paymentDetailsCardIssuer
#${Payment_Details_Payment_Payment_Status_Locator}   id=paymentDetailsPaymentStatus
#${Payment_Details_Payment_Payment_Type_Locator}     id=paymentDetailsPaymentType
#${Payment_Details_Payment_Payment_Date_Locator}     id=paymentDetailsPaymentDate
#${Payment_Details_Payment_Payment_ID_Locator}       id=paymentDetailsPaymentID
#${Product_Name_Table_Locator}                       id=productNameTable
#${Product_Name_Price_Locator}                       id=productNamePrice
#${Product_Name_Vat_Locator}                         id=productNameVat
#${Product_Name_Total_Price_Locator}                 id=productNameTotalPrice
#${Supplier_Details_Company_Locator}                 id=supplierDetailsCompany
#${Supplier_Details_Supplier_Type_Locator}           id=supplierDetailsSupplierType
#${Supplier_Details_Email_Locator}                   id=supplierDetailsEmail
#${Supplier_Details_Currency_Locator}                id=supplierDetailsCurrency
#${Supplier_Details_Address_Locator}                 id=supplierDetailsAddress
#${Supplier_Details_Contact_Locator}                 id=supplierDetailsContact
#${Traveller_Name_Table_Locator}                     id=travellerNameTable
#${Check_Cancellation_Fee_Button_Locator}            id=checkCancellationFee
#${View_Booking_Documents_Tax_Invoice_Oridinal_Locator}          id=viewBookingDocumentsTaxInvoiceOriginal
#${View_Booking_Documents_Tax_Invoice_Copy_Locator}              id=viewBookingDocumentsTaxInvoiceCopy
#${View_Booking_Documents_Tax_Invoice_Cancel_Oridinal_Locator}   id=viewBookingDocumentsTaxInvoiceCancelOriginal
#${View_Booking_Documents_Tax_Invoice_Cancel_Copy_Locator}       id=viewBookingDocumentsTaxInvoiceCancelCopy
#${View_Booking_Documents_Credit_Note_Original_Locator}          id=viewBookingDocumentsCreditNoteOriginal
#${View_Booking_Documents_Credit_Note_Copy_Locator}              id=viewBookingDocumentsCreditNoteCopy
#${View_Booking_Documents_Voucher_Oridinal_Locator}              id=viewBookingDocumentsVoucherOriginal
${lbl_sending_status_supplier_confirm_status}               id=sendingStatusSupplierConfirmStatus
${lbl_sending_status_supplier_cancel_status}                id=sendingStatusSupplierCancelStatus
${lbl_sending_status_supplier_confirm_date_send}            id=sendingStatusSupplierConfirmDateSend
${lbl_sending_status_supplier_cancel_date_send}             id=sendingStatusSupplierCancelDateSend
${lbl_sending_status_booker_confirm_status}                 id=sendingStatusBookerConfirmStatus
${lbl_sending_status_booker_cancel_status}                  id=sendingStatusBookerCancelStatus
${lbl_sending_status_booker_confirm_date_send}              id=sendingStatusBookerConfirmDateSend
${lbl_sending_status_booker_cancel_date_send}               id=sendingStatusBookerCancelDateSend
${lbl_sending_status_traveller_confirm_status}              id=sendingStatusTravellerConfirmStatus
${lbl_sending_status_traveller_cancel_status}               id=sendingStatusTravellerCancelStatus
${lbl_sending_status_traveller_confirm_date_send}           id=sendingStatusTravellerConfirmDateSend
${lbl_sending_status_traveller_cancel_date_send}            id=sendingStatusTravellerCancelDateSend
#${Sending_Status_Confirm_Supplier_Locator}                      id=sendingStatusConfirmSupplier
#${Sending_Status_Confirm_Booker_Locator}                        id=sendingStatusConfirmBooker
#${Sending_Status_Confirm_Traveller_Locator}                     id=sendingStatusConfirmTraveller
#${Sending_Status_Cancel_Supplier_Locator}                       id=sendingStatusCancelSupplier
#${Sending_Status_Cancel_Booker_Locator}                         id=sendingStatusCancelBooker
#${Sending_Status_Cancel_Traveller_Locator}                      id=sendingStatusCancelTraveller
#${Resend_History_Table}                                         id=resendHistoryTable