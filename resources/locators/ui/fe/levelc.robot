*** Variables ***
${hotel_name_path_c} 	//span[@class='hotel-text-name' and contains(text(), '_$hotelName')]
${your_destination} 	//div[@class='search-destination']//span[@class='destination-text']
${checkin_checkout_date} 	//div[@class='search-date']//span
${number_of_room}   //div[@class='search-num-of-guest']//div[@class='text-inline'][1]
${number_of_guest}  //div[@class='search-num-of-guest']//div[@class='text-inline'][2]
${number_of_child}  //div[@class='search-num-of-guest']//div[@class='text-inline'][3]
${lnk_change_search}    id=search-change
${btn_toggle_search_roombox}                     id=search-btn-toggle-roombox
${ddl_room_amount}                               id=search-select-room
${ddl_adult_amount}                              id=search-select-adult
${ddl_child_amount}                              id=search-select-child
${ddl_child_age}                                 id=search-select-child-age
${btn_search_submit_levelc}                         //*[@class="ota-btn-main btn-submit-horizontal"]
${txt_levelc_budget_limit}     id=levelC-text-budget-limit
