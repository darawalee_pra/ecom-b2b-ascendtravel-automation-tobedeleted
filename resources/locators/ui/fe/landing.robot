*** Variables ***
${link_register_login}                  id=header-btn-show-login-desktop
${txt_email_register_login}    			id=landing-inp-email-verify-domain
${btn_continue_register_login}			id=landing-btn-email-verify-domain
${txt_password_login}    			    id=landing-inp-password-login
${btn_login}    					    id=landing-btn-login
${btn_getstarted}					    id=landing-btn-getstarted
# language menu - Desktop
${btn_change_language_desktop}    		id=header-btn-lang-dropdown
${btn_english_language_desktop}         id=header-btn-lang-en
${btn_thai_language_desktop}            id=header-btn-lang-th
# language menu - Mobile
${btn_change_language_mobile}    		id=header-lnk-language
${btn_english_language_mobile}          id=header-lnk-language-en
${btn_thai_language_mobile}             id=header-lnk-language-th

${btn_register}    					    id=landing-btn-register
${btn_joinus}    					    id=landing-btn-joinus
${cbo_select_company}    			    id=landing-inp-company-create-account
${txt_password_register}    			id=landing-inp-password-create-account
${txt_confirm_password_register}        id=landing-inp-confirm-password-create-account
${lnk_term_condition}     				id=landing-btn-show-terms-and-conditions
${btn_accept_tc}     				    id=landing-btn-terms-and-conditions

${btn_register_on_popup}				id=landing-btn-create-account
${btn_try_agian}     				    id=landing-btn-close-messages
${lbl_title_popup}     					//div[@class='dialog-wrapper-content']


${btn_hamberger_menu}    				//button[@class='navbar-toggle']
${btn_login_mobile}    					id=header-btn-show-login-mobile
${btn_ok_on_resend_email_popup}    	    id=landing-btn-resend
${txt_interest_name}    				id=landing-inp-name-register
${txt_interest_email}    				id=landing-inp-email-register
${txt_interest_phone}    				id=landing-inp-contact-number-register
${txt_interest_company}    				id=landing-inp-company-name-register
${txt_interest_position}    			id=landing-inp-position-register
${txt_interest_website}    				id=landing-inp-company-website-register
${txt_interest_budget}    				//input[@formcontrolname='travellingExpense']
${btn_submit_info}    					//button[@class='ota-btn-main']
${lnk_forget_password}    				id=landing-a-forgot-password
${btn_send_request_new_passeword}    	id=landing-btn-send-reset-password
${txt_set_new_password}    				id=landing-inp-password-reset-password
${txt_confirm_new_password}    			id=landing-inp-confirm-password-reset-password
${btn_submit_new_password}    			id=landing-btn-reset-password


${msg_login_fail_title}    				Login Failed
${msg_login_fail_content}    			Incorrect E-Mail or password
${msg_login_fail_content_2}    			Please check and try it again.

${msg_reg_success_not_verify_email_1}   Please check your mailbox
${msg_reg_success_not_verify_email_2}   We have sent E-mail to
${msg_reg_success_not_verify_email_3}   Check your mailbox to complete your registration.
${msg_reg_success_not_verify_email_4}   *If you cannot find, please check in your junk mail.
${msg_reg_success_not_verify_email_5}   If you didn't receive your verification email. Click Resend verify email


${msg_interest_1}       Interested to join with Ascend Travel?
${msg_interest_2}       We would like to please you but your company have not contact with us yet. If you are interested to travel with us, please leave your information. Our staff will contact you back soon.
${msg_join_us_1}        Join with Ascend Travel
${msg_join_us_2}        If you are interested to travel with us, please leave your information. Our staff will contact you back soon.

${msg_leave_info_success_1}    We already receive your information.
${msg_leave_info_success_2}    Our staff will contact you back soon.
${msg_leave_info_success_3}    Thank you for interested to travel with us.
${msg_request_new_password_1}    Please enter your corporate E-Mail
${msg_request_new_password_2}    To reset your password, enter the E-Mail you use to register with us.
${msg_sent_request_password_1}    Please check your mailbox
${msg_sent_request_password_2}    We have sent an email to
${msg_sent_request_password_3}    Please check your mailbox to reset new password.
${msg_set_new_pass_success_1}    Your password has been reset
${msg_set_new_pass_success_2}    You can then login to your account using your new password.


## True ##
${popup_term_conditions_true}   //act-dialog-notify-terms-conditions//div[@class='dialog-wrapper-content']
${chb_term_conditions_true}    chk-notify-terms-conditions
${btn_ok_term_conditions_true}  id=btn-notify-terms-conditions-accept
${txt_true_username}     id=affiliate-inp-username-login
${txt_true_password}    id=affiliate-inp-password-login
${btn_true_login}   id=affiliate-btn-login

## SCG ##
${txt_scg_username}     id=userNameInput
${txt_scg_password}     id=passwordInput
${btn_scg_login}    id=submitButton
${popup_notify_budget}  //ota-dialog[@class='dialog-notify-budget center']//div[@class='dialog-container']
${txt_landing_budget_popup}     //ota-dialog[@class='dialog-notify-budget center']//div[@class='dialog-container']//*[@class='alert-text']
${txt_notify_budget}    Please check your budget for book the hotel from your business unit.
${btn_ok_scg_budget_popup}  //div[@class='alert-text']/following-sibling::button[@class='ota-btn-main']
