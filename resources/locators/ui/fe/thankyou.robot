*** Variables ***
${txt_thkyou_booking_status}     id=thkyou-txt-status-booking-title
${btn_thkyou_save_voucher}  id=thkyou-btn-save-voucher
${btn_thkyou_print_voucher}     id=thkyou-btn-print-voucher
${txt_thkyou_booking_id}   id=thkyou-txt-bookingId
${txt_thkyou_booker_full_name}  id=thkyou-txt-booker-full-name
${txt_thkyou_booker_phone}     id=thkyou-txt-booker-phone
${txt_thkyou_booker_email}     id=thkyou-txt-booker-email
${txt_thkyou_company_name}     id=thkyou-txt-company-name
${txt_thkyou_tax_number}     id=thkyou-txt-tax-number
${txt_thkyou_tax_address}     id=thkyou-txt-tax-address
${txt_thkyou_property_name}     id=thkyou-txt-property-name
${txt_thkyou_property_address}     id=thkyou-txt-property-address
${txt_thkyou_check_in_date}     id=thkyou-txt-check-in-date
${txt_thkyou_check_out_date}     id=thkyou-txt-check-out-date
${txt_thkyou_number_of_room}    id=thkyou-txt-booking-detail-room
${txt_thkyou_number_of_guest}    id=thkyou-txt-booking-detail-guest