*** Variables ***
${rdo_tax_invoice_by_name}  //div[@class='address-detail']/*[text()='_$taxAddressByName']
${rdo_tax_invoice_by_tax_id}
${rdo_tax_invoice_by_address}   
${btn_cart_continue}    id=checkout-btn-cart-continue



${btn_checkout_3_credit_card}           id=checkout3-rdo-credit-card
${btn_checkout_3_credit_term}           id=checkout3-rdo-credit-term

#hotel info
${txt_checkout_3_cart_hotel_name}     id=checkout-text-cart-hotel-name
${txt_checkout_3_room_list}     id=checkout-ul-cart-room-list
${txt_checkout_3_room_name}         id=checkout-text-cart-room-title-_$index


# credit card
${lbl_checkout_3_payon_date}            id=checkout3-payon-date
${lbl_checkout_3_card_expire}           id=checkout3-card-expire
${txt_checkout_3_card_no}               id=checkout3-txt-card-number
${txt_checkout_3_card_name}             id=checkout3-txt-card-name
${cbo_checkout_3_card_expire_month}     id=checkout3-sel-card-expire-month
${cbo_checkout_3_card_expire_year}      id=checkout3-sel-card-expire-year
${txt_checkout_3_card_cvv}              id=checkout3-txt-card-cvv
${txt_checkout_3_tax_no}     id=address-list-credit-card-txt-tax-no-_$index
${txt_checkout_3_tax_address}     id=address-list-credit-card-txt-address-_$index

#credit terms
${txt_checkout_3_tax_no_credit_terms}           id=address-list-credit-term-txt-tax-no-_$index
${txt_checkout_3_tax_address_credit_terms}      id=address-list-credit-term-txt-address-_$index
${txt_checkout_3_tax_name_credit_terms}         id=address-list-credit-term-txt-company-_$index

# cost center
${chb_checkout_3_credit_terms_defaul_cost_center}   id=default-cost-center
${btn_checkout_3_credit_terms_defaul_cost_center}   id=credit-terms-form-chb-default-cost-center
${txt_checkout_3_credit_terms_cost_center}          id=credit-terms-form-txt-cost-center
${txt_checkout_3_credit_terms_cost_center_name}     id=credit-terms-form-txt-cost-center-name
${txt_checkout_3_credit_terms_fund_code}            id=credit-terms-form-txt-fund-code

# upload file
${file_checkout_3_credit_terms_upload_file}         id=file
${btn_checkout_3_credit_terms_upload_file}          id=upload-file-btn-file
${btn_checkout_3_credit_terms_remove_upload_file}   id=upload-file-btn-remove


# address
${list_checkout_3_address}                      //div[@class='content-address checkout']/div[contains(@id, 'address-list-credit-card-div-item')]
${rdo_checkout_3_address_list_type_select}         id=address-list-TYPE-div-item-INDEX
${rdo_checkout_3_address_list_select}           id=address-list-TYPE-div-item-INDEX
${lbl_checkout_3_address_list_company}          id=address-list-TYPE-txt-company-INDEX
${lbl_checkout_3_address_list_tax_number}       id=address-list-TYPE-txt-tax-no-INDEX
${lbl_checkout_3_address_list_address}          id=address-list-TYPE-txt-address-INDEX
${btn_checkout_3_address_list_delete}           id=address-list-TYPE-btn-delete-INDEX
${btn_checkout_3_address_list_edit}             id=address-list-TYPE-btn-edit-INDEX
${btn_checkout_3_address_list_new_address}      id=address-list-TYPE-btn-add-new-address

${txt_checkout_3_address_form_company}          id=address-form-txt-name-INDEX
${txt_checkout_3_address_form_tax_number}       id=address-form-txt-taxnumber-INDEX
${txt_checkout_3_address_form_address}          id=address-form-txt-address-INDEX
${btn_checkout_3_address_form_save}             id=address-form-btn-save-INDEX
${btn_checkout_3_address_form_cancel}           id=address-form-btn-cancel-INDEX

# company addres
${list_checkout_3_company_address}              //div[@class='content-address checkout']/div[contains(@id, 'address-list-credit-term-div-item')]
${rdo_checkout_3_company_address_list_select}      id=address-list-credit-term-div-item-INDEX
# ${rdo_checkout_3_company_address_list_select}      id=address-list-div-item-INDEX


# billing address
${rdo_checkout_3_billing_address_list_name}             //div[@class='address-detail']/*[text()='_$billToName']
${list_checkout_3_billing_address}                      //div[@class='content-address checkout3']/div[contains(@id, 'billing-list-div-item')]
${rdo_checkout_3_billing_address_list_select}           id=billing-list-div-item-INDEX
${lbl_checkout_3_billing_address_list_name}             id=billing-list-txt-name-_$index
${lbl_checkout_3_billing_address_list_phone}            id=billing-list-txt-phone-INDEX
${lbl_checkout_3_billing_address_list_address}          id=billing-list-txt-address-INDEX
${btn_checkout_3_billing_address_list_delete}           id=billing-list-btn-delete-INDEX
${btn_checkout_3_billing_address_list_edit}             id=billing-list-btn-edit-INDEX
${btn_checkout_3_billing_address_list_new_address}      id=address-list-btn-add-new-address

${txt_checkout_3_billing_address_form_name}             id=billing-form-txt-name-INDEX
${txt_checkout_3_billing_address_form_phone}            id=billing-form-txt-phone-INDEX
${txt_checkout_3_billing_address_form_address}          id=billing-form-txt-address-INDEX
${btn_checkout_3_billing_address_form_save}             id=billing-form-btn-save-INDEX
${btn_checkout_3_billing_address_form_cancel}           id=billing-form-btn-cancel-INDEX