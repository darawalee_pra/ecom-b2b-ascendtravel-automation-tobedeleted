*** Variables ***
${txt_search_keyword}    					     id=search-input-destination
${btn_search_submit}                             id=search-btn-submit
${btn_search_purpose_work}                       id=search-radio-purpose-work
${btn_search_purpose_leisure}                    id=search-radio-purpose-leisure
${checkin_calendar}                              id=searchbox-inp-checkin
${checkout_calendar}                           id=searchbox-inp-checkout
${btn_close_calendar_desktop}   //div[@id='searchbox-inp-checkout']//span[@class='close-icon icon-close']
${checkin_date_text}    //input[@name='searchbox-inp-checkin_submit']
${checkout_date_text}   //input[@name='searchbox-inp-checkout_submit']
${btn_my_account}   id=header-btn-my-account-desktop
${btn_my_account_my_booking}    id=header-lnk-my-booking-desktop
${ddl_search_number_of_room}     id=search-select-room-personal
${ddl_search_number_of_guest}     id=search-select-guest-personal

#----- from previous repo -----#

${txt_search_minprice}                           id=min_price
${txt_search_maxprice}                           id=max_price
${btn_search_sortby_recommend}                   id=levelC-btn-recommend-by-us
${sel_search_sortby_hotelname}                   id=levelC-cbo-hotel-name
${sel_search_sortby_price}                       id=levelC-cbo-price-night
${sel_search_sortby_rating}                      id=levelC-cbo-hotel-star
${cbo_search_filter_rating_5}                    id=levelC-chk-hotel-5-stars
${cbo_search_filter_rating_4}                    id=levelC-chk-hotel-4-stars
${cbo_search_filter_rating_3}                    id=levelC-chk-hotel-3-stars
${cbo_search_filter_rating_2}                    id=levelC-chk-hotel-2-stars
${cbo_search_filter_rating_1}                    id=levelC-chk-hotel-1-star
${lnk_search_first_hotel}                        id=levelC-li-hotel-item-0
${lnk_search_result_hotel_item}                  //*[@id="levelC-li-hotel-item-INDEX"]
${lnk_search_result_hotel_item_name}             //li[@id="levelC-li-hotel-item-INDEX"]//span[text()="HOTEL_NAME"]
${lnk_search_result_hotel_item_title}            /a/div[2]/div/div[1]/div[1]/span[1]
${lnk_lvc_pagination}                            //div[@class="pagination-wrapper -ipad-mobile"]//*[@class="ota-pagination-lg -desktop"]/li[INDEX]
${lbl_search_result_hotel_item_rack_price}       /a/div[2]/div/div[2]/div/div[2]/span[1]
${lbl_search_result_hotel_item_price}            /a/div[2]/div/div[2]/div/div[2]/strong
${lbl_search_result_hotel_item_no_allotment}     /a/div[2]/div/div[2]/div/div[2]
${lbl_search_result_hotel_distance_from_poi}     //div[@class="hotel-locate-box"]//div[@class="hotel-location"]//span[@class="hotel-poi-distance"]/span[1]
${lbl_lvc_hotel_name}                            //*[@class="hotel-text-name"]
${btn_lvc_next_page}                             //*[@class="ota-pagination-lg -mobile"]//*[@class="ctrl-page next-page"]

${sel_search_today}                              //*[@class="today"]
${btn_search_change_criteria}                    //*[@class="search-change"]

${btn_search_recommended}                        //*[@class="hotel-recommended"]
${btn_search_recommended_s}                      //*[@class="hotel-recommended-s"]
${lbl_search_allotment_remind}                   //*[@class="booking-remind"]
${lbl_search_payon}                              //*[@class="booking-payment"]

${search_suggestions_box}                        //div[@class='tt-menu tt-open']
${search_suggestion_destination_box}             //div[@class='tt-menu tt-open']//div[@class='tt-dataset tt-dataset-product']//p[text()=' Destination']
${search_suggestion_hotel_box}                   //div[@class='tt-menu tt-open']//div[@class='tt-dataset tt-dataset-product']//p[text()=' Properties/Hotels']
${search_suggestion_poi_box}                     //div[@class='tt-menu tt-open']//div[@class='tt-dataset tt-dataset-product']//p[text()=' Point of Interest']
${search_result_suggestion}          /following-sibling::div[@class='tt-suggestion tt-selectable']//*[contains(text(), '$_keyword')]

${lbl_first_suggestion}                          //div[@class='tt-menu tt-open']/div[1]//div[1]
${lbl_first_hotel_suggestion}                    //div[@class='tt-menu tt-open']/div[2]//div[1]
${lbl_suggestion_choice}                         //div[@class='tt-menu tt-open']/div[1]//div[INDEX]
${lbl_suggestion_poi}                            //div[@class="tt-dataset tt-dataset-product"]/p[@class="tt-dataset tt-header"]/i[@class="icon-icon-business-destination"]/../../div[INDEX]
${lbl_title_founded_lv_c}                        //div[@class="title-founded"]//span[1]
${lbl_search_result_bottom_lv_c}                 //div[@class="search-result-bottom"]//span[1]

${inp_search_checkin_hidden}                     //*[@name="searchbox-inp-checkin_submit"]
${inp_search_checkout_hidden}                    //*[@name="searchbox-inp-checkout_submit"]
${sel_search_checkin_choice_left}                //*[@id="searchbox-inp-checkin"]/div[2]/div[2]/table/tbody/tr[ROW]/td[COL]
${sel_search_checkout_choice_left}               //*[@id="searchbox-inp-checkout"]/div[2]/div[2]/table/tbody/tr[ROW]/td[COL]
${sel_search_checkin_choice_right}               //*[@id="searchbox-inp-checkin"]/div[3]/div[2]/table/tbody/tr[ROW]/td[COL]
${sel_search_checkout_choice_right}              //*[@id="searchbox-inp-checkout"]/div[3]/div[2]/table/tbody/tr[ROW]/td[COL]
${sel_search_checkout_choice_left_start_date}    //*[@id="searchbox-inp-checkout"]/div[2]/div[2]/table/tbody/tr/td[contains(@class,"active start-date available")]
${btn_search_checkin_prev_month}                 //*[@id="searchbox-inp-checkin"]//span[@class="prevIcon"]
${btn_search_checkin_next_month}                 //*[@id="searchbox-inp-checkin"]//span[@class="nextIcon"]
${btn_search_checkout_prev_month}                //*[@id="searchbox-inp-checkout"]//span[@class="prevIcon"]
${btn_search_checkout_next_month}                //*[@id="searchbox-inp-checkout"]//span[@class="nextIcon"]

${lnk_change_search}                             //div[@class="search-change"]/a
${btn_change_search}                             //button[@class="ota-btn-main btn-submit-horizontal"]


${btn_search_checkin}                            //input[@id="searchbox-inp-checkin"]
${btn_search_checkout}                           //input[@id="searchbox-inp-checkout"]
${allRow_calendar}                               //div[@id="searchbox-inp-checkin"]/div[2]/div[2]/table/tbody/tr

${txt_budget_limit_lv_c}                         id=levelC-text-budget-limit

# rating
${lbl_lv_c_rating_score}                         id=levelC-rating-score-item-INDEX
${lbl_lv_c_rating_des}                           id=levelC-rating-description-item-INDEX
${img_lv_c_recommended_desk}                     //div[@class="hotel-detail-table"]//span[text()="HOTEL_NAME"]/../../..//div[contains(@id,"levelC-rating-img-recommended-item")]
${img_lv_c_partner_desktop}                      //div[@class="hotel-detail-table"]//span[text()="HOTEL_NAME"]/../../..//div[contains(@id,"levelC-rating-img-partner-item")]
${img_lv_c_recommended_mobile}                   //div[@class="hotel-detail-table"]//span[text()="HOTEL_NAME"]/../../..//img[@src="assets/images/icons/recommend-s.svg"]
${img_lv_c_partner_mobile}                       //div[@class="hotel-detail-table"]//span[text()="HOTEL_NAME"]/../../..//img[@src="assets/images/icons/icon-partner-m.png"]
${lbl_lv_c_rating_score_desk}                    //div[@class="hotel-detail-table"]//span[text()="HOTEL_NAME"]/../../..//div[@class="hotel-review-score"]/span[contains(@id,"levelC-rating-score-item")]
${lbl_lv_c_rating_des_desk}                      //div[@class="hotel-detail-table"]//span[text()="HOTEL_NAME"]/../../..//div[@class="hotel-review-score"]/span[contains(@id,"levelC-rating-description-item")]
${lbl_lv_c_rating_score_mobile}                  //div[@class="hotel-detail-table"]//span[text()="HOTEL_NAME"]/../../..//div[@class="hotel-review-score-s"]/span[contains(@id,"levelC-rating-score-item")]
${lbl_lv_c_rating_des_mobile}                    //div[@class="hotel-detail-table"]//span[text()="HOTEL_NAME"]/../../..//div[@class="hotel-review-score-s"]/span[contains(@id,"levelC-rating-description-item")]

# sort rating
${btn_lv_c_sort_more}                            id=levelC-cbo-more
${btn_lv_c_sort_score}                           //li/a[@id="levelC-btn-score"]
${sel_lv_c_sort_more}                            //a[@class="sort-box-more button selected" and @id="levelC-cbo-more"]
${btn_lv_c_sort_mobile}                          id=levelC-btn-sort
${btn_lv_c_sort_score_mobile}                    //input[@id="levelC-opt-sort-by-score"]

# filter rating
${btn-filter}                                    id=levelC-btn-filter
${btn-choose-button}                             id=levelC-btn-choose-button
#${box_lv_c_filter_rating_desk}                   id=levelC-rating-filter-item-desktop
${box_lv_c_filter_rating_desk}                   id=levelC-rating-filter-radio-desktop
${rad_lv_c_filter_rating_desk}                   id=levelC-rating-filter-radio-desktop-item-INDEX
${lbl_lv_c_filter_rating_desk}                   id=levelC-rating-filter-description-desktop-item-INDEX
#${box_lv_c_filter_rating_mobile}                 id=levelC-rating-filter-item-mobile
${box_lv_c_filter_rating_mobile}                 id=levelC-rating-filter-radio-mobile
${rad_lv_c_filter_rating_mobile}                 id=levelC-rating-filter-radio-mobile-item-INDEX
${lbl_lv_c_filter_rating_mobile}                 id=levelC-rating-filter-description-mobile-item-INDEX

${lnk_poi_distance_mobile}                       //div[@class="cdk-overlay-container"]
${btn_poi_distance}                              //div[@class="row row-padding"]//div[INDEX]
${btn_lvc_sort_poi_distance}                     id=levelC-btn-poi-distance
${radio_lvc_sort_by_poi_distance_mobile}         //*[@id="levelC-opt-sort-by-poi-distance"]
