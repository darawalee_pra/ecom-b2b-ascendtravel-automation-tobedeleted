*** Variables ***
${lbl_checkout_2_purpose}                   id=chkout2-txt-purpose
${txt_checkout_2_booker_first_name}         id=chkout2-inp-firstname-booker
${txt_checkout_2_booker_last_name}          id=chkout2-inp-lastname-booker
${txt_checkout_2_booker_email}              id=chkout2-inp-email-booker
${txt_checkout_2_booker_mobile_no}          id=chkout2-inp-mobileNo-booker
${txt_checkout_2_guest_first_name}          id=chkout2-inp-firstname-guest-room-RINDEX-guest-GINDEX
${txt_checkout_2_guest_last_name}           id=chkout2-inp-lastname-guest-room-RINDEX-guest-GINDEX
${txt_checkout_2_guest_email}               id=chkout2-inp-email-guest-room-RINDEX-guest-GINDEX
${cbo_checkout_2_guest_nationality}         id=chkout2-sel-nationality-guest-room-RINDEX-guest-GINDEX
${txt_checkout_2_guest_special_request}     id=chkout2-text-special-request-guest-room-RINDEX-guest-GINDEX

${btn_checkout_2_guest_add_guest}           id=chkout2-btn-add-guest-room-RINDEX
${btn_cehckout_2_guest_more_detail}         id=chkout2-btn-expand-special-request-room-RINDEX-guest-GINDEX

${btn_checkout_2_continue}  id=checkout-btn-cart-continue
