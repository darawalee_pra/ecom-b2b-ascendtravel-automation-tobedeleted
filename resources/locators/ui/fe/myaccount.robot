*** Variables ***
${booking_id}   id=my-booking-text-booking-id-_$index   
${booking_id_path}  //strong[text()='_$bookingid']

${btn_cancel_booking}   id=my-booking-a-booking-cancel-_$index
${btn_confirm_cancel_booking}   id=cancellation-popup-btn-confirm-cancel
${txt_success_cancel_message}     //*[text()='Cancel Booking ID _$bookingid Successful.']