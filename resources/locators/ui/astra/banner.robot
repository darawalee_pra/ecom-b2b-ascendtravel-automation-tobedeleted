*** Variables ***
#Menu
${main-menu-banner}                     //*[@id="menu-Banner"]
${sub-menu-banner-bannerlist}           //*[@id="menu-Banner List"]/span

#Input
${input_search}                         id=input_search
${browse-file-th}                       id=browse-imageTh
${browse-file-en}                       id=browse-imageEn
${title-name-th}                        id=input-banner-management-title-th
${title-name-en}                        id=input-banner-management-title-en
${description-th}                       id=input-banner-management-description-th   
${description-en}                       id=input-banner-management-description-en                             
${link}                                 id=input-banner-management-link                             
${book-from-date}                       id=bookPeriodFrom-date                             
${book-to-date}                         id=bookPeriodTo-date                             
${stay-from-date}                       id=stayPeriodFrom-date                             
${stay-to-date}                         id=stayPeriodTo-date                             
${banner-start}                         id=bannerStart-date       
${banner-start-time}                    id=input-banner-management-start-time    
${banner-end}                           id=bannerEnd-date                        
${banner-end-time}                      id=input-banner-management-end-time       
${priority}                             id=touchspin-input-priority  
${margin}                               id=touchspin-input-margin

#Dropdown List
${ddl-status}                           id=ddl-status 
${all-status}                           id=select-status-0
${status-active}                        id=select-status-1
${status-inactive}                      id=select-status-2
${status-pending}                       id=select-status-3

#Table
${label-banner-detail-0}                //*[@id="data-banner-detail-0"]/label
${status-0}                             id=data-status-0 
${data-no-record}                       id=data-no-record

#Modal
${modal-button}                         id=btn-preview-save
${modal-delete-save}                    id=btn-dialog-comfirm

#label
${label-showing}                        id=label-showing

#Button
${btn-create}                           id=btn-create
${btn-save}                             id=btn-banner-management-submit
${btn-preview}                          id=btn-banner-management-preview  
${btn-cancel}                           id=btn-banner-management-cancel
${btn-edit-0}                           id=btn-edit-0 
${btn-submit}                           id=search-btn-submit
${btn-delete-0}                         id=btn-delete-0

#Front end
${banner-promotion-topic}               id=banner-promotion-topic
${banner-promotion-sub-topic}           id=banner-promotion-sub-topic
${banner-lbl-hotel-name-0}              id=banner-lbl-hotel-name-0
${banner-btn-book-0}                    id=banner-btn-book-0
${banner-img-hotel-image-0}             id=banner-img-hotel-image-0
${levelD-text-hotel-name}               id=levelD-text-hotel-name
