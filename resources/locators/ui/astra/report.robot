*** Variables ***
${lbl_accrued_report}    						id=landing-inp-email-verify-domain
${txt_accrued_report_chk_in_from}    			id=accured-report-from-date
${txt_accrued_report_chk_in_to}    				id=accured-report-to-date
${txt_accrued_report_booker_email}    			id=accured-report-email
${btn_accrued_report_export}    				id=accured-report-button
${btn_monthly_unearned_report_export}    		id=monthly-unearned-report-button

${lbl_daily_unearned_report}    						id=landing-inp-email-verify-domain
${txt_daily_unearned_report_chk_in_from}    			id=unearned-report-from-date
${txt_daily_unearned_report_chk_in_to}    				id=unearned-report-to-date
${btn_daily_unearned_report_export}    		            id=unearned-report-button


${lbl_daily_revenue_ccw_report}    						id=landing-inp-email-verify-domain
${txt_daily_revenue_ccw_report_chk_in_from}    			id=revenue-card-report-from-date
${txt_daily_revenue_ccw_report_chk_in_to}    			id=revenue-card-report-to-date
${btn_daily_revenue_ccw_report_export}    				id=revenue-card-report-button

${lbl_daily_revenue_cct_report}    						id=landing-inp-email-verify-domain
${txt_daily_revenue_cct_report_chk_in_from}    			id=revenue-term-report-from-date
${txt_daily_revenue_cct_report_chk_in_to}    			id=revenue-term-report-to-date
${btn_daily_revenue_cct_report_export}    				id=revenue-term-report-button

${sel_date_picker_year}                                 //ngb-datepicker//ngb-datepicker-navigation-select/select[@class='custom-select'][2]
${sel_date_picker_month}                                //ngb-datepicker//ngb-datepicker-navigation-select/select[@class='custom-select'][1]
${div_date_picker_weeks}                                //ngb-datepicker//div[@class='ngb-dp-week' or @class='ngb-dp-week ng-star-inserted']
${div_date_picker_days}                                 //div[@class='ngb-dp-day' or @class='ngb-dp-day ng-star-inserted']