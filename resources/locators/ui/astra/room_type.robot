*** Variables ***
## room type info
${ddl_supplier_drop_down_list}                  id=ddl-hotel-supplier
${list_supplier_ddl}                            //select[@id='xxxx'and contains(.,'Select Supplier')]
${input_room_name_th}                           id=nameTh
${input_room_name_en}                           id=nameEn
${input_no_room}                                id=touchspin-input-number-of-rooms
${btn_decrease_no_room}                         id=touchspin-decrease-number-of-rooms
${btn_increase_no_room}                         id=touchspin-increase-number-of-rooms
${rad_allow_child_yes}                          id=allowChild-radio-yes
${rad_allow_child_no}                           id=allowChild-radio-no
${input_max_adult}                              id=touchspin-input-max-adult
${btn_decrease_max_adult}                       id=touchspin-decrease-max-adult
${btn_increase_max_adult}                       id=touchspin-increase-max-adult
${input_max_child}                              id=touchspin-input-max-child
${btn_decrease_max_child}                       id=touchspin-decrease-max-child
${btn_increase_max_child}                       id=touchspin-increase-max-child
${input_room_size}                              id=touchspin-input-room-size
${btn_decrease_room_size}                       id=touchspin-decrease-room-size
${btn_increase_room_size}                       id=touchspin-increase-room-size
${ddl_room_view}                                id=ddl-room-view
${list_room_view}                               //select[@id='xxxx'and contains(.,'Select Room View')]
${input_bathroom}                               id=touchspin-input-bathroom
${btn_dec_bathroom}                             id=touchspin-decrease-bathroom
${btn_inc_bathroom}                             id=touchspin-increase-bathroom
${btn_change_room_status}                       //div[@class='toggle']/button[@id='btn-toggle-room-type-status-0']
${btn_cancel_create_room}                       id=btn-roomtype-management-cancel
${popup_cancel}                                 id=dialog-comfirm-leave-title
${popup_cancel_confirm}                         //button[@id='btn-dialog-comfirm']
${popup_cancel_cancel}                          //button[@id='btn-dialog-cancel']
${btn_create_room}                              id=btn-roomtype-management-save
${btn_collapse_room_info}                       id=roomtype-info-ft-minus

## room facilities
${lbl_fct_header}                               //span[@id='headerRoomtypeFacilities' and text()='Facilities']
${lbl_room_fct_title}                           //div[@class='box-facilities']//span[text() = "Room Facilities"]          
${lbl_room_amt_title}                           //div[@class='box-facilities']//span[text() = "Amenities"]
${cbox_sel_all_fct}                             id=room-type-facilities-checkbox-selectAll
${lbl_sel_all_fct}                              id=room-type-facilities-label-selectAll
${cbox_sel_all_amt}                             id=room-type-amenities-checkbox-selectAll
${lbl_sel_all_amt}                              id=room-type-amenities-label-selectAll
${cbox_each_fct}                                //div[@class='box-facilities']//label[text()="TEXT"]/../input
${cbox_each_fct_index}                          id=room-type-facilities-checkbox-facilities-INDEX
${icon_each_fct_no_sel}                         //div[@class='box-facilities']//label[text()="TEXT"]/../i[@class='ft-check white']
${icon_each_fct_is_sel}                         //div[@class='box-facilities']//label[text()="TEXT"]/../i[@class='ft-check primary']
${icon_each_fct_index_is_sel}                   //i[@class='ft-check primary' and @id='room-type-facilities-icon-facilities-INDEX']
${icon_each_fct_index_no_sel}                   //i[@class='ft-check white' and @id='room-type-facilities-icon-facilities-INDEX']
${lbl_each_fct}                                 //div[@class='box-facilities']//label[text()="TEXT"]
${cbox_each_amt}                                //div[@class='box-facilities']//label[text()="TEXT"]/../input
${cbox_each_amt_index}                          id=room-type-amenities-checkbox-facilities-INDEX
${icon_each_amt_no_sel}                         //div[@class='box-facilities']//label[text()="TEXT"]/../i[@class='ft-check white']
${icon_each_amt_is_sel}                         //div[@class='box-facilities']//label[text()="TEXT"]/../i[@class='ft-check primary']
${icon_each_amt_index_is_sel}                   //i[@class='ft-check primary' and @id='room-type-amenities-icon-facilities-INDEX']
${icon_each_amt_index_no_sel}                   //i[@class='ft-check white' and @id='room-type-amenities-icon-facilities-INDEX']
${lbl_each_amt}                                 //div[@class='box-facilities']//label[text()="TEXT"]
${btn_show_more_fct}                            id=room-type-facilities-showmore-button
${lbl_show_more_fct}                            id=room-type-facilities-label-show-more
${lbl_show_less_fct}                            id=room-type-facilities-label-show-less
${btn_show_more_amt}                            id=room-type-amenities-showmore-button
${lbl_show_more_amt}                            id=room-type-amenities-label-show-more
${lbl_show_less_amt}                            id=room-type-amenities-label-show-less
${btn_hidden_fct}                               id=room-type-facilities-hidden-button
${lbl_fct_seled}                                //span[text()='Room Facilities']/..//span[contains(text(),'selected')]
${lbl_amt_seled}                                //span[text()='Amenities']/..//span[contains(text(),'selected')]
${lbl_no_fct_seled}                             //span[text()='Room Facilities']/..//span[text()="NUMBER selected"]
${lbl_no_amt_seled}                             //span[text()='Amenities']/..//span[text()="NUMBER selected"]
${btn_edit_room_fct}                            id=room-type-facilities-edit-button

## temp switch mode 
${btn_viw_mod}                                  //button[text()="view"]