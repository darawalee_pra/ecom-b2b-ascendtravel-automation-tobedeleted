*** Settings ***
Library             ${CURDIR}/../../../../Library/convertExcelToJson.py

*** Variables ***
${product_url}      /hotel/api/v1/hotel

*** Keywords ***
Get Product Info
    [Arguments]    ${workbook}    ${target_id}    ${start_col}    ${end_col}  
    
    ${sheet}=  get_sheet             ${workbook}  Product Information
    ${obj}=    get_Obj_from_excel    ${sheet}     ${target_id}    ${start_col}    ${end_col}
    
    [Return]   ${obj}

Get Supplier Markup
    [Arguments]    ${workbook}    ${target_id}    ${start_col}    ${end_col}
    
    ${sheet}=  get_sheet    ${workbook}  Supplier Markup
    ${obj}=    get_ArrObj_from_excel    ${sheet}     ${target_id}    ${start_col}    ${end_col}
    
    [Return]   ${obj}

Get Default Policy
    [Arguments]    ${workbook}    ${target_id}
    
    ${sheet}=        get_sheet                ${workbook}    Default Cancellation Policy
    ${policy_type}=  get_value_by_key         ${sheet}       ${target_id}    A    B
   
    ${key}=    Create List  policyType
    ${val}=    Create List  ${policy_type}

    ${policies_key}  ${policies_val}=  Run Keyword If  '${policy_type}'!='Non Refund'  
    ...                                Get Default Policy Details  ${sheet}    ${target_id}    ${policy_type}

    Run Keyword If  '${policy_type}'!='Non Refund'    Append to list  ${key}  ${policies_key}
    Run Keyword If  '${policy_type}'!='Non Refund'    Append to list  ${val}  ${policies_val}

    ${obj}=    convert_list_to_dict  ${key}  ${val}

    [Return]    ${obj}

Get Default Policy Details
    [Arguments]    ${sheet}    ${target_id}    ${policy_type}

    ${start_col}=  Run Keyword If    '${policy_type}'=='Allow Before CheckIn Date (Time)'    Set Variable  D
    ...            ELSE               Set Variable  C
    ${end_col}=    Run Keyword If    '${policy_type}'=='Allow Before CheckIn Date (Time)'    Set Variable  G
    ...            ELSE               Set Variable  F

    ${policies_key}=        Set Variable    policies
    ${policies_val}=        get_ArrObj_from_excel    ${sheet}     ${target_id}    ${start_col}    ${end_col}

    [Return]    ${policies_key}    ${policies_val}


Get JSON Create Product
    [Arguments]    ${workbook}    ${target_id}

    ${product_info} =    Get Product Info        ${workbook}  ${target_id}  F  T
    ${supplier_markup}=  Get Supplier Markup     ${workbook}  ${target_id}  C  E
    ${default_policy}=   Get Default Policy      ${workbook}  ${target_id}

    ${obj}=    Create Dictionary    productInfo=${product_info}  
    ...                             supplierMarkup=${supplier_markup}  
    ...                             defaultCancellationPolicy=${default_policy}

    ${json}=   serialize_JSON    ${obj}

    [Return]    ${json}

POST Create Product API
    [Arguments]      ${access_token}  ${refresh_token}  ${path_folder}  ${product_data_file}
    ${headers}=      Create Dictionary    Content-Type=application/json   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}

    ${workbook}=     open_excel           ${path_folder}/${product_data_file}
    ${sheet}=        get_sheet            ${workbook}  Product Information
    ${length}=       get_excel_max_row    ${sheet}
    ${hotel_ids}=    Create List

    :FOR  ${index}  IN RANGE  1  ${length}
    \  ${flag}=          get_value_by_key               ${sheet}          ${index}    A    D
    \  Continue For Loop If    '${flag}' != 'T'
    \  ${data_list}=     Get JSON Create Product        ${workbook}       ${index}   
    \  Log to console    ${data_list}
    \  ${resp}           POST Request  astra_session    ${product_url}    headers=${headers}  data=${data_list}
    \  Should Be Equal As Strings  ${resp.status_code}  200
    \  ${hotel_id}       Get Value From Json    ${resp.json()}  $.data.id
    \  Append To List    ${hotel_ids}   ${hotel_id[0]}
    [Return]    ${hotel_ids}