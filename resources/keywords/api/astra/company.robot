*** Settings ***
Library     Selenium2Library
Library     String
Library     RequestsLibrary
Library     JSONLibrary
Library     ${CURDIR}/../../../../Library/excel.py
Library     ${CURDIR}/../../../../Library/requestLibrary.py
Resource    ${CURDIR}/../../Common/common.robot
Resource    ${CURDIR}/../../../Config/${ENV}/config.robot

*** Variables ***
${company_v2_url}               /company-v2/api/v1
${company_information_url}      /information
${company_list_url}             /companies

*** Keywords ***
GET Company List API
    [Arguments]      ${access_token}  ${refresh_token}  ${search}=${EMPTY}  ${page}=1   ${size}=50    ${sort_by}=company_id   ${sort_type}=asc
    Create Session   astra_session  ${SERVICE_API}
    &{headers}=  Create Dictionary      Content-Type=application/json   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    &{params}=   Create Dictionary      page=${page}   size=${size}     search=${search}    sort_by=${sort_by}     sort_type=${sortType}
    ${resp}=     Get Request  astra_session     ${company_v2_url}${company_list_url}     headers=${headers}    params=${params}    timeout=1
    [Return]    ${resp}

POST Create Company API
    [Arguments]     ${access_token}  ${refresh_token}   ${json_file_path}   ${doc_file_path}=NONE
    ${json_name}    Fetch From Right    ${json_file_path}   /
    ${doc_name}     Run Keyword If  '${doc_file_path}' != 'NONE'    Fetch From Right    ${doc_file_path}    /
    ${doc_type}     Run Keyword If  '${doc_file_path}' != 'NONE'    Fetch From Right    ${doc_name}         .
    Create Session  astra_session  ${SERVICE_API}
    &{files}    Run Keyword If  '${doc_file_path}' != 'NONE'    Evaluate    {'company': ('${json_name}', open('${json_file_path}', 'rb'), 'multipart/form-data'), 'documents': ('${doc_name}', open('${doc_file_path}', 'rb'), 'image/${doc_type}')}
    ...         ELSE        Evaluate    {'company': ('${json_name}', open('${json_file_path}', 'rb'), 'multipart/form-data')}
    &{headers}  Create Dictionary   Content-Type=multipart/form-data       x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    ${resp}     POST Request  astra_session     ${company_v2_url}${company_information_url}    headers=${headers}  files=${files}
    [Return]    ${resp}

POST Create Update Supplier With Multiple Documents API
    [Arguments]     ${access_token}  ${refresh_token}   ${json_file_path}   ${doc_file_path_list}
    ${json_name}    Fetch From Right    ${json_file_path}   /
    ${count_doc}    Get Length   ${doc_file_path_list}
    ${doc_list}     Set Variable
    :FOR    ${index}    IN RANGE    0   ${count_doc}
    \   ${doc_name}     Fetch From Right    ${doc_file_path_list[${index}]}    /
    \   ${doc_type}     Fetch From Right    ${doc_name}     .
    \   ${doc_list}     Catenate    SEPARATOR=,   ${doc_list}   ('documents', ('${doc_name}', open('${doc_file_path_list[${index}]}', 'rb'), 'image/${doc_type}'))
    ${files}    Evaluate    [('company', ('${json_name}', open('${json_file_path}', 'rb'), 'multipart/form-data')) ${doc_list}]
    &{headers}  Create Dictionary   Content-Type=multipart/form-data    x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    Create Session  astra_session  ${SERVICE_API}
    ${resp}     POST Request  astra_session     ${company_v2_url}${company_information_url}    headers=${headers}  files=${files}
    [Return]    ${resp}

Verify Get Company List API Response Number of Items As Expected
    [Arguments]     ${resp}     ${expected_val}
    ${items}        Get Value From Json    ${resp.json()}   $.data.items
    ${actual_val}   Get Length    ${items[0]}
    Should Be Equal As Strings  ${actual_val}   ${expected_val}

Verify Get Company List Return Total Page Value As Expected 
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.totalPage  ${expectd_val}

Verify Get Company List Return Total Item Value As Expected 
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.totalItem  ${expectd_val}

Verify Get Company List Return Item From Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.itemFrom  ${expectd_val}

Verify Get Company List Return Item To Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.itemTo  ${expectd_val}

Verify Get Company List API Sorting Result By Comapny Name As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${count}=   Get Value From Json	  ${resp.json()}	$..data.itemTo
    :FOR  ${index}  IN RANGE  0   ${count[0]}
    \   ${actual_val}    Get Value From Json   ${resp.json()}   $..data.items[${index}].companyName
    \   Should Be Equal    ${actual_val[0]}    ${expectd_val[${index}][0]}



    

 


