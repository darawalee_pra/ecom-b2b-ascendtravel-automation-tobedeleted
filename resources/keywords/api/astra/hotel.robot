*** Settings ***
Library     Selenium2Library
Library     String
Library     RequestsLibrary
Library     JSONLibrary
Library     ${CURDIR}/../../../../Library/excel.py
Library     ${CURDIR}/../../../../Library/requestLibrary.py
Resource    ${CURDIR}/../../Common/common.robot
Resource    ${CURDIR}/../../../Config/${ENV}/config.robot

*** Variables ***
${hotel_url}                                    /hotel/api/v1
${hotel_supplier_roomType_roomCondition_url}    /suppliers

*** Keywords ***
GET Supplier RoomType RoomCondition API
    [Arguments]     ${access_token}  ${refresh_token}   ${hotel_id}
    Create Session  astra_session  ${SERVICE_API}
    &{headers}  Create Dictionary   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    ${resp}     GET Request  astra_session     ${hotel_url}/${hotel_id}${hotel_supplier_roomType_roomCondition_url}    headers=${headers}
    [Return]    ${resp}

Verify Get Supplier RoomType RoomCondition API Return Hotel Id As Expected
    [Arguments]    ${resp}   ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.hotelId   ${expected_val}

Verify Get Supplier RoomType RoomCondition API Return Hotel Title As Expected
    [Arguments]    ${resp}   ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.hotelTitle   ${expected_val}

Verify Get Supplier RoomType RoomCondition API Return Free Sale As Expected
    [Arguments]    ${resp}   ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.freeSale   ${expected_val}

Verify Get Supplier RoomType RoomCondition API Return Supplier Hotel - Supplier Name As Expected
    [Arguments]    ${resp}  ${supplier_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.supplierId==${supplier_id})].supplierName    ${expected_val}  

Verify Get Supplier RoomType RoomCondition API Return Supplier Hotel - Room Type Name As Expected
    [Arguments]    ${resp}  ${room_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.roomTypeId==${room_type_id})].roomTypeName    ${expected_val} 

Verify Get Supplier RoomType RoomCondition API Return Supplier Hotel - Room Condition Title As Expected
    [Arguments]    ${resp}  ${room_condition_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.roomConditionId==${room_condition_id})].roomConditionTitle    ${expected_val} 