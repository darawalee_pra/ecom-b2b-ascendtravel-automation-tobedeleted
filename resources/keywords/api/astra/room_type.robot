*** Settings ***
Library     Selenium2Library
Library     String
Library     RequestsLibrary
Library     JSONLibrary
Library     ${CURDIR}/../../../../library/requestLibrary.py
Resource    ${CURDIR}/../../common/common.robot
Resource    ${CURDIR}/../../../config/${ENV}/config.robot

*** Variables ***
${room_type_general_data_url}               /hotel/api/v1/roomtype/hotel-info
${room_type_url}                            /hotel/api/v1/roomtype
# ${MOCK_SERVICE_API}                         http://localhost:8081

*** Keywords ***
GET Room Type General Data API
    [Arguments]      ${access_token}  ${refresh_token}  ${hotel_id}
    Create Session   astra_session  ${SERVICE_API}
    &{headers}=  Create Dictionary      Content-Type=application/json   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    ${resp}=     Get Request  astra_session     ${room_type_general_data_url}/${hotel_id}     headers=${headers}
    [Return]    ${resp}

POST Create Room Type API
    [Arguments]      ${access_token}  ${refresh_token}  ${room_type_dict}
    ${resp}     post_multipart_formdata     ${SERVICE_API}${room_type_url}     ${access_token}  ${refresh_token}    ${room_type_dict}  
    [Return]    ${resp}

Verify Get Room Type General Data API Return Hotel Id As Expected
    [Arguments]      ${resp}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.hotelId  ${expected_val}

Verify Get Room Type General Data API Return Hotel Name TH As Expected
    [Arguments]      ${resp}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.hotelNameTH  ${expected_val}   false

Verify Get Room Type General Data API Return Hotel Name EN As Expected
    [Arguments]      ${resp}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.hotelNameEN  ${expected_val}   false

Verify Get Room Type General Data API Return Hotel Address TH As Expected
    [Arguments]      ${resp}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.hotelAddressTH  ${expected_val}  false

Verify Get Room Type General Data API Return Hotel Address EN As Expected
    [Arguments]      ${resp}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.hotelAddressEN  ${expected_val}  false

Verify Get Room Type General Data API Return Hotel Image As Expected
    [Arguments]      ${resp}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.hotelImage  ${expected_val}  false

Verify Get Room Type General Data API Return Supplier Name By Supplier Id As Expected
    [Arguments]      ${resp}     ${supplier_id}    ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $..*[?(@.supplierId==${supplier_id})].supplierName  ${expected_val}    false

Verify Get Room Type General Data API Return Room View Name TH By Room View Id As Expected
    [Arguments]      ${resp}     ${room_view_id}    ${expected_value}
    Verify API Response Json Value As Expected  ${resp}  $..*[?(@.roomViewId==${room_view_id})].roomViewNameTH  ${expected_val}  false

Verify Get Room Type General Data API Return Room View Name EN By Room View Id As Expected
    [Arguments]      ${resp}     ${room_view_id}    ${expected_value}
    Verify API Response Json Value As Expected  ${resp}  $..*[?(@.roomViewId==${room_view_id})].roomViewNameEN  ${expected_val}  false

Verify Get Room Type General Data API Return Number of Supplier Items As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${items}        Get Value From Json    ${resp.json()}   $.data.suppliers
    ${actual_val}   Get Length    ${items[0]} 
    Should Be Equal As Strings  ${actual_val}   ${expectd_val}

Verify Get Room Type General Data API Return Number of Room View Items As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${items}        Get Value From Json    ${resp.json()}   $.data.roomViews
    ${actual_val}   Get Length    ${items[0]} 
    Should Be Equal As Strings  ${actual_val}   ${expectd_val}

Verify Get Room Type General Data API Return Supplier Name List As Expected
    [Arguments]     ${resp}     ${expected_list}
    Verify API Response Json List As Expected    ${resp}	    $..supplierName     ${expected_list}

Verify Get Room Type General Data API Return Room View Name EN List As Expected
    [Arguments]     ${resp}     ${expected_list}
    Verify API Response Json List As Expected    ${resp}	    $..roomViewNameEN     ${expected_list}

Verify Get Room Type General Data API Return Room View Name TH List As Expected
    [Arguments]     ${resp}     ${expected_list}
    Verify API Response Json List As Expected    ${resp}	    $..roomViewNameTH     ${expected_list}

Verify Get Room Type General Data API Should Not Return Hotel Name TH
    [Arguments]     ${resp}
    Should Not Contain     ${resp.content}       hotelNameTH

Verify Get Room Type General Data API Should Not Return Hotel Name EN
    [Arguments]     ${resp}
    Should Not Contain     ${resp.content}       hotelNameEN

Verify Get Room Type General Data API Should Not Return Hotel Address TH
    [Arguments]     ${resp}
    Should Not Contain     ${resp.content}       hotelAddressTH

Verify Get Room Type General Data API Should Not Return Hotel Address EN
    [Arguments]     ${resp}
    Should Not Contain     ${resp.content}       hotelAddressEN

Verify Get Room Type General Data API Should Not Return Hotel Image
    [Arguments]     ${resp}
    Should Not Contain     ${resp.content}       hotelImage 

Verify Get Room Type General Data API Return Bed Type List As Expected
    [Arguments]     ${resp}     ${expected_list}
    Verify API Response Json Value As Expected    ${resp}	    $.data.bedTypes     ${expected_list}    