*** Settings ***
Library     Selenium2Library
Library     String
Library     RequestsLibrary
Library     JSONLibrary
Library     ${CURDIR}/../../../../Library/excel.py
Library     ${CURDIR}/../../../../Library/requestLibrary.py
Resource    ${CURDIR}/../../Common/common.robot
Resource    ${CURDIR}/../../../Config/${ENV}/config.robot

*** Variables ***
${supplier_list_url}                        /supplier/api/v1/suppliers
${supplier_general_data_url}                /supplier/api/v1/supplier/general
${supplier_url}                             /supplier/api/v1/supplier
${supplier_document_url}                    /supplier/api/v1/document

*** Keywords ***
GET Supplier List API
    [Arguments]      ${access_token}  ${refresh_token}  ${filter}=${EMPTY}  ${pages}=1   ${limit}=50    ${sortBy}=supplier_name   ${sortType}=asc
    Create Session   astra_session  ${SERVICE_API}
    &{headers}=  Create Dictionary      Content-Type=application/json   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    &{params}=   Create Dictionary      pages=${pages}   limit=${limit}     filter=${filter}    sortBy=${sortBy}     sortType=${sortType}
    ${resp}=     Get Request  astra_session     ${supplier_list_url}     headers=${headers}    params=${params}    timeout=1
    [Return]    ${resp}

PUT Supplier Status API
    [Arguments]      ${access_token}  ${refresh_token}  ${supplier_id}  ${status}=true
    Create Session   astra_session     ${SERVICE_API}
    &{headers}=  Create Dictionary      Content-Type=application/json   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    &{data}=    Create Dictionary   supplierStatus=${status}
    ${resp}=     PUT Request  astra_session     ${supplier_url}/${supplier_id}     headers=${headers}  data=${data}
    [Return]    ${resp}

GET Supplier General Data API
    [Arguments]      ${access_token}   ${refresh_token}  ${type}
    Create Session  astra_session  ${SERVICE_API}
    &{headers}=  Create Dictionary      Content-Type=application/json   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    &{params}=   Create Dictionary      type=${type}
    ${resp}=     Get Request  astra_session     ${supplier_general_data_url}     headers=${headers}    params=${params}
    [Return]    ${resp}

Verify Get Supplier List API Resposne Number of Items As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${items}        Get Value From Json    ${resp.json()}   $.data.items
    ${actual_val}   Get Length    ${items[0]}
    Should Be Equal As Strings  ${actual_val}   ${expectd_val}

Verify Get Supplier List API Sorting Result By Supplier Main Sap Code As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${count}=   Get Value From Json	  ${resp.json()}	$..data.toItem
    :FOR  ${index}  IN RANGE  0   ${count[0]}
    \   ${actual_val}    Get Value From Json   ${resp.json()}   $..data.items[${index}].mainSapCode
    \   Should Be Equal    ${actual_val}    ${expectd_val[${index}][0]}

Verify Get Supplier List API Sorting Result By Supplier Name As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${count}=   Get Value From Json	  ${resp.json()}	$..data.toItem
    :FOR  ${index}  IN RANGE  0   ${count[0]}
    \   ${actual_val}    Get Value From Json   ${resp.json()}   $..data.items[${index}].supplierName
    \   Should Be Equal    ${actual_val[0]}    ${expectd_val[${index}][0]}

Verify Get Supplier List API Return Supplier Name By Main Sap Code As Expected
    [Arguments]     ${resp}     ${main_sap_code}    ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..*[?(@.mainSapCode==${main_sap_code})].supplierName  ${expectd_val}

Verify Get Supplier List API Return Supplier Status By Main Sap Code As Expected
    [Arguments]     ${resp}     ${main_sap_code}    ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..*[?(@.mainSapCode==${main_sap_code})].status  ${expectd_val}

Verify Get Supplier List API Return Supplier Default Percent Markup By Main Sap Code As Expected
    [Arguments]     ${resp}     ${main_sap_code}    ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..*[?(@.mainSapCode==${main_sap_code})].defaultPercentMarkup  ${expectd_val}

Verify Get Supplier List API Return Supplier Data By Main Sap Code As Expected
    [Arguments]     ${resp}     ${main_sap_code}    ${name}   ${mark_up}=10.0    ${status}=True
    Verify Get Supplier List API Return Supplier Name By Main Sap Code As Expected  ${resp}    ${main_sap_code}    ${name}
    Verify Get Supplier List API Return Supplier Status By Main Sap Code As Expected    ${resp}    ${main_sap_code}    ${status}
    Verify Get Supplier List API Return Supplier Default Percent Markup By Main Sap Code As Expected     ${resp}    ${main_sap_code}   ${mark_up}

Verify Get Supplier List API Return Empty Supplier List
    [Arguments]     ${resp}
    Verify API Response Json Value As Expected  ${resp}  $..data.items  []

Verify Update Supplier Status API Return Supplier Status As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.status  ${expectd_val}

Verify Get Supplier List Return Sort By Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.sortBy  ${expectd_val}

Verify Get Supplier List Return Sort Type Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value Ignore Case Alphabet As Expected  ${resp}  $..data.sortType  ${expectd_val}

Verify Get Supplier List Return From Item Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.fromItem  ${expectd_val}

Verify Get Supplier List Return To Item Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.toItem  ${expectd_val}

Verify Get Supplier List Return Current Page Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.page  ${expectd_val}

Verify Get Supplier List Return Total Item Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.totalItem  ${expectd_val}

Verify Get Supplier List Return Total Page Value As Expected
    [Arguments]     ${resp}     ${expectd_val}
    Verify API Response Json Value As Expected  ${resp}  $..data.totalPage  ${expectd_val}

Get Supplier Id By Supplier Main Sap Code
    [Arguments]     ${resp}     ${main_sap_code}
    ${val}=    Get Value From Json	  ${resp.json()}	 $..*[?(@.mainSapCode==${main_sap_code})].supplierId
    [Return]  ${val[0]}

Verify Get Supplier General Data Return Supplier Type Title As Expected
    [Arguments]     ${resp}     ${supplier_type_id}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $..*[?(@.supplierTypeID==${supplier_type_id})].title  ${expected_val}

Verify Get Supplier General Data Return Country As Expected
    [Arguments]     ${resp}     ${country_id}     ${expected_val_en}    ${expected_val_th}=null
    Verify API Response Json Value As Expected  ${resp}   $..*[?(@.countryId==${country_id})].countryEn  ${expected_val_en}
    Verify API Response Json Value As Expected  ${resp}   $..*[?(@.countryId==${country_id})].countryTh  ${expected_val_th}

Verify Get Supplier General Data Return Supplier Payment Type As Expected
    [Arguments]     ${resp}     ${type_id}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}   $..*[?(@.supplierPaymentTypeId==${type_id})].title  ${expected_val}

Verify Get Supplier General Data Return Bank As Expected
    [Arguments]     ${resp}     ${bank_id}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}   $..*[?(@.bankId==${bank_id})].title  ${expected_val}

Verify Get Supplier General Data Return Currency As Expected
    [Arguments]     ${resp}     ${currency_id}     ${expected_val_code}     ${expected_val_title}
    Verify API Response Json Value As Expected  ${resp}   $..*[?(@.currencyId==${currency_id})].currencyCode  ${expected_val_code}
    Verify API Response Json Value As Expected  ${resp}   $..*[?(@.currencyId==${currency_id})].title  ${expected_val_title}

Verify Supplier Type Sorting As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${count}=   Get Length	  ${resp.json()}
    :FOR  ${index}  IN RANGE  0   ${count}
    \   ${actual_val}    Get Value From Json	 ${resp.json()}   $..data.supplierType[${index}].supplierTypeID
    \   Should Be Equal    ${actual_val[0]}    ${expectd_val[${index}][0]}

Verify Supplier Payment Type Sorting As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${json}     To Json  ${resp.content}
    ${count}=   Get Length	  ${resp.json()}
    :FOR  ${index}  IN RANGE  0   ${count}
    \   ${actual_val}    Get Value From Json	 ${resp.json()}   $..data.supplierPaymentType[${index}].supplierPaymentTypeId
    \   Should Be Equal    ${actual_val[0]}    ${expectd_val[${index}][0]}

Verify Bank List Sorting As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${json}     To Json  ${resp.content}
    ${count}=   Get Length	  ${resp.json()}
    :FOR  ${index}  IN RANGE  0   ${count}
    \   ${actual_val}    Get Value From Json	 ${resp.json()}   $..data.bank[${index}].title
    \   Should Be Equal    ${actual_val[0]}    ${expectd_val[${index}][0]}

Verify Currency List Sorting As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${json}     To Json  ${resp.content}
    ${count}=   Get Length	  ${resp.json()}
    :FOR  ${index}  IN RANGE  0   ${count}
    \   ${actual_val}    Get Value From Json	 ${resp.json()}   $..data.currency[${index}].currencyCode
    \   Should Be Equal    ${actual_val[0]}    ${expectd_val[${index}][0]}

Verify Country List Sorting As Expected
    [Arguments]     ${resp}     ${expectd_val}
    ${json}     To Json  ${resp.content}
    ${count}=   Get Length	  ${resp.json()}
    :FOR  ${index}  IN RANGE  0   ${count}
    \   ${actual_val}    Get Value From Json	 ${resp.json()}   $..data.country[${index}].countryEn
    \   Should Be Equal    ${actual_val[0]}    ${expectd_val[${index}][0]}

POST Create Update Supplier API
    [Arguments]     ${access_token}  ${refresh_token}   ${json_file_path}   ${doc_file_path}=NONE
    ${json_name}    Fetch From Right    ${json_file_path}   /
    ${doc_name}     Run Keyword If  '${doc_file_path}' != 'NONE'    Fetch From Right    ${doc_file_path}    /
    ${doc_type}     Run Keyword If  '${doc_file_path}' != 'NONE'    Fetch From Right    ${doc_name}         .
    Create Session  astra_session  ${SERVICE_API}
    &{files}    Run Keyword If  '${doc_file_path}' != 'NONE'    Evaluate    {'supplier': ('${json_name}', open('${json_file_path}', 'rb'), 'application/json'), 'documents': ('${doc_name}', open('${doc_file_path}', 'rb'), 'image/${doc_type}')}
    ...         ELSE        Evaluate    {'supplier': ('${json_name}', open('${json_file_path}', 'rb'), 'application/json')}
    &{headers}  Create Dictionary   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    ${resp}     POST Request  astra_session     ${supplier_url}    headers=${headers}  files=${files}
    [Return]    ${resp}

POST Create Update Supplier With Multiple Documents API
    [Arguments]     ${access_token}  ${refresh_token}   ${json_file_path}   ${doc_file_path_list}
    ${json_name}    Fetch From Right    ${json_file_path}   /
    ${count_doc}    Get Length   ${doc_file_path_list}
    ${doc_list}     Set Variable
    :FOR    ${index}    IN RANGE    0   ${count_doc}
    \   ${doc_name}     Fetch From Right    ${doc_file_path_list[${index}]}    /
    \   ${doc_type}     Fetch From Right    ${doc_name}     .
    \   ${doc_list}     Catenate    SEPARATOR=,   ${doc_list}   ('documents', ('${doc_name}', open('${doc_file_path_list[${index}]}', 'rb'), 'image/${doc_type}'))
    ${files}    Evaluate    [('supplier', ('${json_name}', open('${json_file_path}', 'rb'), 'application/json')) ${doc_list}]
    &{headers}  Create Dictionary   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    Create Session  astra_session  ${SERVICE_API}
    ${resp}     POST Request  astra_session     ${supplier_url}    headers=${headers}  files=${files}
    [Return]    ${resp}

GET Supplier Detail API
    [Arguments]     ${access_token}  ${refresh_token}   ${supplier_id}
    Create Session  astra_session  ${SERVICE_API}
    &{headers}  Create Dictionary   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    ${resp}     GET Request  astra_session     ${supplier_url}/${supplier_id}    headers=${headers}
    [Return]    ${resp}

DELETE Supplier API
    [Arguments]     ${access_token}  ${refresh_token}   ${supplier_id}
    Create Session  astra_session  ${SERVICE_API}
    &{headers}  Create Dictionary   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    ${resp}     DELETE Request  astra_session     ${supplier_url}/${supplier_id}    headers=${headers}
    [Return]    ${resp}

GET Supplier Document API
    [Arguments]     ${access_token}  ${refresh_token}   ${document_id}  ${action}
    Create Session  astra_session  ${SERVICE_API}
    &{headers}  Create Dictionary   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    &{params}   Create Dictionary   id=${document_id}   action=${action}
    ${resp}     GET Request  astra_session     ${supplier_document_url}    headers=${headers}   params=${params}
    [Return]    ${resp}

Verify Get Supplier Detail API Return Error Code As Expected
    [Arguments]     ${resp}     ${expected_val}
    Verify API Response Json Value As Expected    ${resp}   $.errors[0].code    ${expected_val}

Verify Get Supplier Detail API Return Error Message As Expected
    [Arguments]     ${resp}     ${expected_val}
    Verify API Response Json Value As Expected    ${resp}   $.errors[0].message    ${expected_val}

Verify Get Supplier Detail API Return Supplier Type Id As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.supplierTypeId    ${expected_val}

Verify Get Supplier Detail API Return Supplier Type Title As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.supplierTypeTitle    ${expected_val}

Verify Get Supplier Detail API Return Country Id As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.countryId    ${expected_val}

Verify Get Supplier Detail API Return Country Title As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.countryTitle    ${expected_val}

Verify Get Supplier Detail API Return Supplier Name As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.supplierName    ${expected_val}

Verify Get Supplier Detail API Return Supplier Address As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.supplierAddress    ${expected_val}

Verify Get Supplier Detail API Return Supplier Tax Name As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.supplierTaxName    ${expected_val}

Verify Get Supplier Detail API Return Supplier Tax Id As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.supplierTaxId    ${expected_val}

Verify Get Supplier Detail API Return Supplier Tax Address As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.supplierTaxAddress    ${expected_val}

Verify Get Supplier Detail API Return Not Follow Up Tax Invoice As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.notFollowUpTaxInvoice    ${expected_val}

Verify Get Supplier Detail API Return Email Reservation List As Expected
    [Arguments]   ${resp}   ${expected_val_list}
    ${actual_val_list}    Get Value From Json     ${resp.json()}     $..data.supplierInformation.emailReservation
    Sort List   ${expected_val_list}
    Sort List   ${actual_val_list[0]}
    Should Be Equal As Strings   ${actual_val_list[0]}    ${expected_val_list}

Verify Get Supplier Detail API Return Default Markup As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.defaultMarkUp    ${expected_val}

Verify Get Supplier Detail API Return Include Vat As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.supplierInformation.includeVat    ${expected_val}

Verify Get Supplier Detail API Return Supplier Status As Expected
    [Arguments]    ${resp}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}  $.data.supplierInformation.supplierStatus  ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Payment Type Title By Type Id As Expected
    [Arguments]     ${resp}   ${payment_type_id}   ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].paymentTypeTitle   ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Days By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].days   ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Receiver By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].receiver   ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Credit By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].credit   ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Is Active By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].isActive   ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Note By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].note   ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Account Name By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}     $..*[?(@.paymentTypeId==${payment_type_id[0]})].bankAccount.accountName     ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Account Number By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].bankAccount.accountNo    ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Bank Id By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].bankAccount.bankId    ${expected_val}

Verify Get Supplier Detail API Return Supplier Payment Method - Bank Title By Type Id As Expected
    [Arguments]    ${resp}  ${payment_type_id}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.paymentTypeId==${payment_type_id[0]})].bankAccount.bankTitle   ${expected_val}

Verify Get Supplier Detail API Return Supplier SAP Code - Currency Id By Sap Code As Expected
    [Arguments]    ${resp}   ${sap_code}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.sapCode==${sap_code})].currencyId   ${expected_val}

Verify Get Supplier Detail API Return Supplier SAP Code - Currency Code By Sap Code As Expected
    [Arguments]   ${resp}   ${sap_code}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.sapCode==${sap_code})].currencyCode   ${expected_val}

Verify Get Supplier Detail API Return Supplier SAP Code - Currency Title By Sap Code As Expected
    [Arguments]   ${resp}   ${sap_code}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.sapCode==${sap_code})].currencyTitle   ${expected_val}

Verify Get Supplier Detail API Return Supplier SAP Code - Is Main By Sap Code As Expected
    [Arguments]   ${resp}   ${sap_code}  ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.sapCode==${sap_code})].isMainSapCode   ${expected_val}

Verify Get Supplier Detail API Return Supplier Contact - Position By Contact Name As Expected
    [Arguments]   ${resp}   ${name}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.name=='${name}')].position   ${expected_val}

Verify Get Supplier Detail API Return Supplier Contact - Email By Contact Name As Expected
    [Arguments]    ${resp}   ${name}    ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.name=='${name}')].email    ${expected_val}

Verify Get Supplier Detail API Return Supplier Contact - Phone By Contact Name As Expected
    [Arguments]    ${resp}   ${name}    ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.name=='${name}')].phone    ${expected_val}

Verify Get Supplier Detail API Return Supplier Document or Not as Expected
    [Arguments]    ${reqs}     ${resp}      ${doc_list}
    ${doc_list_size}=   Get Length    ${doc_list}
    Run Keyword If   ${doc_list_size}==0    Verify API Response Json Value As Expected  ${resp}    $.data.supplierDocument    ${doc_list}
    ...    ELSE    Verify Get Supplier Detail API Return Supplier Documents as Expected     ${reqs}     ${resp}    ${doc_list}

Verify Get Supplier Detail API Return Supplier Documents as Expected
   [Arguments]    ${reqs}     ${resp}      ${doc_name}
   Verify API Response Json Value As Expected   ${resp}   $..*[?(@.originalName==‘${doc_name}’)].originalName    ${doc_name}

POST Supplier Ids API
    [Arguments]      ${access_token}  ${refresh_token}  ${supplier_list}    ${timeout}=1
    Create Session   astra_session  ${SERVICE_API}
    &{headers}=      Create Dictionary      Content-Type=application/json   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    &{data}=         Create Dictionary      supplierIds=${supplier_list}
    ${resp}=         Post Request        astra_session      ${supplier_list_url}    headers=${headers}      data=${data}    timeout=${timeout}
    [Return]         ${resp}

Verify POST Supplier Ids API Return Supplier Name By Supplier Id As Expected
    [Arguments]     ${resp}        ${supplier_id}      ${expected_val}
    Verify API Response Json Value As Expected        ${resp}      $..*[?(@.supplierId==${supplier_id})].supplierName   ${expected_val}   false

Verify Supplier Ids API Return Supplier Name List As Expected
   [Arguments]     ${resp}     ${expectd_val}
   Verify API Response Json List As Expected   ${resp}   $..supplierName   ${expectd_val}