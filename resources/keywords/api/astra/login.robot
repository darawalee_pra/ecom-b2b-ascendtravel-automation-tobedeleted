#########################################################################
# Page Object Name : Global keywords for API 
# Writer : Apinya P
# Date : 04-Sep-2018
# Dependencies : N/A
#########################################################################
*** Settings ***
Library 	Collections
Library 	JSONLibrary
Library 	RequestsLibrary
Library 	String

*** Keywords ***
POST Astra Login API
    [Arguments]     ${username}=${USERNAME_ASTRA}     ${password}=${PASSWORD_ASTRA}
    &{headers}=  Create Dictionary      Content-Type=application/json
    Create Session      astra_session     ${SERVICE_API}
    ${resp}=  POST Request  astra_session     ${ASTRA_LOGIN_API}    headers=${headers}    data={"username": "${username}", "password": "${password}"}
    Should Be Equal As Strings        ${resp.status_code}  200
    ${json}=  To Json  ${resp.content}
    ${access_token}=    Get Value From Json	  ${json}	    $..data.token.access_token
    ${refresh_token}=   Get Value From Json   ${json}	    $..data.token.refresh_token
    [Return]    ${access_token[0]}  ${refresh_token[0]}