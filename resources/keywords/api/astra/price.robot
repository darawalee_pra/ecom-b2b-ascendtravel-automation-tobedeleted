*** Settings ***
Library     Selenium2Library
Library     String
Library     RequestsLibrary
Library     JSONLibrary
Library     ${CURDIR}/../../../../Library/excel.py
Library     ${CURDIR}/../../../../Library/requestLibrary.py
Resource    ${CURDIR}/../../Common/common.robot
Resource    ${CURDIR}/../../../Config/${ENV}/config.robot

*** Variables ***
${price_url}              /price/api/v1
${price_allotment_url}    /allotment
${price_rate_url}         /rate

*** Keywords ***
GET Allotment And Rate API
    [Arguments]     ${access_token}  ${refresh_token}   ${room_type_id}     ${from}    ${to}      ${roomConditionId}=${EMPTY}
    Create Session  astra_session  ${SERVICE_API}
    &{headers}  Create Dictionary   x-act-accesstoken=${access_token}    x-act-refreshtoken=${refresh_token}
    &{params}=   Create Dictionary      from=${from}   to=${to}     roomConditionId=${roomConditionId}
    ${resp}     GET Request  astra_session     ${price_url}${price_allotment_url}/${room_type_id}    headers=${headers}     params=${params}    timeout=1
    [Return]    ${resp}

POST Set Allotment API
    [Arguments]     ${access_token}  ${refresh_token}   ${room_type_id}     ${allotment_dict}
    Create Session  astra_session    ${SERVICE_API}  
    &{headers}      Create Dictionary   Content-Type=application/json   x-act-accesstoken=${access_token}   x-act-refreshtoken=${refresh_token}
    ${resp}         POST Request    astra_session      ${price_url}${price_allotment_url}/${room_type_id}   headers=${headers}    data=${allotment_dict}
    [Return]        ${resp}

POST Set Rate API
    [Arguments]     ${access_token}  ${refresh_token}   ${room_condition_id}     ${rate_dict}
    Create Session  astra_session    ${SERVICE_API}  
    &{headers}      Create Dictionary   Content-Type=application/json   x-act-accesstoken=${access_token}   x-act-refreshtoken=${refresh_token}
    ${resp}         POST Request    astra_session      ${price_url}${price_rate_url}/${room_condition_id}   headers=${headers}    data=${rate_dict}
    [Return]        ${resp}

Verify Get Allotment API Return Default Percent Margin As Expected
    [Arguments]   ${resp}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.defaultPercentMargin   ${expected_val}

Verify Get Allotment API Return Allotment By Date As Expected
    [Arguments]   ${resp}   ${date}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.date=='${date}')].allotment   ${expected_val}

Verify Get Rate API Return Currency Code As Expected
    [Arguments]   ${resp}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $.data.currencyCode   ${expected_val}

Verify Get Rate API Return Selling Price By Date As Expected
    [Arguments]   ${resp}   ${date}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.date=='${date}')].rate.sellingPrice   ${expected_val}     

Verify Get Rate API Return Cost By Date As Expected
    [Arguments]   ${resp}   ${date}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.date=='${date}')].rate.cost   ${expected_val}     

Verify Get Rate API Return Percent Margin By Date As Expected
    [Arguments]   ${resp}   ${date}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.date=='${date}')].rate.percentMargin   ${expected_val}    

Verify Get Rate API Return Rack Price By Date As Expected
    [Arguments]   ${resp}   ${date}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.date=='${date}')].rate.rackPrice   ${expected_val}    

Verify Get Rate API Return Promo Code Ref By Date As Expected
    [Arguments]   ${resp}   ${date}     ${expected_val}
    Verify API Response Json Value As Expected  ${resp}    $..*[?(@.date=='${date}')].rate.promoCodeRef   ${expected_val}