#########################################################################
# Page Object Name : Global keywords for API 
# Writer : Darawalee P
# Date : 16-Aug-2018
# Dependencies : N/A
#########################################################################
*** Settings ***
Library 	Collections
Library 	DatabaseLibrary
Library 	DateTime
Library 	JSONLibrary
Library 	RequestsLibrary
Library 	String

*** Keywords ***

##### HTTP STATUS CODES ASSERTION #####
Response Status Should Be 200 OK
	[Arguments]	${response}
	Should Be Equal As Integers 	200 	${response.status_code}

Response Status Should Be 201 Created
	[Arguments]	${response}
	Should Be Equal As Integers 	201 	${response.status_code}

Response Status Should Be 204 No Content
	[Arguments]	${response}
	Should Be Equal As Integers 	204 	${response.status_code}

Response Status Should Be 400 Bad Request
	[Arguments]	${response}
	Should Be Equal As Integers 	400 	${response.status_code}
	Log to console 	${response.json()}

Response Status Should Be 401 Unauthorized
	[Arguments]	${response}
	Should Be Equal As Integers 	401 	${response.status_code}

Response Status Should Be 404 Not Found
	[Arguments]	${response}
	Should Be Equal As Integers 	404 	${response.status_code}

Response Status Should Be 403 Forbidden
	[Arguments]	${response}
	Should Be Equal As Integers 	403 	${response.status_code}
	
Response Status Should Be 405 Method Not Allowed
	[Arguments]	${response}
	Should Be Equal As Integers 	405 	${response.status_code}

Response Status Should Be 409 Conflict
	[Arguments]	${response}
	Should Be Equal As Integers 	409 	${response.status_code}

Response Status Should Be 500 Internal Server Error
	[Arguments]	${response}
	Should Be Equal As Integers 	500 	${response.status_code}

##### VERIFY API RESPONSE VALUES #####

Verify API Response Json Value As Expected
    [Arguments]     ${resp}     ${json_path}    ${expected_val}    ${ignore_case}=true
    ${actual_val}=    Get Value From Json	  ${resp.json()}	    ${json_path} 
    Should Be Equal As Strings  ${actual_val[0]}  ${expected_val}   ignore_case=${ignore_case}   

Verify API Response Json Value Ignore Case Alphabet As Expected
    [Arguments]     ${resp}     ${json_path}    ${expected_val}
    ${actual_val}=    Get Value From Json	  ${resp.json()}	    ${json_path} 
    Should Be Equal As Strings  ${actual_val[0]}  ${expected_val}   ignore_case=true

Verify API Response Json List As Expected
    [Arguments]     ${resp}     ${json_path}    ${expected_list}
    ${actual_list}=    Get Value From Json	  ${resp.json()}	    ${json_path} 
    Lists Should Be Equal  ${actual_list}  ${expected_list} 

Verify API Return Header Content-Type As Expected
    [Arguments]     ${resp}     ${expected_val}
    ${actual_val}   Get From Dictionary    ${resp.headers}    Content-Type
    Should Be Equal As Strings  ${actual_val}   ${expected_val}

Verify API Return Header Content-Disposition As Expected
    [Arguments]     ${resp}     ${expected_val}
    ${actual_val}   Get From Dictionary    ${resp.headers}    Content-Disposition
    Should Be Equal As Strings  ${actual_val}   ${expected_val}