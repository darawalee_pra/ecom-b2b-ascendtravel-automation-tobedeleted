*** Settings ***
Library     Selenium2Library
Library     RequestsLibrary
Library     DatabaseLibrary
Library     DateTime
Library     openpyxl
Library     Collections
Library     HttpLibrary.HTTP
Library     BuiltIn
Library     String
Library     JSONLibrary
Resource    ${CURDIR}/../../Config/${ENV}/config.robot

*** Variables ***
@{chrome_arguments}    --disable-infobars    --headless    --disable-gpu
${UI}=    YES

*** Keywords ***
Scroll To Top
     Execute Javascript    window.scrollTo(0, 0)

Scroll To Bottom
    Execute Javascript     window.scrollTo(0,document.body.scrollHeight);

Open Browser To Landing Page
	[Arguments]    ${BROWSER}=chrome

	Run Keyword If    '${UI}'=='YES'    Open browser    ${FRONTEND_BASE_URL}    ${BROWSER}
    Run Keyword If    '${UI}'=='NO'     Open Browser without UI

Open Browser without UI
    ${chrome_options}=    Set Chrome Options
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Go to  ${FRONTEND_BASE_URL}

Change Resolution To Desktop
    Set Window Size    1205    800

Change Resolution To Tablet
    Set Window Size    834    1112

Change Resolution To Mobile
    Set Window Size    375    667

Connect ACT Database
    [Arguments]     ${DB_NAME}=${DB_NAME}
    connect to database   pymssql    ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOSTNAME}    ${DB_PORT}

Connect Authorization Database
    connect to database   pymssql    ${DB_NAME_AAD_AUTHZ}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOSTNAME}    ${DB_PORT}

Connect Authentication Database
    connect to database   pymssql    ${DB_NAME_AAD_AUTHN}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOSTNAME}    ${DB_PORT}

Close Connect Database
    Disconnect from Database

Get Total Page Number
    [Arguments]     ${total_items}      ${page_size}
    ${total_page_no}=    Evaluate    ${total_items}/${page_size}
    ${total_page_no}=    Convert To Number      ${total_page_no}    1
    ${string}=           Convert To String      ${total_page_no}
    ${output}=           Fetch From Right       ${string}   .
    ${result}   RUN KEYWORD IF  '${output}' <= '4'   Round Up      ${total_page_no}
    ...         ELSE             Convert To Number      ${total_page_no}      0
    ${result}=  Convert To Integer  ${result}
    [Return]    ${result}

Round Up
    [Arguments]     ${number}
    ${output}=      Convert To Number      ${number}      0
    ${result}=      Evaluate    ${output}+1
    [Return]        ${result}

Generate Random Number
    [Arguments]     ${length}=8
    ${numbers}=     Generate Random String	${length}	[NUMBERS]
    [Return]        ${numbers}

Generate Random Text
    [Arguments]     ${length}=10
    ${text}=        Generate Random String  ${length}   [LETTERS]
    [Return]        ${text}