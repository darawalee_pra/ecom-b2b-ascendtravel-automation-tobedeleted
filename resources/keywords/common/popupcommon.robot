#==============================POPUP CANCEL==============================#
Verify Confirm To Leave Without Finish Popup Display
            [Arguments]        ${expected_popup_cancel_name}
            Wait Until Page Contains Element          ${position_popup_cancel_name}        10s
            ${actual}=               Get Text                ${position_popup_cancel_name}
            Should Be True          '${actual}'=='${expected_popup_cancel_name}'

Click Cancel Button On Popup
            Wait Until Element Is Visible       ${btn_popup_cancel_dialog}       10s
            Click Element       ${btn_popup_cancel_dialog}

Click Confirm Button On Popup
            Wait Until Element Is Visible       ${btn_popup_comfirm_dialog}       10s
            Click Element       ${btn_popup_comfirm_dialog}


#==============================POPUP LEAVE==============================#
Verify Confirm To Leave This Page Popup Display
            [Arguments]        ${expected_popup_leave_name}
            Wait Until Page Contains Element          ${position_popup_leave_name}        10s
            ${actual}=               Get Text                ${position_popup_leave_name}
            Should Be True          '${actual}'=='${expected_popup_leave_name}'

Click Stay On This Page Button
            Wait Until Element Is Visible       ${btn_popup_cancel_dialog}       10s
            Click Element       ${btn_popup_cancel_dialog}
  
Click Leave This Page Button
            Wait Until Element Is Visible       ${btn_popup_comfirm_dialog}       10s
            Click Element       ${btn_popup_comfirm_dialog}