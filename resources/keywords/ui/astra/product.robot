*** Settings ***
Library 		Selenium2Library
Library 		String
Library         Collections
Library         DateTime
Library         OperatingSystem
Library         ${CURDIR}/../../../../Library/common.py


*** Variables ***
#login
${btn_astra_login}                                  id=login
${input_username_astra}                             id=username
${input_password_astra}                             id=password
${img_astra_home_logo}                              id=stackLogo

#menu
${btn_side_menu_product}                            id=menu-Product
${btn_side_menu_product_list}                       
${btn_side_menu_create_product}                     id=menu-Create Product

#product information
${ddl_product_catagory}                             id=ddl-catagory
${txt_product_name_th}                              id=nameTh
${txt_product_name_en}                              id=nameEn
${txt_product_address_th}                           id=addressTh
${txt_product_address_en}                           id=addressEn
${txt_product_latitude}                             id=latitude
${txt_product_longitude}                            id=longitude
${ddl_product_destination}                          id=ddl-destination
${ddl_search_destination}                           id=filterInput-destination
${ddl_destination}                                  //a[@id='item-destination-INDEX']
${txt_product_contact}                              id=contactNo
${ddl_product_hotel_star}                           //select[@id='hotelStar'and contains(.,'Select Hotel Star')]
${input_product_priority}                           id=touchspin-input-priority
${btn_product_priority_decrease}                    id=touchspin-decrease-priority
${btn_product_priority_increase}                    id=touchspin-increase-priority
${input_product_checkin_time}                       id=checkInTime
${input_product_checkout_time}                      id=checkOutTime
${rdb_product_free_sell}                            id=free-sell
${rdb_product_allotment_set}                        id=allotment-set
${txt_search_keyword}                               id=input-text-items
${tgl_product_status}                               id=btn-toggle-product-status-0

#supplier markup
${ddl_product_supplier}                             id=ddl-0
${input_supplier}                                   id=filterInput-0
${ddl_select_supplier}                              //a[@id='item-0-INDEX']
${input_supplier_markup}                            id=touchspin-input-markup-INDEX
${btn_supplier_markup_decrease}                     id=touchspin-decrease-markup-INDEX
${btn_supplier_markup_increase}                     id=touchspin-increase-markup-INDEX
${tgl_supplier_status}                              id=btn-toggle-markup-INDEX
${btn_add_supplier}                                 id=supplier-list-btn-create-supplier
${btn_del_supplier}                                 id=btn-delete-INDEX

#default cancellation policy
${rdb_non_refundable}                               id=non-refundable

${rdb_allow_cancel_checkin_date}                    id=allow-chcekin-date
${input_cancel_before_day}                          id=touchspin-input-policy-days-INDEX
${btn_cancel_before_day_decrease}                   id=touchspin-decrease-policy-days-INDEX
${btn_cancel_before_day_increase}                   id=touchspin-increase-policy-days-INDEX
${ddl_cancellation_policy_day}                      id=policy-charge-type-days-INDEX
${input_charge_amount_day}                          id=policy-amount-days-INDEX
${ddl_charge_unit_day}                              id=policy-charge-unit-days-INDEX
${input_no_show_amount_day}                         id=input-no-show-charge-amount-days
${ddl_no_show_unit_day}                             id=input-no-show-charge-unit-days
${btn_add_cancellation_day}                         id=btn-add-cancellation-day
${btn_del_cancellation_day}                         id=btn-delete-days-INDEX

${rdb_allow_cancel_checkin_time}                    id=allow-checkin-time
${input_cancel_before_time}                         id=input-policy-time-INDEX
${ddl_cancellation_policy_time}                     id=policy-charge-type-time-INDEX
${ddl_charge_amount_time}                           id=policy-amount-time-INDEX
${ddl_charge_unit_time}                             id=policy-charge-unit-time-INDEX
${input_no_show_amount_time}                        id=input-no-show-charge-amount-time
${ddl_no_show_unit_time}                            id=input-no-show-charge-unit-time
${btn_add_cancellation_time}                        id=btn-add-cancellation-time
${btn_del_cancellation_time}                        id=btn-delete-time-INDEX

${btn_cancel_product}                               id=btn-product-management-cancel
${btn_draft_product}                                id=btn-product-management-draft
${first_index}                                      0

#specific cancellation policy
${expand_section_specific}                          //div/a[@id='btn-hidden-session']
${btn_add_specific_cancellation}                    id=btn-add-specific
${datepicker_period_form}                           id=period-from-date
${datepicker_period_to}                             id=period-to-date
${lbl_set_specific_day}                             //a/span[@id='show']
${chk_specific_day}                                 id=chk-INDEX
${chk_except_by_period}                             id=selected-except-by-period
${datepicker_except_from}                           id=except-period-from-0-date
${datepicker_except_to}                             id=except-period-to-0-date
${chk_except_by_date}                               id=selected-except-by-date
${datepicker_except_date}                           id=except-date-0-date
${rdb_specific_non_refundable}                      id=specific-non-refundable

id=specific-allow-chcekin-date
${input_specific_cancel_before_day}                 
${btn_cancel_before_day_decrease_specific}          
${btn_cancel_before_day_increase_specific}
${ddl_cancellation_policy_day_specific}
${input_charge_amount_day_specific} 
${btn_del_cancellation_day_specific}

id=specific-allow-checkin-time
${ddl_cancellation_policy_time_specific}            
${ddl_charge_amount_time_specific}
${ddl_charge_unit_time_specific}
${btn_del_cancellation_time_specific}

${btn_save_specific_cancellation}                   id=btn-specific-add
${btn_cancel_specific_cancellation}                 id=btn-specific-cancel
${btn_close_details}                                //div/div/button[@class='btn btn-light' and @id='btn-specific-cancel']


#date picker
${sel_date_picker_year}                             //ngb-datepicker//ngb-datepicker-navigation-select/select[@class='custom-select'][2]
${sel_date_picker_month}                            //ngb-datepicker//ngb-datepicker-navigation-select/select[@class='custom-select'][1]
${div_date_picker_weeks}                            //ngb-datepicker//div[@class='ngb-dp-week' or @class='ngb-dp-week ng-star-inserted']
${div_date_picker_days}                             //div[@class='ngb-dp-day' or @class='ngb-dp-day ng-star-inserted']

#product content
${expand_section_product_content}                   //a/i[@class='font-weight-bold ft-plus']
${input_hotel_overview_th}                          //textarea[@id='hotel-overview-th']
${input_hotel_overview_en}                          //textarea[@id='hotel-overview-en']
${browse_file}                                      id=browse-file-photo-content
${btn_upload_upload}                                id=photo-management-upload
${btn_upload_cancel}                                id=photo-management-cancel

#contact
${txt_contact_name}                                 id=manager-owner-input-name-0
${txt_contact_email}                                id=manager-owner-input-email-0
${txt_contact_phone}                                id=manager-owner-input-phone-0
${ddl_contact_position}                             id=manager-owner-select-position-0
${btn_add_new_contact}                              id=manager-owner-btn-create-contact

*** Keywords ***

# Go To ASTRA And Login
#     [Arguments]    ${username}     ${password}    ${url_version}=${ASTRA_BASE_URL}
#     Go To ASTRA Page            ${url_version}
#     Input Username On ASTRA Landing Page       ${username}
#     Input Password On ASTRA Landing Page       ${password}
#     Click Login On ASTRA Landing Page
#     Wait Until Page Contains Element           ${img_astra_home_logo}      ${TIMEOUT}
#     Sleep       2s

# Go To ASTRA Page
#     [Arguments]    ${url_version}=${ASTRA_BASE_URL}    ${BROWSER}=chrome
#     Open browser    ${url_version}    ${BROWSER}
#     Wait Until Element Is Visible    ${btn_astra_login}

# Input Username On ASTRA Landing Page
#     [Arguments]    ${username}
#     Input Text     ${input_username_astra}    ${username}

# Input Password On ASTRA Landing Page
#     [Arguments]    ${password}
#     Input Text     ${input_password_astra}    ${password}

# Click Login On ASTRA Landing Page
#     Click Element    ${btn_astra_login}

Click Product Menu
    Wait Until Element Is Visible       ${btn_side_menu_product}           ${TIMEOUT}
    Click Element                       ${btn_side_menu_product}

Go To Product List
    Wait Until Element Is Visible       ${btn_side_menu_list_product}      ${TIMEOUT}                      
    Click Element                          

Go To Create Product
    Wait Until Element Is Visible       ${btn_side_menu_create_product}    ${TIMEOUT}         
    Click Element                       ${btn_side_menu_create_product} 

Verify Product Catagory Is Hotel
    [Arguments]    ${expect}
    Wait Until Element Is Visible      ${ddl_product_catagory}
    ${ddl_product_catagory}=        Get Text               ${ddl_product_catagory}
    ${ddl_product_catagory}         Strip String           ${ddl_product_catagory}
    Should Be Equal             ${ddl_product_catagory}    ${expect}

Input Product Information
    [Arguments]    ${product_name_th}   ${product_name_en}     ${address_th}  
    ...            ${address_en}        ${latitude}            ${longitude}           
    ...            ${destination_name}  ${contact_num}       
    ...            ${hotel_star}        ${priority}            ${check_in_aft}
    ...            ${check_out_bef}     ${allotment_type}      ${keyword_list}

    Input Product Name          ${product_name_th}     TH
    Input Product Name          ${product_name_en}     EN 
    Input Address               ${address_th}          TH
    Input Address               ${address_en}          EN
    Input Latitude              ${latitude}
    Input Longitude             ${longitude}
    Select Destination          ${destination_name}
    Input Contact Number        ${contact_num}
    Select Hotel Star           ${hotel_star}
    Input Priority              ${priority}
    Input Checkin Time After    ${check_in_aft}
    Input Checkout Time Before  ${check_out_bef}
    Select Allotment Type       ${allotment_type}
    Input Search Keyword        ${keyword_list}
    Select Product Status

Input Product Name
    [Arguments]    ${value}    ${lang}

    ${locator}=    Run Keyword If    '${lang}'=='TH'    Set Variable    ${txt_product_name_th}
    ...            ELSE                                 Set Variable    ${txt_product_name_en}
    Input Text    ${locator}    ${value}

Input Address
    [Arguments]    ${value}    ${lang}

    ${locator}=    Run Keyword If    '${lang}'=='TH'    Set Variable    ${txt_product_address_th}
    ...            ELSE                                 Set Variable    ${txt_product_address_en}
    Input Text    ${locator}    ${value}

Input Latitude
    [Arguments]    ${value}
    Input Text    ${txt_product_latitude}    ${value}

Input Longitude
    [Arguments]    ${value}
    Input Text     ${txt_product_longitude}    ${value}

Select Destination
    [Arguments]    ${destination_name}   ${index}=2968

    Click Element      ${ddl_product_destination}
    Click Element      ${ddl_search_destination}
    Input Text         ${ddl_search_destination}      ${destination_name}
    ${suggestion_locator}=     Replace String    ${ddl_destination}    INDEX    ${index}
    Click Element      ${suggestion_locator}

Input Contact Number
    [Arguments]    ${value}
    Input Text    ${txt_product_contact}    ${value}
    
Select Hotel Star
    [Arguments]   ${value}
    Select From List   ${ddl_product_hotel_star}    ${value}

Input Priority
    [Arguments]    ${value}

    Input Text         ${input_product_priority}            ${value}
    Click Element      ${btn_product_priority_decrease}                   
    Click Element      ${btn_product_priority_increase}

Input Checkin Time After
    [Arguments]    ${value}
    Press Key      ${input_product_checkin_time}     ${value}

Input Checkout Time Before
    [Arguments]    ${value}
    Press Key      ${input_product_checkout_time}      ${value}

Select Allotment Type
    [Arguments]    ${value}

    ${locator}=      Run Keyword If    '${value}'=='Free Sell'    Set Variable    ${rdb_product_free_sell}
    ...              ELSE                                         Set Variable    ${rdb_product_allotment_set}

    Click Element    ${locator}

Input Search Keyword
    [Arguments]    ${keyword_list}

    ${length}=  Get Length  ${keyword_list}

    :FOR  ${index}  IN RANGE  0  ${length}
    \  ${value}=    Get From List            ${keyword_list}    ${index}
    \  Press Key    ${txt_search_keyword}    ${value}
    \  Press Key    ${txt_search_keyword}    \\13

Select Product Status
    Click Element      ${tgl_product_status} 

Select Supplier Markup
    [Arguments]    ${search_supplier_name}    ${index}
    Click Element      ${ddl_product_supplier}
    Click Element      ${input_supplier}
    Input Text         ${input_supplier}    ${search_supplier_name}
    ${suggestion_locator}=     Replace String    ${ddl_select_supplier}   INDEX    ${index}
    Click Element      ${suggestion_locator}

Default Cancellation - Non-Refundable
    Click Element      ${rdb_non_refundable}

Default Cancellation - Before Checkin Date - Charge
    Click Element      ${rdb_allow_cancel_checkin_date}
    ${input_cancel_before_day}=        Replace String    ${input_cancel_before_day}           INDEX    ${first_index}
    ${cancel_before_day_decrease}=     Replace String    ${btn_cancel_before_day_decrease}    INDEX    ${first_index}
    ${cancel_before_day_increase}=     Replace String    ${btn_cancel_before_day_increase}    INDEX    ${first_index}
    ${suggestion_locator}=             Replace String    ${ddl_cancellation_policy_day}       INDEX    ${first_index}
    ${charge_amount_day}=              Replace String    ${input_charge_amount_day}           INDEX    ${first_index}
    ${del_cancellation_day}=           Replace String    ${btn_del_cancellation_day}          INDEX    1

    Click Element         ${input_cancel_before_day}
    Input Text            ${input_cancel_before_day}    255
    Click Element         ${cancel_before_day_decrease}                   
    Click Element         ${cancel_before_day_increase}
    ${cancel_day}=    Get Value    ${input_cancel_before_day}
    Should Be Equal As Integers    ${cancel_day}        255  
    
    #select cancellation policy
    Select From List      ${suggestion_locator}         Charge
    Click Element         ${charge_amount_day}
    Input Text            ${charge_amount_day}          99

    #no show charge
    Double Click Element  ${input_no_show_amount_day}
    Input Text            ${input_no_show_amount_day}   100

    Click Element         ${ddl_no_show_unit_day}
    Select From List      ${ddl_no_show_unit_day}       Night

    #delete other cancellation policy
    Click Element         ${btn_add_cancellation_day}
    Click Element         ${del_cancellation_day}


Default Cancellation - Before Checkin Date - Is Free Of Charge
    Click Element      ${rdb_allow_cancel_checkin_date}
    ${input_cancel_before_day}=        Replace String    ${input_cancel_before_day}           INDEX    ${first_index}
    ${cancel_before_day_decrease}=     Replace String    ${btn_cancel_before_day_decrease}    INDEX    ${first_index}
    ${cancel_before_day_increase}=     Replace String    ${btn_cancel_before_day_increase}    INDEX    ${first_index}
    ${suggestion_locator}=             Replace String    ${ddl_cancellation_policy_day}       INDEX    ${first_index}
    ${del_cancellation_day}=           Replace String    ${btn_del_cancellation_day}          INDEX    1

    Click Element         ${input_cancel_before_day}
    Input Text            ${input_cancel_before_day}    255
    Click Element         ${cancel_before_day_decrease}                   
    Click Element         ${cancel_before_day_increase}
    ${cancel_day}=    Get Value    ${input_cancel_before_day}
    Should Be Equal As Integers    ${cancel_day}        255  
    
    #select cancellation policy
    Select From List      ${suggestion_locator}    Free of Charge

    #no show charge
    Double Click Element  ${input_no_show_amount_day}
    Input Text            ${input_no_show_amount_day}   99

    Click Element         ${ddl_no_show_unit_day}
    Select From List      ${ddl_no_show_unit_day}       Percent

    #delete other cancellation policy
    Click Element         ${btn_add_cancellation_day}
    Click Element         ${del_cancellation_day}


Default Cancellation - On Checkin Date - Charge
    [Arguments]    ${cancel_before_time}
    Click Element         ${rdb_allow_cancel_checkin_time}
    ${suggestion_locator}=             Replace String    ${ddl_cancellation_policy_time}      INDEX    ${first_index}
    ${charge_amount_time}=             Replace String    ${ddl_charge_amount_time}            INDEX    ${first_index}
    ${sel_charge_unit_time}=           Replace String    ${ddl_charge_unit_time}              INDEX    ${first_index}
    ${del_cancellation_time}=          Replace String    ${btn_del_cancellation_time}         INDEX    1

    #select cancellation policy
    Select From List      ${suggestion_locator}         Charge
    Click Element         ${charge_amount_time}
    Input Text            ${charge_amount_time}         100

    Click Element         ${sel_charge_unit_time}
    Select From List      ${sel_charge_unit_time}       Night

    #no show charge
    Double Click Element  ${input_no_show_amount_time}
    Input Text            ${input_no_show_amount_time}   50

    Click Element         ${ddl_no_show_unit_time}
    Select From List      ${ddl_no_show_unit_time}       Percent

    #delete other cancellation policy
    Click Element         ${btn_add_cancellation_time}
    Click Element         ${del_cancellation_time}

Default Cancellation - On Checkin Date - Is Free Of Charge
    [Arguments]    ${cancel_before_time}
    Click Element      ${radio_allow_cancel_checkin_time}
    ${suggestion_locator}=             Replace String    ${ddl_cancellation_policy_time}      INDEX    1
    ${charge_amount_time}=             Replace String    ${ddl_charge_amount_time}            INDEX    1
    ${input_cancel_before_time}=       Replace String    ${input_cancel_before_time}          INDEX    1
    ${del_cancellation_time}=          Replace String    ${btn_del_cancellation_time}         INDEX    ${first_index}

    Click Element         ${btn_add_cancellation_time}

    #cancel before time
    Press Key             ${input_cancel_before_time}    ${cancel_before_time} 

    #select cancellation policy
    Select From List      ${suggestion_locator}          Free of Charge

    #no show charge
    Double Click Element  ${input_no_show_amount_time}
    Input Text            ${input_no_show_amount_time}   50

    Click Element         ${ddl_no_show_unit_time}
    Select From List      ${ddl_no_show_unit_time}       Night

    #delete other cancellation policy
    Click Element         ${btn_del_cancellation_time}


Expand Section Specific Cancellation
    Wait Until Element Is Visible    ${expand_section_specific}
    Click Element    ${expand_section_specific}

Add Specific Cancellation Policy
    Expand Section Specific Cancellation  
    Wait Until Element Is Visible    ${btn_add_specific_cancellation}
    Click Element    ${btn_add_specific_cancellation}

Set Specific Cancellation Policy Period From
    [Arguments]    ${add_date}
    Input Date On ASTRA From Today          ${datepicker_period_form}                 ${add_date}   

Set Specific Cancellation Policy Period To
    [Arguments]    ${add_date}
    Input Date On ASTRA From Today          ${datepicker_period_to}                   ${add_date}  

########### date picker ###########
Input Date On ASTRA From Today
    [Arguments]    ${txt_datepicker}    ${value}

    ${date}=    	Get Current Date    result_format=%Y-%m-%d
    ${date}=    	Add Time To Date	${date}    ${value} days
    Select Date Picker ASTRA    ${txt_datepicker}    ${date}

Select Date Picker ASTRA
    [Arguments]    ${datepicker}    ${date}

    ${day}    Convert Date    ${date}    result_format=%d
    ${month}    Convert Date    ${date}    result_format=%m
    ${year}    Convert Date    ${date}    result_format=%Y
    ${year}    Convert To Integer    ${year}
    ${month}    Convert To Integer    ${month}
    ${day}    Convert To Integer    ${day}
    ${str_year}    Convert To String    ${year}
    ${str_month}    Convert To String    ${month}
    ${str_day}    Convert To String    ${day}
    Click Element    ${datepicker}
    Select Year In Date Picker ASTRA    ${str_year}
    Select Month In Date Picker ASTRA    ${str_month}
    Select Day In Date Picker ASTRA    ${str_day}

Select Year In Date Picker ASTRA
    [Arguments]    ${year}

    Wait Until Element Is Visible    ${sel_date_picker_year}
    Select From List By Value    ${sel_date_picker_year}    ${year}

Select Month In Date Picker ASTRA
    [Arguments]    ${month}

    Wait Until Element Is Visible    ${sel_date_picker_month}
    Select From List By Value    ${sel_date_picker_month}    ${month}

Select Day In Date Picker ASTRA
    [Arguments]    ${day}
    
    ${index_week}    Set Variable    0
    ${index_day}    Set Variable    0
    ${count_week}    Get Element Count    ${div_date_picker_weeks}
    :FOR    ${index}    IN RANGE    1    ${count_week} + 1
    \    ${index_day}    Get Index Day In Date Picker ASTRA by Week    ${index}    ${day}
    \    ${index_week}    Set Variable If    ${index_day} > 0    ${index}    0
    \    Run Keyword If    ${index_week} > 0    Exit For Loop
    ${locator_select_day}    Set Variable    ${div_date_picker_weeks}[${index_week}]${div_date_picker_days}[${index_day}]
    Wait Until Element Is Visible    ${locator_select_day}
    Click Element    ${locator_select_day}

Get Index Day In Date Picker ASTRA by Week
    [Arguments]    ${week}    ${day}
    ${locator_days}    Set Variable    ${div_date_picker_weeks}[${week}]${div_date_picker_days}
    ${count_day}    Get Element Count    ${locator_days}
    ${select_day}    Set Variable    0
    :FOR    ${index}    IN RANGE    1    ${count_day} + 1
    \    ${locator_day}    Set Variable    ${locator_days}[${index}]
    \    ${is_active}    Check Day Active In Date Picker ASTRA    ${locator_day}
    \    ${text_day}    Get Text    ${locator_day}
    \    ${text_day_active}    Set Variable If    ${is_active}    ${text_day}    ${EMPTY}
    \    ${select_day}    Set Variable If    '${text_day_active}' == '${day}'    ${index}    0
    \    Run Keyword If    ${select_day} > 0    Exit For Loop
    Return From Keyword    ${select_day}

Check Day Active In Date Picker ASTRA
    [Arguments]    ${locator_day}
    Wait Until Element Is Visible    ${locator_day}
    ${class}    Get Element Attribute    ${locator_day}/div    class
    ${status}    Run Keyword And Return Status    Should Not End With    ${class}    outside
    [Return]    ${status}

######################
Click Set Specific Days
    Wait Until Element Is Visible    ${lbl_set_specific_day}
    Click Element    ${lbl_set_specific_day}

Select Days To Specific
    [Arguments]    ${list_day}

    Click Set Specific Days

    ${length}=      Get Length    ${list_day}

    :FOR    ${index}    IN RANGE   0   ${length}
    \  ${day}=    Get From List        ${list_day}  ${index}
    \  ${day}=    Convert To String    ${day}
    \  ${locator}=    Replace String      ${chk_specific_day}    INDEX    ${day}
    \  Click Element   ${locator}


    # \  Run Keyword If    '${day}' == 'Everyday'    Click Element    ${chk_everyday}
    # \  ...    ELSE IF    '${day}' == 'Monday'      Click Element    ${chk_mon}
    # \  ...    ELSE IF    '${day}' == 'Tuesday'     Click Element    ${chk_tue}
    # \  ...    ELSE IF    '${day}' == 'Wednesday'   Click Element    ${chk_wed}
    # \  ...    ELSE IF    '${day}' == 'Thursday'    Click Element    ${chk_thu}
    # \  ...    ELSE IF    '${day}' == 'Friday'      Click Element    ${chk_fri}
    # \  ...    ELSE IF    '${day}' == 'Saturday'    Click Element    ${chk_sat}   
    # \  ...    ELSE IF    '${day}' == 'Sunday'      Click Element    ${chk_sun}

Select Specific Days Except On By Period
    Wait Until Element Is Visible    ${chk_except_by_period}
    Click Element    ${chk_except_by_period}

Set Specific Days Except On By Period From
    [Arguments]    ${add_date}
    Select Specific Days Except On By Period
    Input Date On ASTRA From Today          ${datepicker_except_from}                  ${add_date}  

Set Specific Days Except On By Period To
    [Arguments]    ${add_date}
    Input Date On ASTRA From Today          ${datepicker_except_to}                    ${add_date}  

Select Specific Days Except On By Date
    Wait Until Element Is Visible    ${chk_except_by_date}
    Click Element    ${chk_except_by_date}

Set Specific Days Except On By Date
    [Arguments]    ${add_date}
    Select Specific Days Except On By Date
    Input Date On ASTRA From Today          ${datepicker_except_date}                   ${add_date}

Specific Cancellation - Non-Refundable
    Wait Until Element Is Visible    ${rdb_specific_non_refundable}
    Click Element      ${rdb_specific_non_refundable}

Specific Cancellation - Before Checkin Date - Charge
    Click Element      ${rdb_allow_cancel_checkin_date}
    ${input_cancel_before_day}=        Replace String    ${input_specific_cancel_before_day}           INDEX    ${first_index}
    ${cancel_before_day_decrease}=     Replace String    ${btn_cancel_before_day_decrease_specific}    INDEX    ${first_index}
    ${cancel_before_day_increase}=     Replace String    ${btn_cancel_before_day_increase_specific}    INDEX    ${first_index}
    ${suggestion_locator}=             Replace String    ${ddl_cancellation_policy_day_specific}       INDEX    ${first_index}
    ${charge_amount_day}=              Replace String    ${input_charge_amount_day_specific}           INDEX    ${first_index}
    ${del_cancellation_day}=           Replace String    ${btn_del_cancellation_day_specific}          INDEX    1

    Click Element         ${input_cancel_before_day}
    Input Text            ${input_cancel_before_day}    255
    Click Element         ${cancel_before_day_decrease}                   
    Click Element         ${cancel_before_day_increase}
    ${cancel_day}=    Get Value    ${input_cancel_before_day}
    Should Be Equal As Integers    ${cancel_day}        255  
    
    #select cancellation policy
    Select From List      ${suggestion_locator}         Charge
    Click Element         ${charge_amount_day}
    Input Text            ${charge_amount_day}          99

    #no show charge
    Double Click Element  ${input_no_show_amount_day}
    Input Text            ${input_no_show_amount_day}   100

    Click Element         ${ddl_no_show_unit_day}
    Select From List      ${ddl_no_show_unit_day}       Night

    #delete other cancellation policy
    Click Element         ${btn_add_cancellation_day}
    Click Element         ${del_cancellation_day}


Specific Cancellation - Before Checkin Date - Is Free Of Charge
    Click Element      ${rdb_allow_cancel_checkin_date}
    ${input_cancel_before_day}=        Replace String    ${input_specific_cancel_before_day}           INDEX    ${first_index}
    ${cancel_before_day_decrease}=     Replace String    ${btn_specific_cancel_before_day_decrease}    INDEX    ${first_index}
    ${cancel_before_day_increase}=     Replace String    ${btn_specific_cancel_before_day_increase}    INDEX    ${first_index}
    ${suggestion_locator}=             Replace String    ${ddl_specific_cancellation_policy_day}       INDEX    ${first_index}
    ${del_cancellation_day}=           Replace String    ${btn_specific_del_cancellation_day}          INDEX    1

    Click Element         ${input_cancel_before_day}
    Input Text            ${input_cancel_before_day}    255
    Click Element         ${cancel_before_day_decrease}                   
    Click Element         ${cancel_before_day_increase}
    ${cancel_day}=    Get Value    ${input_cancel_before_day}
    Should Be Equal As Integers    ${cancel_day}        255  
    
    #select cancellation policy
    Select From List      ${suggestion_locator}    Free of Charge

    #no show charge
    Double Click Element  ${input_no_show_amount_day}
    Input Text            ${input_no_show_amount_day}   99

    Click Element         ${ddl_no_show_unit_day}
    Select From List      ${ddl_no_show_unit_day}       Percent

    #delete other cancellation policy
    Click Element         ${btn_add_cancellation_day}
    Click Element         ${del_cancellation_day}


Specific Cancellation - On Checkin Date - Charge
    [Arguments]    ${cancel_before_time}
    Click Element         ${rdb_allow_cancel_checkin_time}
    ${suggestion_locator}=             Replace String    ${ddl_cancellation_policy_time_specific}      INDEX    ${first_index}
    ${charge_amount_time}=             Replace String    ${ddl_charge_amount_time_specific}            INDEX    ${first_index}
    ${sel_charge_unit_time}=           Replace String    ${ddl_charge_unit_time_specific}              INDEX    ${first_index}
    ${del_cancellation_time}=          Replace String    ${btn_del_cancellation_time_specific}         INDEX    1

    #select cancellation policy
    Select From List      ${suggestion_locator}         Charge
    Click Element         ${charge_amount_time}
    Input Text            ${charge_amount_time}         100

    Click Element         ${sel_charge_unit_time}
    Select From List      ${sel_charge_unit_time}       Night

    #no show charge
    Double Click Element  ${input_no_show_amount_time}
    Input Text            ${input_no_show_amount_time}   50

    Click Element         ${ddl_no_show_unit_time}
    Select From List      ${ddl_no_show_unit_time}       Percent

    #delete other cancellation policy
    Click Element         ${btn_add_cancellation_time}
    Click Element         ${del_cancellation_time}

Specific Cancellation - On Checkin Date - Is Free Of Charge
    [Arguments]    ${cancel_before_time}
    Click Element      ${radio_allow_cancel_checkin_time}
    ${suggestion_locator}=             Replace String    ${ddl_cancellation_policy_time_specific}      INDEX    1
    ${charge_amount_time}=             Replace String    ${ddl_charge_amount_time_specific}            INDEX    1
    ${input_cancel_before_time}=       Replace String    ${input_cancel_before_time_specific}          INDEX    1
    ${del_cancellation_time}=          Replace String    ${btn_del_cancellation_time_specific}         INDEX    ${first_index}

    Click Element         ${btn_add_cancellation_time}

    #cancel before time
    Press Key             ${input_cancel_before_time}    ${cancel_before_time} 

    #select cancellation policy
    Select From List      ${suggestion_locator}          Free of Charge

    #no show charge
    Double Click Element  ${input_no_show_amount_time}
    Input Text            ${input_no_show_amount_time}   50

    Click Element         ${ddl_no_show_unit_time}
    Select From List      ${ddl_no_show_unit_time}       Night

    #delete other cancellation policy
    Click Element         ${btn_del_cancellation_time}

Click Add To Save Specific Cancellation
    Wait Until Element Is Visible    ${btn_save_specific_cancellation}
    Click Element    ${btn_save_specific_cancellation}

Click Cancel Specific Cancellation
    Wait Until Element Is Visible    ${btn_cancel_specific_cancellation}
    Click Element    ${btn_cancel_specific_cancellation}

Click To Close Specific Details
    Wait Until Element Is Visible    ${btn_cancel_specific_cancellation} 
    Click Element    ${btn_cancel_specific_cancellation} 

Expand Section Product Content
    Wait Until Element Is Visible    ${expand_section_product_content}
    Click Element    ${expand_section_product_content}

Product Content
    [Arguments]    ${text_th}    ${text_en}
    Expand Section Product Content
    #hotel overview
    Click Element         ${input_hotel_overview_th}
    Input Text            ${input_hotel_overview_th}     ${text_th}
    Click Element         ${input_hotel_overview_en}
    Input Text            ${input_hotel_overview_en}     ${text_en}

Browse Hotel Photo
    [Arguments]           ${photo_base_path}

    Log     [CURDIR]=${CURDIR}
    Choose Multiple Files   ${browse_file}  ${photo_base_path}
    
    Wait Until Element Is Visible       ${btn_upload_upload}      ${TIMEOUT}
    Click Element   ${btn_upload_upload}   

########### facilities ###########

Click Show More Hotel Facilities
    Wait Until Element Is Visible
    Click Element

Select Hotel Type Facilities
    [Arguments]    ${list_facility}

    Click Show More Hotel Facilities

    ${length}=      Get Length    ${list_facility}

    :FOR    ${index}     IN RANGE   0   ${length}
    \  ${facility}=    Get From List       ${list_facility}  ${index}
    \  ${facility}=    Convert To String   ${facility}
    \  ${locator}=     Replace String      ${chk_facility}    INDEX    ${facility}
    \  Click Element   ${locator}

Click Show More Language
    Wait Until Element Is Visible
    Click Element

Select Staff Language
    [Arguments]    ${list_language}

    Click Show More Language

    ${length}=      Get Length    ${list_language}

    :FOR    ${index}     IN RANGE   0   ${length}
    \  ${language}=    Get From List       ${list_language}  ${index}
    \  ${language}=    Convert To String   ${language}
    \  ${locator}=     Replace String      ${chk_language}    INDEX    ${language}
    \  Click Element   ${locator}

Click Show More Sport And Rereation
    Wait Until Element Is Visible
    Click Element

Select Sport And Rereation
    [Arguments]    ${list_hobby}

    Click Show More Sport And Rereation

    ${length}=      Get Length    ${list_hobby}

    :FOR    ${index}     IN RANGE   0   ${length}
    \  ${hobby}=    Get From List       ${list_hobby}  ${index}
    \  ${hobby}=    Convert To String   ${hobby}
    \  ${locator}=     Replace String      ${chk_hobby}    INDEX    ${hobby}
    \  Click Element   ${locator}


######################
Expand Section Policy
    Wait Until Element Is Visible    
    Click Element    

Set Policy
    [Arguments]    ${age_from}    ${age_to}

    Expand Section Policy
    Wait Until Element Is Visible    ${chk_age_policy}
    Click Element    ${chk_age_policy}
    Press Key    ${txt_age_from}    ${age_from}
    Press Key    ${txt_age_to}    ${age_to}


Expand Section Product Document
    Wait Until Element Is Visible    
    Click Element 

Product Document
    Expand Section Product Document


Contracting Manager Owner
    [Arguments]    ${contact_locator_list}    ${contact_name_list}    ${contact_email_list}    ${contact_phone_list}    ${contact_position}
    # Press Key    ${txt_contact_name}    ${name}
    # Select From List    ${ddl_contact_position}    ${position}
    # Press Key    ${txt_contact_email}
    # Press Key    ${txt_contact_phone}
    # Click Add New Contact

    ${txt_contact_name}=     Replace String    ${txt_contact_name}    INDEX    ${index}
    ${txt_contact_email}=    Replace String    ${txt_contact_name}    INDEX    ${index}
    ${txt_contact_phone}=    Replace String    ${txt_contact_name}    INDEX    ${index}


    ${length}=    Get Length    ${product_contact_value_list}

    :FOR    ${index}    IN RANGE    0    ${length}
    \  Input Text     ${contact_locator_list[${index}]}    ${contact_name_list[${index}]}
    \  Input Text     ${contact_locator_list[${index}]}    ${contact_email_list[${index}]}
    \  Input Text     ${contact_locator_list[${index}]}    ${contact_phone_list[${index}]}
    \  Select From List    ${ddl_contact_position}    ${value}
    \  Run Keyword If    ${index} > 0    Click Add New Contact




    # :FOR    ${index}    IN RANGE    0    ${length}
    # \  ${value}=    Get From List        ${product_contact_value_list}    ${index}
    # \  Press Key    ${txt_contact_name}     ${value}
    # \  Select From List    ${ddl_contact_position}    ${value}
    # \  Press Key    ${txt_contact_email}    ${value}
    # \  Press Key    ${txt_contact_phone}    ${value}
    # \  Run Keyword If    ${length} > 0    Click Add New Contact




Click Add New Contact
    Wait Until Element Is Visible    
    Click Element 


