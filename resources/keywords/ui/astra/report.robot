*** Settings ***
Library 		Selenium2Library
Library 		DateTime
#Library         ArchiveLibrary

# Resource        ${CURDIR}/../../library/common.py
# Resource 		${CURDIR}/../../library/excel.py
Resource 		${CURDIR}/../../../locators/ui/astra/report.robot


*** Keywords ***
Calculate Checkin From Today
    [Arguments]    ${value}
    ${current_date}=    	Get Current Date    result_format=%Y-%m-%d
    ${the_date}=    	Add Time To Date	${current_date}    ${value} days    result_format=datetime
    [Return]   				${the_date}

Verify Accrued Report Section Is Visible
    Wait Until Element Is Visible    ${btn_accrued_report_export}    ${TIMEOUT}

Verify Daily Revenued Report Section Is Visible
    Wait Until Element Is Visible  ${btn_daily_revenue_ccw_report_export}    ${TIMEOUT}

Verify Daily Unearned Report Section Is Visible
    Wait Until Element Is Visible    ${btn_daily_unearned_report_export}    ${TIMEOUT}

Verify Daily Revenue Credit Terms Report Section Is Visible
    Wait Until Element Is Visible    ${btn_daily_revenue_cct_report_export}    ${TIMEOUT}

Input Booker Email On Accrued Credit Term Report
    [Arguments]    ${booker_email}
    Input Text    	${txt_accrued_report_booker_email}    ${booker_email}

Input Date On ASTRA From Today
    [Arguments]    ${txt_datepicker}    ${value}
    ${date}=    	Get Current Date    result_format=%Y-%m-%d
    ${date}=    	Add Time To Date	${date}    ${value} days
    Select Date Picker ASTRA    ${txt_datepicker}    ${date}

Input Checkin Date From On Accrued Credit Term Report From Today
    [Arguments]    ${value}
    Input Date On ASTRA From Today    ${txt_accrued_report_chk_in_from}    ${value}

Input Checkin Date To On Accrued Credit Term Report From Today
    [Arguments]    ${value}
    Input Date On ASTRA From Today    ${txt_accrued_report_chk_in_to}    ${value}

Input Checkin Date From On Daily Unearned Report From Today
    [Arguments]    ${value}
    Input Date On ASTRA From Today    ${txt_daily_unearned_report_chk_in_from}    ${value}

Input Checkin Date To On Daily Unearned Report From Today
    [Arguments]    ${value}
    Input Date On ASTRA From Today    ${txt_daily_unearned_report_chk_in_to}    ${value}

Input Checkin Date From On Daily Revenue Credit Card Report From Today
    [Arguments]    ${value}
    Input Date On ASTRA From Today    ${txt_daily_revenue_ccw_report_chk_in_from}    ${value}

Input Checkin Date To On Daily Revenue Credit Card Report From Today
    [Arguments]    ${value}
    Input Date On ASTRA From Today    ${txt_daily_revenue_ccw_report_chk_in_to}    ${value}

Input Checkin Date From On Revenue Credit Terms Report From Today
    [Arguments]    ${value}
    Input Date On ASTRA From Today    ${txt_daily_revenue_cct_report_chk_in_from}    ${value}

Input Checkin Date To On Revenue Credit Terms Report From Today
    [Arguments]    ${value}
    Input Date On ASTRA From Today    ${txt_daily_revenue_cct_report_chk_in_to}    ${value}

Click Export Accrued Credit Term Report
	Click Element    ${btn_accrued_report_export}

Click Export Daily Unearned Report
    Click Element    ${btn_daily_unearned_report_export}

Click Export Daily Revenue Credit Card Report
    Click Element    ${btn_daily_revenue_ccw_report_export}

Click Export Daily Revenue Credit Terms Report
    Click Element    ${btn_daily_revenue_cct_report_export}

Open ASTRA Backend Browser For Download File
    [Arguments]    ${folder_name}    ${TESTDATA_PATH}    ${ASTRA_BASE_URL}
    ${TESTDATA_PATH}    get_canonical_path    ${TESTDATA_PATH}
    ${system}=    Evaluate    platform.system()    platform
    ${CHROME_OPTIONS}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    ${prefs}    Create Dictionary    download.default_directory=${TESTDATA_PATH}/${folder_name}
    Call Method    ${CHROME_OPTIONS}    add_experimental_option    prefs    ${prefs}
    Call Method    ${CHROME_OPTIONS}    add_argument    --disable-extensions
    Create Webdriver    Chrome    chrome_options=${CHROME_OPTIONS}
    Go To    ${ASTRA_BASE_URL}

Verify Excel Report By Booking Id
    [Arguments]    ${booking_id}    ${sheet_name}    ${column_name}    ${expect_value}    ${test_data_folder}
    ${actual_file}=    List Files In Directory          ${CURDIR}/../../data_test/${test_data_folder}    *.xlsx
    ${actual_file_path}=    get_canonical_path                ${CURDIR}/../../data_test/${test_data_folder}/${actual_file[0]}

    ${number_row}    find_number_row_excel    ${actual_file_path}    ${sheet_name}    ${booking_id_column}    ${booking_id}
    ${value_cell}    Get Value Excel    ${actual_file_path}    ${sheet_name}    ${column_name}${number_row}

    Should Be Equal As Strings    ${value_cell}    ${expect_value}

Verify Date On Excel Report By Booking Id
    [Arguments]    ${booking_id}    ${sheet_name}    ${column_name}    ${expect_value}    ${test_data_folder}    ${is_empty}=false
    ${actual_file}=    List Files In Directory          ${CURDIR}/../../data_test/${test_data_folder}    *.xlsx
    ${actual_file_path}=    get_canonical_path                ${CURDIR}/../../data_test/${test_data_folder}/${actual_file[0]}

    ${number_row}    find_number_row_excel    ${actual_file_path}    ${sheet_name}    ${booking_id_column}    ${booking_id}
    ${value_cell}    Get Value Excel    ${actual_file_path}    ${sheet_name}    ${column_name}${number_row}

    Run Keyword If    '${is_empty}' == 'true'    Should Be Equal As Strings    ${value_cell}    ${expect_value}
    ...    ELSE    Should Be Equal As Strings    ${value_cell.day}/${value_cell.month}/${value_cell.year}    ${expect_value}

    # Should Be Equal As Strings    ${value_cell.day}/${value_cell.month}/${value_cell.year}    ${expect_value}

Verify Excel Report By Booking Id Multiple Row
    [Arguments]    ${booking_id}    ${sheet_name}    ${column_name}    ${expect_values}    ${test_data_folder}
    ${actual_file}=    List Files In Directory          ${CURDIR}/../../data_test/${test_data_folder}    *.xlsx
    ${actual_file_path}=    get_canonical_path                ${CURDIR}/../../data_test/${test_data_folder}/${actual_file[0]}

    ${number_rows}    find_multiple_number_row_excel    ${actual_file_path}    ${sheet_name}    ${booking_id_column}    ${booking_id}

    ${index}=    Set Variable    0
    :FOR    ${number_row}    IN    @{number_rows}
    \    ${value_cell}    Get Value Excel    ${actual_file_path}    ${sheet_name}    ${column_name}${number_row}
    \    Should Be Equal As Strings    ${value_cell}    @{expect_values}[${index}]
    # \    Should Be Equal As Strings    ${value_cell}    ${expect_values}
    \    ${index}=    Evaluate    ${index}+1

Verify Does Not Exist In Excel Report By Booking Id
    [Arguments]    ${booking_id}    ${sheet_name}     ${test_data_folder}
    ${actual_file}=    List Files In Directory          ${CURDIR}/../../data_test/${test_data_folder}    *.xlsx
    ${actual_file_path}=    get_canonical_path                ${CURDIR}/../../data_test/${test_data_folder}/${actual_file[0]}

    ${number_row}    find_number_row_excel    ${actual_file_path}    ${sheet_name}    ${booking_id_column}    ${booking_id}

    Should Be Equal As Strings    ${number_row}    None

Get BookingItemID By BookingID
    [Arguments]    ${booking_id}
    ${bookingProduct_id}             Get BookingProductID By BookingID      ${booking_id}
    ${bookingItem_id}             Get BookingItemID By BookingProductID      ${bookingProduct_id[0][0]}
    [Return]    ${bookingItem_id[0][0]}

Unzip Accrued Report
    [Arguments]    ${test_data_folder}
    Log To Console    ${CURDIR}/../../data_test/${test_data_folder}
    ${actual_file}=    List Files In Directory    ${CURDIR}/../../data_test/${test_data_folder}    *.zip
    ${actual_file_path}=    get_canonical_path    ${CURDIR}/../../data_test/${test_data_folder}/${actual_file[0]}
    Extract Zip File    ${actual_file_path}    ${CURDIR}/../../data_test/${test_data_folder}

Select Date Picker ASTRA
    [Arguments]    ${datepicker}    ${date}
    ${day}    Convert Date    ${date}    result_format=%d
    ${month}    Convert Date    ${date}    result_format=%m
    ${year}    Convert Date    ${date}    result_format=%Y
    ${year}    Convert To Integer    ${year}
    ${month}    Convert To Integer    ${month}
    ${day}    Convert To Integer    ${day}
    ${str_year}    Convert To String    ${year}
    ${str_month}    Convert To String    ${month}
    ${str_day}    Convert To String    ${day}
    Click Element    ${datepicker}
    Select Year In Date Picker ASTRA    ${str_year}
    Select Month In Date Picker ASTRA    ${str_month}
    Select Day In Date Picker ASTRA    ${str_day}

Select Year In Date Picker ASTRA
    [Arguments]    ${year}
    Wait Until Element Is Visible    ${sel_date_picker_year}
    Select From List By Value    ${sel_date_picker_year}    ${year}

Select Month In Date Picker ASTRA
    [Arguments]    ${month}
    Wait Until Element Is Visible    ${sel_date_picker_month}
    Select From List By Value    ${sel_date_picker_month}    ${month}

Select Day In Date Picker ASTRA
    [Arguments]    ${day}
    ${index_week}    Set Variable    0
    ${index_day}    Set Variable    0
    ${count_week}    Get Element Count    ${div_date_picker_weeks}
    :FOR    ${index}    IN RANGE    1    ${count_week} + 1
    \    ${index_day}    Get Index Day In Date Picker ASTRA by Week    ${index}    ${day}
    \    ${index_week}    Set Variable If    ${index_day} > 0    ${index}    0
    \    Run Keyword If    ${index_week} > 0    Exit For Loop
    ${locator_select_day}    Set Variable    ${div_date_picker_weeks}[${index_week}]${div_date_picker_days}[${index_day}]
    Wait Until Element Is Visible    ${locator_select_day}
    Click Element    ${locator_select_day}

Get Index Day In Date Picker ASTRA by Week
    [Arguments]    ${week}    ${day}
    ${locator_days}    Set Variable    ${div_date_picker_weeks}[${week}]${div_date_picker_days}
    ${count_day}    Get Element Count    ${locator_days}
    ${select_day}    Set Variable    0
    :FOR    ${index}    IN RANGE    1    ${count_day} + 1
    \    ${locator_day}    Set Variable    ${locator_days}[${index}]
    \    ${is_active}    Check Day Active In Date Picker ASTRA    ${locator_day}
    \    ${text_day}    Get Text    ${locator_day}
    \    ${text_day_active}    Set Variable If    ${is_active}    ${text_day}    ${EMPTY}
    \    ${select_day}    Set Variable If    '${text_day_active}' == '${day}'    ${index}    0
    \    Run Keyword If    ${select_day} > 0    Exit For Loop
    Return From Keyword    ${select_day}

Check Day Active In Date Picker ASTRA
    [Arguments]    ${locator_day}
    Wait Until Element Is Visible    ${locator_day}
    ${class}    Get Element Attribute    ${locator_day}/div    class
    ${status}    Run Keyword And Return Status    Should Not End With    ${class}    outside
    [Return]    ${status}

####################
Get OrderID By BookingID
    [Arguments]    ${booking_id}
    Connect ACT Database
    ${order_id}=    query    SELECT OrderID FROM ReportBookingFull WHERE BookingID = '${booking_id}'
    Disconnect from Database
    [Return]    ${order_id}

Get ReportBookingFull By BookingID
    [Arguments]    ${booking_id}
    Connect ACT Database
    ${reports}=    query    SELECT * FROM ReportBookingFull WHERE BookingID = '${booking_id}'
    Disconnect from Database
    [Return]    ${reports}

Get BookingTaxInfo By BookingID
    [Arguments]    ${booking_id}
    Connect ACT Database
    ${data}=    query    SELECT * FROM BookingTaxInfo WHERE BookingID = '${booking_id}' AND Active=1
    Disconnect from Database
    [Return]    ${data}

Get PaperInvoiceOrigin By BookingID
    [Arguments]    ${booking_id}
    Connect ACT Database
    ${data}=    query    SELECT PaperID FROM BookingPaperRecord WHERE BookingID = '${booking_id}' AND PaperCatID = 1
    Disconnect from Database
    [Return]    ${data}

Get CrediNote By BookingID
    [Arguments]    ${booking_id}
    Connect ACT Database
    ${data}=    query     SELECT PaperID FROM BookingPaperRecord WHERE BookingID = '${booking_id}' AND PaperCatID = 3
    Disconnect from Database
    [Return]    ${data}

Get Billing Name By CompanyID
    [Arguments]    ${company_id}    ${LANG}
    ${LANG}    Convert To Lowercase    ${LANG}
    ${LANG}=    Set Variable If    '${LANG}' == 'th'    2
    ...    '${LANG}' == 'en'    1
    Connect ACT Database
    ${company_billing_name}=    query    SELECT CompanyName FROM CompanyAddress WHERE CompanyID = '${company_id}' and LangID = '${LANG}' and AddressTypeOption = '2'
    Disconnect from Database
    [Return]    ${company_billing_name}

Get Tax Invoice Name Credit Terms By CompanyID
    [Arguments]    ${company_id}    ${LANG}
    Connect ACT Database
    ${company_taxinvoice_name_creditterms}=    query    SELECT CompanyName FROM CompanyAddress WHERE CompanyID = '${company_id}' and LangID = '${LANG}' and AddressTypeOption = '1'
    Disconnect from Database
    [Return]    ${company_taxinvoice_name_creditterms}

Get Tax Invoice Name Credit Card By Email and CompanyID
    [Arguments]    ${email}    ${company_id}    ${LANG}
    Connect ACT Database
    ${UserDetails}=    Get User Details by Email    ${email}
    ${MemberID}=    Set Variable    ${UserDetails[3]}
    ${company_taxinvoice_name_creditcard}=    query    SELECT Title FROM CompanyTaxAddress WHERE MemberID = '${MemberID}' and CompanyID = '${company_id}'
    Disconnect from Database
    [Return]    ${company_taxinvoice_name_creditcard}

Set Company SAP Code By CompanyID
    [Arguments]    ${company_id}    ${sap_code}
    Connect ACT Database
    Execute Sql String    UPDATE CompanyBilling SET SapCode = '${sap_code}' WHERE CompanyID = '${company_id}'
    Disconnect from Database

Get BookingProductID By BookingID
    [Arguments]    ${booking_id}
    Connect ACT Database
    ${bookingProduct_id}=    query    SELECT BookingProductID FROM BookingProduct WHERE BookingID = '${booking_id}'
    Disconnect from Database
    [Return]    ${bookingProduct_id}

Get BookingItemID By BookingProductID
    [Arguments]    ${bookingProduct_id}
    Connect ACT Database
    ${bookingItem_id}=    query    SELECT BookingItemID FROM BookingItem WHERE BookingProductID = '${bookingProduct_id}'
    Disconnect from Database
    [Return]    ${bookingItem_id}

Get Booking Product By BookingID
    [Arguments]    ${booking_id}
    Connect ACT Database
    ${data}=    query     SELECT * FROM BookingProduct WHERE BookingID = '${booking_id}'
    Disconnect from Database
    [Return]    ${data}

Get Booking Product Supplier Profile By BookingProductID
    [Arguments]     ${booking_product_id}
    Connect ACT Database
    ${data}=    query     SELECT * FROM BookingProductSupplierProfile WHERE BookingProductID = '${booking_product_id}'
    Disconnect from Database
    [Return]    ${data}

Get Supplier Payment Type By PaymentTypeID
    [Arguments]     ${payment_type_id}
    Connect ACT Database
    ${data}=    query     SELECT * FROM SupplierPaymentType WHERE PaymentTypeID = '${payment_type_id}'
    Disconnect from Database
    [Return]    ${data}

Get PaperInvoiceCancel By BookingID
    [Arguments]    ${booking_id}
    Connect ACT Database
    ${data}=     query    SELECT PaperID FROM BookingPaperRecord WHERE BookingID = '${booking_id}' AND PaperCatID = 2
    Disconnect from Database
    [Return]    ${data}

Verify Email Booker Field Is Not Visible  
    Page Should Not Contain Element     ${txt_accrued_report_booker_email} 

Get Selling Price in THB
    [Arguments]     ${selling_price_foreign}   ${currency_rate}    ${night}=1 
    ${selling_price_th}=         Evaluate   ${selling_price_foreign}*${currency_rate}*${night} 
    ${selling_ex_vat}=           Convert To Number      ${selling_price_th}    2 
    ${string}=                   Convert To String  ${selling_ex_vat}
    ${output}=                   Fetch From Right   ${string}   .
    ${result}   RUN KEYWORD IF  '${output}' <= '49'   Round Up Price      ${selling_price_th}
    ...         ELSE             Convert To Number      ${selling_price_th}      0
    [Return]    ${result}

Round Up Price
    [Arguments]     ${selling_price}
    ${output}=      Convert To Number      ${selling_price}      0
    ${result}=      Evaluate    ${output}+1
    [Return]    ${result}

Verify Excel Report Contains Only One Booker Email
    [Arguments]    ${company_sap_code}    ${sheet_name}    ${column_name}    ${expect_values}    ${test_data_folder}
    ${actual_file}=    List Files In Directory          ${CURDIR}/../../data_test/${test_data_folder}    *.xlsx
    ${actual_file_path}=    get_canonical_path                ${CURDIR}/../../data_test/${test_data_folder}/${actual_file[0]}

    ${number_rows}    find_multiple_number_row_excel    ${actual_file_path}    ${sheet_name}    ${accrued_ct_company_sap_code_column}    ${company_sap_code}

    :FOR    ${number_row}    IN    @{number_rows}
    \    ${value_cell}    Get Value Excel    ${actual_file_path}    ${sheet_name}    ${column_name}${number_row}
    \    Should Be Equal As Strings    ${value_cell}    ${expect_values}
    \    Log To Console     ${value_cell}