*** Settings ***
Library 		Selenium2Library
Library 		String
Resource        ${CURDIR}/../../../Config/${ENV}/config.robot

*** Variables ***
# locators
${btn_side_menu_supplier}                           //a[@id="menu-Supplier"]//span[text()="Supplier"]
${btn_side_menu_supplier_list}                      //a[@id="menu-Supplier List"]//span[text()="Supplier List"]
${btn_side_menu_create_supplier}                    //a[@id="menu-Create Supplier"]//span[text()="Create Supplier"]
${lbl_supplier_list_bread_crumb}                    id=supplier-list-a-breadcrumb
${lbl_supplier_list_bread_crumb_root}               //li[@class='breadcrumb-item primary']/a
${lbl_supplier_list_bread_crumb_cur}                //li[@class='breadcrumb-item']/a
${lbl_create_supplier_bread_crumb_root}             //li[@class='breadcrumb-item primary']/a[text()="Home"]
${lbl_create_supplier_bread_crumb_cur}              //a[@id="supplier-create-a-breadcrumb-primary"]
# //li[@class='breadcrumb-item primary']/a[text()="Create New Supplier"]
${lbl_create_supplier_bread_crumb_cur2}             //a[@id="supplier-create-a-breadcrumb"]
#//li[@class='breadcrumb-item']/a 
${lbl_supplier_list_title}                          //h3[text()='SUPPLIER']
#${lbl_create_supplier_title}                        //h3[text()='CREATE NEW SUPPLIER']
${btn_supplier_list_create}                         //button[@id='supplier-list-btn-create-supplier']//span[text()='Create Supplier']
${txt_supplier_list_page_size}                      //button[@id='supplier-list-btn-ddl-pageSize' and contains(.,'Show NUMBER Rows')]
${btn_supplier_list_page_size}                      id=supplier-list-btn-ddl-pageSize
${sel_supplier_list_page_size}                      id=supplier-list-a-ddl-pagesize-NUMBER
${table_supplier_list}                              id=supplier-list-table-data
${input_supplier_list_search}                       id=supplier-list-input-search
${lbl_supplier_list_search}                         //input[@placeholder='Supplier SAP Code, Supplier Name ']
${lbl_supplier_list_main_sap_code}                  //th[@id='supplier-list-th-main-sap-code' and contains(.,'Main Sap Code')]
${lbl_supplier_list_supplier_name}                  //th[@id='supplier-list-th-supplier-name' and contains(.,'Supplier Name')]
${lbl_supplier_list_status}                         //th[@id='supplier-list-th-status' and contains(.,'Status')]
${lbl_supplier_list_action}                         //th[@id='supplier-list-th-actions' and contains(.,'Actions')]
${icn_supplier_list_sort_main_sap_code}             id=supplier-list-i-sort-main-sap-code
${icn_supplier_list_sort_up_main_sap_code}          id=supplier-list-i-sort-up-main-sap-code
${icn_supplier_list_sort_dw_main_sap_code}          id=supplier-list-i-sort-down-main-sap-code
${active_supplier_list_sort_up_main_sap_code}       //div[@id='supplier-list-i-sort-main-sap-code']/i[@class='ft-arrow-up pointer gray-light font-weight-bold gray']
${active_supplier_list_sort_dw_main_sap_code}       //div[@id='supplier-list-i-sort-main-sap-code']/i[@class='ft-arrow-down arrow-down pointer gray-light font-weight-bold gray']
${inactive_supplier_list_sort_up_main_sap_code}     //div[@id='supplier-list-i-sort-main-sap-code']/i[@class='ft-arrow-up pointer gray-light font-weight-bold']
${inactive_supplier_list_sort_dw_main_sap_code}     //div[@id='supplier-list-i-sort-main-sap-code']/i[@class='ft-arrow-down arrow-down pointer gray-light font-weight-bold']
${icn_supplier_list_sort_name}                      id=supplier-list-i-sort-supplier-name
${icn_supplier_list_sort_up_name}                   id=supplier-list-i-sort-up-supplier-name
${icn_supplier_list_sort_dw_name}                   id=supplier-list-i-sort-down-supplier-name
${active_supplier_list_sort_up_name}                //div[@id='supplier-list-i-sort-supplier-name']/i[@class='ft-arrow-up pointer gray-light font-weight-bold gray']
${inactive_supplier_list_sort_up_name}              //div[@id='supplier-list-i-sort-supplier-name']/i[@class='ft-arrow-up pointer gray-light font-weight-bold']
${active_supplier_list_sort_dw_name}                //div[@id='supplier-list-i-sort-supplier-name']/i[@class='ft-arrow-down arrow-down pointer gray-light font-weight-bold gray']
${inactive_supplier_list_sort_dw_name}              //div[@id='supplier-list-i-sort-supplier-name']/i[@class='ft-arrow-down arrow-down pointer gray-light font-weight-bold']
${index_supplier_list_main_sap_code}                id=supplier-list-a-main-sap-code-INDEX
${val_supplier_list_main_sap_code}                  //a[@class='supplier-list-a-main-sap-code' and text()='TEXT']
${index_supplier_list_supplier_name}                id=supplier-list-td-supplier-name-INDEX
${val_supplier_list_supplier_name}                  //td[contains(@id,'supplier-list-td-supplier-name') and contains(.,'TEXT')]
${val_supplier_list_supplier_name_by_sap_code}      //tr/td/a[@class='supplier-list-a-main-sap-code' and text()='TEXT']/../..//td[contains(@id,'supplier-list-td-supplier-name')]
${btn_supplier_list_status}                         id=btn-toggle-INDEX
${btn_supplier_list_status_by_sap_code}             //td/a[@class='supplier-list-a-main-sap-code' and text()='TEXT']/../../td[contains(@id,'supplier-list-td-status')]//button
${val_supplier_list_status_on_by_sap_code}          //td/a[@class='supplier-list-a-main-sap-code' and text()='TEXT']/../../td[contains(@id,'supplier-list-td-status')]//button[@class='toggle-btn toggle-btn-on']
${val_supplier_list_status_off_by_sap_code}         //td/a[@class='supplier-list-a-main-sap-code' and text()='TEXT']/../../td[contains(@id,'supplier-list-td-status')]//button[@class='toggle-btn toggle-btn-off']
${btn_supplier_list_action_view}                    id=supplier-list-btn-view-detail-INDEX
${btn_supplier_list_action_view_by_sap_code}        //td/a[@class='supplier-list-a-main-sap-code' and text()='TEXT']/../../td[contains(@id,'supplier-list-td-action')]//a[contains(@id,'supplier-list-btn-view-detail')]
${btn_supplier_list_action_edit}                    id=supplier-list-btn-edit-INDEX
${btn_supplier_list_action_edit_by_sap_code}        //td/a[@class='supplier-list-a-main-sap-code' and text()='TEXT']/../../td[contains(@id,'supplier-list-td-action')]//button[contains(@id,'supplier-list-btn-edit')]
${btn_supplier_list_action_del}                     id=supplier-list-btn-delete-INDEX
${btn_supplier_list_action_del_by_sap_code}         //td/a[@class='supplier-list-a-main-sap-code' and text()='TEXT']/../../td[contains(@id,'supplier-list-td-action')]//button[contains(@id,'supplier-list-btn-delete')]
${lbl_supplier_list_no_result}                      //td[@id='supplier-list-td-no-seasrch-result']//p[text()='No Search Result']
${div_supplier_list_page_items}                     //div[@class="margin-top-14"]
${item_supplier_list_main_sap_code}                 //td[contains(@id,'supplier-list-td-main-sap-code')]
${item_supplier_list_name}                          //td[contains(@id,'supplier-list-td-supplier-name')]    
${popup_supplier_list_del}                          id=dialog-comfirm-leave-title
${txt_popup_supplier_list_del}                      //h2[text()='Confirm to delete?']
${btn_popup_supplier_list_confirm_del}              //button[@id='btn-dialog-comfirm' and text()='Confirm']
${btn_popup_supplier_list_cancel_del}               //button[@id='btn-dialog-cancel' and text()='Cancel']
${popup_supplier_list_error}                        //mat-dialog-container//p[@id='alert-message' and text()='Update Supplier Status Failed']
${btn_popup_supplier_list_ok}                       //mat-dialog-actions/button//span[text()='OK']
${row_table_body_supplier_list}                     //table[@id='supplier-list-table-data']//tbody/tr/td[contains(@id,'supplier-list-td-main-sap-code')]
${lbl_create_supplier_title}                        //h3[contains(.,'CREATE NEW SUPPLIER')]
## Supplier Information ##
${lbl_create_supplier_information}                              id=supplier-information-template-create
${lbl_create_supplier_information_supplier_type}                //div[@class='form-group']/label[text()='Supplier Type']
${ddl_create_supplier_information_supplier_type}                id=supplier-information-select-supplier-type
${lbl_create_supplier_information_country}                      //div[@class='form-group']/label[text()='Country']
${ddl_create_supplier_information_country}                      id=supplier-information-select-country
${lbl_create_supplier_information_supplier_name}                //div[@class='form-group']/label[text()='Supplier Name']
${txt_create_supplier_information_supplier_name}                id=supplier-information-input-supplier-name
${lbl_create_supplier_information_supplier_address}             //div[@class='form-group']/label[text()='Supplier Address']
${txt_create_supplier_information_supplier_address}             id=supplier-information-textarea-supplier-address
${lbl_create_supplier_information_supplier_tax_name}            //div[@class='form-group']/label[text()='Supplier Tax Name']
${chk_create_supplier_information_same_as_supplier_name}        id=supplier-information-input-same-supplier-name
${lbl_create_supplier_information_same_as_supplier_name}        //label[text()=" Same as Supplier Name"]
${txt_create_supplier_information_supplier_tax_name}            id=supplier-information-input-supplier-tax-name
${lbl_create_supplier_information_supplier_tax_id}              //div[@class='form-group']/label[text()='Supplier Tax ID']
${txt_create_supplier_information_supplier_tax_id}              id=supplier-information-input-supplier-tax-id
${lbl_create_supplier_information_supplier_tax_address}         //div[@class='form-group']/label[text()='Supplier Tax Address']
${chk_create_supplier_information_same_as_supplier_address}     id=supplier-information-input-same-supplier-address
${lbl_create_supplier_information_same_as_supplier_address}     //label[text()=" Same as Supplier Address"]
${txt_create_supplier_information_supplier_tax_address}         id=supplier-information-textarea-supplier-tax-address
${chk_create_supplier_information_tax_invoice}                  id=supplier-information-input-tax-invoice
${lbl_create_supplier_information_tax_invoice}                  //label[text()=" No need to follow up Tax invoice from supplier"]
${lbl_create_supplier_information_email_reserv}                 //div[@class='form-group']/label[text()='E-mail Reservation']
${txt_create_supplier_information_email_reserv}                 //div[@class='form-control']
${lbl_create_supplier_information_default_markup}               //div[@class='form-group']/label[text()='Default % Markup ']
${txt_create_supplier_information_default_markup}               id=touchspin-input-markup
${lbl_create_supplier_information_supplier_cost_type}           //div[@class='form-group']/label[text()='Supplier Cost Type']
${rdb_create_supplier_information_supplier_cost_type_incl}      id=supplier-information-input-include-vat
${lbl_create_supplier_information_supplier_cost_type_incl}      //span[text()='Include VAT']
${rdb_create_supplier_information_supplier_cost_type_excl}      id=supplier-information-input-exclude-vat
${lbl_create_supplier_information_supplier_cost_type_excl}      //span[text()='Exclude VAT']
${lbl_create_supplier_information_supplier_status}              //div[@class='form-group']/label[text()='Supplier Status']
${tgl_create_supplier_information_supplier_status}              id=btn-toggle-
## Supplier Payment ##
${lbl_create_supplier_payment}                                              //div[@class='card-header bg-white']/h4[text()='Supplier Payment']
${lbl_create_supplier_payment_noncredit}                                    //div[@class='card-header card-header-bg-color']/div[text()=' Non Credit ']
${lbl_create_supplier_payment_credit}                                       //div[@class='card-header card-header-bg-color']/div[text()=' Credit Terms ']  
${chk_create_supplier_payment_noncredit}                                    id=supplier-payment-radio-noncredit
${chk_create_supplier_payment_credit}                                       id=supplier-payment-radio-credit
${lbl_create_supplier_payment_method}                                       //div[@class='card-header bg-white']/h4[text()='Supplier Payment Method']
${chk_create_supplier_payment_method_noncredit_pettycash}                   id=supplier-payment-radio-noncredit-pettycash
${lbl_create_supplier_payment_method_noncredit_pettycash}                   //div[@class='card-header card-header-bg-color']/div[text()=' Petty Cash - Non Credit ']
${txt_create_supplier_payment_method_noncredit_pettycash_bf_ckin_date}      id=touchspin-input-ncmt
${txt_create_supplier_payment_method_noncredit_pettycash_acct_name}         id=supplier-payment-input-noncredit-pettycash-account-name
${txt_create_supplier_payment_method_noncredit_pettycash_acct_no}           id=supplier-payment-input-noncredit-pettycash-account-no
${ddl_create_supplier_payment_method_noncredit_pettycash_acct_bank}         id=supplier-payment-select-noncredit-pettycash-bank
${chk_create_supplier_payment_method_noncredit_creditcard}                  id=supplier-payment-radio-noncredit-creditcard
${lbl_create_supplier_payment_method_noncredit_creditcard}                  //div[@class='card-header card-header-bg-color']/div[text()=' Credit Card - Non Credit ']
${txt_create_supplier_payment_method_noncredit_creditcard_bf_ckin_date}     id=touchspin-input-nccp
${chk_create_supplier_payment_method_noncredit_link}                        id=supplier-payment-radio-noncredit-link
${lbl_create_supplier_payment_method_noncredit_link}                        //div[@class='card-header card-header-bg-color']/div[text()=' Link (Corporate Card) - Non Credit']
${txt_create_supplier_payment_method_noncredit_link_bf_ckin_date}           id=touchspin-input-ncl
${chk_create_supplier_payment_method_credit_pettycash}                      id=supplier-payment-radio-credit-pettycash
${lbl_create_supplier_payment_method_credit_pettycash}                      //div[@class='card-header card-header-bg-color']/div[text()=' Petty Cash - Credit Terms ']
${txt_create_supplier_payment_method_credit_pettycash_af_ckout_date}        id=touchspin-input-cp
${txt_create_supplier_payment_method_credit_pettycash_acct_name}            id=supplier-payment-input-credit-pettycash-account-name
${txt_create_supplier_payment_method_credit_pettycash_acct_no}              id=supplier-payment-input-credit-pettycash-account-no
${ddl_create_supplier_payment_method_credit_pettycash_bank}                 id=supplier-payment-select-credit-pettycash-bank
${txt_create_supplier_payment_method_credit_pettycash_note}                 id=supplier-payment-txtarea-credit-pettycash-note
${chk_create_supplier_payment_method_credit_creditcard}                     id=supplier-payment-radio-credit-creditcard
${lbl_create_supplier_payment_method_credit_creditcard}                     //div[@class='card-header card-header-bg-color']/div[text()=' Credit Card - Credit Terms']
${txt_create_supplier_payment_method_credit_creditcard_af_ckout_date}       id=touchspin-input-cc
${txt_create_supplier_payment_method_credit_creditcard_note}                id=supplier-payment-txtarea-credit-creditcard-note
${chk_create_supplier_payment_method_credit_link}                           id=supplier-payment-radio-credit-link
${lbl_create_supplier_payment_method_credit_link}                           //div[@class='card-header card-header-bg-color']/div[text()=' Link (Corporate Card) - Credit Terms']
${txt_create_supplier_payment_method_credit_link_af_ckout_date}             id=touchspin-input-cl
${txt_create_supplier_payment_method_credit_link_note}                      id=supplier-payment-txtarea-credit-link-note
${chk_create_supplier_payment_method_credit_cheque}                         id=supplier-payment-radio-credit-cheque
${lbl_create_supplier_payment_method_credit_cheque}                         //div[@class='card-header card-header-bg-color']/div[text()=' Cheque - Credit Terms']
${txt_create_supplier_payment_method_credit_cheque_af_ckout_date}           id=touchspin-input-cche
${lbl_create_supplier_payment_method_credit_cheque_receiver}                //div[@class='form-group']/label[text()='Receiver']
${txt_create_supplier_payment_method_credit_cheque_receiver}                id=supplier-payment-input-credit-cheque-receiver
${txt_create_supplier_payment_method_credit_cheque_note}                    id=supplier-payment-txtarea-credit-cheque-note
${chk_create_supplier_payment_method_credit_acct_transfer}                  id=supplier-payment-radio-credit-account-transfer
${lbl_create_supplier_payment_method_credit_acct_transfer}                  //div[@class='card-header card-header-bg-color']/div[text()=' Account Transfer - Credit Terms']
${txt_create_supplier_payment_method_credit_acct_transfer_af_ckout_date}    id=touchspin-input-act
${txt_create_supplier_payment_method_credit_acct_transfer_acct_name}        id=supplier-payment-input-credit-account-transfer-account-name
${txt_create_supplier_payment_method_credit_acct_transfer_acct_no}          id=supplier-payment-input-credit-account-transfer-account-no
${ddl_create_supplier_payment_method_credit_acct_transfer_bank}             id=supplier-payment-select-credit-account-transfer-bank
${txt_create_supplier_payment_method_credit_acct_transfer_note}             id=supplier-payment-txtarea-credit-account-transfer-note
# Shared locator #
${lbl_create_supplier_payment_method_noncredit_bf_ckin_date}    //div[@class='form-group']/label[text()='Before Check-in Date']
${lbl_create_supplier_payment_method_credit_af_ckout_date}      //div[@class='form-group']/label[text()='After Check-out Date']
${lbl_create_supplier_payment_method_acct_name}                 //div[@class='form-group']/label[text()='Account Name']
${lbl_create_supplier_payment_method_acct_no}                   //div[@class='form-group']/label[text()='Account No.']
${lbl_create_supplier_payment_method_bank}                      //div[@class='form-group']/label[text()='Bank']
${lbl_create_supplier_payment_method_credit_note}               //div[@class='form-group']/label[text()='Note (Optional)']
## Supplier SAP Code ## 
## Supplier Document ##
## Supplier Contact ##
# Waiting for fix
${btn_create_supplier_cancel}                       id=suplier-management-button-cancel
${btn_create_supplier_create_supplier}              id=suplier-management-button-submit

*** Keywords ***
Click Supplier Main Menu
    Wait Until Page Contains Element    ${btn_side_menu_supplier}
    Click Element   ${btn_side_menu_supplier}
    Wait Until Page Contains Element    ${btn_side_menu_supplier_list}
    Wait Until Page Contains Element    ${btn_side_menu_create_supplier}

Click Supplier List Menu
    Wait Until Page Contains Element    ${btn_side_menu_supplier_list}
    Click Element   ${btn_side_menu_supplier_list}

Click Create Supplier Menu
    Wait Until Page Contains Element    ${btn_side_menu_create_supplier}
    Click Element   ${btn_side_menu_create_supplier}

Go To Supplier List
    Click Supplier Main Menu
    Click Supplier List Menu

Go To Create Supplier
   Go To Supplier List
   Click Create Supplier Button

Verify Supplier List Page Display Static Elements Correctly
    Verify That Supplier List Page Display Bread Crumb Correctly
    Verify That Supplier List Page Display Page Title Correctly
    Verify That Supplier List Page Display Create Supplier Button
    Verify That Supplier List Page Display Selected Page Size Correctly
    Verify That Supplier List Page Display Search Text Field
    Verify That Supplier List Page Display Search Result Table 
    Verify That Supplier List Page Display Sort Icons

Verify Create Supplier Page Display Static Elements Correctly
    Verify That Create Supplier Page Display Bread Crumb Correctly
    Verify That Create Supplier Page Display Page Title Correctly
    Verify That Create Supplier Page Display Supplier Information Section Correctly
    Verify That Create Supplier Page Display Supplier Payment Section Correctly
    # Verify That Create Supplier Page Display Supplier SAP Code Section Correctly
    # Verify That Create Supplier Page Display Supplier Document Section Correctly
    # Verify That Create Supplier Page Display Supplier Contact Section Correctly
    Verify That Create Supplier Page Display Cancel Button Correctly
    Verify That Create Supplier Page Display Create Supplier Button Correctly

Search Supplier
    [Arguments]     ${keywords}
    Input Search Supplier Keywords     ${keywords}
    Press Enter To Search Supplier Info

Verify Supplier Search Result Row Display Correctly By Main Sap Code
    [Arguments]     ${main_sap_code}    ${name}    ${status}=active
    Verify Supplier Name Display Correctly By Main Sap Code     ${main_sap_code}    ${name}
    Verify Supplier Status Display Correctly By Main Sap Code   ${main_sap_code}    ${status}
    Verify Supplier Action Items Display Correctly By Main Sap Code     ${main_sap_code}

Verify That Supplier List Page Display Bread Crumb Correctly
    [Arguments]     ${path}=Home/Supplier
    ${exp_path}=    Split String    ${path}     /
    Wait Until Page Contains Element    ${lbl_supplier_list_bread_crumb_root}  
    Wait Until Page Contains Element    ${lbl_supplier_list_bread_crumb_cur}
    ${root_path}=   Get Text    ${lbl_supplier_list_bread_crumb_root}
    ${cur_path}=    Get Text    ${lbl_supplier_list_bread_crumb_cur} 
    Should Be Equal As Strings      ${root_path}    ${exp_path[0]}
    Should Be Equal As Strings      ${cur_path}     ${exp_path[1]}

Verify That Supplier List Page Display Page Title Correctly
    Wait Until Page Contains Element    ${lbl_supplier_list_title}

Verify That Supplier List Page Display Create Supplier Button
    Wait Until Page Contains Element    ${btn_supplier_list_create} 

Verify That Supplier List Page Display Selected Page Size Correctly
    [Arguments]     ${row_no}=50
    ${locator}=     Replace String      ${txt_supplier_list_page_size}      NUMBER      ${row_no}
    Wait Until Page Contains Element    ${locator}

Verify That Supplier List Page Display Search Text Field
    Wait Until Page Contains Element    ${lbl_supplier_list_search}

Verify That Supplier List Page Display Search Result Table 
    Wait Until Page Contains Element    ${table_supplier_list}
    Wait Until Page Contains Element    ${lbl_supplier_list_main_sap_code} 
    Wait Until Page Contains Element    ${lbl_supplier_list_supplier_name}
    Wait Until Page Contains Element    ${lbl_supplier_list_status} 
    Wait Until Page Contains Element    ${lbl_supplier_list_action}

Verify That Supplier List Page Display Sort Icons
    Wait Until Page Contains Element    ${icn_supplier_list_sort_main_sap_code}   
    Wait Until Page Contains Element    ${icn_supplier_list_sort_name}

Verify Supplier Main Sap Code Column Display Sort Icon Correctly
    [Arguments]     ${sort_type}   ${sort_status}
    Run Keyword If  '${sort_type}' == 'DESC' and '${sort_status}' == 'active'       Wait Until Page Contains Element    ${active_supplier_list_sort_dw_main_sap_code}
    ...    ELSE IF  '${sort_type}' == 'ASC' and '${sort_status}' == 'active'        Wait Until Page Contains Element    ${active_supplier_list_sort_up_main_sap_code}
    ...    ELSE IF  '${sort_type}' == 'DESC' and '${sort_status}' == 'inactive'     Wait Until Page Contains Element    ${inactive_supplier_list_sort_dw_main_sap_code}
    ...    ELSE IF  '${sort_type}' == 'ASC' and '${sort_status}' == 'inactive'      Wait Until Page Contains Element    ${inactive_supplier_list_sort_up_main_sap_code}

Verify Supplier Name Column Display Sort Icon Correctly
    [Arguments]     ${sort_type}   ${sort_status}
    Run Keyword If  '${sort_type}' == 'DESC' and '${sort_status}' == 'active'       Wait Until Page Contains Element    ${active_supplier_list_sort_dw_name} 
    ...    ELSE IF  '${sort_type}' == 'ASC' and '${sort_status}' == 'active'        Wait Until Page Contains Element    ${active_supplier_list_sort_up_name}
    ...    ELSE IF  '${sort_type}' == 'DESC' and '${sort_status}' == 'inactive'     Wait Until Page Contains Element    ${inactive_supplier_list_sort_dw_name}
    ...    ELSE IF  '${sort_type}' == 'ASC' and '${sort_status}' == 'inactive'      Wait Until Page Contains Element    ${inactive_supplier_list_sort_up_name}

Verify Supplier List Display Items Per Page As Expected
    [Arguments]     ${page_size}
    Wait Until Page Contains Element    ${row_table_body_supplier_list}
    ${item_per_page}=   Get Matching Xpath Count    ${row_table_body_supplier_list}
    Log To Console  item per page = ${item_per_page}
    Should Be Equal  ${page_size}   ${item_per_page}

Verify No Search Result Found
    Wait Until Page Contains Element   ${lbl_supplier_list_no_result}

Select Supplier List Page Size From Drop Dowm List
    [Arguments]     ${page_size}
    Click Element   ${btn_supplier_list_page_size} 
    ${locator}=     Replace String      ${sel_supplier_list_page_size}  NUMBER  ${page_size} 
    Wait Until Page Contains Element    ${locator}
    Click Element   ${locator}

Input Search Supplier Keywords
    [Arguments]     ${keyword}
    Wait Until Page Contains Element    ${input_supplier_list_search} 
    Input Text      ${input_supplier_list_search}   ${keyword}

Press Enter To Search Supplier Info
    Wait Until Page Contains Element    ${input_supplier_list_search}
    Press Key       ${input_supplier_list_search}   \\13

Verify Search Result in Main Sap Code Column Matched with Search Keyword
    [Arguments]     ${keyword}
    Wait Until Page Contains Element      ${row_table_body_supplier_list} 
    ${row}=     Get Matching Xpath Count    ${row_table_body_supplier_list}
    :FOR  ${element}  IN RANGE  0   ${row}
    \   ${element}=     Convert To String   ${element}
    \   ${locator}=     Replace String      ${index_supplier_list_main_sap_code}    INDEX   ${element}
    \   Wait Until Page Contains Element    ${locator}
    \   ${value}=   Get Text    ${locator}
    \   Should Contain  ${value}    ${keyword}

Verify Search Result in Supplier Name Column Matched with Search Keyword
    [Arguments]     ${keyword}
    Wait Until Page Contains Element        ${row_table_body_supplier_list} 
    ${row}=     Get Matching Xpath Count    ${row_table_body_supplier_list}
    :FOR  ${element}  IN RANGE  0   ${row}
    \   ${element}=     Convert To String   ${element}
    \   ${locator}=     Replace String      ${index_supplier_list_supplier_name}    INDEX   ${element}
    \   Wait Until Page Contains Element    ${locator}
    \   ${value}=   Get Text    ${locator}
    \   Should Contain  ${value}    ${keyword}  ignore_case=True    

Clear Search Supplier Keywords 
    Wait Until Page Contains Element    ${input_supplier_list_search}
    Clear Element Text      ${input_supplier_list_search}

Click Create Supplier Button
    Wait Until Page Contains Element    ${btn_side_menu_create_supplier}
    Click Element   ${btn_side_menu_create_supplier}
    Wait Until Page Contains Element    ${lbl_create_supplier_title}

Click Supplier Main Sap Code Hyperlink
    [Arguments]     ${main_sap_code}
    ${locator}=     Replace String      ${val_supplier_list_main_sap_code}    TEXT     ${main_sap_code}
    Wait Until Page Contains Element    ${locator}
    Click Element   ${locator}

Click View Supplier Button By Main Sap Code
    [Arguments]     ${main_sap_code}
    ${locator}=     Replace String      ${btn_supplier_list_action_view_by_sap_code}    TEXT     ${main_sap_code}
    Wait Until Page Contains Element    ${locator}
    Click Element   ${locator}

Click Edit Supplier Button By Main Sap Code
    [Arguments]     ${main_sap_code}
    ${locator}=     Replace String      ${btn_supplier_list_action_edit_by_sap_code}    TEXT     ${main_sap_code}
    Wait Until Page Contains Element    ${locator}
    Click Element   ${locator}

Change Supplier Status By Main Sap Code
    [Arguments]     ${main_sap_code}
    ${locator}=     Replace String      ${btn_supplier_list_status_by_sap_code}   TEXT   ${main_sap_code}
    Wait Until Page Contains Element    ${locator}
    Click Element   ${locator}

Verify Supplier Status Display Correctly By Main Sap Code
    [Arguments]     ${main_sap_code}   ${status}=active
    ${locator_status_on}=       Replace String     ${val_supplier_list_status_on_by_sap_code}       TEXT   ${main_sap_code}
    ${locator_status_off}=      Replace String     ${val_supplier_list_status_off_by_sap_code}      TEXT   ${main_sap_code}
    Run Keyword If  '${status}' == 'active'     Wait Until Page Contains Element     ${locator_status_on}
    ...    ELSE IF  '${status}' == 'inactive'   Wait Until Page Contains Element     ${locator_status_off} 

Verify Supplier Name Display Correctly By Main Sap Code
    [Arguments]     ${main_sap_code}    ${name}
    ${locator_name}=            Replace String     ${val_supplier_list_supplier_name_by_sap_code}   TEXT   ${main_sap_code}
    Wait Until Page Contains Element        ${locator_name}
    ${val_name}=    Get Text    ${locator_name}
    Should Be Equal As Strings  ${name}     ${val_name.strip()}

Verify Supplier Action Items Display Correctly By Main Sap Code
    [Arguments]     ${main_sap_code}
    ${locator_view}=            Replace String     ${btn_supplier_list_action_view_by_sap_code}     TEXT   ${main_sap_code}
    ${locator_edit}=            Replace String     ${btn_supplier_list_action_edit_by_sap_code}     TEXT   ${main_sap_code}
    ${locator_del}=             Replace String     ${btn_supplier_list_action_del_by_sap_code}      TEXT   ${main_sap_code}
    Wait Until Page Contains Element        ${locator_view}  
    Wait Until Page Contains Element        ${locator_edit}
    Wait Until Page Contains Element        ${locator_del}

Verify Supplier Name Sorting Correctly
    [Arguments]     ${exp_name_list}
    Wait Until Page Contains Element    ${row_table_body_supplier_list}
    ${row}=     Get Matching Xpath Count    ${row_table_body_supplier_list}
    :FOR  ${index}  IN RANGE  0   ${row}
    \   ${index}=      Convert To String   ${index}
    \   ${locator}=    Replace String      ${index_supplier_list_supplier_name}    INDEX   ${index}
    \   ${act_name}=   Get Text    ${locator}
    \   Should Be Equal As Strings   ${act_name.strip()}    ${exp_name_list[${index}][0]}      

Verify Supplier Main Sap Code Sorting Correctly
    [Arguments]     ${exp_name_list}
    Wait Until Page Contains Element    ${row_table_body_supplier_list}
    ${row}=     Get Matching Xpath Count    ${row_table_body_supplier_list}
    :FOR  ${index}  IN RANGE  0   ${row}
    \   ${index}=      Convert To String   ${index}
    \   ${locator}=    Replace String      ${index_supplier_list_main_sap_code}    INDEX   ${index}
    \   ${act_name}=   Get Text    ${locator}
    \   Should Be Equal As Strings   '${act_name.strip()}'    '${exp_name_list[${index}][0]}'  

Click Sort Supplier Name List Button
    Wait Until Page Contains Element    ${lbl_supplier_list_supplier_name} 
    Click Element   ${lbl_supplier_list_supplier_name} 

Click Sort Supplier Main Sap Code List Button
    Wait Until Page Contains Element    ${lbl_supplier_list_main_sap_code} 
    Click Element   ${lbl_supplier_list_main_sap_code} 

Click Delete Supplier Button By Main Sap Code
    [Arguments]     ${main_sap_code}
    ${locator}      Replace String  ${btn_supplier_list_action_del_by_sap_code}     TEXT    ${main_sap_code}
    Wait Until Page Contains Element    ${locator}
    Click Element   ${locator}

Verify Confirm To Delete Popup Display
    Wait Until Page Contains Element    ${popup_supplier_list_del}
    Wait Until Page Contains Element    ${txt_popup_supplier_list_del}
    Wait Until Page Contains Element    ${btn_popup_supplier_list_confirm_del}
    Wait Until Page Contains Element    ${btn_popup_supplier_list_cancel_del}

Click Cancel to Delete Supplier Button
    Wait Until Page Contains Element    ${btn_popup_supplier_list_cancel_del}
    Click Element   ${btn_popup_supplier_list_cancel_del}

Click Confirm To Delete Supplier Button
    Wait Until Page Contains Element    ${btn_popup_supplier_list_confirm_del}
    Click Element   ${btn_popup_supplier_list_confirm_del}

Verify That Create Supplier Page Display Bread Crumb Correctly
    [Arguments]     ${path}=Home/Create New Supplier/Create New Supplier
    ${exp_path}=    Split String    ${path}     /
    Wait Until Page Contains Element    ${lbl_create_supplier_bread_crumb_root}  
    Wait Until Page Contains Element    ${lbl_create_supplier_bread_crumb_cur}
    Wait Until Page Contains Element    ${lbl_create_supplier_bread_crumb_cur2}
    ${root_path}=   Get Text    ${lbl_create_supplier_bread_crumb_root}
    ${cur_path}=    Get Text    ${lbl_create_supplier_bread_crumb_cur}
    ${cur2_path}=   Get Text    ${lbl_create_supplier_bread_crumb_cur2}
    Should Be Equal As Strings      ${root_path}    ${exp_path[0]}
    Should Be Equal As Strings      ${cur_path}     ${exp_path[1]}
    Should Be Equal As Strings      ${cur2_path}     ${exp_path[2]}

Verify That Create Supplier Page Display Page Title Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_title}

Verify That Create Supplier Page Display Supplier Information Section Correctly
    Verify Supplier Information Header Display Correctly
    Verify Supplier Type Label And Dropdown List Display Correctly                            
	Verify Country Label And Dropdown List Display Correctly                           
	Verify Supplier Name Label And Text Box Display Correctly
	Verify Supplier Address Label And Text Box Display Correctly
	Verify Supplier Tax Name Label And Text Box Display Correctly
	Verify Same as Supplier Name Label And Checkbox Display Correctly
	Verify Supplier Tax ID Label And Text Box Display Correctly
	Verify Supplier Tax Address Label And Text Box Display Correctly
	Verify Same as Supplier Address Label And Checkbox Display Correctly
	Verify No need to follow up Tax invoice from supplier
	Verify E-mail Reservation Label And Text Box Display Correctly
	Verify Default Markup Label And Text Box Display Correctly
	Verify Supplier Cost Type Label And Radio Button Display Correctly
	Verify Supplier Status Label And Toggle Display Correctly

Verify That Create Supplier Page Display Supplier Payment Section Correctly
    Verify Supplier Payment Header Display Correctly
    Verify Supplier Payment Method Non Credit Petty Cash Display Correctly 
    Verify Supplier Payment Method Non Credit Credit Card Display Correctly
    Verify Supplier Payment Method Non Credit Link Corporate Card Display Correctly
    Verify Supplier Payment Method Credit Terms Petty Cash Display Correctly
    Verify Supplier Payment Method Credit Terms Credit Card Display Correctly
    Verify Supplier Payment Method Credit Terms Link Corporate Card Display Correctly
    Verify Supplier Payment Method Credit Terms Cheque Display Correctly
    Verify Supplier Payment Method Credit Terms Account Transfer Display Correctly

Verify That Create Supplier Page Display Supplier SAP Code Section Correctly
Verify That Create Supplier Page Display Supplier Document Section Correctly
Verify That Create Supplier Page Display Supplier Contact Section Correctly

Verify Supplier Information Header Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information}

Verify Supplier Type Label And Dropdown List Display Correctly	
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_type}
    Wait Until Page Contains Element    ${ddl_create_supplier_information_supplier_type}

Verify Country Label And Dropdown List Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_country}
    Wait Until Page Contains Element    ${ddl_create_supplier_information_country}
	                       
Verify Supplier Name Label And Text Box Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_name}
    Wait Until Page Contains Element    ${txt_create_supplier_information_supplier_name}
	
Verify Supplier Address Label And Text Box Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_address}
    Wait Until Page Contains Element    ${txt_create_supplier_information_supplier_address}
	
Verify Supplier Tax Name Label And Text Box Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_tax_name}
    Wait Until Page Contains Element    ${txt_create_supplier_information_supplier_tax_name}
	
Verify Same as Supplier Name Label And Checkbox Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_information_same_as_supplier_name}
    Wait Until Page Contains Element    ${lbl_create_supplier_information_same_as_supplier_name}
	
Verify Supplier Tax ID Label And Text Box Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_tax_id}
    Wait Until Page Contains Element    ${txt_create_supplier_information_supplier_tax_id}
	
Verify Supplier Tax Address Label And Text Box Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_tax_address}
    Wait Until Page Contains Element    ${txt_create_supplier_information_supplier_tax_address}
	
Verify Same as Supplier Address Label And Checkbox Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_information_same_as_supplier_address}
    Wait Until Page Contains Element    ${lbl_create_supplier_information_same_as_supplier_address}
	
Verify No need to follow up Tax invoice from supplier
    Wait Until Page Contains Element    ${chk_create_supplier_information_tax_invoice} 
    Wait Until Page Contains Element    ${lbl_create_supplier_information_tax_invoice} 
	
Verify E-mail Reservation Label And Text Box Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_email_reserv}
    Wait Until Page Contains Element    ${txt_create_supplier_information_email_reserv}
	
Verify Default Markup Label And Text Box Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_default_markup}
    Wait Until Page Contains Element    ${txt_create_supplier_information_default_markup}
	
Verify Supplier Cost Type Label And Radio Button Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_cost_type}
    Wait Until Page Contains Element    ${rdb_create_supplier_information_supplier_cost_type_incl}
	Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_cost_type_incl}
    Wait Until Page Contains Element    ${rdb_create_supplier_information_supplier_cost_type_excl}
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_cost_type_excl}

Verify Supplier Status Label And Toggle Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_information_supplier_status} 
    Wait Until Page Contains Element    ${tgl_create_supplier_information_supplier_status} 

Verify Supplier Payment Header Display Correctly
    Wait Until Page Contains Element    ${lbl_create_supplier_payment}

Click Supplier Payment Method 

Verify Supplier Payment Method Non Credit Petty Cash Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_payment_noncredit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_noncredit}
    Click Elements      ${chk_create_supplier_payment_noncredit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method} 
    Wait Until Page Contains Element    ${chk_create_supplier_payment_method_noncredit_pettycash}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_noncredit_pettycash}
    Click Elements      ${chk_create_supplier_payment_method_noncredit_pettycash}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_noncredit_bf_ckin_date}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_noncredit_pettycash_bf_ckin_date}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_acct_name}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_noncredit_pettycash_acct_name}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_acct_no}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_noncredit_pettycash_acct_no}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_bank}
    Wait Until Page Contains Element    ${ddl_create_supplier_payment_method_noncredit_pettycash_acct_bank}

Verify Supplier Payment Method Non Credit Credit Card Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_payment_noncredit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_noncredit}
    Click Elements      ${chk_create_supplier_payment_noncredit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method} 
    Wait Until Page Contains Element    ${chk_create_supplier_payment_method_noncredit_creditcard}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_noncredit_creditcard}
    Click Elements      ${chk_create_supplier_payment_method_noncredit_creditcard}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_noncredit_bf_ckin_date}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_noncredit_creditcard_bf_ckin_date}
       
Verify Supplier Payment Method Non Credit Link Corporate Card Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_payment_noncredit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_noncredit}
    Click Elements      ${chk_create_supplier_payment_noncredit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method} 
    Wait Until Page Contains Element    ${chk_create_supplier_payment_method_noncredit_link}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_noncredit_link}
    Click Elements      ${chk_create_supplier_payment_method_noncredit_link}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_noncredit_bf_ckin_date}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_noncredit_link_bf_ckin_date}
       
Verify Supplier Payment Method Credit Terms Petty Cash Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_credit}
    Click Elements      ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method} 
    Wait Until Page Contains Element    ${chk_create_supplier_payment_method_credit_pettycash}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_pettycash}
    Click Elements      ${chk_create_supplier_payment_method_credit_pettycash}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_af_ckout_date}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_pettycash_af_ckout_date}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_acct_name}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_pettycash_acct_name}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_acct_no}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_pettycash_acct_no}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_bank}
    Wait Until Page Contains Element    ${ddl_create_supplier_payment_method_credit_pettycash_bank}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_note}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_pettycash_note}
       
Verify Supplier Payment Method Credit Terms Credit Card Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_credit}
    Click Elements      ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method} 
    Wait Until Page Contains Element    ${chk_create_supplier_payment_method_credit_creditcard}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_creditcard}
    Click Elements      ${chk_create_supplier_payment_method_credit_creditcard}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_af_ckout_date}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_creditcard_af_ckout_date}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_note}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_creditcard_note}
       
Verify Supplier Payment Method Credit Terms Link Corporate Card Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_credit}
    Click Elements      ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method} 
    Wait Until Page Contains Element    ${chk_create_supplier_payment_method_credit_link}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_link}
    Click Elements      ${chk_create_supplier_payment_method_credit_link}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_af_ckout_date}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_link_af_ckout_date}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_note}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_link_note}
       
Verify Supplier Payment Method Credit Terms Cheque Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_credit}
    Click Elements      ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method} 
    Wait Until Page Contains Element    ${chk_create_supplier_payment_method_credit_cheque}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_cheque}
    Click Elements      ${chk_create_supplier_payment_method_credit_cheque}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_af_ckout_date}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_cheque_af_ckout_date}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_cheque_receiver}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_cheque_receiver}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_note}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_cheque_note}
       
Verify Supplier Payment Method Credit Terms Account Transfer Display Correctly
    Wait Until Page Contains Element    ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_credit}
    Click Elements      ${chk_create_supplier_payment_credit}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method} 
    Wait Until Page Contains Element    ${chk_create_supplier_payment_method_credit_acct_transfer}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_acct_transfer}
    Click Elements      ${chk_create_supplier_payment_method_credit_acct_transfer}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_af_ckout_date}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_acct_transfer_af_ckout_date}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_acct_name}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_acct_transfer_acct_name}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_acct_no}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_acct_transfer_acct_no}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_bank}
    Wait Until Page Contains Element    ${ddl_create_supplier_payment_method_credit_acct_transfer_bank}
    Wait Until Page Contains Element    ${lbl_create_supplier_payment_method_credit_note}
    Wait Until Page Contains Element    ${txt_create_supplier_payment_method_credit_acct_transfer_note}

Verify That Create Supplier Page Display Cancel Button Correctly
    Wait Until Page Contains Element    ${btn_create_supplier_cancel}

Verify That Create Supplier Page Display Create Supplier Button Correctly
    Wait Until Page Contains Element    ${btn_create_supplier_create_supplier}
