*** Settings ***
Library 		Selenium2Library
Library 		String
Resource        ${CURDIR}/../../../locators/ui/astra/room_type.robot
Resource        ${CURDIR}/../../../config/${ENV}/config.robot

*** Keywords ***
## room type info
Click Supplier Drop Down List For Room Type
    Wait Until Page Contains Element    ${ddl_supplier_drop_down_list} 
    Click Element    ${ddl_supplier_drop_down_list}

Select Supplier For Room Type
    [Arguments]    ${value_supplier_room_type}
    Select From List   ${list_supplier_ddl}    ${value_supplier_room_type}

Input Room Type Name TH
    [Arguments]    ${room_name_th}
    Wait Until Page Contains Element    ${input_room_name_th}
    Input Text    ${input_room_name_th}    ${room_name_th}

Input Room Type Name EN
    [Arguments]    ${room_name_en}
    Wait Until Page Contains Element    ${input_room_name_en}
    Input Text    ${input_room_name_en}    ${room_name_en}

Input No. Of Room
    [Arguments]    ${no_room}
    Wait Until Page Contains Element    ${input_no_room}
    Input Text    ${input_no_room}    ${no_room}

Click Decrease No. Of Room Button
    Wait Until Page Contains Element    ${input_no_room}
    Click Button    ${btn_decrease_no_room}

Click Increase No. Of Room Button
    Wait Until Page Contains Element    ${input_no_room}
    Click Button    ${btn_increase_no_room}

Select Allow Child - Yes Radio Button
    Wait Until Page Contains Element    ${rad_allow_child_yes}
    Click Element    ${rad_allow_child_yes}

Select Allow Child - No Radio Button
    Wait Until Page Contains Element    ${rad_allow_child_no}
    Click Element    ${rad_allow_child_no}

Input Max Adult 
    [Arguments]    ${max_adult}
    Wait Until Page Contains Element    ${input_max_adult}
    Input Text    ${input_max_adult}    ${max_adult}

Click Decrease Max Adult Button
    Wait Until Page Contains Element    ${btn_decrease_max_adult}
    Click Button    ${btn_decrease_max_adult}

Click Increase Max Adult Button
    Wait Until Page Contains Element    ${btn_increase_max_adult}
    Click Button    ${btn_increase_max_adult}

Input Max Child
    [Arguments]    ${max_child}
    Wait Until Page Contains Element    ${input_max_child}
    Input Text    ${input_max_child}    ${max_child}

Click Decrease Max Child Button
    Wait Until Page Contains Element    ${btn_decrease_max_child}
    Click Button    ${btn_decrease_max_child}

Click Increase Max Child Button
    Wait Until Page Contains Element    ${btn_increase_max_child}
    Click Button    ${btn_increase_max_child}

Input Room Size
    [Arguments]    ${room_size}
    Wait Until Page Contains Element    ${input_room_size}
    Input Text    ${input_room_size}    ${room_size}

Click Decrease Room Size Button
    Wait Until Page Contains Element    ${btn_decrease_room_size}
    Click Button    ${btn_decrease_room_size}

Click Increase Room Size Button
    Wait Until Page Contains Element    ${btn_increase_room_size}
    Click Button    ${btn_increase_room_size}

Click Room View Drop Down List
    Wait Until Page Contains Element    ${ddl_room_view}
    Click Element    ${ddl_room_view}

Select Room View
    [Arguments]    ${value_room_view}
    Select From List   ${list_room_view}    ${value_room_view}

Input Bathrooms
    [Arguments]    ${bathroom}
    Wait Until Page Contains Element    ${input_bathroom}
    Input Text    ${input_bathroom}    ${bathroom}

Click Decrease Bathrooms Button
    Wait Until Page Contains Element    ${btn_dec_bathroom} 
    Click Button    ${btn_dec_bathroom} 

Click Increase Bathrooms Button
    Wait Until Page Contains Element    ${btn_inc_bathroom} 
    Click Button    ${btn_inc_bathroom} 

Change Room Type Status
    [Arguments]    ${room_status}
    Wait Until Page Contains Element    ${btn_change_room_status}
    Push Toggle Button    ${btn_change_room_status}
    Toggle Button Should Be Selected    ${btn_change_room_status}
    Label Should Exist    ${room_status}

Click Cancel Create Room Type Button
    Wait Until Page Contains Element    ${btn_cancel_create_room} 
    Click Button    ${btn_cancel_create_room}
    Select From Popup Menu    ${popup_cancel}    ${popup_cancel_confirm} 

Click Create Room Type Button
    Wait Until Page Contains Element    ${btn_create_room}
    Click Button    ${btn_create_room}

Click Expand - Hide Room Information Button
    Wait Until Page Contains Element    ${btn_collapse_room_info}
    Click Element    ${btn_collapse_room_info}

## room type facilities & amenities
Click Edit Room Type Facilites Button
    Wait Until Page Contains Element    ${btn_edit_room_fct}
    Focus   ${btn_edit_room_fct}
    Click Element   ${btn_edit_room_fct}  

Click Expend - Hide Room Facilities Button
    Wait Until Page Contains Element    ${btn_hidden_fct}
    Focus   ${btn_hidden_fct}
    Click Element    ${btn_hidden_fct}

Select All Room Facilities Checkbox
    Wait Until Page Contains Element    ${cbox_sel_all_fct}
    Focus   ${cbox_sel_all_fct} 
    Select Checkbox    ${cbox_sel_all_fct}

Unselect All Room Facilities Checkbox
    Wait Until Page Contains Element    ${cbox_sel_all_fct}
    Focus   ${cbox_sel_all_fct} 
    Unselect Checkbox    ${cbox_sel_all_fct}

Click Show More - Show Less Room Facilities Button
    Wait Until Page Contains Element    ${btn_show_more_fct}
    Focus   ${btn_show_more_fct}
    Click Element    ${btn_show_more_fct}

Select Facilities Checkbox By Name
    [Arguments]     ${fct_name}
    ${locator}      Replace String      ${cbox_each_fct}    TEXT    ${fct_name}
    Wait Until Page Contains Element    ${locator}
    Select Checkbox    ${locator}

Unselect Facilities Checkbox By Name
    [Arguments]     ${fct_name}
    ${locator}      Replace String      ${cbox_each_fct}    TEXT    ${fct_name}
    Wait Until Page Contains Element    ${locator}
    Unselect Checkbox    ${locator}

Select All Room Amenities Checkbox
    Wait Until Page Contains Element    ${cbox_sel_all_amt}
    Select Checkbox    ${cbox_sel_all_amt} 

Unselect All Room Amenities Checkbox
    Wait Until Page Contains Element    ${cbox_sel_all_amt}
    Unselect Checkbox  ${cbox_sel_all_amt}   

Click Show More - Show Less Room Amenities Button
    Wait Until Page Contains Element    ${btn_show_more_amt}
    Click Element    ${btn_show_more_amt}

Select Amenities Checkbox By Name
    [Arguments]     ${amt_name}
    ${locator}      Replace String      ${cbox_each_amt}    TEXT    ${amt_name}
    Wait Until Page Contains Element    ${locator}
    Select Checkbox    ${locator}

Unselect Amenities Checkbox By Name
    [Arguments]     ${amt_name}
    ${locator}      Replace String      ${cbox_each_amt}    TEXT    ${amt_name}
    Wait Until Page Contains Element    ${locator}
    Unselect Checkbox    ${locator}

Verify Room Facilities Header
    Wait Until Page Contains Element    ${lbl_room_fct_title}

Verify Room Amenities Header
    Wait Until Page Contains Element    ${lbl_room_amt_title}

Verify Facilities Section Should Be Expanding
    Wait Until Element Is Visible    ${lbl_room_fct_title} 

Verify Facilities Section Should Be Hiding
    Wait Until Element Is Not Visible     ${lbl_room_fct_title} 

Verify Select All Rooom Facilities Checkbox Should Be Selected
    Wait Until Page Contains Element    ${cbox_sel_all_fct}
    Checkbox Should Be Selected         ${cbox_sel_all_fct}

Verify Select All Amenities Checkbox Should Be Selected
    Wait Until Page Contains Element    ${cbox_sel_all_amt}
    Checkbox Should Be Selected         ${cbox_sel_all_amt}   

Verify Select All Room Facilities Checkbox Should Not Be Selected
    Wait Until Page Contains Element    ${cbox_sel_all_fct}
    Checkbox Should Not Be Selected     ${cbox_sel_all_fct}

Verify Select All Amenities Checkbox Should Not Be Selected
    Wait Until Page Contains Element    ${cbox_sel_all_amt}
    Checkbox Should Not Be Selected     ${cbox_sel_all_amt}

Verify Room Facilities Checkbox Should Be Selected By Name
    [Arguments]     ${fct_name}
    ${locator}      Replace String      ${cbox_each_fct}    TEXT    ${fct_name}
    Wait Until Page Contains Element    ${locator}   
    Checkbox Should Be Selected         ${locator}

Verify Room Facilities Checkbox Should Not Be Selected By Name
    [Arguments]     ${fct_name}
    ${locator}      Replace String      ${cbox_each_fct}    TEXT    ${fct_name}
    Wait Until Page Contains Element    ${locator}   
    Checkbox Should Not Be Selected     ${locator}

Verify Room Facilities Checkbox Should Be Selected By Index
    [Arguments]     ${fct_index}
    ${locator}      Replace String      ${cbox_each_fct_index}    INDEX    ${fct_index}
    Wait Until Page Contains Element    ${locator}   
    Checkbox Should Be Selected         ${locator}

Verify Room Facilities Checkbox Should Not Be Selected By Index
    [Arguments]     ${fct_index}
    ${locator}      Replace String      ${cbox_each_fct_index}    INDEX    ${fct_index}
    Wait Until Page Contains Element    ${locator}   
    Checkbox Should Not Be Selected     ${locator}

Verify Room Amenities Checkbox Should Be Selected By Name
    [Arguments]     ${amt_name}
    ${locator}      Replace String      ${cbox_each_amt}    TEXT    ${amt_name}
    Wait Until Page Contains Element    ${locator}   
    Checkbox Should Be Selected         ${locator}

Verify Room Amenities Checkbox Should Not Be Selected By Name
    [Arguments]     ${amt_name}
    ${locator}      Replace String      ${cbox_each_amt}    TEXT    ${amt_name}
    Wait Until Page Contains Element    ${locator}   
    Checkbox Should Not Be Selected     ${locator}

Verify Room Amenities Checkbox Should Be Selected By Index
    [Arguments]     ${amt_index}
    ${locator}      Replace String      ${cbox_each_amt_index}    INDEX    ${amt_index}
    Wait Until Page Contains Element    ${locator}   
    Checkbox Should Be Selected         ${locator}

Verify Room Amenities Checkbox Should Not Be Selected By Index
    [Arguments]     ${amt_index}
    ${locator}      Replace String      ${cbox_each_amt_index}    INDEX    ${amt_index}
    Wait Until Page Contains Element    ${locator}   
    Checkbox Should Not Be Selected     ${locator}

Verify Room Facilities Check Icon Should Display By Name
    [Arguments]     ${fct_name}
    ${locator}      Replace String      ${icon_each_fct_is_sel}    TEXT    ${fct_name}
    Wait Until Page Contains Element    ${locator}

Verify Room Amenities Check Icon Should Display By Name
    [Arguments]     ${amt_name}
    ${locator}      Replace String      ${icon_each_amt_is_sel}    TEXT    ${amt_name}
    Wait Until Page Contains Element    ${locator}

Verify Room Facilities Check Icon Should Display By Index
    [Arguments]     ${fct_index}
    ${locator}      Replace String      ${icon_each_fct_index_is_sel}    INDEX    ${fct_index}
    Wait Until Page Contains Element    ${locator}

Verify Room Amenities Check Icon Should Display By Index
    [Arguments]     ${amt_index}
    ${locator}      Replace String      ${icon_each_amt_index_is_sel}    INDEX    ${amt_index}
    Wait Until Page Contains Element    ${locator}

Verify Room Facilities Check Icon Should Not Display By Name
    [Arguments]     ${fct_name}
    ${locator}      Replace String      ${icon_each_fct_no_sel}    TEXT    ${fct_name}
    Wait Until Element Is Visible       ${locator}

Verify Room Amenities Check Icon Should Not Display By Name
    [Arguments]     ${amt_name}
    ${locator}      Replace String      ${icon_each_amt_no_sel}    TEXT    ${amt_name}
    Wait Until Element Is Visible       ${locator}

Verify Room Facilities Check Icon Should Not Display By Index
    [Arguments]     ${fct_index}
    ${locator}      Replace String      ${icon_each_fct_index_no_sel}    INDEX    ${fct_index}
    Wait Until Element Is Visible       ${locator}

Verify Room Amenities Check Icon Should Not Display By Index
    [Arguments]     ${amt_index}
    ${locator}      Replace String      ${icon_each_amt_index_no_sel}    INDEX    ${amt_index}
    Wait Until Element Is Visible       ${locator}

Verify Number of Selected Room Facilities Display Correctly
    [Arguments]     ${number}
    ${locator}      Replace String      ${lbl_no_fct_seled}    NUMBER    ${number}
    Wait Until Page Contains Element    ${locator}

Verify Number of Selected Room Amenities Display Correctly
    [Arguments]     ${number}
    ${locator}      Replace String      ${lbl_no_amt_seled}    NUMBER    ${number}
    Wait Until Page Contains Element    ${locator}

Verify Facilities Header
    Wait Until Page Contains Element    ${lbl_fct_header}

Verify Number of Selected Room Facilities Should Not Display
    Wait Until Element Is Not Visible   ${lbl_fct_seled}

Verify Number of Selected Amenities Should Not Display
    Wait Until Element Is Not Visible   ${lbl_amt_seled}

## temp switch mode
Click Temp Switch To Create Mode Button
    Wait Until Page Contains Element    ${btn_cre_mod}
    Focus   ${btn_cre_mod}
    Click Element   ${btn_cre_mod}

Click Temp Switch To View Mode Button
    Wait Until Page Contains Element    ${btn_viw_mod}
    Focus   ${btn_viw_mod}
    Click Element   ${btn_viw_mod}