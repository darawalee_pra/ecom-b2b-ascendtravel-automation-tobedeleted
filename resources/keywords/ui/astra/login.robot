*** Settings ***
Library         Selenium2Library
Library         ${CURDIR}/../../../../Library/common.py

*** Variables ***
${txt_astra_email_login}    			id=username
${txt_astra_password_login}    			id=password
${btn_astra_login}    					id=login
${img_astra_act_logo}                   id=landing-btn-login
${lbl_astra_login_err_msg}              id=loginError
${img_astra_home_logo}                  id=stackLogo

*** Keywords ***
Go To ASTRA Page
    [Arguments]    ${url_version}=${ASTRA_BASE_URL}    ${BROWSER}=chrome
    Open browser    ${url_version}    ${BROWSER}
    Wait Until Element Is Visible    ${btn_astra_login}

Go To ASTRA V2
    [Arguments]    ${BROWSER}=chrome
    Open browser    ${ASTRA_BASE_URL_V2}    ${BROWSER}
    Wait Until Element Is Visible    ${btn_astra_login}

Input Username On ASTRA Landing Page
    [Arguments]    ${username}
    Input Text    ${txt_astra_email_login}    ${username}

Input Password On ASTRA Landing Page
    [Arguments]    ${password}
    Input Text    ${txt_astra_password_login}    ${password}

Click Login On ASTRA Landing Page
    Click Element    ${btn_astra_login}

Login To ASTRA
    [Arguments]    ${username}    ${password}   ${url_version}=${ASTRA_BASE_URL}
    Go To ASTRA Page    ${url_version}
    Input Username On ASTRA Landing Page    ${username}
    Input Password On ASTRA Landing Page    ${password}
    Click Login On ASTRA Landing Page
    Wait Until Page Contains Element        ${img_astra_home_logo}      ${TIMEOUT}
    # Sleep    2

Verify Error Message On ASTRA Landing Page
    [Arguments]    ${expected_error_msg}=xxxxxxxxxx
    Wait Until Element Is Visible    ${lbl_astra_login_err_msg}
    ${actual_error_msg}=    Get Text    ${lbl_astra_login_err_msg}
    Should Be Equal As Strings    ${expected_error_msg}    ${actual_error_msg}

Login To Astra V2
    [Arguments]    ${username}    ${password}
    Go To ASTRA V2
    Input Username On ASTRA Landing Page    ${username}
    Input Password On ASTRA Landing Page    ${password}
    Click Login On ASTRA Landing Page
    Wait Until Page Contains Element        ${img_astra_home_logo}      ${TIMEOUT}
    # Sleep       2s

