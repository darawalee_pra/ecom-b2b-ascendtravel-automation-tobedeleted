*** Settings ***
Library 		Selenium2Library
Library         String
Library         ${CURDIR}/../../../../Library/common.py
Resource        ${CURDIR}/../../../Config/${ENV}/config.robot

*** Variables ***
${div_page_no}          //div[@class="row no-margin"]
${btn_next_page}        id=btn-next
${btn_pre_page}         id=btn-previous
${btn_page_no}          //a[contains(@id,'btn-page') and text()=INDEX]
${div_page_no}          //a[contains(@id,'btn-page')
${div_page_detail}      id=label-showing

*** Keywords ***
Get Total Page Number
    [Arguments]     ${total_items}      ${page_size}
    ${total_page_no}=    Evaluate    ${total_items}/${page_size}
    ${total_page_no}=    Convert To Number      ${total_page_no}    1
    ${string}=           Convert To String      ${total_page_no}
    ${output}=           Fetch From Right       ${string}   .
    ${result}   RUN KEYWORD IF  '${output}' <= '4'   Round Up      ${total_page_no}
    ...         ELSE             Convert To Number      ${total_page_no}      0
    ${result}=  Convert To Integer  ${result}
    [Return]    ${result}

Round Up
    [Arguments]     ${number}
    ${output}=      Convert To Number      ${number}      0
    ${result}=      Evaluate    ${output}+1
    [Return]        ${result}

Click Next Page Button
    Wait Until Page Contains Element    ${btn_next_page}
    Click Element   ${btn_next_page}

Click Previous Page Button
    Wait Until Page Contains Element    ${btn_pre_page}
    Click Element   ${btn_pre_page}   

Click Page Number Button
    [Arguments]     ${page_no}  
    ${page_no}      Convert To String   ${page_no}
    ${locator}      Replace String      ${btn_page_no}  INDEX   ${page_no}
    Wait Until Page Contains Element    ${locator}
    Click Element   ${locator}

Verify Page Number Button Should Display
    [Arguments]     ${page_no}  
    ${page_no}      Convert To String   ${page_no}
    ${locator}      Replace String      ${btn_page_no}  INDEX   ${page_no}
    Wait Until Page Contains Element    ${locator}

Verify Previous Button Should Display
    Wait Until Page Contains Element     ${btn_pre_page}

Verify Previous Button Should Not Display
    Page Should Not Contain Element      ${btn_pre_page}

Verify Next Button Should Display
    Wait Until Page Contains Element     ${btn_next_page}

Verify Next Button Should Not Display
    Page Should Not Contain Element      ${btn_next_page}

Verify Page Navigate Buttons Should Not Display
    Page Should Not Contain Element      ${div_page_no}
    Verify Next Button Should Not Display
    Verify Previous Button Should Not Display

Verify Total Page Number
    [Arguments]     ${exp_total_page_no}
    ${act_total_page_no}    Get Matching Xpath Count      ${page_no}
    Should Be Equal     ${act_total_page_no}      ${exp_total_page_no}

Verify Pagination Details
    [Arguments]     ${from}     ${to}   ${total}
    Wait Until Page Contains Element    ${div_page_detail} 
    ${value}=   Get Text    ${div_page_detail}
    Should Contain  '${value}'  'Showing ${from} to ${to} of ${total} entries'

Verify Pagination Details Should Not Display
    Page Should Not Contain Element     ${div_page_detail}

Verify Page Number Button Should Not Display
    Page Should Not Contain Element     ${div_page_no}