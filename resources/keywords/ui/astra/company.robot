*** Settings ***
Library 		Selenium2Library
Library 		String
Resource        ${CURDIR}/../../../Config/${ENV}/config.robot

*** Variables ***
# locators
${btn_side_menu_company}                                        //*[@id="menu-Create Company"]/span
${lbl_create_company_bread_crumb_root}                          //li[@class='breadcrumb-item primary']/a[text()="Home"]
${lbl_create_company_bread_crumb_cur}                           //li[@class='breadcrumb-item primary']/a[text()="Company"]
${lbl_create_company_bread_crumb_cur2}                          //li[@class='breadcrumb-item']
${lbl_create_company_title}                                     //h3[text()='Create Company']                       
${lbl_company_information}                                      //a[@id="company-management-navtabs-information"]
${lbl_payment_method}                                           //a[@id="company-management-navtabs-payment-method"]
${lbl_user_permission}                                          //a[@id="company-management-navtabs-user-permission"]
${lbl_company_user_list}                                        //a[@id="company-management-navtabs-company-user-list"]
# General Information #
${lbl_create_company_general_information}                       //h4[text()='General Information']  

${lbl_create_company_company_type}                              //*[@id="company-information-label-company-type"]
${rdb_create_company_company_type_parent_company}               //input[@id="company-information-radio-parent-company"]
${lbl_create_company_company_type_parent_company}               //div[@class='radio col-lg-3']/label[text()='Parent Company']
${rdb_create_company_company_type_sub_company}                  //input[@id="company-information-radio-sub-company"]
${lbl_create_company_company_type_sub_company}                  //div[@class='radio col-lg-3']/label[text()='Sub-Company']
${ddl_create_company_company_type_company_list}                 //select[@id="company-information-select-parent-company"]

${lbl_create_company_customer_type}                             //*[@id="company-information-label-customer-type"]
${rdb_create_company_customer_type_b2b}                         //input[@id="company-information-radio-b2b"]
${lbl_create_company_customer_type_b2b}                         //div[@class='radio col-lg-3']/label[text()='B2B']
${rdb_create_company_customer_type_agent}                       //input[@id="company-information-radio-agent"]
${lbl_create_company_customer_type_agent}                       //div[@class='radio col-lg-3']/label[text()='Agent']

${lbl_create_company_user_register_type}                        //*[@id="company-information-label-user-register-type"]
${rdb_create_company_user_register_type_self_register}          //input[@id="company-information-radio-self-register"]
${lbl_create_company_user_register_type_self_register}          //div[@class='radio col-lg-3']/label[text()='Self Register by Domain']
${txt_create_company_user_register_type_company_domain}         //input[@id="company-information-input-company-domain"]
${rdb_create_company_user_register_type_sys_integration}        //input[@id="company-information-radio-system-integration"]
${lbl_create_company_user_register_type_sys_integration}        //div[@class='radio col-lg-3']/label[text()='System Integration']
${rdb_create_company_user_register_type_upload_user}            //input[@id="company-information-radio-upload-user"]
${lbl_create_company_user_register_type_upload_user}            //div[@class='radio col-lg-6']/label[text()='Upload User (Company without domain)']

${lbl_create_company_company_name_th}                           //div[@class='col-md-6']/label[text()='Company Name (TH)']
${txt_create_company_company_name_th}                           //*[@id="company-information-input-company-name-th"]
${lbl_create_company_company_name_en}                           //div[@class='col-md-6']/label[text()='Company Name (EN)']
${txt_create_company_company_name_en}                           //*[@id="company-information-input-company-name-en"]
${lbl_create_company_company_address_th}                        //div[@class='col-md-6']/label[text()='Company Address (TH)']
${txt_create_company_company_address_th}                        //*[@id="company-information-textarea-company-address-th"]
${lbl_create_company_company_address_en}                        //div[@class='col-md-6']/label[text()='Company Address (EN)']
${txt_create_company_company_address_en}                        //*[@id="company-information-textarea-company-address-en"]

# Company Doument #
${lbl_create_company_company_document}                          //h4[text()='Company Document'] 
${lbl_create_company_company_document_drop_file}                //p[text()='Drop file here to upload']
${lbl_create_company_company_document_browse}                   //span[text()='Browse']
${lbl_create_company_company_document_warning_txt}              //small[text()='File can be .png, .jpg, .pdf, maximum size 500 KB']

# Company Contact #
${lbl_create_company_company_contact}                           //h4[text()='Company Contact'] 
${lbl_create_company_company_contact_name}
${lbl_create_company_company_contact_name}
${lbl_create_company_company_contact_position}
${lbl_create_company_company_contact_position}
${lbl_create_company_company_contact_email}
${txt_create_company_company_contact_email}
${ddl_create_company_company_contact_phone}                     //select[@id=""]
${txt_create_company_company_contact_phone}
${txt_create_company_company_contact_action}
${btn_create_company_company_contact_action}
${btn_create_company_company_contact_add_new_contact}

# Account Manager Owner #
${lbl_create_company_account_manager_owner}                     //h4[text()='Account Manager Owner'] 
${lbl_create_company_account_manager_owner_name}
${lbl_create_company_account_manager_owner_name}
${lbl_create_company_account_manager_owner_position}
${lbl_create_company_account_manager_owner_position}
${lbl_create_company_account_manager_owner_email}
${txt_create_company_account_manager_owner_email}
${ddl_create_company_account_manager_owner_phone}               //select[@id=""]
${txt_create_company_account_manager_owner_phone}
${txt_create_company_account_manager_owner_action}
${btn_create_company_account_manager_owner_action}
${btn_create_company_account_manager_owner_add_new_contact}     

${btn_create_company_cancel}
${btn_create_company_submit}

*** Keywords ***
Click Supplier Main Menu
    Wait Until Page Contains Element    ${btn_side_menu_supplier}
    Click Element   ${btn_side_menu_supplier}
    Wait Until Page Contains Element    ${btn_side_menu_supplier_list}
    Wait Until Page Contains Element    ${btn_side_menu_create_supplier}

Click Create Supplier Menu
    Wait Until Page Contains Element    ${btn_side_menu_create_supplier}
    Click Element   ${btn_side_menu_create_supplier}

Go To Create Supplier
   Go To Supplier List
   Click Create Supplier Button

Click Create Supplier Button
    Wait Until Page Contains Element    ${btn_side_menu_create_supplier}
    Click Element   ${btn_side_menu_create_supplier}
    Wait Until Page Contains Element    ${lbl_create_supplier_title}

Verify Create Supplier Page Display Static Elements Correctly
    Verify That Create Supplier Page Display Bread Crumb Correctly
    Verify That Create Supplier Page Display Page Title Correctly
    Verify That Create Supplier Page Display Supplier Information Section Correctly
    Verify That Create Supplier Page Display Supplier Payment Section Correctly
    Verify That Create Supplier Page Display Cancel Button Correctly
    Verify That Create Supplier Page Display Create Supplier Button Correctly

Verify That Supplier List Page Display Search Text Field
    Wait Until Page Contains Element    ${lbl_supplier_list_search}