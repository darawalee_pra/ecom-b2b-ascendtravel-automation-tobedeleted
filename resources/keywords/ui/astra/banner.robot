*** Settings ***
Resource            ${CURDIR}/../../../keywords/ui/astra/login.robot
Resource            ${CURDIR}/../../../keywords/ui/astra/report.robot
Resource            ${CURDIR}/../../../locators/ui/astra/banner.robot
Library         ${CURDIR}/../../../../Library/common.py
#Resource            ${CURDIR}/../../feature/login.robot
#Resource            ${CURDIR}/../../feature/header.robot

*** Keywords ***
Click Banner Menu
    Wait Until Element Is Visible           ${main-menu-banner}                         ${TIMEOUT}  
    Sleep                                   1s                     
    Click Element                           ${main-menu-banner}

Click Banner List Sub Menu 
    Wait Until Element Is Visible           ${sub-menu-banner-bannerlist}               ${TIMEOUT} 
    Sleep                                   1s                        
    Click Element                           ${sub-menu-banner-bannerlist}
    
Click Button Create New
    Wait Until Element Is Visible           ${btn-create}                               ${TIMEOUT}           
    Click Element                           ${btn-create}
    
Click Button Save
    Wait Until Element Is Visible           ${btn-save}                                 ${TIMEOUT}                 
    Click Element                           ${btn-save}   

Click Modal Alert
#    Wait Until Element Is Visible           ${modal-alert}                              ${TIMEOUT}
#    Wait Until Element Is Visible           ${modal-alert-div}                          ${TIMEOUT}
    Wait Until Element Is Visible           ${modal-button}                             ${TIMEOUT}   
    Sleep                                   0.5s 
#    Click Element                           ${modal-alert-div}        
    Click Element                           ${modal-button}  

Click Delete And Save
    Wait Until Element Is Visible           ${modal-delete-save}                        ${TIMEOUT}        
    Click Element                           ${modal-delete-save}          
     
Click Button Edit
    Wait Until Element Is Visible           ${btn-edit-0}                               ${TIMEOUT}                 
    Click Element                           ${btn-edit-0}    
    Sleep                                   2s     

Click Button Delete
    Wait Until Element Is Visible           ${btn-delete-0}                             ${TIMEOUT}               
    Click Element                           ${btn-delete-0}       
         
Click Button Book
    Wait Until Element Is Visible           ${banner-btn-book-0}                        ${TIMEOUT}             
    Click Element                           ${banner-btn-book-0}        
     
Click Dropdown Status
    Wait Until Element Is Visible           ${ddl-status}                               ${TIMEOUT}     
    Click Element                           ${ddl-status}   

Click Dropdown Status All Status
    Wait Until Element Is Visible           ${all-status}                               ${TIMEOUT}   
    Click Element                           ${all-status}             

Click Dropdown Status Active
    Wait Until Element Is Visible           ${status-active}                             ${TIMEOUT}   
    Click Element                           ${status-active} 

Click Dropdown Status Inactive
    Wait Until Element Is Visible           ${status-inactive}                           ${TIMEOUT}   
    Click Element                           ${status-inactive}   

Click Dropdown Status Pending
    Wait Until Element Is Visible           ${status-pending}                            ${TIMEOUT}   
    Click Element                           ${status-pending}   

Browse File Image Th
    [Arguments]                             ${path}
    Browse File Image                       ${browse-file-th}                           ${path}

Browse File Image En
    [Arguments]                             ${path}  
    Browse File Image                       ${browse-file-en}                           ${path}

Input Title Name Th
    [Arguments]                             ${text}
    Input Text                              ${title-name-th}                            ${text}

Input Title Name En
    [Arguments]                             ${text}
    Input Text                              ${title-name-en}                            ${text} 

Input Description Th
    [Arguments]                             ${text}
    Input Text                              ${description-th}                           ${text}      
   
Input Description En
    [Arguments]                             ${text}
    Input Text                              ${description-en}                           ${text}  

Input Link
    [Arguments]                             ${url}
    Input Text                              ${link}                                     ${url}  

Input Book Period From
    [Arguments]                             ${add_date}
    Input Date From Today                   ${book-from-date}                           ${add_date}                         

Input Book Period To
    [Arguments]                             ${add_date}
    Input Date From Today                   ${book-to-date}                             ${add_date}   

Input Stay Period From
    [Arguments]                             ${add_date}
    Input Date From Today                   ${stay-from-date}                           ${add_date}       

Input Stay Period To
    [Arguments]                             ${add_date}
    Input Date From Today                   ${stay-to-date}                             ${add_date} 

Input Banner Start
    [Arguments]                             ${add_date}
    Input Date From Today                   ${banner-start}                             ${add_date}   

Input Banner End
    [Arguments]                             ${add_date}
    Input Date From Today                   ${banner-end}                               ${add_date}       

Input Banner Start Time
    [Arguments]                             ${add_time}
    Input Time Picker ASTRA                 ${banner-start-time}                        ${add_time}  

Input Banner End Time
    [Arguments]                             ${add_time}
    Input Time Picker ASTRA                 ${banner-end-time}                          ${add_time}              
       
Input Priority 
    [Arguments]                             ${text}
    Input Text                              ${priority}                                 ${text}  

Input Margin 
    [Arguments]                             ${text}
    Input Text                              ${margin}                                   ${text}  
        
Input Time Picker ASTRA
    [Arguments]                             ${timepicker}                               ${value}
    ${date}=    	                        Get Current Date                            result_format=%Y-%m-%d-%H-%M
    ${date}=    	                        Add Time To Date	                        ${date}    ${value}
    ${hour}=                                Convert Date                                ${date}    result_format=%H
    ${minute}=                              Convert Date                                ${date}    result_format=%M      
    Press Key                               ${timepicker}                               ${hour}    
    Press Key                               ${timepicker}                               ${minute}  

Input Search 
    [Arguments]                             ${value}
    Wait Until Element Is Visible           ${input_search}                             ${TIMEOUT}                
    Input Text                              ${input_search}                             ${value}
    Press Key                               ${input_search}                             \\13    

Input Date From Today
    [Arguments]                             ${date-picker}                              ${value}
    Input Date On ASTRA From Today          ${date-picker}                              ${value}    

Browse File Image
    [Arguments]                             ${browse-file}                              ${path}
    ${PATH}                                 get_canonical_path                          ${path}
    Choose File                             ${browse-file}                              ${path}      

Wait    
    [Arguments]                             ${time}
    Log To Console                          Wait...${time}       
    Sleep                                   ${time}    

Verify Description En First Row  
    [Arguments]                             ${expect}
    Wait Until Element Is Visible           ${label-banner-detail-0}                    ${TIMEOUT}  
    ${label-banner-detail-0}=               Get Text                                    ${label-banner-detail-0}
    Should Be Equal                         ${label-banner-detail-0}                    ${expect} 

Verify Status First Row  
    [Arguments]                             ${expect}
    Wait Until Element Is Visible           ${status-0}                                 ${TIMEOUT} 
    ${status-0}=                            Get Text                                    ${status-0} 
    Should Be Equal                         ${status-0}                                 ${expect}   
    
Verify Banner First Item 
    [Arguments]                             ${expect}
    Wait Until Element Is Visible           ${banner-lbl-hotel-name-0}                  ${TIMEOUT} 
    ${banner-lbl-hotel-name-0} =            Get Text                                    ${banner-lbl-hotel-name-0} 
    Should Be Equal                         ${banner-lbl-hotel-name-0}                  ${expect}  

Verify Banner First Item Not Contain 
    [Arguments]                             ${expect}
    Wait Until Element Is Visible           ${banner-lbl-hotel-name-0}                  ${TIMEOUT} 
    ${banner-lbl-hotel-name-0}=             Get Text                                    ${banner-lbl-hotel-name-0} 
    Should Not Be Equal                     ${banner-lbl-hotel-name-0}                  ${expect}          

Verify Hotel Name
    [Arguments]                             ${url}                                      ${expect}
    Select Window                           url=${url}                           
    Wait Until Element Is Visible           ${levelD-text-hotel-name}                   ${TIMEOUT}   
    ${levelD-text-hotel-name}=              Get Text                                    ${levelD-text-hotel-name} 
    Should Be Equal                         ${levelD-text-hotel-name}                   ${expect} 
    Close Window
    Select Window

Verify No Records
    Wait Until Element Is Visible                       ${data-no-record}                           ${TIMEOUT}  
    Element Should Be Visible                           ${data-no-record}
       

Go To ASTRA Page And Login
    [Arguments]                             ${username}     ${password}
    Go To ASTRA Page
    Input Username On ASTRA Landing Page                ${username}
    Input Password On ASTRA Landing Page                ${password}
    Click Login On ASTRA Landing Page
    Sleep       2s

Go To Ascend Travel And Change Lang En
    Login To Ascend Travel Frontend          ${CREDIT_CARD_USER2}          ${CREDIT_CARD_PASS2}
    Change Language - Desktop                ${TYPES_LANGUAGE_EN}
    Sleep       2s

Go To Banner List
    Click Banner Menu
    Click Banner List Sub Menu

Go To Button Create New Banner
    Click Button Create New

Go To Button Edit First Row
    Click Button Edit

Go To Save Banner
    Click Button Save
    Click Modal Alert

Go Book
    Click Button Book

Create Banner/Edit
    [Arguments]                             ${path-img-th}      ${path-img-en}      ${title-th}     ${title-en}     ${des-th}       ${des-en}       ${url}      ${book-from}
    ...                                     ${book-to}      ${stay-from}    ${stay-to}      ${banner-start}     ${banner-start-time}        ${banner-end}         ${banner-end-time}
    ...                                     ${priority}     ${margin}
    Browse File Image Th                    ${path-img-th}
    Browse File Image En                    ${path-img-en}
    Input Title Name Th                     ${title-th}
    Input Title Name En                     ${title-en}
    Input Description Th                    ${des-th}
    Input Description En                    ${des-en}
    Input Link                              ${url}
    Input Book Period From                  ${book-from}
    Input Book Period To                    ${book-to}
    Input Stay Period From                  ${stay-from}
    Input Stay Period To                    ${stay-to}
    Input Banner Start                      ${banner-start}
    Input Banner Start Time                 ${banner-start-time}
    Input Banner End                        ${banner-end}
    Input Banner End Time                   ${banner-end-time}
    Input Priority                          ${priority}
    Input Margin                            ${margin}

Switch To ASTRA
    Switch Browser      1

Switch To Frontend
    Switch Browser      2
    Reload Page

Select Status All
    Click Dropdown Status
    Click Dropdown Status All Status

Select Status Active
    Click Dropdown Status
    Click Dropdown Status Active

Select Status Inactive
    Click Dropdown Status
    Click Dropdown Status Inactive

Select Status Pending
    Click Dropdown Status
    Click Dropdown Status Pending

Input Search In Banner List
    [Arguments]         ${text}
    Input Search        ${text}

Save Banner
    Click Button Save

Delete Banner
    Click Button Delete
    Click Delete And Save
    Sleep   5s

Verify Banner List First Row
    [Arguments]         ${des-en}       ${status}
    Sleep   2s
    Verify Description En First Row        ${des-en}
    Verify Status First Row     ${status}

Verify Hotel Name Level D
    [Arguments]     ${url}      ${hotel}
    Click Button Book
    Verify Hotel Name        ${url}      ${hotel}

Verify No Reccords In Banner List
    Verify No Records

Verify Banner Frontend First Item
    [Arguments]     ${label}
    Verify Banner First Item        ${label}

Verify Banner Frontend First Item Should Not Show
    [Arguments]     ${expect}
    Verify Banner First Item Not Contain        ${expect}

Wait Time
    [Arguments]     ${time}
    Wait        ${time}




