*** Settings ***
Library 	String
Library 	Collections
Library 	Telnet
Library 	Process

Resource 	${CURDIR}/../../config/${ENV}/config.robot
Resource 	${CURDIR}/../../locators/ui/uicommon.robot

*** Variables ***
${APPIUMTIMEOUT}	${30}
${DEVICETIMEOUT}	${3000}
${GLOBALRETRYINTERVAL}	${5}
${GLOBALSELENIUMSPEED}	${0.8}
${chrome_browser}	Chrome
${CHROMEDRIVERPATH}     /usr/local/bin/chromedriver
# ${marionetteBinaryPath}	${CURDIR}${/}..${/}tools${/}geckodriver.exe
${DOWNLOADDIRECTORY}	../Temp

*** Keywords ***
_change resolution to desktop
    Set Window Size    1280    800

_change resolution to tablet
    # Set Window Size    768    1024
    Set Window Size    834    1112

_change resolution to mobile
    Set Window Size    375    667

Open Browser
	[Arguments]     	${url} 	${device_type}=${desktop_device} 	${BROWSER}=${chrome_browser}

	Selenium2Library.Open browser    ${url}    ${BROWSER}
	Set Selenium Speed  0.01s
	Run Keyword If    '${device_type}'=='${desktop_device}'    _change resolution to desktop
    Run Keyword If    '${device_type}'=='${tablet_device}'     _change resolution to tablet
	Run Keyword If    '${device_type}'=='${mobile_device}'     _change resolution to mobile

Create Random email address
	${random_email_address}= 	Generate Random String	8	[LOWER]
	Log To Console 	[random_email_address]=${random_email_address}@ascendcorp.com
	[Return] 	${random_email_address}@ascendcorp.com


Browse File
	[Arguments] 	${file_path}

	${file_path}  get_canonical_path    ${file_path}
    Choose File 	${btn_upload_file} 	${file_path}
