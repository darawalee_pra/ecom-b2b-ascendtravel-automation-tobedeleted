*** Settings ***
Library     DateTime
Library     String
Library     Selenium2Library
Library     DatabaseLibrary
Library     OperatingSystem
Library     ${CURDIR}/../../Python_Library/excel.py

Resource    ${CURDIR}/../feature/backend/login.robot
Resource    ${CURDIR}/../../../locators/ui/backend/export_reconcile_revise_payment_flow.robot

*** Keywords ***
Teardown Export Reconcile Revise Payment Flow
    Remove File In Directory Reconcile Excel
    Close All Browsers

Open ACT Backend Browser For Download File
    [Arguments]    ${URL}    ${Folder_name}    ${TESTDATA_PATH}
    ${TESTDATA_PATH}    get_canonical_path    ${TESTDATA_PATH}
    ${system}=    Evaluate    platform.system()    platform
    ${CHROME_OPTIONS}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    ${prefs}    Create Dictionary    download.default_directory=${TESTDATA_PATH}/${Folder_name}
    Call Method    ${CHROME_OPTIONS}    add_experimental_option    prefs    ${prefs}
    Call Method    ${CHROME_OPTIONS}    add_argument    --disable-extensions
    Create Webdriver    Chrome    chrome_options=${CHROME_OPTIONS}
    Go To    ${URL}

Go To Download Reconcile Report Revise Payment Flow
    go to    ${BACKEND_BASE_URL}/admin/report/export-revise-payment-booking.aspx
    wait until page contains    Export Booking Report (Revise Payment Flow)    30s

Click Export Report Button
    wait until element is visible   ${btn_export_button}    30s
    click element                   ${btn_export_button}

Get Value Excel
    [Arguments]    ${path_excel}    ${sheet_name}    ${cell_name}
    ${value_cell}    get_value_excel_by_column    ${path_excel}    ${sheet_name}    ${cell_name}
    [Return]    ${value_cell}

Remove File In Directory Reconcile Excel
    Empty Directory    ${CURDIR}/../../data_test/reconcile

Get Date Now Format For Input Period Range
    ${current_date}         Get Current Date
    ${new_date_format}    Convert Date    ${current_date}    result_format=%B %d, %Y
    [Return]    ${new_date_format}

Input Period Range
    [Arguments]    ${start}    ${end}
    Execute JavaScript    window.$('.input-daterange').setDateRangeValue(new Date('${start}'), new Date('${end}'))

Wait For File to Finish Downloading
    [Arguments]    ${path}    ${ext}
    ${count}=    Set Variable    0
    :FOR    ${tries}    IN RANGE    120
    \    ${count}=    Count Files In Directory    ${path}    *.${ext}
    \    Exit For Loop If    ${count} == ${1}
    \    Sleep   1
    Should Be True    ${count} > 0

Input Period Range From
    [Arguments]    ${period_range_from_value}
    Input Text    ${txt_period_range_from}    ${period_range_from_value}

Input Period Range To
    [Arguments]    ${period_range_to_value}
    Input Text    ${txt_period_range_to}    ${period_range_to_value}

Verify Reconcile Excel By Booking Id
    [Arguments]    ${booking_id}    ${sheet_name}    ${column_name}    ${expect_value}
    ${actual_file}=    List Files In Directory          ${CURDIR}/../../data_test/reconcile
    ${actual_file_path}=    get_canonical_path                ${CURDIR}/../../data_test/reconcile/${actual_file[0]}

    ${number_row}    find_number_row_excel    ${actual_file_path}    ${sheet_name}    ${booking_id_column}    ${booking_id}
    ${value_cell}    Get Value Excel    ${actual_file_path}    Sheet1    ${column_name}${number_row}

    Should Be Equal As Strings    ${value_cell}    ${expect_value}

Verify Reconcile Excel By Booking Id Multiple Row
    [Arguments]    ${booking_id}    ${sheet_name}    ${column_name}    ${expect_values}
    ${actual_file}=    List Files In Directory          ${CURDIR}/../../data_test/reconcile
    ${actual_file_path}=    get_canonical_path                ${CURDIR}/../../data_test/reconcile/${actual_file[0]}

    ${number_rows}    find_multiple_number_row_excel    ${actual_file_path}    ${sheet_name}    ${booking_id_column}    ${booking_id}

    ${index}=    Set Variable    0
    :FOR    ${number_row}    IN    @{number_rows}
    \    ${value_cell}    Get Value Excel    ${actual_file_path}    Sheet1    ${column_name}${number_row}
    \    Should Be Equal As Strings    ${value_cell}    @{expect_values}[${index}]
    \    ${index}=    Evaluate    ${index}+1

Remove File In Directory
    [Arguments]    ${path_folder}
    Empty Directory    ${path_folder}

Verify Reconcile Excel Is Not Null By Booking Id
    [Arguments]    ${booking_id}    ${sheet_name}    ${column_name}
    ${actual_file}=    List Files In Directory          ${CURDIR}/../../data_test/reconcile
    ${actual_file_path}=    get_canonical_path                ${CURDIR}/../../data_test/reconcile/${actual_file[0]}

    ${number_row}    find_number_row_excel    ${actual_file_path}    ${sheet_name}    ${booking_id_column}    ${booking_id}
    ${value_cell}    Get Value Excel    ${actual_file_path}    Sheet1    ${column_name}${number_row}

    Should Not Be Equal As Strings    ${value_cell}    None