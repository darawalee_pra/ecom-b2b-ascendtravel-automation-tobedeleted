*** Settings ***
Library     Selenium2Library
Library     DateTime
Library     String
Resource    ${CURDIR}/../../../locators/ui/backend/order_detail.robot

*** Keywords ***
Go To Order Detail Page
    [Arguments]     ${booking_id}
    Go To   ${BACKEND_BASE_URL}/admin/order/detail.aspx?bid=${booking_id}
    wait until page contains      Booking Details     50

Open Web And Login
    Login To Ascend Travel Frontend    ${BIG_COMPANY_USERNAME}    ${BIG_COMPANY_PASSWORD}
    Sleep    2s
    Change Language - Desktop    ${TYPES_LANGUAGE_EN}

Perpare Booking For Order Detail
    ${room1}            Create LV D Select Room Type Object    ${DIRECT_ROOM_NON_REFUNDABLE}    ${quantity}
    ${list_room}        Create List    ${room1}
    ${GUEST1_ROOM1}    Create Checkout 2 Guest Object    0    0
    ...    ${BOOKER_FIRST_NAME}    ${BOOKER_LAST_NAME}    ${GUEST_EMAIL}
    ...    ${GUEST_NATIONALITY}
    ${list_guest}       Create List    ${GUEST1_ROOM1}
    ${thk_msg_status}   Create Thankyou Booking Status Object
    ...    ${MSG_STATUS_PAYMENT_SUCCESS}    ${MSG_STATUS_PAYMENT_SUCCESS_DESC}

    ${checkout_day}         Evaluate    ${CHECKIN_DAY} + ${NIGHT}

    ${BOOKER_INFO}          Create Checkout 2 Booker Object    ${BOOKER_FIRST_NAME}    ${BOOKER_LAST_NAME}
    ...    ${BIG_COMPANY_USERNAME}    ${BOOKER_MOBILE_NO}

    ${current_date}         Get Current Date
    ${card_expire_date}     Add Time To Date    ${current_date}    365 days
    ${card_expire_month}    Convert Date    ${card_expire_date}    result_format=%m
    ${card_expire_year}     Convert Date    ${card_expire_date}    result_format=%Y
    ${valid_card_expire}    Convert Date    ${current_date}    result_format=%m %Y
    ${PAYNOW_PAYON_DATE}    Convert Date    ${current_date}    result_format=%Y-%m-%d

    ${payment_date_short}     Convert Date    ${PAYNOW_PAYON_DATE}    result_format=X%d %b %y
    ${payment_date_short}     Replace String    ${payment_date_short}    X0    ${EMPTY}
    ${payment_date}     Convert Date    ${PAYNOW_PAYON_DATE}    result_format=X%d %B %Y
    ${payment_date}     Replace String    ${payment_date}    X0    ${EMPTY}
    ${payment}          Create Thankyou Payment Object    ${PAYMENT_METHOD_CREDIT_CARD}    ${payment_date}

    ${current_date}         Get Current Date
    ${BOOKING_DATE}         Convert Date    ${current_date}    result_format=%Y-%m-%d

    Open Web And Login
    # search
    Search Home Hotel By Purpose Keyword CheckIn CheckOut
    ...    ${purpose}    ${DIRECT_HOTEL_SEARCH}    ${CHECKIN_DAY}    ${CHECKOUT_DAY}
    # level D
    Select Room Condition Quantity By List    ${list_room}
    ${booking_info}    Get Object LV D Booking Information
    ${list_room_selected}    Get List Object LV D Room Condition Selected    ${night}
    Verify Cart LV D List Room Selected And PayOn Date    ${list_room_selected}    ${PAYNOW_PAYON_DATE}
    Click Cart Book Now - Desktop
    # checkout 1
    Verify Checkout 1 And Cart    ${booking_info}    ${list_room_selected}
    # checkout 2
    Verify And Fill Data Checkout 2 Booker Guest    ${purpose}    ${BOOKER_INFO}    ${list_guest}
    # checkout 3
    ${CREDIT_CARD_INFO}          Create Checkout 3 Credit Card Object
    ...    ${CREDIT_CARD_NO}    ${CREDIT_CARD_NAME}
    ...    ${card_expire_month}    ${card_expire_year}    ${CREDIT_CARD_CVV}
    ...    ${valid_card_expire}

    Verify And Fill Data Checkout 3 Credit Card
    ...    ${PAYNOW_PAYON_DATE}    ${CREDIT_CARD_INFO}

    ${NEW_TAX_ADDRESS}      Create Checkout 3 Tax Address Object
    ...    ${ADDRESS_COMPANY}    ${ADDRESS_TAX_NUMBER}    ${ADDRESS_ADDRESS}

    Select Checkout 3 Tax Address By    0    ${NEW_TAX_ADDRESS}
    ${tax_address}    Get Checkout 3 Tax Address Object By Index    0
    Click Cart Checkout Continue - Desktop
    # thank you
    Verify Thankyou Page Is Visible
    ${thk_list_room}    Convert List Room Object For Thankyou Object    ${list_room_selected}
    ${thk_list_guest}    Convert List Guest Object For Thankyou Object    ${list_guest}
    Verify Thankyou Booking Status And Detail    ${thk_msg_status}    ${BOOKING_DATE}    ${BOOKER_INFO}
    ...    ${tax_address}    ${booking_info}    ${thk_list_room}    ${thk_list_guest}    ${payment}
    ${thk_booking_id}    Get Thankyou Booking Id
    [Return]    ${thk_booking_id}

Login And Go To Order Detail Page And Verify Case Pay Later, Direct hotel, Credit Card, Free Cancellation
    [Arguments]    ${BookingID}
    Open Browser And Login Backend
    Go To   ${BACKEND_BASE_URL}/admin/order/detail.aspx?bid=${BookingID}
    wait until page contains      Booking Details     50
    sleep      10s

    Log To Console  Verify Booker Details On Booking Detail
    wait until element is visible     ${lbl_booker_details_name}                      10
    ${booker_details_name_ui}=      Get Text    ${lbl_booker_details_name}
    Should Be Equal   ${booker_first_name} ${booker_last_name}          ${booker_details_name_ui}

    wait until element is visible     ${lbl_booker_details_email}                      10
    ${booker_details_email_ui}=      Get Text    ${lbl_booker_details_email}
    Should Be Equal   ${booker_email}          ${booker_details_email_ui}

    wait until element is visible     ${lbl_booker_details_contact_no}                      10
    ${booker_details_contact_no_ui}=      Get Text    ${lbl_booker_details_contact_no}
    Should Be Equal   ${booker_mobile}          ${booker_details_contact_no_ui}

    wait until element is visible     ${lbl_booker_details_nationality}                      10
    ${booker_details_nationality_no_ui}=      Get Text    ${lbl_booker_details_nationality}
    Should Be Equal   ${booker_nationality}          ${booker_details_nationality_no_ui}

    Log To Console  Verify Tax Invoice Address on Order Detail
    wait until element is visible     ${lbl_tax_invoice_address_name_company_name}                      10
    ${company_name_ui}=      Get Text    ${lbl_tax_invoice_address_name_company_name}
    Should Be Equal   ${company_name}          ${company_name_ui}

    wait until element is visible     ${lbl_tax_invoice_address_thaiid_tax_id}                      10
    ${company_tax_number_ui}=      Get Text    ${lbl_tax_invoice_address_thaiid_tax_id}
    Should Be Equal   ${tax_number}          ${company_tax_number_ui}

    wait until element is visible     ${lbl_tax_invoice_address_address}                      10
    ${company_tax_address_ui}=      Get Text    ${lbl_tax_invoice_address_address}
    Should Be Equal   ${tax_address}          ${company_tax_address_ui}

    Log To Console  Verify Booking Requirement on Order Detail
#    ${current_date}    Get Current Date
#    ${check_in_date}     Add Time To Date    ${current_date}    ${check_in_day_pay_later} Days
#    ${check_out_date}    Add Time To Date    ${current_date}    ${check_out_day_pay_later} Days
#
#    ${order_detail_check_in_date}     Convert Date Checkin Checkout In Order Detail    ${check_in_date}
#    ${order_detail_check_out_date}    Convert Date Checkin Checkout In Order Detail    ${check_out_date}
#
#    Should Contain    ${lbl_booking_requirement_check_in}     ${order_detail_check_in_date}
#    Should Contain    ${lbl_booking_requirement_check_out}    ${order_detail_check_out_date}

    Log To Console  Verify Payment Details On Order Detail
    wait until element is visible     ${Payment_Details_Payment_Method_Locator}           10
    ${value}=      get text           ${Payment_Details_Payment_Method_Locator}

    ${Payment_Details_Payment_Method_UI_Value}=                 get payment method
    ${Payment_Details_Payment_Gateway_UI_Value}=                get payment gateway
    ${Payment_Details_Payment_Card_Issuer_UI_Value}=            get payment card issuer
    ${Payment_Details_Payment_Payment_Status_UI_Value}=         get payment status
    ${Payment_Details_Payment_Payment_Type_UI_Value}=           get payment type
    ${Payment_Details_Payment_Payment_Date_UI_Value}=           get payment date
    ${Payment_Details_Payment_Payment_ID_Value}=                get payment ID


    Should Be Equal   ${Payment_Details_Payment_Method}           ${Payment_Details_Payment_Method_UI_Value}

    Should Be Equal   ${Payment_Details_Payment_Gateway}          ${Payment_Details_Payment_Gateway_UI_Value}
    Should Be Equal   ${Payment_Details_Payment_Card_Issuer}      ${Payment_Details_Payment_Card_Issuer_UI_Value}
    Should Be Equal   ${Payment_Details_Payment_Payment_Status}   ${Payment_Details_Payment_Payment_Status_UI_Value}
    Should Be Equal   ${Payment_Details_Payment_Payment_Type}     ${Payment_Details_Payment_Payment_Type_UI_Value}
    Should Contain     ${Payment_Details_Payment_Payment_Date_UI_Value}    ${Payment_Details_Payment_Payment_Date}




#    Verify Payment Details on Order Detail    Credit Card    TrueMoney    -
#    ...    Pending    Pay Later    ${orderDetailPaymentDate}    ${EMPTY}

#    Verify Product Name on Order Detail    ${hotelName}    ${orderDetailExcludVat}    ${orderDetailVat}    ${orderDetailGrandTotal}
#    Verify Sending Email Conformation to Supplier    Success    ${orderDetailEmailSendDate}
#    Verify Sending Email Conformation to Booker    Success    ${orderDetailEmailSendDate}
#    Verify Sending Email Conformation to Traveller    Success    ${orderDetailEmailSendDate}

Convert Date Checkin Checkout In Order Detail
    [Arguments]    ${date}
    ${day}    Convert Date    ${date}    result_format=%d
    ${day}    Convert To Integer    ${day}
    ${month}    Convert Date    ${date}    result_format=%b
    ${year}    Convert Date    ${date}    result_format=%Y
    ${two_last_charector_in_year}    Get Substring    ${year}    2
    [Return]    ${day} ${month} ${two_last_charector_in_year}

get device information os
    wait until element is visible     ${Device_Information_OS_Locator}                    10
    ${value}=      get text           ${Device_Information_OS_Locator}
    [Return]       ${value}
get device information os version
    wait until element is visible     ${Device_Information_OS_Version_Locator}            10
    ${value}=      get text           ${Device_Information_OS_Version_Locator}
    [Return]       ${value}
get device informaton browser
    wait until element is visible     ${Device_Information_Browser_Locator}               10
    ${value}=      get text           ${Device_Information_Browser_Locator}
    [Return]       ${value}
get device information browser version
    wait until element is visible     ${Device_Information_Browser_Version_Locator}       10
    ${value}=      get text           ${Device_Information_Browser_Version_Locator}
    [Return]       ${value}
get tax invoice company name
    wait until element is visible     ${lbl_tax_invoice_address_name_company_name}    10
    ${value}=      get text           ${lbl_tax_invoice_address_name_company_name}
    [Return]       ${value}
get tax invoice ID
    wait until element is visible     ${lbl_tax_invoice_address_thaiid_tax_id}         10
    ${value}=      get text           ${lbl_tax_invoice_address_thaiid_tax_id}
    [Return]       ${value}
get tax invoice address
    wait until element is visible     ${lbl_tax_invoice_address_address}              10
    ${value}=      get text           ${lbl_tax_invoice_address_address}
    [Return]       ${value}
get booking checkin date
    wait until element is visible     ${Booking_Requirement_Check_In_Locator}             10
    ${value}=      get text           ${Booking_Requirement_Check_In_Locator}
    [Return]       ${value}
get booking checkout date
    wait until element is visible     ${Booking_Requirement_Check_Out_Locator}            10
    ${value}=      get text           ${Booking_Requirement_Check_Out_Locator}
    [Return]       ${value}
get requirement guest
    wait until element is visible     ${Booking_Requirement_Guest_Locator}                10
    ${value}=      get text           ${Booking_Requirement_Guest_Locator}
    [Return]       ${value}
get payment method
    wait until element is visible     ${lbl_payment_details_payment_method}           10
    ${value}=      get text           ${lbl_payment_details_payment_method}
    [Return]       ${value}

get payment gateway
    wait until element is visible     ${lbl_Payment_Details_Payment_Gateway_Locator}          10
    ${value}=      get text           ${lbl_Payment_Details_Payment_Gateway_Locator}
    [Return]       ${value}
get payment card issuer
    wait until element is visible     ${lbl_Payment_Details_Payment_Card_Issuer_Locator}      10
    ${value}=      get text           ${lbl_Payment_Details_Payment_Card_Issuer_Locator}
    [Return]       ${value}
get payment status
    wait until element is visible     ${lbl_Payment_Details_Payment_Payment_Status_Locator}   10
    ${value}=      get text           ${lbl_Payment_Details_Payment_Payment_Status_Locator}
    [Return]       ${value}
get payment type
    wait until element is visible     ${lbl_Payment_Details_Payment_Payment_Type_Locator}     10
    ${value}=      get text           ${lbl_Payment_Details_Payment_Payment_Type_Locator}
    [Return]       ${value}
get payment date
    wait until element is visible     ${lbl_Payment_Details_Payment_Payment_Date_Locator}     10
    ${value}=      get text           ${lbl_Payment_Details_Payment_Payment_Date_Locator}
    [Return]       ${value}
get payment ID
    wait until element is visible     ${lbl_Payment_Details_Payment_Payment_ID_Locator}       10
    ${value}=      get text           ${lbl_Payment_Details_Payment_Payment_ID_Locator}
    [Return]       ${value}
get product name
    wait until element is visible     ${Product_Name_Table_Locator}                       10
    ${value}=      get text           ${Product_Name_Table_Locator}
    [Return]       ${value}
get product price exclude vat
    wait until element is visible     ${Product_Name_Price_Locator}                       10
    ${value}=      get text           ${Product_Name_Price_Locator}
    [Return]       ${value}
get product price vat
    wait until element is visible     ${Product_Name_Vat_Locator}                         10
    ${value}=      get text           ${Product_Name_Vat_Locator}
    [Return]       ${value}
get product total price
    wait until element is visible     ${Product_Name_Total_Price_Locator}                 10
    ${value}=      get text           ${Product_Name_Total_Price_Locator}
    [Return]       ${value}
get supplier company
    wait until element is visible     ${Supplier_Details_Company_Locator}                 10
    ${value}=      get text           ${Supplier_Details_Company_Locator}
    [Return]       ${value}
get supplier type
    wait until element is visible     ${Supplier_Details_Supplier_Type_Locator}           10
    ${value}=      get text           ${Supplier_Details_Supplier_Type_Locator}
    [Return]       ${value}
get supplier email
    wait until element is visible     ${Supplier_Details_Email_Locator}                   10
    ${value}=      get text           ${Supplier_Details_Email_Locator}
    [Return]       ${value}
get supplier currency
    wait until element is visible     ${Supplier_Details_Currency_Locator}                10
    ${value}=      get text            ${Supplier_Details_Currency_Locator}
    [Return]       ${value}
get supplier address
    wait until element is visible     ${Supplier_Details_Address_Locator}                 10
    ${value}=      get text           ${Supplier_Details_Address_Locator}
    [Return]       ${value}
get supplier contact
    wait until element is visible     ${Supplier_Details_Contact_Locator}                 10
    ${value}=      get text           ${Supplier_Details_Contact_Locator}
    [Return]       ${value}
get list of traveller name
    wait until element is visible     ${Traveller_Name_Table_Locator}                     10
    ${value}=      get text           ${Traveller_Name_Table_Locator}
    [Return]       ${value}

#######    Supplier      ########
get sending status supplier confirm status
    wait until element is visible    ${lbl_sending_status_supplier_confirm_status}
     ${value}=      Get Text   ${lbl_sending_status_supplier_confirm_status}
     [Return]       ${value}

get sending status supplier confirm date send
    wait until element is visible    ${lbl_sending_status_supplier_confirm_date_send}
     ${value}=      Get Text   ${lbl_sending_status_supplier_confirm_date_send}
     [Return]       ${value}

get sending status supplier cancel status
    wait until element is visible    ${lbl_sending_status_supplier_cancel_status}
     ${value}=      Get Text   ${lbl_sending_status_supplier_cancel_status}
     [Return]       ${value}

get sending status supplier cancel date send
    wait until element is visible    ${lbl_sending_status_supplier_cancel_date_send}
     ${value}=      Get Text   ${lbl_sending_status_supplier_cancel_date_send}
     [Return]       ${value}
#######    Supplier      ########

#######    Booker      ########
get sending status booker confirm status
    wait until element is visible    ${lbl_sending_status_booker_confirm_status}
     ${value}=      Get Text   ${lbl_sending_status_booker_confirm_status}
     [Return]       ${value}

get sending status booker confirm date send
    wait until element is visible    ${lbl_sending_status_booker_confirm_date_send}
     ${value}=      Get Text   ${lbl_sending_status_booker_confirm_date_send}
     [Return]       ${value}

get sending status booker cancel status
    wait until element is visible    ${lbl_sending_status_booker_cancel_status}
     ${value}=      Get Text   ${lbl_sending_status_booker_cancel_status}
     [Return]       ${value}

get sending status booker cancel date send
    wait until element is visible    ${lbl_sending_status_booker_cancel_date_send}
     ${value}=      Get Text   ${lbl_sending_status_booker_cancel_date_send}
     [Return]       ${value}
#######    Booker      ########

#######    Traveller      ########
get sending status traveller confirm status
    wait until element is visible    ${lbl_sending_status_traveller_confirm_status}
     ${value}=      Get Text   ${lbl_sending_status_traveller_confirm_status}
     [Return]       ${value}

get sending status traveller confirm date send
    wait until element is visible    ${lbl_sending_status_traveller_confirm_date_send}
     ${value}=      Get Text   ${lbl_sending_status_traveller_confirm_date_send}
     [Return]       ${value}

get sending status traveller cancel status
    wait until element is visible    ${lbl_sending_status_traveller_cancel_status}
     ${value}=      Get Text   ${lbl_sending_status_traveller_cancel_status}
     [Return]       ${value}

get sending status traveller cancel date send
    wait until element is visible    ${lbl_sending_status_traveller_cancel_date_send}
     ${value}=      Get Text   ${lbl_sending_status_traveller_cancel_date_send}
     [Return]       ${value}
#######    Traveller      ########

##### Get Purpose
Get Purpose from Booking by BookingID
    [Arguments]    ${bookingId}
    ${Purpose}=    query    SELECT Purpose FROM Booking WHERE BookingID = ${bookingId}
    Return From Keyword    ${Purpose[0][0]}

########## Sanity ##########
Verify Booking Details On Order Detail
    [Arguments]    ${booker_details_name}    ${booker_details_email}    ${booker_details_contact_no}    ${booker_details_nationality}
    ${booker_details_name_ui_value}=               order_detail.Get Booker Name
    ${booker_details_email_ui_value}=              order_detail.Get Booker Email
    ${booker_details_contact_no_ui_value}=         order_detail.Get Booker Contact No
    ${booker_details_nationality_ui_value}=        order_detail.Get Booker Nationality
    Should Be Equal   ${booker_details_name}          ${booker_details_name_ui_value}
    Should Be Equal   ${booker_details_email}         ${booker_details_email_ui_value}
    Should Be Equal   ${booker_details_contact_no}     ${booker_details_contact_no_ui_value}
    Should Be Equal   ${booker_details_nationality}   ${booker_details_nationality_ui_value}

Verify Tax Invoice Address On Order Detail
    [Arguments]    ${tax_invoice_address_name_company_name}    ${tax_invoice_address_thai_id_tax_id}    ${tax_invoice_address_address}
    ${tax_invoice_address_name_company_name_ui_value}=          get tax invoice company name
    ${tax_invoice_address_thai_id_tax_id_ui_value}=               get tax invoice ID
    ${tax_invoice_address_address_ui_value}=                    get tax invoice address
    Should Be Equal   ${tax_invoice_address_name_company_name}    ${tax_invoice_address_name_company_name_ui_value}
    Should Be Equal   ${tax_invoice_address_thai_id_tax_id}         ${tax_invoice_address_thai_id_tax_id_ui_value}
    Should Be Equal   ${tax_invoice_address_address}              ${tax_invoice_address_address_ui_value}

Verify Tax Invoice Address On Order Detail From Dictionary
    [Arguments]    ${tax_invoice_address}
    ${tax_invoice_address_name_company_name}=             Get From Dictionary    ${tax_invoice_address}    ${CONST_COMPANY_NAME}
    ${tax_invoice_address_thai_id_tax_id}=                Get From Dictionary    ${tax_invoice_address}    ${CONST_COMPANY_TAX_NUMBER}
    ${tax_invoice_address_address}=                       Get From Dictionary    ${tax_invoice_address}    ${CONST_COMPANY_ADDRESS}
    ${tax_invoice_address_name_company_name_ui_value}=    Get Tax Invoice Company Name
    ${tax_invoice_address_thai_id_tax_id_ui_value}=       Get Tax Invoice ID
    ${tax_invoice_address_address_ui_value}=              Get Tax Invoice Address
    Should Be Equal   ${tax_invoice_address_name_company_name}    ${tax_invoice_address_name_company_name_ui_value}
    Should Be Equal   ${tax_invoice_address_thai_id_tax_id}       ${tax_invoice_address_thai_id_tax_id_ui_value}
    Should Be Equal   ${tax_invoice_address_address}              ${tax_invoice_address_address_ui_value}


Verify Payment Details on Order Detail
    [Arguments]    ${payment_details_payment_method}    ${payment_details_payment_gateway}    ${payment_details_payment_card_issuer}
    ...    ${payment_details_payment_payment_status}    ${payment_details_payment_payment_type}
    ...    ${payment_details_payment_payment_date}    ${payment_details_payment_payment_id}

    ${payment_details_payment_method_ui_value}=                 get payment method
    ${payment_details_payment_gateway_ui_value}=                get payment gateway
    ${payment_details_payment_card_issuer_ui_value}=            get payment card issuer
    ${payment_details_payment_payment_status_ui_value}=         get payment status
    ${payment_details_payment_payment_type_ui_value}=           get payment type
    ${payment_details_payment_payment_date_ui_value}=           get payment date
    ${payment_details_payment_payment_id_value}=                get payment id

    Should Be Equal   ${payment_details_payment_method}           ${payment_details_payment_method_ui_value}
    Should Be Equal   ${payment_details_payment_gateway}          ${payment_details_payment_gateway_ui_value}
    Should Be Equal   ${payment_details_payment_card_issuer}      ${payment_details_payment_card_issuer_ui_value}
    Should Be Equal   ${payment_details_payment_payment_status}   ${payment_details_payment_payment_status_ui_value}
    Should Be Equal   ${payment_details_payment_payment_type}     ${payment_details_payment_payment_type_ui_value}
    Should Contain     ${payment_details_payment_payment_date_ui_value}    ${payment_details_payment_payment_date}

Verify Sending Email Conformation To Supplier
    [Arguments]    ${sending_status_supplier_confirm_status}    ${sending_status_supplier_confirm_date_send}
    ${sending_status_supplier_confirm_status_ui_value}    get sending status supplier confirm status
    ${sending_status_supplier_confirm_date_send_ui_value}    get sending status supplier confirm date send
    Should Be Equal    ${sending_status_supplier_confirm_status}    ${sending_status_supplier_confirm_status_ui_value}
    Should Contain    ${sending_status_supplier_confirm_date_send_ui_value}    ${sending_status_supplier_confirm_date_send}

Verify Sending Email Conformation To Booker
    [Arguments]    ${sending_status_booker_confirm_status}    ${sending_status_booker_confirm_date_send}
    ${sending_status_booker_confirm_status_ui_value}    get sending status booker confirm status
    ${sending_status_booker_confirm_date_send_ui_value}    get sending status booker confirm date send
    Should Be Equal    ${sending_status_booker_confirm_status}    ${sending_status_booker_confirm_status_ui_value}
    Should Contain    ${sending_status_booker_confirm_date_send_ui_value}    ${sending_status_booker_confirm_date_send}

Verify Sending Email Conformation To Traveller
    [Arguments]    ${sending_status_traveller_confirm_status}    ${sending_status_traveller_confirm_date_send}
    ${sending_status_traveller_confirm_status_ui_value}    get sending status traveller confirm status
    ${sending_status_traveller_confirm_date_send_ui_value}    get sending status traveller confirm date send
    Should Be Equal    ${sending_status_traveller_confirm_status}    ${sending_status_traveller_confirm_status_ui_value}
    Should Contain    ${sending_status_traveller_confirm_date_send_ui_value}    ${sending_status_traveller_confirm_date_send}

Verify Sending Email Cancel To Supplier
    [Arguments]    ${sending_status_supplier_cancel_status}    ${sending_status_supplier_cancel_date_send}    ${verify_status_only}=false

    ${sending_status_supplier_cancel_status_ui_value}    get sending status supplier cancel status
    ${sending_status_supplier_cancel_date_send_ui_value}    get sending status supplier cancel date send
    Should Be Equal    ${sending_status_supplier_cancel_status}    ${sending_status_supplier_cancel_status_ui_value}
    Run Keyword If    '${verify_status_only}' == 'false'    Should Contain     ${sending_status_supplier_cancel_date_send_ui_value}    ${sending_status_supplier_cancel_date_send}

Verify Sending Email Cancel To Booker
    [Arguments]    ${sending_status_booker_cancel_status}    ${sending_status_booker_cancel_date_send}    ${verify_status_only}=false

    ${sending_status_booker_cancel_status_ui_value}    get sending status booker cancel status
    ${sending_status_booker_cancel_date_send_ui_value}    get sending status booker cancel date send
    Should Be Equal    ${sending_status_booker_cancel_status}    ${sending_status_booker_cancel_status_ui_value}
    Run Keyword If    '${verify_status_only}' == 'false'    Should Contain    ${sending_status_booker_cancel_date_send_ui_value}    ${sending_status_booker_cancel_date_send}

Verify Sending Email Cancel To Traveller
    [Arguments]    ${sending_status_traveller_cancel_status}    ${sending_status_traveller_cancel_date_send}    ${verify_status_only}=false

    ${sending_status_traveller_cancel_status_ui_value}    get sending status traveller cancel status
    ${sending_status_traveller_cancel_date_send_ui_value}    get sending status traveller cancel date send
    Should Be Equal    ${sending_status_traveller_cancel_status}    ${sending_status_traveller_cancel_status_ui_value}
    Run Keyword If    '${verify_status_only}' == 'false'    Should Contain    ${sending_status_traveller_cancel_date_send_ui_value}    ${sending_status_traveller_cancel_date_send}

Get Booker Name
    Wait Until Element Is Visible     ${lbl_booker_details_name}                      10
    ${value}=      Get Text           ${lbl_booker_details_name}
    [Return]       ${value}
Get Booker Email
    Wait Until Element Is Visible     ${lbl_booker_details_email}                     10
    ${value}=      Get Text           ${lbl_booker_details_email}
    [Return]       ${value}
Get Booker Contact No
    Wait Until Element Is Visible     ${lbl_booker_details_contact_no}                 10
    ${value}=      Get Text           ${lbl_booker_details_contact_no}
    [Return]       ${value}
Get Booker Nationality
    Wait Until Element Is Visible     ${lbl_booker_details_nationality}               10
    ${value}=      Get Text           ${lbl_booker_details_nationality}
    [Return]       ${value}

Convert Date Payment Sending Email In Order Detail
    [Arguments]    ${date}
    ${day}    Convert Date    ${date}    result_format=%d
    ${day}    Convert To Integer    ${day}
    ${month}    Convert Date    ${date}    result_format=%b
    ${year}    Convert Date    ${date}    result_format=%Y
    [Return]    ${day} ${month} ${year}