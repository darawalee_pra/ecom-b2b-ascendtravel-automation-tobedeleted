*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot
Resource    ${CURDIR}/../../../locators/ui/backend/login.robot

*** Variables ***

*** Keywords ***

Input Username And Password Login Backend
    [Arguments]    ${username}    ${password}
    wait until element is enabled    ${txt_backend_login_page_user}     30
    Input Text      ${txt_backend_login_page_user}    ${USERNAME_BACKEND}
    wait until element is enabled    ${txt_backend_login_page_pass}     30
    Input Text      ${txt_backend_login_page_pass}    ${PASSWORD_BACKEND}
    wait until element is enabled    ${btn_backend_login_page_login}      30
    Click Button    ${btn_backend_login_page_login}
    wait until page contains      Dashboard     50