*** Settings ***
Resource    ${CURDIR}/../../../locators/ui/backend/order_status.robot

*** Keywords ***
Verify Status Pay Now Booking And Payment Success
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     success
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     success
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     success
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     unearn
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     no
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Pay Now Booking Fail
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     fail
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     fail
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     pending
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     fail
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     no
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     none
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     none

Verify Status Pay Now Payment Fail
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     fail
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     fail
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     fail
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     none
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     no
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     none
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     none

Verify Status Pay Now Cancel Booking Before Settle Free Cancellation Fee
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     cancel non credit
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     success
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     cancel
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     unearn
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     void
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Pay Now Cancel Booking Before Settle Have Some Charge Cancellation Fee
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     cancel non credit
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     success
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     cancel
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     revenue
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     void
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Pay Now Cancel Booking Before Settle Have Free Cancellation Fee
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     cancel non credit
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     success
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     cancel
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     revenue
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     refund
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Pay Later Booking And Payment Authorize Success
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     success
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     authorize
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     success
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     none
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     no
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Pay Later Payment Authorize Fail
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     fail
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     fail
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     pending
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     none
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     no
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     none
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     none

Verify Status Pay Later Booking Fail
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     fail
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     authorize
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     fail
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     none
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     no
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     none
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     none

Verify Status Pay Later Cancel Booking Before Payment Date
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     cancel non credit
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     authorize
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     cancel
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     none
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     non refund
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Credit Term Booking Success
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     success
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     pending
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     success
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     unearn
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     no
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Credit Term Booking Fail
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     fail
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     pending
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     fail
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     none
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     no
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     none
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     none

Verify Status Credit Term Cancel Booking Free Cancellation Fee
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     cancel credit
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     pending
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     cancel
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     unearn
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     non refund
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Credit Term Cancel Booking Some Charge Cancellation Fee
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     cancel credit
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     pending
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     cancel
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     revenue
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     non refund
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success

Verify Status Credit Term Cancel Booking Full Charge Cancellation Fee
    [Arguments]    ${BookingID}
    ${OrderStatus}     Get_Order_Status    ${BookingID}
    Should Not Be Empty     ${OrderStatus}
    Should Be Equal As Integers     ${OrderStatus[0][0]}     ${BookingID}
    Log To Console      Verify Order Status
    Should Be Equal     ${OrderStatus[0][1]}     cancel credit
    Log To Console      Verify Payment Status
    Should Be Equal     ${OrderStatus[0][2]}     pending
    Log To Console      Verify Booking Status
    Should Be Equal     ${OrderStatus[0][3]}     cancel
    Log To Console      Verify Financial Status
    Should Be Equal     ${OrderStatus[0][4]}     revenue
    Log To Console      Verify Cancel Status
    Should Be Equal     ${OrderStatus[0][5]}     non refund
    Log To Console      Verify Email Booker Status
    Should Be Equal     ${OrderStatus[0][6]}     notify sent success
    Log To Console      Verify Email Supplier Status
    Should Be Equal     ${OrderStatus[0][7]}     notify sent success
    Log To Console      Verify Email Traveller Status
    Should Be Equal     ${OrderStatus[0][8]}     notify sent success