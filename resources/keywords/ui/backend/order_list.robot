*** Settings ***
Library     DateTime
Library     String
Library     Selenium2Library
Library     DatabaseLibrary
Resource    ${CURDIR}/../../../locators/ui/backend/order_list.robot

*** Variables ***
${timeout}                         50

*** Keywords ***

Search Order List By Booking Id
    [Arguments]    ${booking_id}
    Input Search searchText    ${booking_id}
    Click Search Order List Result
    Loading Done
    Verify Is Can Search

Verify Booking Status By Booking Id Or Customer Name Or Email Or Traveler Name
    [Arguments]    ${expect_booking_status}
    # Open Browser And Login Backend
    # Maximize Browser Window
    # Go To Order List Page
    # Input Search searchText    ${input_search}
    # Click Search Order List Result
    # Loading Done
    # Verify Is Can Search
    Hide All Column Order List
    Verify Order List Column Booking Status    ${expect_booking_status}

Verify Booking Status By Booking Period
    [Arguments]    ${booking_period_from}    ${booking_period_to}    ${expect_booking_id}    ${expect_booking_status}
    Open Browser And Login Backend
    Maximize Browser Window
    Go To Order List Page
    Input Booking Period    ${booking_period_from}    ${booking_period_to}
    Click Search Order List Result
    Loading Done
    Verify Is Can Search
    Page Should Contain        ${expect_booking_id}
    Input Search searchText    ${expect_booking_id}
    Click Search Order List Result
    Loading Done
    Hide All Column Order List
    Verify Order List Column Booking Status    ${expect_booking_status}
    Verify Order List Column Booking Date Between Date Filter    ${booking_period_from}    ${booking_period_to}

Verify Booking Status By Check In Period
    [Arguments]    ${check_in_period_from}     ${check_in_period_to}    ${expect_booking_id}    ${expect_booking_status}
    Open Browser And Login Backend
    Maximize Browser Window
    Go To Order List Page
    Input Check In Period    ${check_in_period_from}    ${check_in_period_to}
    Click Search Order List Result
    Loading Done
    Verify Is Can Search
    Page Should Contain        ${expect_booking_id}
    Input Search searchText    ${expect_booking_id}
    Click Search Order List Result
    Loading Done
    Hide All Column Order List
    Verify Order List Column Booking Status    ${expect_booking_status}
    Verify Order List Column Check In Date Between Date Filter    ${check_in_period_from}    ${check_in_period_to}

Verify Payment Method By Filter Credit Card
    Open Browser And Login Backend
    Maximize Browser Window
    Go To Order List Page
    Check Payment Method Credit Card
    Click Search Order List Result
    Loading Done
    Verify Is Can Search
    Hide All Column Order List
    Verify Order List Column Payment Method    Credit Card

Verify Payment Method By Filter Credit Terms
    Open Browser And Login Backend
    Maximize Browser Window
    Go To Order List Page
    Check Payment Method Credit Terms
    Click Search Order List Result
    Loading Done
    Verify Is Can Search
    Hide All Column Order List
    Verify Order List Column Payment Method    Credit Terms

Go To Order List Page
    Go To    ${ORDER_LIST_PAGE_URL}

Input Search searchText
    [Arguments]    ${search_text}=test
    Wait Until Element Is Visible    ${txt_searchtext}    ${timeout}
    Input Text          ${txt_searchtext}             ${search_text}

Input Booking Period
    [Arguments]    ${booking_period_from}=01/09/2016     ${booking_period_to}=01/01/2017
    Wait Until Element Is Visible    ${txt_booking_period}    ${timeout}
    Click Element       ${txt_booking_period}
    Input Text          ${txt_booking_period_from}     ${booking_period_from}
    Input Text          ${txt_booking_period_to}       ${booking_period_to}
    Click Element       ${btn_booking_period_apply}

Input Check In Period
    [Arguments]    ${check_in_period_from}=01/02/2017     ${check_in_period_to}=31/03/2017
    Wait Until Element Is Visible    ${txt_booking_period}    ${timeout}
    Click Element       ${txt_checkin_period}
    Input Text          ${txt_check_in_period_from}     ${check_in_period_from}
    Input Text          ${txt_check_in_period_to}       ${check_in_period_to}
    Click Element       ${btn_check_in_period_apply}

Check Product Type Hotel
    Wait Until Element Is Visible    ${checkbox_product_type_hotel}    ${timeout}
    Click Element                    ${checkbox_product_type_hotel}

Check Payment Method Credit Card
    Wait Until Element Is Visible    ${checkbox_payment_method_credit_card}    ${timeout}
    Click Element                    ${checkbox_payment_method_credit_card}

Check Payment Method Credit Terms
    Wait Until Element Is Visible    ${checkbox_payment_method_credit_term}    ${timeout}
    Click Element                    ${checkbox_payment_method_credit_term}

Click Search Order List Result
    Wait Until Element Is Visible    ${btn_search_order_list}    ${timeout}
    Click Element                    ${btn_search_order_list}
    wait until element is visible    ${table_order_list_result}   ${timeout}

Loading Done
    Wait Until Element Is Not Visible    ${div_loading}    ${timeout}

#######################

Hide All Column Order List
    :FOR    ${index}    IN RANGE    2    22
    \    Click Hide Column Order List By Index    ${index}

Click Hide Column Order List By Index
    [Arguments]    ${index}
    ${index}       Evaluate    (${index} - 1) * 2
    ${string_index}    Convert To String    ${index}
    ${xpath}       Replace string    ${button_active_column}    replace_me    ${string_index}
    ${visible}=    Run Keyword And Return Status    Element Should Be Visible    ${xpath}
    Run Keyword If   ${visible} == False    Click Show Hide List Column Order List
    Wait Until Element Is Visible    ${xpath}
    Click Element    ${xpath}

Click Show Column Order List By Index
    [Arguments]    ${index}
    ${index}       Evaluate    ((${index} - 1) * 2) + 1
    ${string_index}    Convert To String    ${index}
    ${xpath}       Replace string    ${button_active_column}    replace_me    ${string_index}
    ${visible}=    Run Keyword And Return Status    Element Should Be Visible    ${xpath}
    Run Keyword If    ${visible} == False    Click Show Hide List Column Order List
    Wait Until Element Is Visible    ${xpath}    ${timeout}
    Click Element     ${xpath}

Click Show Hide List Column Order List
    Wait Until Element Is Visible    ${button_active_list}    ${timeout}
    Click Element    ${button_active_list}

Click To Un Filter Booking Status Column
    wait until element is visible   ${close_booking_status_column}    ${timeout}
    click element                   ${close_booking_status_column}

Click To Un Filter Booking Date Column
    wait until element is visible   ${close_booking_date_column}      ${timeout}
    click element                   ${close_booking_date_column}

Click To Un Filter Booker Name Column
    wait until element is visible   ${close_booker_name_column}       ${timeout}
    click element                   ${close_booker_name_column}

Click To Un Filter Booker Email Column
    wait until element is visible   ${close_booker_email_column}      ${timeout}
    click element                   ${close_booker_email_column}

Click To Un Filter Traveller Name Column
    wait until element is visible   ${close_traveller_name_column}    ${timeout}
    click element                   ${close_traveller_name_column}

Click To Un Filter Product Type Column
    wait until element is visible   ${close_product_type_column}      ${timeout}
    click element                   ${close_product_type_column}

Click To Un Filter Product Name Column
    wait until element is visible   ${close_product_name_column}      ${timeout}
    click element                   ${close_product_name_column}

Click To Un Filter Supplier Name Column
    wait until element is visible   ${close_supplier_name_column}     ${timeout}
    click element                   ${close_supplier_name_column}

Click To Un Filter Payment Status Column
    wait until element is visible   ${close_payment_status_column}    ${timeout}
    click element                   ${close_payment_status_column}

Click To Un Filter Payment Date Column
    wait until element is visible   ${close_payment_date_column}      ${timeout}
    click element                   ${close_payment_date_column}

Click To Un Filter Check In Date Column
    wait until element is visible   ${close_checkin_date_column}      ${timeout}
    click element                   ${close_checkin_date_column}

Click To Un Filter Check Out Date Column
    wait until element is visible   ${close_checkout_date_column}     ${timeout}
    click element                   ${close_checkout_date_column}

Click To Un Filter Payment Method Column
    wait until element is visible   ${close_payment_method_column}    ${timeout}
    click element                   ${close_payment_method_column}

Click To Un Filter Payment Id Column
    wait until element is visible   ${close_payment_id_column}        ${timeout}
    click element                   ${close_payment_id_column}

Click To Un Filter Selling Price Column
    wait until element is visible   ${close_selling_price_column}     ${timeout}
    click element                   ${close_selling_price_column}

Click To Un Filter Cancel Status Column
    wait until element is visible   ${close_cancel_status_column}     ${timeout}
    click element                   ${close_cancel_status_column}

Click To Un Filter Cancel Date Column
    wait until element is visible   ${close_cancel_date_column}       ${timeout}
    click element                   ${close_cancel_date_column}

Click To Un Filter Cancellation Fee Column
    wait until element is visible   ${close_cancellation_fee_column}   ${timeout}
    click element                   ${close_cancellation_fee_column}

Click To Un Filter Refund Column
    wait until element is visible   ${close_refund_column}            ${timeout}
    click element                   ${close_refund_column}

Click To Un Filter Affiliate Column
    wait until element is visible   ${close_affiliate_column}         ${timeout}
    click element                   ${close_affiliate_column}

Click To Filter Booking Status Column
    wait until element is visible   ${open_booking_status_column}     ${timeout}
    click element                   ${open_booking_status_column}

Click To Filter Booking Date Column
    wait until element is visible   ${open_booking_date_column}       ${timeout}
    click element                   ${open_booking_date_column}

Click To Filter Booker Name Column
    wait until element is visible   ${open_booker_name_column}        ${timeout}
    click element                   ${open_booker_name_column}

Click To Filter Booker Email Column
    wait until element is visible   ${open_booker_email_column}       ${timeout}
    click element                   ${open_booker_email_column}

Click To Filter Traveller Name Column
    wait until element is visible   ${open_traveller_name_column}     ${timeout}
    click element                   ${open_traveller_name_column}

Click To Filter Product Type Column
    wait until element is visible   ${open_product_type_column}       ${timeout}
    click element                   ${open_product_type_column}

Click To Filter Product Name Column
    wait until element is visible   ${open_product_name_column}       ${timeout}
    click element                   ${open_product_name_column}

Click To Filter Supplier Name Column
    wait until element is visible   ${open_supplier_name_column}      ${timeout}
    click element                   ${open_supplier_name_column}

Click To Filter Payment Status Column
    wait until element is visible   ${open_patment_status_column}     ${timeout}
    click element                   ${open_patment_status_column}

Click To Filter Payment Date Column
    wait until element is visible   ${open_payment_date_column}       ${timeout}
    click element                   ${open_payment_date_column}

Click To Filter Check In Date Column
    wait until element is visible   ${open_checkin_date_column}       ${timeout}
    click element                   ${open_checkin_date_column}

Click To Filter Check Out Date Column
    wait until element is visible   ${open_checkout_date_column}      ${timeout}
    click element                   ${open_checkout_date_column}

Click To Filter Payment Method Column
    wait until element is visible   ${open_payment_method_column}     ${timeout}
    click element                   ${open_payment_method_column}

Click To Filter Payment Id Column
    wait until element is visible   ${open_payment_id_column}         ${timeout}
    click element                   ${open_payment_id_column}

Click To Filter Selling Price Column
    wait until element is visible   ${open_selling_price_column}      ${timeout}
    click element                   ${open_selling_price_column}

Click To Filter Cancel Status Column
    wait until element is visible   ${open_cancel_status_column}      ${timeout}
    click element                   ${open_cancel_status_column}

Click To Filter Cancel Date Column
    wait until element is visible   ${open_cancel_date_column}        ${timeout}
    click element                   ${open_cancel_date_column}

Click To Filter Cancellation Fee Column
    wait until element is visible   ${open_cancellation_fee_column}   ${timeout}
    click element                   ${open_cancellation_fee_column}

Click To Filter Refund Column
    wait until element is visible   ${open_refund_column}             ${timeout}
    click element                   ${open_refund_column}

Click To Filter Affiliate Column
    wait until element is visible   ${open_affiliate_column}          ${timeout}
    click element                   ${open_affiliate_column}

Get Text Order List Column Second
    Wait Until Element Is Visible    ${table_order_list_column_2}
    ${text_column_2}    Get Text    ${table_order_list_column_2}
    [Return]    ${text_column_2}

Verify Is Can Search
    Element Should Be Visible    ${display_search_result}

Verify Is Not Search
    Element Should Not Be Visible    ${display_search_result}

Verify Order List Column Booking Status Without Filter Column
    [Arguments]    ${status}
    ${text}    Get Text Order List Column Second
    Should Be Equal As Strings    ${text}    ${status}

Verify Order List Column Booking Status
    [Arguments]    ${status}
    Click Show Column Order List By Index    2
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    2
    Should Be Equal As Strings    ${text}    ${status}

Verify Order List Column Booking Date
    [Arguments]    ${date}=${EMPTY}
    Click Show Column Order List By Index    3
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    3
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${date}
    Run Keyword If    ${isEmpty} == True    Should Not Be Empty    ${text}
    Run Keyword If    ${isEmpty} == False    Should Start With    ${text}    ${date}

Verify Order List Column Booker Name
    [Arguments]    ${name}=${EMPTY}
    Click Show Column Order List By Index    4
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    4
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${name}
    Run Keyword If    ${isEmpty} == True    Should Not Be Empty    ${text}
    Run Keyword If    ${isEmpty} == False    Should Be Equal As Strings    ${text}    ${name}

Verify Order List Column Booker Email
    [Arguments]    ${email}=${EMPTY}
    Click Show Column Order List By Index    5
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    5
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${email}
    Run Keyword If    ${isEmpty} == True    Should Not Be Empty    ${text}
    Run Keyword If    ${isEmpty} == False    Should Be Equal As Strings    ${text}    ${email}

Verify Order List Column Traveller Name
    [Arguments]    ${name}=${EMPTY}
    Click Show Column Order List By Index    6
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    6
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${name}
    Run Keyword If    ${isEmpty} == True    Should Not Be Empty    ${text}
    Run Keyword If    ${isEmpty} == False    Should Contain    ${text}    ${name}

Verify Order List Column Product Type
    [Arguments]    ${type}
    Click Show Column Order List By Index    7
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    7
    Should Be Equal As Strings    ${text}    ${type}

Verify Order List Column Product Name
    Click Show Column Order List By Index    8
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    8
    Should Not Be Empty    ${text}

Verify Order List Column Supplier Name
    Click Show Column Order List By Index    9
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    9
    Should Not Be Empty    ${text}

Verify Order List Column Payment Status
    [Arguments]    ${status}
    Click Show Column Order List By Index    10
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    10
    Should Be Equal As Strings    ${text}    ${status}

Verify Order List Column Payment Date
    [Arguments]    ${date}=${EMPTY}
    Click Show Column Order List By Index    11
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    11
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${date}
    Run Keyword If    ${isEmpty} == True    Should Be Equal As Strings    ${text}    ${date}
    Run Keyword If    ${isEmpty} == False    Should Start With    ${text}    ${date}

Verify Order List Column Checkin Date
    [Arguments]    ${date}=${EMPTY}
    Click Show Column Order List By Index    12
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    12
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${date}
    Run Keyword If    ${isEmpty} == True    Should Be Equal As Strings    ${text}    ${date}
    Run Keyword If    ${isEmpty} == False    Should Start With    ${text}    ${date}

Verify Order List Column Checkout Date
    [Arguments]    ${date}=${EMPTY}
    Click Show Column Order List By Index    13
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    13
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${date}
    Run Keyword If    ${isEmpty} == True    Should Be Equal As Strings    ${text}    ${date}
    Run Keyword If    ${isEmpty} == False    Should Start With    ${text}    ${date}

Verify Order List Column Payment Method
    [Arguments]    ${payment}
    Click Show Column Order List By Index    14
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    14
    Should Be Equal As Strings    ${text}    ${payment}

Verify Order List Column Payment Id
    [Arguments]    ${id}=${EMPTY}
    ${stringId}    Convert To String    ${id}
    Click Show Column Order List By Index    15
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    15
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${stringId}
    Run Keyword If    ${isEmpty} == True    Should Not Be Empty    ${text}
    Run Keyword If    ${isEmpty} == False    Should Be Equal As Strings    ${text}    ${stringId}

Verify Order List Column Selling Price
    [Arguments]    ${price}=${EMPTY}
    ${price} =  Replace String    ${price}       ,     ${EMPTY}
    Click Show Column Order List By Index    16
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    16
    ${isEmptyPrice}    Run Keyword And Return Status    Should Be Empty    ${price}
    ${textPrice}    Run Keyword If    ${isEmptyPrice} == True    Set Variable    0
    ...    ELSE    Set Variable    ${price}
    ${numPrice}    Convert To Number    ${textPrice}    2
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${text}
    ${textCompare}    Run Keyword If    ${isEmpty} == True    Set Variable    0
    ...    ELSE    Set Variable    ${text}
    ${numCompare}    Convert To Number    ${textCompare}    2
    Should Be Equal As Numbers    ${numPrice}    ${numCompare}

Verify Order List Column Cancel Status
    [Arguments]    ${status}
    Click Show Column Order List By Index    17
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    17
    Should Be Equal As Strings    ${text}    ${status}

Verify Order List Column Cancel Date
    [Arguments]    ${date}=${EMPTY}
    Click Show Column Order List By Index    18
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    18
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${date}
    Run Keyword If    ${isEmpty} == True    Should Be Equal As Strings    ${text}    ${date}
    Run Keyword If    ${isEmpty} == False    Should Start With    ${text}    ${date}

Verify Order List Column Cancellation Fee
    [Arguments]    ${fee}=${EMPTY}
    Click Show Column Order List By Index    19
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    19
    ${isEmptyFee}    Run Keyword And Return Status    Should Be Empty    ${fee}
    ${textFee}    Run Keyword If    ${isEmptyFee} == True    Set Variable    0
    ...    ELSE    Set Variable    ${fee}
    ${numFee}    Convert To Number    ${textFee}    2
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${text}
    ${textCompare}    Run Keyword If    ${isEmpty} == True    Set Variable    0
    ...    ELSE    Set Variable    ${text}
    ${numCompare}    Convert To Number    ${textCompare}    2
    Should Be Equal As Numbers    ${numFee}    ${numCompare}

Verify Order List Column Refund
    [Arguments]    ${refund}=${EMPTY}
    Click Show Column Order List By Index    20
    ${text}    Get Text Order List Column Second
    Click Hide Column Order List By Index    20
    ${isEmptyRefund}    Run Keyword And Return Status    Should Be Empty    ${refund}
    ${textRefund}    Run Keyword If    ${isEmptyRefund} == True    Set Variable    0
    ...    ELSE    Set Variable    ${refund}
    ${numRefund}    Convert To Number    ${textRefund}    2
    ${isEmpty}    Run Keyword And Return Status    Should Be Empty    ${text}
    ${textCompare}    Run Keyword If    ${isEmpty} == True    Set Variable    0
    ...    ELSE    Set Variable    ${text}
    ${numCompare}    Convert To Number    ${textCompare}    2
    Should Be Equal As Numbers    ${numRefund}    ${numCompare}

Verify Order List Column Booking Date Between Date Filter
    [Arguments]    ${date_form}=${EMPTY}    ${date_to}=${EMPTY}
    Click To Flter Booking Date Column
    ${text_booking_date}    Get Text Order List Column Second
    ${date_booking_for_epoch}=    Prepare Date Format dd/mm//YY From Ordet List For Convert Epoch Time    ${text_booking_date}
    ${date_booking_epoch}    Convert Date    ${date_booking_for_epoch}    epoch
    ${date_from_for_epoch}=    Prepare Date Format dd/mm//YYYY From Ordet List For Convert Epoch Time    ${date_form}
    ${date_from_epoch}    Convert Date    ${date_from_for_epoch}    epoch
    ${date_to_for_epoch}=    Prepare Date Format dd/mm//YYYY From Ordet List For Convert Epoch Time    ${date_to}
    ${date_to_epoch}    Convert Date    ${date_to_for_epoch}    epoch

    Should Be True    ${date_booking_epoch} >= ${date_from_epoch}
    Should Be True    ${date_booking_epoch} <= ${date_to_epoch}

Verify Order List Column Check In Date Between Date Filter
    [Arguments]    ${date_form}=${EMPTY}    ${date_to}=${EMPTY}
    Click To Filter Check In Date Column
    ${text_booking_date}    Get Text Order List Column Second
    ${date_booking_for_epoch}=    Prepare Date Format dd/mm//YY From Ordet List For Convert Epoch Time    ${text_booking_date}
    ${date_booking_epoch}    Convert Date    ${date_booking_for_epoch}    epoch
    ${date_from_for_epoch}=    Prepare Date Format dd/mm//YYYY From Ordet List For Convert Epoch Time    ${date_form}
    ${date_from_epoch}    Convert Date    ${date_from_for_epoch}    epoch
    ${date_to_for_epoch}=    Prepare Date Format dd/mm//YYYY From Ordet List For Convert Epoch Time    ${date_to}
    ${date_to_epoch}    Convert Date    ${date_to_for_epoch}    epoch

    Should Be True    ${date_booking_epoch} >= ${date_from_epoch}
    Should Be True    ${date_booking_epoch} <= ${date_to_epoch}

Prepare Date Format dd/mm//YY From Ordet List For Convert Epoch Time
    [Arguments]    ${time}=01/01/17
    ${day}=        Get Substring   ${time}    0  2
    ${month}=      Get Substring   ${time}    3  5
    ${year}=       Get Substring   ${time}    6  8
    [Return]       20${year}-${month}-${day}

Prepare Date Format dd/mm//YYYY From Ordet List For Convert Epoch Time
    [Arguments]    ${time}=01/01/2017
    ${day}=        Get Substring   ${time}    0  2
    ${month}=      Get Substring   ${time}    3  5
    ${year}=       Get Substring   ${time}    8  10
    [Return]       20${year}-${month}-${day}

Convert Date To Text In Order List
    [Arguments]    ${date}
    ${day}         Convert Date    ${date}    result_format=%d
    ${month}       Convert Date    ${date}    result_format=%m
    ${year}        Convert Date    ${date}    result_format=%Y
    ${year_2_digi}       Get Substring   ${year}    2
    [Return]       ${day}/${month}/${year_2_digi}