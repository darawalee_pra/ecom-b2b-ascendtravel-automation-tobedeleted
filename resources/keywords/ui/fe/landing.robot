*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot

Resource    ${CURDIR}/../../../locators/UI/fe/landing.robot

*** Variables ***

*** Keywords ***
Verify Message On Landing Page Popup
    [Arguments]    ${content}
    Wait Until Element Contains    ${lbl_title_popup}    ${content}    ${TIMEOUT}

### Mobile ###
Click Hamberger Menu
    Wait Until Element Is Visible    ${btn_hamberger_menu}    ${TIMEOUT}
    Click Element   ${btn_hamberger_menu}

Click Register/login In Hamberger Menu
    Wait Until Element Is Visible    ${btn_login_mobile}    ${TIMEOUT}
    Click Element   ${btn_login_mobile}


### Change Language ###
Click menu language
    [Arguments]     ${device}

    ${btn_change_language} =	Set Variable If		
    ...	'${device}' == 'Desktop'    ${btn_change_language_desktop}
    ...	'${device}' == 'Tablet'     ${btn_change_language_desktop}	
    ...	'${device}' == 'Mobile'     ${btn_change_language_mobile}		
  
    Log     [device]=${device}
    Log     [btn_change_language]=${btn_change_language}

    Wait Until Element Is Visible    ${btn_change_language}    ${TIMEOUT}
    Click Element    ${btn_change_language}

Select language
    [Arguments]     ${device}   ${lang}

    ${btn_language} =	Set Variable If		
    ...	'${device}' == 'Desktop' and '${lang}' == 'EN'    ${btn_english_language_desktop}
    ...	'${device}' == 'Desktop' and '${lang}' == 'TH'    ${btn_thai_language_desktop}
    ...	'${device}' == 'Tablet' and '${lang}' == 'EN'    ${btn_english_language_desktop}
    ...	'${device}' == 'Tablet' and '${lang}' == 'TH'    ${btn_thai_language_desktop}
    ...	'${device}' == 'Mobile' and '${lang}' == 'EN'    ${btn_english_language_mobile}
    ...	'${device}' == 'Mobile' and '${lang}' == 'TH'    ${btn_thai_language_mobile}
    
    Log     [device]=${device}
    Log     [btn_language]=${btn_language}

    Wait Until Element Is Visible    ${btn_language}    ${TIMEOUT}
    Click Element    ${btn_language}


### Login Steps ###
Click 'Get Started' button
    Wait Until Element Is Visible    ${btn_getstarted}    ${TIMEOUT}
    Click Element   ${btn_getstarted}

Click 'Register/Login' link
    Wait Until Element Is Visible    ${link_register_login}    ${TIMEOUT}
    Click Element   ${link_register_login}

Enter email address to login
    [Arguments]    ${email_address} 
    Wait Until Element Is Visible    ${txt_email_register_login}    ${TIMEOUT}
    Input Text    ${txt_email_register_login}    ${email_address}

Click 'Continue' button
    Wait Until Element Is Visible    ${btn_continue_register_login}    ${TIMEOUT}
    Click Element   ${btn_continue_register_login}    

Enter password for login
    [Arguments]    ${password}
    Wait Until Element Is Visible    ${txt_password_login}    ${TIMEOUT}
    Input Text    ${txt_password_login}    ${password}


Click 'Login' button
    Wait Until Element Is Visible    ${btn_login}    ${TIMEOUT}
    Click Element   ${btn_login}


Click Forget Password
    Wait Until Element Is Visible    ${lnk_forget_password}    ${TIMEOUT}
    Click Element   ${lnk_forget_password}

Click Request New Password
    Wait Until Element Is Visible    ${btn_send_request_new_passeword}    ${TIMEOUT}
    Click Element   ${btn_send_request_new_passeword}

Input New Password
    [Arguments]    ${password}
    Wait Until Element Is Visible    ${txt_set_new_password}    ${TIMEOUT}
    Input Text    ${txt_set_new_password}    ${password}

Input Confirm New Password
    [Arguments]    ${password}
    Wait Until Element Is Visible    ${txt_confirm_new_password}    ${TIMEOUT}
    Input Text    ${txt_confirm_new_password}    ${password}

Click Submit New Password
    Wait Until Element Is Visible    ${btn_submit_new_password}    ${TIMEOUT}
    Click Element   ${btn_submit_new_password}

### Registration Steps ###
Select Main Company For Register
    [Arguments]    ${company}
    Wait Until Element Is Visible    ${cbo_select_company}    ${TIMEOUT}
    Select From List By Label    ${cbo_select_company}    ${company}

Input password for register
    [Arguments]    ${password}
    Wait Until Element Is Visible    ${txt_password_register}    ${TIMEOUT}
    Input Text    ${txt_password_register}    ${password}

Input confirm password for register
    [Arguments]    ${password}
    Wait Until Element Is Visible    ${txt_confirm_password_register}    ${TIMEOUT}
    Input Text    ${txt_confirm_password_register}    ${password}

Click link read terms and conditions
    Wait Until Element Is Visible    ${lnk_term_condition}    ${TIMEOUT}
    Click Element   ${lnk_term_condition}

Click accept terms and conditions button on pop up
    Wait Until Element Is Visible    ${btn_accept_tc}    ${TIMEOUT}
    Click Element   ${btn_accept_tc}

Click 'Register' button
    Wait Until Element Is Visible    ${btn_register_on_popup}    ${TIMEOUT}
    Click Element   ${btn_register_on_popup}


### Login Failed ###
Verify Error Message Login Fail
    [Arguments]    ${content_line1}    ${content_line2}
    Sleep    5
    Wait Until Element Contains    ${lbl_title_popup}    ${content_line1}
    Wait Until Element Contains    ${lbl_title_popup}    ${content_line2}



### Register Failed ###

### Register by leaving information ###

Input Name For Leave Information
    [Arguments]    ${name}
    Wait Until Element Is Visible    ${txt_interest_name}    ${TIMEOUT}
    Input Text    ${txt_interest_name}    ${name}

Input Email For Leave Information
    [Arguments]    ${email}
    Wait Until Element Is Visible    ${txt_interest_email}    ${TIMEOUT}
    Input Text    ${txt_interest_email}    ${email}

Input Phone For Leave Information
    [Arguments]    ${phone}
    Wait Until Element Is Visible    ${txt_interest_phone}    ${TIMEOUT}
    Input Text    ${txt_interest_phone}    ${phone}

Input Company For Leave Information
    [Arguments]    ${company}
    Wait Until Element Is Visible    ${txt_interest_company}    ${TIMEOUT}
    Input Text    ${txt_interest_company}    ${company}

Input Position For Leave Information
    [Arguments]    ${position}
    Wait Until Element Is Visible    ${txt_interest_position}    ${TIMEOUT}
    Input Text    ${txt_interest_position}    ${position}

Input Website For Leave Information
    [Arguments]    ${website}
    Wait Until Element Is Visible    ${txt_interest_website}    ${TIMEOUT}
    Input Text    ${txt_interest_website}    ${website}

Input Budget For Leave Information
    [Arguments]    ${budget}
    Wait Until Element Is Visible    ${txt_interest_budget}    ${TIMEOUT}
    Input Text    ${txt_interest_budget}    ${budget}

Click Submit Information
    Wait Until Element Is Visible    ${btn_submit_info}    ${TIMEOUT}
    Click Element   ${btn_submit_info}

Check Auto field Email On Leave Information Popup
    [Arguments]    ${email}
    Wait Until Element Is Visible    ${txt_interest_email}    ${TIMEOUT}
    ${email_on_leave_info_pop_up}    Get Value    ${txt_interest_email}
    Should Be Equal As Strings    ${email_on_leave_info_pop_up}    ${email}

Click Join Us
    Wait Until Element Is Visible    ${btn_joinus}    ${TIMEOUT}
    Click Element   ${btn_joinus}

Click Close Message
    Wait Until Element Is Visible    ${btn_close_message}    ${TIMEOUT}
    Click Element   ${btn_close_message}


### TRUE ###
Verify term and conditions popup is display
    Wait Until Element Is Visible    ${popup_term_conditions_true}    ${TIMEOUT}
    Element Should Be Visible   ${popup_term_conditions_true}

Accept Term and conditions 

    Execute Javascript	return document.getElementById('${chb_term_conditions_true}').click();

Click 'OK' button from Term and conditions popup 
    Wait Until Element Is Visible    ${btn_ok_term_conditions_true}    ${TIMEOUT}
    Click Element   ${btn_ok_term_conditions_true}

Enter True Username
    [Arguments]     ${true_username}
    Wait Until Element Is Visible    ${txt_true_username}    ${TIMEOUT}
    Input Text    ${txt_true_username}    ${true_username}

Enter True Password
    [Arguments]     ${true_password}
    Wait Until Element Is Visible    ${txt_true_password}    ${TIMEOUT}
    Input Text    ${txt_true_password}    ${true_password}

Click True Login Button 
    Wait Until Element Is Visible    ${btn_true_login}    ${TIMEOUT}
    Click Element   ${btn_true_login}

Wait until term and condition popup is dismissed
    Wait Until Element Is Not Visible    ${popup_term_conditions_true}    ${TIMEOUT}




### SCG ###
Enter SCG Username
    [Arguments]     ${scg_username}
    Wait Until Element Is Visible    ${txt_scg_username}    ${TIMEOUT}
    Input Text    ${txt_scg_username}    ${scg_username}

Enter SCG Password
    [Arguments]     ${scg_password}
    Wait Until Element Is Visible    ${txt_scg_password}    ${TIMEOUT}
    Input Text    ${txt_scg_password}    ${scg_password}

Click SCG Login Button 
    Wait Until Element Is Visible    ${btn_scg_login}    ${TIMEOUT}
    Click Element   ${btn_scg_login}

Verify Budget Limit notification popup is displayed
    Wait Until Element Is Visible    ${popup_notify_budget}    ${TIMEOUT}
    Element Should Be Visible   ${popup_notify_budget}

Verify Budget Limit Text display correctly
    [Arguments]     ${expected_budget_limit_text}

    Wait Until Element Is Visible    ${txt_landing_budget_popup}    ${TIMEOUT}
    ${actual_budget_limit_text}=    Get Text  ${txt_landing_budget_popup}
    Should Be Equal     ${actual_budget_limit_text}     ${expected_budget_limit_text}

Click 'OK' button from Budget Limit notification popup
    Wait Until Element Is Visible    ${btn_ok_scg_budget_popup}    ${TIMEOUT}
    Click Element   ${btn_ok_scg_budget_popup}

Wait until budget limit popup is dismissed
    Wait Until Element Is Not Visible    ${popup_notify_budget}    ${TIMEOUT}

