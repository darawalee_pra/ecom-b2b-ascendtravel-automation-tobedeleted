*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot
Resource    ${CURDIR}/../../../locators/ui/fe/myaccount.robot

*** Variables ***

*** Keywords ***
Cancel Booking using Booking ID
    [Arguments]     ${txt_booking_id}
    
	${actual_path}=     Replace String    ${booking_id_path}   _$bookingid    ${txt_booking_id}
    Wait Until Element Is Visible   ${actual_path} 	${TIMEOUT}
    ${booking_id_index}=    Get Element Attribute   ${actual_path}  id
    Log     [booking_id_index]=${booking_id_index}
    ${index}=   Fetch From Right    ${booking_id_index}     -
    Log     [index]=${index}

    ${btn_cancel_booking_actual_path}=     Replace String    ${btn_cancel_booking}   _$index    ${index}
    Wait Until Element Is Visible   ${btn_cancel_booking_actual_path} 	${TIMEOUT}
    Click Element   ${btn_cancel_booking_actual_path}

Click Confirm on cancellation pop-up 
    Wait Until Element Is Visible   ${btn_confirm_cancel_booking} 	${TIMEOUT}
    Click Element   ${btn_confirm_cancel_booking}

Verify successfully cancel message is displayed 
    [Arguments]    ${txt_booking_id}

    ${actual_path}=     Replace String    ${txt_success_cancel_message}   _$bookingid    ${txt_booking_id}
    Wait Until Element Is Visible   ${actual_path} 	${TIMEOUT}
