*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot
Resource 	${CURDIR}/../../../locators/ui/fe/levelc.robot

*** Variables ***

*** Keywords ***
Select Hotel from Level C screen
	[Arguments] 	${hotel_name}

	${actual_path}=     Replace String    ${hotel_name_path_c}   _$hotelName    ${hotel_name}
    Wait Until Element Is Visible   ${actual_path} 	${TIMEOUT}
    Click Element   ${actual_path}

Verify hotel display on level c screen
	[Arguments] 	${hotel_name}
	${actual_path}=     Replace String    ${hotel_name_path_c}   _$hotelName    ${hotel_name}
    Wait Until Element Is Visible   ${actual_path} 	${TIMEOUT}

Verify search result on level c page display correctly   
	[Arguments] 	${expected_destination_name}
	${actual_destination_text}= 	Get Text 	${your_destination}
	Should Contain 	${actual_destination_text} 	${expected_destination_name}


Verify CheckIn/CheckOut date on level c page display correctly
	[Arguments] 	${expected_checkin_checkout_date}
	${actual_checkin_checkout_date}= 	Get Text 	${checkin_checkout_date}
	Should Be Equal As Strings 	${actual_checkin_checkout_date} 	${expected_checkin_checkout_date}

Verify number of Room&Guest on level c page display correctly
	[Documentation] 	Example format of ${expected_room_and_guest} is 1 Room, 2 Adult, 0 Child
	[Arguments] 	${expected_room_and_guest}

	${actual_room}= 	Get Text 	${number_of_room}
	${actual_guest}= 	Get Text 	${number_of_guest}
	${actual_child}= 	Get Text 	${number_of_child}
	
	Should Be Equal As Strings 	${actual_room}${actual_guest}${actual_child} 	${expected_room_and_guest}

Click 'Change' link
	Wait Until Element Is Visible   ${lnk_change_search}
    Click Element   ${lnk_change_search}

Click 'Room&Guest' dropdown list
    Wait Until Element Is Visible   ${btn_toggle_search_roombox}
    Click Element   ${btn_toggle_search_roombox}

Select number of Room from dropdown list   
	[Arguments] 	${number_of_room}

	Wait Until Element Is Visible   ${ddl_room_amount}
    Click Element   ${ddl_room_amount}
	Selenium2Library.Select From List By Value 	${ddl_room_amount} 	${number_of_room}

Select number of Adult  
	[Arguments] 	${number_of_adult}

	Wait Until Element Is Visible   ${ddl_adult_amount}
    Click Element   ${ddl_adult_amount}
	Selenium2Library.Select From List By Value 	${ddl_adult_amount} 	${number_of_adult}

Select number of Child  
	[Arguments] 	${number_of_child}

	Wait Until Element Is Visible   ${ddl_child_amount}
    Click Element   ${ddl_child_amount}
	Selenium2Library.Select From List By Value 	${ddl_child_amount} 	${number_of_child}

Select number of child age 
	[Arguments] 	${number_of_child_age}

	Wait Until Element Is Visible   ${ddl_child_age}
    Click Element   ${ddl_child_age}
	Selenium2Library.Select From List By Value 	${ddl_child_age} 	${number_of_child_age}

Close Room&Guest dropdown list
	Wait Until Element Is Visible   ${btn_toggle_search_roombox}
    Click Element   ${btn_toggle_search_roombox}

Click 'Search' button
    Wait Until Element Is Visible   ${btn_search_submit_levelc}        ${TIMEOUT}
    Click Element   ${btn_search_submit_levelc}

Verify budget limit banner is displayed
    Wait Until Element Is Visible   ${txt_levelc_budget_limit} 	${TIMEOUT}
    Element Should Be Visible   ${txt_levelc_budget_limit}