*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot
Resource    ${CURDIR}/../../../locators/ui/fe/leveld.robot

*** Variables ***

*** Keywords ***
Verify hotel display on level d screen
	[Arguments] 	${expected_hotel_name}

    Wait Until Element Is Visible   ${hotel_name_d}     ${TIMEOUT}
	${actual_hotel_name}=     Get Text  ${hotel_name_d}
    Should Contain  ${actual_hotel_name}    ${expected_hotel_name}
    
Select number of room from room type name   
    [Arguments]     ${room_type_name}   ${number_of_room}

	${actual_path}=     Replace String    ${txt_room_type}   _$roomTypeName    ${room_type_name}
    Wait Until Element Is Visible   ${actual_path} 	${TIMEOUT}
    ${room_type_id}=    Get Element Attribute   ${actual_path}  id
    Log     [room_type_id]${room_type_id}
    ${index}=   Fetch From Right    ${room_type_id}     -
    Log     [index]=${index}

    ${room_amount_actual_path}=     Replace String    ${ddl_number_of_room}   _$index    ${index}
    Select From List    ${room_amount_actual_path}  ${number_of_room}

# Verify 'Check-in' on 'Booking Details'  
#     [Arguments]     ${check_in_date}
# Verify 'Check-out' on 'Booking Details'
#     [Arguments]     ${check_out_date}  
# Verify 'Room Information' on 'Booking Details'
#     [Arguments]     ${room_type_name}
# Verify 'Number of room and night' on 'Booking Details'  
#     [Arguments]     ${number_of_room}   ${number_of_night}
# Verify 'Grand Total' on 'Booing Details'    
#     [Arguments]     ${grand_total_in_thb}

Click 'Book Now' button
    Wait Until Element Is Visible   ${btn_continue_leveld} 	${TIMEOUT}
    Click Element   ${btn_continue_leveld} 

Verify budget limit banner is displayed
    Wait Until Element Is Visible   ${txt_leveld_budget_limit} 	${TIMEOUT}
    Element Should Be Visible   ${txt_leveld_budget_limit}





