*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot
Resource    ${CURDIR}/../../../locators/ui/fe/checkout2.robot

*** Variables ***

*** Keywords ***
Verify 'Travelling For' on 'Booker Information'
    [Arguments]     ${expected_travelling_purpose}
    Wait Until Element Is Visible   ${lbl_checkout_2_purpose} 	${TIMEOUT}
    ${actual_travelling_purpose}=   Get Text   ${lbl_checkout_2_purpose}
    Should Be Equal     ${actual_travelling_purpose}    ${expected_travelling_purpose}

Get Travelling Purpose
    Wait Until Element Is Visible    ${lbl_checkout_2_purpose}    ${TIMEOUT}
    ${purpose}    Get Text    ${lbl_checkout_2_purpose}
    [Return]    ${purpose}

Get Booker Email
    Wait Until Element Is Visible    ${txt_checkout_2_booker_email}    ${TIMEOUT}
    ${email}    Get Value    ${txt_checkout_2_booker_email}
    [Return]    ${email}

Verify Booker Email
    [Arguments]    ${value}
    ${email}    checkout2.Get Booker Email
    Should Be Equal    ${email}    ${value}

Enter Booker First Name
    [Arguments]    ${first_name}
    Wait Until Element Is Visible    ${txt_checkout_2_booker_first_name}    ${TIMEOUT}
    Input Text    ${txt_checkout_2_booker_first_name}    ${first_name}

Enter Booker Last Name
    [Arguments]    ${last_name}
    Wait Until Element Is Visible    ${txt_checkout_2_booker_last_name}    ${TIMEOUT}
    Input Text    ${txt_checkout_2_booker_last_name}    ${last_name}

Enter Booker Moblie No
    [Arguments]    ${mobile_no}
    Wait Until Element Is Visible    ${txt_checkout_2_booker_mobile_no}    ${TIMEOUT}
    Input Text    ${txt_checkout_2_booker_mobile_no}    ${mobile_no}

Enter Guest First Name Using Room and Guest Index
    [Arguments]    ${room_index}    ${guest_index}    ${first_name}
    ${locator_first_name}    Replace String    ${txt_checkout_2_guest_first_name}    RINDEX    ${room_index}
    ${locator_first_name}    Replace String    ${locator_first_name}    GINDEX    ${guest_index}
    Wait Until Element Is Visible    ${locator_first_name}    ${TIMEOUT}
    Input Text    ${locator_first_name}    ${first_name}

Enter Guest Last Name Using Room and Guest Index
    [Arguments]    ${room_index}    ${guest_index}    ${last_name}
    ${locator_last_name}    Replace String    ${txt_checkout_2_guest_last_name}    RINDEX    ${room_index}
    ${locator_last_name}    Replace String    ${locator_last_name}    GINDEX    ${guest_index}
    Wait Until Element Is Visible    ${locator_last_name}    ${TIMEOUT}
    Input Text    ${locator_last_name}    ${last_name}

Enter Guest Email Address Using Room and Guest Index
    [Arguments]    ${room_index}    ${guest_index}    ${email}
    ${locator_email}    Replace String    ${txt_checkout_2_guest_email}    RINDEX    ${room_index}
    ${locator_email}    Replace String    ${locator_email}    GINDEX    ${guest_index}
    Wait Until Element Is Visible    ${locator_email}    ${TIMEOUT}
    Input Text    ${locator_email}    ${email}

Select Guest Nationality Using Room and Guest Index
    [Arguments]    ${room_index}    ${guest_index}    ${nationality}
    ${locator_nationality}    Replace String    ${cbo_checkout_2_guest_nationality}    RINDEX    ${room_index}
    ${locator_nationality}    Replace String    ${locator_nationality}    GINDEX    ${guest_index}
    Wait Until Element Is Visible    ${locator_nationality}    ${TIMEOUT}
    Select From List    ${locator_nationality}    ${nationality}

Enter Guest Special Request Using Room and Guest Index
    [Arguments]    ${room_index}    ${guest_index}    ${special_request}
    ${locator_special_request}    Replace String    ${txt_checkout_2_guest_special_request}    RINDEX    ${room_index}
    ${locator_special_request}    Replace String    ${locator_special_request}    GINDEX    ${guest_index}
    Wait Until Element Is Visible    ${locator_special_request}    ${TIMEOUT}
    Input Text    ${locator_special_request}    ${special_request}

Click Add Guest button
    [Arguments]    ${room_index}
    ${locator_add_guest}    Replace String    ${btn_checkout_2_guest_add_guest}    RINDEX    ${room_index}
    Wait Until Element Is Visible    ${locator_add_guest}    ${TIMEOUT}
    Click Element    ${locator_add_guest}

Click Guest More Detail
    [Arguments]    ${room_index}    ${guest_index}
    ${locator_more_detail}    Replace String    ${btn_cehckout_2_guest_more_detail}    RINDEX    ${room_index}
    ${locator_more_detail}    Replace String    ${locator_more_detail}    GINDEX    ${guest_index}
    Wait Until Element Is Visible    ${locator_more_detail}    ${TIMEOUT}
    Click Element    ${locator_more_detail}
    













# Enter booker name
# Enter booker surname
# Enter booker mobile phone number

Verify Booker Firstname
Verify Booker Lastname
Verify Booker Email address
Verify Booker Mobile Number

Verify 'Room Type' information on 'Guest Information'
Enter Guest1 Firstname
Enter Guest1 Lastname
Enter Guest1 Email address
Select Guest1 Nationality

Click 'Continue' button
    Wait Until Element Is Visible    ${btn_checkout_2_continue}    ${TIMEOUT}
    Click Element    ${btn_checkout_2_continue}
    
    




# Enter guest name
# Enter guest surname
# Enter guest email address
# Select guest nationality 

# Click 'Add Guest' button

# Verify 'Travelling Purpose'
#     [Arguments]     ${chkout2-txt-purpose}

# Verify Hotel name of Booking Details
# Verify Check-in date/time
# Verify Check-out date/time
# Verify Room Type name
# Verify number of room and night
# Verify 'Grand Total'

# Click 'Continue' button








