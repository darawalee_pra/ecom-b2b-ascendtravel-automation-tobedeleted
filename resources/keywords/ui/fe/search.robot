*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot

Resource    ${CURDIR}/../../..//locators/ui/fe/search.robot

*** Variables ***

*** Keywords ***
Verify search home page is displayed
    Wait Until Element Is Visible    ${btn_search_purpose_work}    ${TIMEOUT}
    Wait Until Element Is Visible    ${btn_search_purpose_leisure}    ${TIMEOUT}
    Wait Until Element Is Visible    ${txt_searchkeyword}    ${TIMEOUT}
    Wait Until Element Is Visible    ${btn_search_submit}    ${TIMEOUT}
    
Select travelling purpose
    [Arguments]     ${purpose}
    Wait Until Element Is Visible    ${btn_search_purpose_work}    ${TIMEOUT}
    Wait Until Element Is Visible    ${btn_search_purpose_leisure}    ${TIMEOUT}

    Run Keyword If	'${purpose}' == 'Work'    Click Element    ${btn_search_purpose_work}
    Run Keyword If  '${purpose}' == 'Leisure'   Click Element    ${btn_search_purpose_leisure}

Enter search keyword
    [Arguments]     ${search_keyword}
    Wait Until Element Is Visible    ${txt_searchkeyword}    ${TIMEOUT}
    Input Text    ${txt_searchkeyword}    ${search_keyword}

Wait until search suggestions box display
    Wait Until Element Is Visible    ${search_suggestions_box}     ${TIMEOUT}
 
Select search result from suggestion box
    [Arguments]     ${search_type}  ${search_keyword}

    ${actual_path}=     Replace String    ${search_result_suggestion}   $_keyword    ${search_keyword}
    ${actual_path}=	Set Variable If		
    ...	'${search_type}' == 'Destination'    ${search_suggestion_destination_box}${actual_path}
    ...	'${search_type}' == 'Hotel'     ${search_suggestion_hotel_box}${actual_path}	
    ...	'${search_type}' == 'POI'     ${search_suggestion_poi_box}${actual_path}	
    
    Wait Until Element Is Visible   ${actual_path}
    Click Element   ${actual_path}

_Find Calendar Col
    [Arguments]    ${target}    ${day_locator}    ${row}    ${count}    ${now}
    ${today_found}    Set Variable    ${false}
    :FOR    ${col}    IN RANGE    1    8
    \    ${str_col}=    Convert To String    ${col}
    \    ${col_locator}=    Replace String    ${day_locator}    COL    ${str_col}
    \    ${class}=    Get Element Attribute  ${col_locator}   class
    \    ${today}=    Get Regexp Matches    ${class}    ${now}
    \    ${other_month}=    Get Regexp Matches    ${class}    otherMonth
    \    ${today_count}=    Get Length    ${today}
    \    ${other_month_count}=    Get Length    ${other_month}
    \    ${count}=    Run Keyword If    ${count} >= 0 and ${other_month_count} == 0    Evaluate    ${count} + 1
    \                 ...    ELSE IF    ${count} >= 0 and ${today_found}    Evaluate    ${count} + 1
    \                 ...       ELSE    Set Variable    ${count}
    \    ${count}=    Set Variable If    ${today_count} > 0    0    ${count}
    \    ${today_found}=    Evaluate    ${today_count} > 0 or ${today_found}
    \    ${today_found}=    Run Keyword If    ${today_found} and ${other_month_count} == 0    Set Variable    ${false}
    \                       ...       ELSE    Set Variable    ${today_found}
    \    Exit For Loop If    ${target} == ${count}
    ${count}=    Set Variable If    ${today_found}    ${-1}    ${count}
    [Return]    ${count}    ${col_locator}


_Find Calendar Row
    [Arguments]    ${target}    ${target_locator}    ${now}    ${count}=-1    ${total}=7
    :FOR    ${row_left}    IN RANGE    1    ${total}
    \    ${str_row_left}=    Convert To String    ${row_left}
    \    ${left_day_locator}=    Replace String    ${target_locator}    ROW    ${str_row_left}
    \    ${new_count}    ${result_locator}    Find Calendar Col    ${target}    ${left_day_locator}
    \    ...    ${row_left}    ${count}    ${now}
    \    ${count}=    Set Variable    ${new_count}
    \    Exit For Loop If    ${target} == ${count}
    [Return]    ${count}    ${result_locator}

Select 'Check-in' date
    [Arguments]    ${target}    ${now}=today

    Wait Until Element Is Visible   ${checkin_calendar}
    Click Element    ${checkin_calendar}

    #Click Search Checkin Previous Month
    ${show_button_previous}    Run Keyword And Return Status    Element Should Be Visible    ${btn_search_checkin_prev_month}
    Run Keyword If    ${show_button_previous}    Click Element    ${btn_search_checkin_prev_month}

    ${left_count}    ${left_result}    _Find Calendar Row    ${target}    ${sel_search_checkin_choice_left}    ${now}
    ${right_count}    ${right_result}    _Find Calendar Row    ${target}    ${sel_search_checkin_choice_right}    ${now}    ${left_count}
    Run Keyword If    ${left_count} == ${target}     Click Element    ${left_result}
    ...    ELSE IF    ${right_count} == ${target}    Click Element    ${right_result}
    Click Element    ${checkin_calendar}

Select 'Check-Out' date
    [Arguments]    ${target}    ${now}=today

    Wait Until Element Is Visible   ${checkout_calendar}
    Click Element    ${checkout_calendar}
    ${left_count}    ${left_result}    Find Calendar Row    ${target}    ${sel_search_checkout_choice_left}    ${now}
    ${right_count}    ${right_result}    Find Calendar Row    ${target}    ${sel_search_checkout_choice_right}    ${now}    ${left_count}
    Run Keyword If    ${left_count} == ${target}     Click Element    ${left_result}
    ...    ELSE IF    ${right_count} == ${target}    Click Element    ${right_result}
    Click Element    ${checkout_calendar}

Close calendar
    [Arguments]     ${device}

    ${btn_close_calendar} =	Set Variable If		
    ...	'${device}' == 'Desktop'    ${btn_close_calendar_desktop}
    ...	'${device}' == 'Tablet'     ${btn_close_calendar_desktop}	
    ...	'${device}' == 'Mobile'     ${checkout_calendar}		
  
    Log     [device]=${device}
    Log     [btn_close_calendar]=${btn_close_calendar}

    Wait Until Element Is Visible   ${btn_close_calendar}    ${TIMEOUT}
    Click Element    ${btn_close_calendar}

Click 'Search' button
    Wait Until Element Is Visible   ${btn_search_submit}        ${TIMEOUT}
    Click Element   ${btn_search_submit}
    
_Get CheckIn date as text
    Selenium2Library.Wait Until Page Contains Element     ${checkin_date_text}      ${TIMEOUT}
    ${_checkin_date}=    Selenium2Library.Get Element Attribute   ${checkin_date_text}  value

    [Return]    ${_checkin_date}

_Get CheckOut date as text
    Selenium2Library.Wait Until Page Contains Element     ${checkout_date_text}         ${TIMEOUT}
    ${_checkout_date}=    Selenium2Library.Get Element Attribute   ${checkout_date_text}    value

    [Return]    ${_checkout_date}

Get CheckIn and CheckOut date

    ${_checkin_date}=   _Get CheckIn date as text
    ${_checkout_date}=  _Get CheckOut date as text

    [Return]    ${_checkin_date} - ${_checkout_date}

Click My Account botton 
    Wait Until Element Is Visible    ${btn_my_account}    ${TIMEOUT}
    Click Element   ${btn_my_account}


Click My Booking button 
    Wait Until Element Is Visible    ${btn_my_account_my_booking}    ${TIMEOUT}
    Click Element   ${btn_my_account_my_booking}

Select Number Of Room 
    [Arguments]    ${search_number_of_room}
    Wait Until Element Is Visible    ${ddl_search_number_of_room}    ${TIMEOUT}    
    Select From List   ${ddl_search_number_of_room}    ${search_number_of_room}

Select Number Of Guest 
    [Arguments]    ${search_number_of_guest}
    Wait Until Element Is Visible    ${ddl_search_number_of_guest}    ${TIMEOUT}    
    Select From List   ${ddl_search_number_of_guest}    ${search_number_of_guest}
