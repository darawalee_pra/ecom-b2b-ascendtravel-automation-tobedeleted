*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot
Resource    ${CURDIR}/../../../locators/ui/fe/checkout3.robot

*** Variables ***

*** Keywords ***

Verify 'Hotel Information' on 'Booking Details'
    [Arguments]     ${expected_checkout_3_hotel_name}
    Wait Until Element Is Visible   ${txt_checkout_3_cart_hotel_name} 	${TIMEOUT}
    ${actual_checkout_3_hotel_name}=   Get Text    ${txt_checkout_3_cart_hotel_name}
    Should Be Equal     ${actual_checkout_3_hotel_name}    ${expected_checkout_3_hotel_name}

Verify 'Check-in' on 'Booking Details'
    [Arguments]  ${check_in_date}
Verify 'Check-out' on 'Booking Details'
    [Arguments]     ${check_out_date}  

_Get Room List
    [Arguments]     ${is_same_room_type}=${FALSE}     

    Wait Until Element Is Visible   ${txt_checkout_3_room_list} 	${TIMEOUT}
    ${actual_number_of_room}=   Get Element Attribute   ${txt_checkout_3_room_list}     data-selenium-total-room
    Log     [data-selenium-total-room]=${actual_number_of_room}

    ${actual_number_of_room} =	Set Variable If     ${is_same_room_type}==${TRUE}   ${actual_number_of_room}-1  ${actual_number_of_room}
    Log     [actual_number_of_room]=${actual_number_of_room}

    @{actual_room_list}=   Create List

    : FOR    ${INDEX}    IN RANGE    ${actual_number_of_room}
    \    Log    ${INDEX}
    \    ${INDEX}=  Convert To String   ${INDEX}
    \    ${actual_path_room_name}=     Replace String    ${txt_checkout_3_room_name}   _$index    ${INDEX}
    \    Wait Until Element Is Visible   ${actual_path_room_name} 	${TIMEOUT}
    \    ${item_room_name}=  Get Text   ${actual_path_room_name}
    \    Append To List     ${actual_room_list}    ${item_room_name}
    \    Log    [actual_room_list]=@{actual_room_list}

    [Return]    @{actual_room_list}

Verify 'Room Information' on 'Booking Details'
    [Arguments]  ${checkout_3_number_of_room}   ${expected_list_of_room_type_name}  ${is_same_room_type}=${FALSE}
    
    ${checkout_3_number_of_room} =	Set Variable If     ${is_same_room_type}==${TRUE}   ${checkout_3_number_of_room}-1  ${checkout_3_number_of_room}
    
    @{actual_room_list}=    _Get Room List  ${is_same_room_type}
    : FOR    ${INDEX}    IN RANGE    ${checkout_3_number_of_room}
    \    ${expected_value}=     Get From List   ${expected_list_of_room_type_name}  ${INDEX}
    \    List Should Contain Value  ${actual_room_list}     ${expected_value}

Verify 'Number of room and night' on 'Booking Details'
    [Arguments]  ${number_of_room}   ${number_of_night}
Verify 'Grand Total' on 'Booing Details'
    [Arguments]    ${grand_total_in_thb}

Select payment method
    [Arguments]       ${payment_method}

    ${btn_payment_method} =	Set Variable If		
    ...	'${payment_method}' == 'Visa Card/Master Card'    ${btn_checkout_3_credit_card}
    ...	'${payment_method}' == 'Credit Terms'     ${btn_checkout_3_credit_term}	
  
    Log     [payment_method]=${payment_method}
    Log     [btn_payment_method]=${btn_payment_method}

    Wait Until Element Is Visible   ${btn_payment_method}    ${TIMEOUT}
    Click Element    ${btn_payment_method}

Select Tax Invoice/​Receipt Information by company name
    [Arguments]     ${tax_invoice_company_name}
    ${actual_path}    Replace String    ${rdo_tax_invoice_by_name}    _$taxAddressByName    ${tax_invoice_company_name}
    Wait Until Element Is Visible    ${actual_path}    ${TIMEOUT}
    Click Element    ${actual_path}

Click 'Continue' button
    Wait Until Element Is Visible    ${btn_cart_continue}    ${TIMEOUT}
    Click Element    ${btn_cart_continue}


###### Paid using Credit Card #######
Enter credit card number
    [Arguments]     ${credit_card_number}
    Wait Until Element Is Visible    ${txt_checkout_3_card_no}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_card_no}    ${credit_card_number}

Enter name on credit card
    [Arguments]     ${name_on_credit_card}
    Wait Until Element Is Visible    ${txt_checkout_3_card_name}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_card_name}    ${name_on_credit_card}

Select card expiry date
    [Arguments]     ${card_expiry_month}    ${card_expiry_year}
    Wait Until Element Is Visible    ${cbo_checkout_3_card_expire_month}    ${TIMEOUT}
    Select From List    ${cbo_checkout_3_card_expire_month}    ${card_expiry_month}

    Wait Until Element Is Visible    ${cbo_checkout_3_card_expire_year}    ${TIMEOUT}
    Select From List    ${cbo_checkout_3_card_expire_year}    ${card_expiry_year}

Enter CVV Number
   [Arguments]    ${card_cvv}
    Wait Until Element Is Visible    ${txt_checkout_3_card_cvv}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_card_cvv}    ${card_cvv}

Click 'Add New Address' button 
    Wait Until Element Is Visible    ${btn_checkout_3_address_list_new_address}    ${TIMEOUT}
    Click Element    ${btn_checkout_3_address_list_new_address}

Enter Tax-Invoice - Company Name or Name-Surname
    [Arguments]    ${str_index}    ${company_name}
    ${locator_company}    Replace String    ${txt_checkout_3_address_form_company}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_company}    ${TIMEOUT}
    Input Text    ${locator_company}    ${company_name}


Enter Tax-Invoice - TaxID/Personal ID 
    [Arguments]    ${str_index}    ${tax_id}
    ${locator_tax_no}    Replace String    ${txt_checkout_3_address_form_tax_number}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_tax_no}    ${TIMEOUT}
    Input Text    ${locator_tax_no}    ${tax_id}


Enter Tax-Invoice - Tax Invoice or Receipt Address
    [Arguments]    ${str_index}    ${tax_invoice_address}
    ${locator_address}    Replace String    ${txt_checkout_3_address_form_address}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    Input Text    ${locator_address}    ${tax_invoice_address}

Click 'Save' button
    [Arguments]    ${str_index}
    ${locator_save}    Replace String    ${btn_checkout_3_address_form_save}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_save}    ${TIMEOUT}
    Click Element    ${locator_save}

Click 'Cancel' button
    [Arguments]    ${str_index}
    ${locator_cancel}    Replace String    ${btn_checkout_3_address_form_cancel}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_cancel}    ${TIMEOUT}
    Click Element    ${locator_cancel}


###### Paid using Credit Term #######
Enter Cost Center 
    [Arguments]     ${txt_checkout_3_cost_center}
    Wait Until Element Is Visible    ${txt_checkout_3_credit_terms_cost_center}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_credit_terms_cost_center}    ${txt_checkout_3_cost_center}

Enter Cost Center Name
    [Arguments]     ${txt_checkout_3_cost_center_name}
    Wait Until Element Is Visible    ${txt_checkout_3_credit_terms_cost_center_name}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_credit_terms_cost_center_name}    ${txt_checkout_3_cost_center_name}

Enter Company Refernece code
    [Arguments]     ${txt_checkout_3_cost_center_fund_code}
    Wait Until Element Is Visible    ${txt_checkout_3_credit_terms_fund_code}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_credit_terms_fund_code}    ${txt_checkout_3_cost_center_fund_code}

_get element id of tax invoice by name
    [Arguments]          ${tax_invoice_name}

	${actual_path}=     Replace String    ${rdo_tax_invoice_by_name}   _$taxAddressByName    ${tax_invoice_name}
    Wait Until Element Is Visible   ${actual_path} 	${TIMEOUT}
    ${tax_invoice_name_id}=    Get Element Attribute   ${actual_path}  id
    Log     [tax_invoice_name_id]=${tax_invoice_name_id}
    ${index}=   Fetch From Right    ${tax_invoice_name_id}     -
    Log     [index]=${index}

    [Return]    ${index}

_get element id of bill to by name
    [Arguments]          ${bill_to_checkout_3_name}

	${actual_path}=     Replace String    ${rdo_checkout_3_billing_address_list_name}   _$billToName    ${bill_to_checkout_3_name}
    Wait Until Element Is Visible   ${actual_path} 	${TIMEOUT}
    ${bill_to_name_id}=    Get Element Attribute   ${actual_path}  id
    Log     [bill_to_name_id]=${bill_to_name_id}
    ${index}=   Fetch From Right    ${bill_to_name_id}     -
    Log     [index]=${index}
    [Return]    ${index}

Get Tax ID by company name for credit terms
    [Arguments]     ${tax_invoice_name}

    ${index}=   _get element id of tax invoice by name  ${tax_invoice_name}

    ${txt_checkout_3_tax_no_actual_path}=     Replace String    ${txt_checkout_3_tax_no_credit_terms}   _$index    ${index}
    Wait Until Element Is Visible   ${txt_checkout_3_tax_no_actual_path} 	${TIMEOUT}
    ${txt_tax_number}=  Get Text    ${txt_checkout_3_tax_no_actual_path}
    
    [Return]    ${txt_tax_number}

Get Tax Address by company name for credit terms
    [Arguments]     ${tax_invoice_name}

    ${index}=   _get element id of tax invoice by name  ${tax_invoice_name}

    ${txt_checkout_3_tax_address_actual_path}=     Replace String    ${txt_checkout_3_tax_address_credit_terms}   _$index    ${index}
    Wait Until Element Is Visible   ${txt_checkout_3_tax_address_actual_path} 	${TIMEOUT}
    ${txt_tax_number}=  Get Text    ${txt_checkout_3_tax_address_actual_path}
    
    [Return]    ${txt_tax_number}


Get Tax ID by company name  
    [Arguments]     ${tax_invoice_name}

    ${index}=   _get element id of tax invoice by name  ${tax_invoice_name}

    ${txt_checkout_3_tax_no_actual_path}=     Replace String    ${txt_checkout_3_tax_no}   _$index    ${index}
    Wait Until Element Is Visible   ${txt_checkout_3_tax_no_actual_path} 	${TIMEOUT}
    ${txt_tax_number}=  Get Text    ${txt_checkout_3_tax_no_actual_path}

    [Return]    ${txt_tax_number}

Get Tax Address by company name     
    [Arguments]     ${tax_invoice_name}

	${index}=   _get element id of tax invoice by name  ${tax_invoice_name}

    ${txt_checkout_3_tax_address_actual_path}=     Replace String    ${txt_checkout_3_tax_address}   _$index    ${index}
    Wait Until Element Is Visible   ${txt_checkout_3_tax_address_actual_path} 	${TIMEOUT}
    ${txt_tax_address}=  Get Text    ${txt_checkout_3_tax_address_actual_path}

    [Return]    ${txt_tax_address}


Verify Tax Invoice/Receipt Information name
    [Arguments]     ${expecte_tax_invoice_name}

    ${txt_checkout_3_tax_name_actual_path}=     Replace String    ${txt_checkout_3_tax_name_credit_terms}   _$index    0
    Wait Until Element Is Visible   ${txt_checkout_3_tax_name_actual_path} 	${TIMEOUT}
    ${actual_txt_tax_name}=  Get Text    ${txt_checkout_3_tax_name_actual_path}
    Should Be Equal     ${actual_txt_tax_name}  ${expecte_tax_invoice_name}

Verify Tax Invoice/Receipt Information id by Tax Invoice Name
    [Arguments]   ${expected_tax_invoice_name}    ${expected_tax_invoice_id}

    ${actual_tax_id}=   Get Tax ID by company name for credit terms     ${expected_tax_invoice_name}
    Should Be Equal     ${actual_tax_id}    ${expected_tax_invoice_id}

Verify Tax Invoice/Receipt Information address by Tax Invoice Name
    [Arguments]  ${expected_tax_invoice_name}    ${expecte_tax_invoice_address}
     
    ${actual_tax_address}=  Get Tax Address by company name for credit terms    ${expected_tax_invoice_name}
    Should Be Equal     ${actual_tax_address}    ${expecte_tax_invoice_address}

Select 'Bill To' information from billing name
    [Arguments]     ${expected_bill_to_name}

    ${index}=   _get element id of bill to by name  ${expected_bill_to_name}
    ${txt_checkout_3_bill_to_actual_path}=     Replace String    ${lbl_checkout_3_billing_address_list_name}   _$index    ${index}
    Wait Until Element Is Visible   ${txt_checkout_3_bill_to_actual_path} 	${TIMEOUT}
    Click Element   ${txt_checkout_3_bill_to_actual_path}

    
### TRUE ###
Verify cost center 
    [Arguments]     ${expected_cost_center_value}

    Wait Until Element Is Visible   ${txt_checkout_3_credit_terms_cost_center} 	${TIMEOUT}
    ${actual_checkout_3_cost_center}=   Get Value   ${txt_checkout_3_credit_terms_cost_center}
    Log     [actual_checkout_3_cost_center]=${actual_checkout_3_cost_center}  
    Should Be Equal     ${actual_checkout_3_cost_center}    ${expected_cost_center_value}


Verify cost center name
    [Arguments]     ${expected_cost_center_name}

    Wait Until Element Is Visible   ${txt_checkout_3_credit_terms_cost_center_name} 	${TIMEOUT}
    ${actual_checkout_3_cost_center_name}=   Get Value  ${txt_checkout_3_credit_terms_cost_center_name}  
    Log     [actual_checkout_3_cost_center_name]=${actual_checkout_3_cost_center_name}
    Should Be Equal     ${actual_checkout_3_cost_center_name}    ${expected_cost_center_name}


#------ Som ------#

Get Checkout 3 Credit Card PayOn Date
    Wait Until Element Is Visible    ${lbl_checkout_3_payon_date}    ${TIMEOUT}
    ${payon}    Get Element Attribute    ${lbl_checkout_3_payon_date}    data-selenium-payon-date
    [Return]    ${payon}

Get Checkout 3 Credit Card Card Expire
    Wait Until Element Is Visible    ${lbl_checkout_3_card_expire}    ${TIMEOUT}
    ${expire}    Get Element Attribute    ${lbl_checkout_3_card_expire}    data-selenium-card-expire
    [Return]    ${expire}

Verify Checkout 3 Credit Card PayOn Date
    [Arguments]    ${value}
    ${payon}    Get Checkout 3 Credit Card PayOn Date
    Should Be Equal    ${payon}    ${value}

Verify Checkout 3 Credit Card Card Expire
    [Arguments]    ${value}
    ${expire}    Get Checkout 3 Credit Card Card Expire
    ${value_num}    Replace String    ${value}    ${space}    ${empty}
    ${expire_num}    Replace String    ${expire}    ${space}    ${empty}
    ${value_num}    Convert To Integer    ${value_num}
    ${expire_num}    Convert To Integer    ${expire_num}
    Should Be True    ${expire_num} >= ${value_num}


Input Checkout 3 New Address Company
    [Arguments]    ${company}
    Input Checkout 3 Address Company    NaN    ${company}

Input Checkout 3 New Address Tax Number
    [Arguments]    ${tax_no}
    Input Checkout 3 Address Tax Number    NaN    ${tax_no}

Input Checkout 3 New Address Address
    [Arguments]    ${address}
    Input Checkout 3 Address Address    NaN    ${address}

Click Checkout 3 New Address Save
    Click Checkout 3 Address Save    NaN

Click Checkout 3 New Address Cancel
    Click Checkout 3 Address Cancel    NaN

Get Count Checkout 3 List Tax Address
    ${count}    Get Matching Xpath Count    ${list_checkout_3_address}
    [Return]    ${count}

Select Checkout 3 Tax Address By Index
    [Arguments]    ${str_index}    ${TYPE}
    # ${locator_address}    Replace String    ${rdo_checkout_3_address_list_select}    INDEX    ${str_index}
    ${locator_address}    Replace String    ${rdo_checkout_3_address_list_type_select}     INDEX    ${str_index}
    ${locator_address}    Replace String    ${locator_address}    TYPE    ${TYPE}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    Click Element    ${locator_address}

Select Checkout 3 Tax Address Credit Card By Index
    [Arguments]    ${str_index}
    ${locator_address}    Replace String    ${rdo_checkout_3_address_list_ccw_select}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    Click Element    ${locator_address}

Select Checkout 3 Company Tax Address By Index
    [Arguments]    ${str_index}
    ${locator_address}    Replace String    ${rdo_checkout_3_company_address_list_select}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    Click Element    ${locator_address}

Get Checkout 3 Address List Company By Index
    [Arguments]    ${str_index}    ${TYPE}
    ${locator_company}    Replace String    ${lbl_checkout_3_address_list_company}    INDEX    ${str_index}
    ${locator_company}    Replace String    ${locator_company}    TYPE    ${TYPE}
    Wait Until Element Is Visible    ${locator_company}    ${TIMEOUT}
    ${company}    Get Text    ${locator_company}
    [Return]    ${company}

Get Checkout 3 Address List Tax Number By Index
    [Arguments]    ${str_index}    ${TYPE}
    ${locator_tax_no}    Replace String    ${lbl_checkout_3_address_list_tax_number}    INDEX    ${str_index}
    ${locator_tax_no}    Replace String    ${locator_tax_no}    TYPE    ${TYPE}
    Wait Until Element Is Visible    ${locator_tax_no}    ${TIMEOUT}
    ${tax_no}    Get Text    ${locator_tax_no}
    [Return]    ${tax_no}

Get Checkout 3 Address List Address By Index
    [Arguments]    ${str_index}    ${TYPE}
    ${locator_address}    Replace String    ${lbl_checkout_3_address_list_address}    INDEX    ${str_index}
    ${locator_address}    Replace String    ${locator_address}    TYPE    ${TYPE}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    ${address}    Get Text    ${locator_address}
    [Return]    ${address}

Input Checkout 3 Credit Terms Cost Center
    [Arguments]    ${cost_center}
    Wait Until Element Is Visible    ${txt_checkout_3_credit_terms_cost_center}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_credit_terms_cost_center}    ${cost_center}

Input Checkout 3 Credit Terms Cost Center Name
    [Arguments]    ${cost_center_name}
    Wait Until Element Is Visible    ${txt_checkout_3_credit_terms_cost_center_name}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_credit_terms_cost_center_name}    ${cost_center_name}

Input Checkout 3 Credit Terms Fund Code
    [Arguments]    ${fund_code}
    Wait Until Element Is Visible    ${txt_checkout_3_credit_terms_fund_code}    ${TIMEOUT}
    Input Text    ${txt_checkout_3_credit_terms_fund_code}    ${fund_code}

Click Check Checkout 3 Credit Terms Use Default Cost Center
    Wait Until Element Is Visible    ${btn_checkout_3_credit_terms_defaul_cost_center}    ${TIMEOUT}
    ${is_unchecked}    Run Keyword And Return Status    Checkbox Should Not Be Selected    ${chb_checkout_3_credit_terms_defaul_cost_center}
    Run Keyword If    ${is_unchecked}    Click Element    ${btn_checkout_3_credit_terms_defaul_cost_center}

Click Uncheck Checkout 3 Credit Terms Use Default Cost Center
    Wait Until Element Is Visible    ${btn_checkout_3_credit_terms_defaul_cost_center}    ${TIMEOUT}
    ${is_checked}    Run Keyword And Return Status    Checkbox Should Be Selected    ${chb_checkout_3_credit_terms_defaul_cost_center}
    Run Keyword If    ${is_checked}    Click Element    ${btn_checkout_3_credit_terms_defaul_cost_center}

Select Checkout 3 Credit Terms Upload File
    [Arguments]    ${file_path}
    ${file_path}  get_canonical_path    ${file_path}
    Wait Until Element Is Visible    ${btn_checkout_3_credit_terms_upload_file}    ${TIMEOUT}
    Choose File    ${file_checkout_3_credit_terms_upload_file}    ${file_path}

Click Checkout 3 Credit Terms Remove Upload File
    Wait Until Element Is Visible    ${btn_checkout_3_credit_terms_remove_upload_file}    ${TIMEOUT}
    Click Element    ${btn_checkout_3_credit_terms_remove_upload_file}

Get Read Only Checkout 3 Credit Terms Cost Center
    Wait Until Element Is Visible    ${txt_checkout_3_credit_terms_cost_center}    ${TIMEOUT}
    ${is_readonly}    Get Element Attribute    ${txt_checkout_3_credit_terms_cost_center}    readonly
    [Return]    ${is_readonly}

Get Read Only Checkout 3 Credit Terms Cost Center Name
    Wait Until Element Is Visible    ${txt_checkout_3_credit_terms_cost_center_name}    ${TIMEOUT}
    ${is_readonly}    Get Element Attribute    ${txt_checkout_3_credit_terms_cost_center_name}    readonly
    [Return]    ${is_readonly}

Click Checkout 3 Add New Billing Address
    Wait Until Element Is Visible    ${btn_checkout_3_billing_address_list_new_address}    ${TIMEOUT}
    Click Element    ${btn_checkout_3_billing_address_list_new_address}

Input Checkout 3 Billing Address Name
    [Arguments]    ${str_index}    ${name}
    ${locator_name}    Replace String    ${txt_checkout_3_billing_address_form_name}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_name}    ${TIMEOUT}
    Input Text    ${locator_name}    ${name}

Input Checkout 3 Billing Address Phone
    [Arguments]    ${str_index}    ${phone}
    ${locator_phone}    Replace String    ${txt_checkout_3_billing_address_form_phone}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_phone}    ${TIMEOUT}
    Input Text    ${locator_phone}    ${phone}

Input Checkout 3 Billing Address Address
    [Arguments]    ${str_index}    ${address}
    ${locator_address}    Replace String    ${txt_checkout_3_billing_address_form_address}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    Input Text    ${locator_address}    ${address}

Click Checkout 3 Billing Address Save
    [Arguments]    ${str_index}
    ${locator_save}    Replace String    ${btn_checkout_3_billing_address_form_save}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_save}    ${TIMEOUT}
    Click Element    ${locator_save}

Click Checkout 3 Billing Address Cancel
    [Arguments]    ${str_index}
    ${locator_cancel}    Replace String    ${btn_checkout_3_billing_address_form_cancel}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_cancel}    ${TIMEOUT}
    Click Element    ${locator_cancel}

Input Checkout 3 New Billing Address Name
    [Arguments]    ${name}
    Input Checkout 3 Billing Address Name    form-add    ${name}

Input Checkout 3 New Billing Address Phone
    [Arguments]    ${phone}
    Input Checkout 3 Billing Address Phone    form-add    ${phone}

Input Checkout 3 New Billing Address Address
    [Arguments]    ${address}
    Input Checkout 3 Billing Address Address    form-add    ${address}

Click Checkout 3 New Billing Address Save
    Click Checkout 3 Billing Address Save    form-add

Click Checkout 3 New Billing Address Cancel
    Click Checkout 3 Billing Address Cancel    form-add

Get Count Checkout 3 List Billing Address
    ${count}    Get Matching Xpath Count    ${list_checkout_3_billing_address}
    [Return]    ${count}

Select Checkout 3 Billing Address By Index
    [Arguments]    ${str_index}
    ${locator_address}    Replace String    ${rdo_checkout_3_billing_address_list_select}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    Click Element    ${locator_address}

Get Checkout 3 Billing Address List Name By Index
    [Arguments]    ${str_index}
    ${locator_name}    Replace String    ${lbl_checkout_3_billing_address_list_name}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_name}    ${TIMEOUT}
    ${name}    Get Text    ${locator_name}
    [Return]    ${name}

Get Checkout 3 Billing Address List Phone By Index
    [Arguments]    ${str_index}
    ${locator_phone}    Replace String    ${lbl_checkout_3_billing_address_list_phone}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_phone}    ${TIMEOUT}
    ${phone}    Get Text    ${locator_phone}
    [Return]    ${phone}

Get Checkout 3 Billing Address List Address By Index
    [Arguments]    ${str_index}
    ${locator_address}    Replace String    ${lbl_checkout_3_billing_address_list_address}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    ${address}    Get Text    ${locator_address}
    [Return]    ${address}

Get Text Input Checkout 3 Address Company By Index
    [Arguments]    ${str_index}
    ${locator_company}    Replace String    ${txt_checkout_3_address_form_company}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_company}    ${TIMEOUT}
    ${company}    Get Value    ${locator_company}
    [Return]    ${company}

Get Text Input Checkout 3 Address Tax Number By Index
    [Arguments]    ${str_index}
    ${locator_tax_no}    Replace String    ${txt_checkout_3_address_form_tax_number}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_tax_no}    ${TIMEOUT}
    ${tax_no}    Get Value    ${locator_tax_no}
    ${tax_no}    Replace String    ${tax_no}    -    ${EMPTY}
    ${tax_no}    Replace String    ${tax_no}    ${SPACE}    ${EMPTY}
    [Return]    ${tax_no}

Get Text Input Checkout 3 Address Address By Index
    [Arguments]    ${str_index}
    ${locator_address}    Replace String    ${txt_checkout_3_address_form_address}    INDEX    ${str_index}
    Wait Until Element Is Visible    ${locator_address}    ${TIMEOUT}
    ${address}    Get Value    ${locator_address}
    [Return]    ${address}

