*** Settings ***
Library 	String
Library 	Selenium2Library 	run_on_failure=Selenium2Library.Capture Page Screenshot
Resource    ${CURDIR}/../../../locators/ui/fe/thankyou.robot

*** Variables ***

*** Keywords ***
Verify booking status on Thank you page     
    [Arguments]     ${expected_booking_status_success}

    Wait Until Element Is Visible   ${txt_thkyou_booking_status} 	${BOOKING_TIMEOUT}
    ${actual_booking_status_success}=   Get Text    ${txt_thkyou_booking_status}
    Should Be Equal     ${actual_booking_status_success}    ${expected_booking_status_success}

Verify 'Save Voucher' button is displayed
    Wait Until Element Is Visible   ${btn_thkyou_save_voucher} 	${TIMEOUT}
    Element Should Be Visible   ${btn_thkyou_save_voucher}

Verify 'Print' button is displayed 
    Wait Until Element Is Visible   ${btn_thkyou_print_voucher} 	${TIMEOUT}
    Element Should Be Visible   ${btn_thkyou_print_voucher}


Verify Booker Name
    [Arguments]     ${expected_booker_firstname}     ${expected_booker_lastname}

    Wait Until Element Is Visible   ${txt_thkyou_booker_full_name} 	${TIMEOUT}
    ${actual_booker_full_name}=   Get Text    ${txt_thkyou_booker_full_name}
    Should Be Equal     ${actual_booker_full_name}    ${expected_booker_firstname}${SPACE}${expected_booker_lastname}

Verify Booker Mobile Number
    [Arguments]     ${expected_booker_phone}

    Wait Until Element Is Visible   ${txt_thkyou_booker_phone} 	${TIMEOUT}
    ${actual_booker_phone}=   Get Text    ${txt_thkyou_booker_phone}
    Should Be Equal     ${actual_booker_phone}    ${expected_booker_phone}

Verify Booker Email Address
    [Arguments]     ${expected_ooker_email_address} 

    Wait Until Element Is Visible   ${txt_thkyou_booker_email} 	${TIMEOUT}
    ${actual_booker_email}=   Get Text    ${txt_thkyou_booker_email}
    Should Be Equal     ${actual_booker_email}    ${expected_ooker_email_address}

Verify Tax Invoice Company Name
    [Arguments]     ${expected_tax_invoice_company_name} 

    Wait Until Element Is Visible   ${txt_thkyou_company_name} 	${TIMEOUT}
    ${actual_tax_invoice_company_name}=   Get Text    ${txt_thkyou_company_name}
    Should Be Equal     ${actual_tax_invoice_company_name}    ${expected_tax_invoice_company_name}

Verify Tax Invoice TaxID/Personal ID
    [Arguments]    ${expected_tax_number} 

    Wait Until Element Is Visible   ${txt_thkyou_tax_number} 	${TIMEOUT}
    ${actual_tax_number}=   Get Text    ${txt_thkyou_tax_number}
    Should Be Equal     ${actual_tax_number}    ${expected_tax_number}

Verify Tax Invoice Address
    [Arguments]  ${expected_tax_address}

    Wait Until Element Is Visible   ${txt_thkyou_tax_address} 	${TIMEOUT}
    ${actual_tax_address}=   Get Text    ${txt_thkyou_tax_address}
    Should Be Equal     ${actual_tax_address}    ${expected_tax_address}

Verify Property Name
    [Arguments]    ${expected_property_name} 

    Wait Until Element Is Visible   ${txt_thkyou_property_name} 	${TIMEOUT}
    ${actual_property_name}=   Get Text    ${txt_thkyou_property_name}
    Should Be Equal     ${actual_property_name}    ${expected_property_name}

Verify Property Address
    [Arguments]     ${expected_property_address} 

    Wait Until Element Is Visible   ${txt_thkyou_property_address} 	${TIMEOUT}
    ${actual_property_address}=   Get Text    ${txt_thkyou_property_address}
    Should Be Equal     ${actual_property_address}    ${expected_property_address}

Verify Check-In date
    [Arguments]    ${expected_check_in_date} 

    Wait Until Element Is Visible   ${txt_thkyou_check_in_date} 	${TIMEOUT}
    ${actual_check_in_date}=   Get Text    ${txt_thkyou_check_in_date}
    Should Contain     ${actual_check_in_date}    ${expected_check_in_date}
    
Verify Check-Out date
    [Arguments]     ${expected_check_out_date} 

    Wait Until Element Is Visible   ${txt_thkyou_check_out_date} 	${TIMEOUT}
    ${actual_check_out_date}=   Get Text    ${txt_thkyou_check_out_date}
    Should Contain     ${actual_check_out_date}    ${expected_check_out_date}

Verify Room Type
    [Arguments]     ${hotel_direct_room_type_name}

Verify number of room
    [Arguments]     ${expected_number_of_room}

    Wait Until Element Is Visible   ${txt_thkyou_number_of_room} 	${TIMEOUT}
    ${actual_number_of_room}=   Get Text    ${txt_thkyou_number_of_room}
    Should Be Equal     ${actual_number_of_room}    ${expected_number_of_room}
    
Verify number of guest
    [Arguments]     ${expected_number_of_guest}

    Wait Until Element Is Visible   ${txt_thkyou_number_of_guest} 	${TIMEOUT}
    ${expected_number_of_guest}=   Get Text    ${txt_thkyou_number_of_guest}
    Should Be Equal     ${expected_number_of_guest}    ${expected_number_of_guest}

Get Booking Id from Thank you screen
    Wait Until Element Is Visible   ${txt_thkyou_booking_id} 	${TIMEOUT}
    ${actual_booking_id}=   Get Text    ${txt_thkyou_booking_id}
    [Return]    ${actual_booking_id}
