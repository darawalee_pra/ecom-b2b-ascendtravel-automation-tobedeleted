*** Settings ***
Library     String
Library     Selenium2Library
Library     DatabaseLibrary
Library     DateTime
Library     openpyxl
Library     Collections
Library     RequestsLibrary
Resource    ${CURDIR}/../common/common.robot

*** Keywords ***
Insert Supplier Data Into Supplier Table
    [Arguments]     ${country_id}=212   ${type_id}=1    ${name}=default_name     ${address}=address  
    ...     ${tax_id}=1234567890123     ${tax_name}=default_tax_name     ${tax_address}=tax_address  
    ...     ${followup_tax_invoice}=0    ${rev_email}=abs@g.hhh     ${default_per_markup}=10    
    ...     ${in_vat}=1   ${vat}=7  ${status}=1   ${active}=1
    ${output_insert}=   Execute SQL String    INSERT INTO Supplier ( CountryID, SupplierTypeID, Name, Address, TaxID, TaxName, TaxAddress, notFollowUpTaxInvoice, ReservationEmail, DefaultPercentMarkup, IncludeVAT, VAT, Status, Active) VALUES ( ${country_id}, ${type_id}, N'${name}', N'${address}', '${tax_id}', N'${tax_name}', N'${tax_address}', ${followup_tax_invoice}, '${rev_email}', ${default_per_markup}, ${in_vat}, ${vat}, ${status}, ${active})
    Should Be Equal As Strings    ${output_insert}    None
    ${output_query}=    Query      SELECT SupplierID FROM Supplier WHERE name = '${name}'
    Log To Console    ${output_query[0][0]}
    [Return]    ${output_query[0][0]}

Insert Supplier MultiCurrency Into SupplierMultiCurrency Table
    [Arguments]     ${supplier_id}      ${sap_code}=19960227    ${currency_id}=1    ${main_currency}=1
    ${output_insert} =    Execute SQL String    INSERT INTO SupplierMultiCurrency ( SupplierID, CurrencyID, SapCode, MainCurrency) VALUES (${supplier_id}, ${currency_id}, '${sap_code}', ${main_currency})
    Should Be Equal As Strings    ${output_insert}    None

Count Total Supplier
    ${output}=      Query   SELECT count(*) FROM Supplier JOIN SupplierMultiCurrency ON Supplier.SupplierID = SupplierMultiCurrency.SupplierId WHERE Supplier.Active =1 and SupplierMultiCurrency.SapCode is not null and SupplierMultiCurrency.MainCurrency =1
    [Return]        ${output}

Select Supplier Name Order By Name
    [Arguments]     ${sort_type}=ASC    ${number}=50    
    ${output}=      Query   SELECT TOP ${number} Name FROM Supplier WHERE Active = 1 Order by name ${sort_type}
    [Return]        ${output}

Select Supplier Main Sap Code Order By Sap Code
    [Arguments]     ${sort_type}=ASC    ${number}=50   
    ${output}=      Query   SELECT TOP ${number} SapCode FROM Supplier JOIN SupplierMultiCurrency ON Supplier.SupplierID = SupplierMultiCurrency.SupplierId WHERE Supplier.Active =1 and SupplierMultiCurrency.SapCode is not null and SupplierMultiCurrency.MainCurrency =1 order by SapCode ${sort_type}
    [Return]        ${output}

Disable Supplier By Supplier Name 
    [Arguments]     ${supplier_name}
    ${output}=      Execute SQL String  UPDATE Supplier Set Active = 0 WHERE name = N'${supplier_name}'
    Should Be Equal As Strings    ${output}    None

Delete Supplier MultiCurrency By Supplier Id 
    [Arguments]     ${supplier_id}
    ${output}=      Execute SQL String  DELETE FROM SupplierMultiCurrency WHERE SupplierID = ${supplier_id}
    Should Be Equal As Strings    ${output}    None

Delete Supplier Bank By Supplier Payment Id
    [Arguments]     ${supplier_payment_id}
    ${output}=      Execute SQL String  DELETE FROM SupplierBank WHERE SupplierPaymentID = ${supplier_payment_id}
    Should Be Equal As Strings    ${output}    None

Delete Supplier Payment By Supplier Id
    [Arguments]     ${supplier_id}
    ${output}=      Execute SQL String  DELETE FROM SupplierPayment WHERE SupplierID = ${supplier_id}
    Should Be Equal As Strings    ${output}    None

Delete Supplier Contact By Supplier Id
    [Arguments]     ${supplier_id}
    ${output}=      Execute SQL String  DELETE FROM SupplierContact WHERE SupplierID = ${supplier_id}
    Should Be Equal As Strings    ${output}    None

Delete Supplier Document By Supplier Id
    [Arguments]     ${supplier_id}
    ${output}=      Execute SQL String  DELETE FROM SupplierDocument WHERE SupplierID = ${supplier_id}
    Should Be Equal As Strings    ${output}    None

Delete Supplier By Supplier Id
    [Arguments]     ${supplier_id}
    ${output}=      Execute SQL String  DELETE FROM Supplier WHERE SupplierID = ${supplier_id}
    Should Be Equal As Strings    ${output}    None

Select Supplier ID By Supplier Name
    [Arguments]     ${supplier_name}
    ${byte_string}=     Encode String To Bytes      ${supplier_name}    UTF-8   
    ${supplier_name}=   Decode Bytes To String      ${byte_string}      UTF-8
    ${output}=      Query   SELECT SupplierID FROM Supplier WHERE Name = N'${supplier_name}'
    [Return]        ${output}

Delete Supplier Data From DB
    [Arguments]     ${supplier_name}
    ${supplier_id}    Select Supplier ID By Supplier Name   ${supplier_name}
    Run Keyword If  ${supplier_id} != []    Delete Supplier MultiCurrency By Supplier Id    ${supplier_id[0][0]}
    Run Keyword If  ${supplier_id} != []    Delete Supplier By Supplier Id      ${supplier_id[0][0]}
    ...     ELSE    Log To Console  Supplier name '${supplier_name}' doesn't exists

Select Supplier Type List
    ${output}=  Query   SELECT * FROM SupplierType
    [Return]    ${output}

Select Country List
    ${output}=  Query   SELECT * FROM Country
    [Return]    ${output}

Select Supplier Payment Type List
    ${output}=  Query   SELECT * FROM SupplierPaymentType
    [Return]    ${output}

Select Bank List
    ${output}=  Query   SELECT * FROM Bank
    [Return]    ${output}

Select Currency List
    ${output}=  Query   SELECT * FROM Currency
    [Return]    ${output} 

Select Supplier Type Order By ID
    [Arguments]     ${sort_by}=ASC 
    ${output}=  Query   SELECT * FROM SupplierType Order by SupplierTypeID ${sort_by}
    [Return]    ${output} 

Select Supplier Payment Type Order By ID
    [Arguments]     ${sort_by}=ASC 
    ${output}=  Query   SELECT * FROM SupplierPaymentType Order by SupplierPaymentTypeID ${sort_by}
    [Return]    ${output} 

Select Bank List Order By Name
    [Arguments]     ${sort_by}=ASC 
    ${output}=  Query   SELECT Title FROM Bank Order by Title ${sort_by}
    [Return]    ${output} 

Select Currency List Order By Code
    [Arguments]     ${sort_by}=ASC 
    ${output}=  Query   SELECT CurrencyCode FROM Currency Order by CurrencyCode ${sort_by}
    [Return]    ${output} 

Select Currency By Id
    [Arguments]    ${currencyId}
    ${output}=  Query   SELECT * FROM Currency WHERE CurrencyID = ${currencyId}
    [Return]    ${output} 

Select Country List Order By Name EN
    [Arguments]     ${sort_by}=ASC 
    ${output}=  Query   SELECT TitleEn FROM Country Order by TitleEn ${sort_by}
    [Return]    ${output} 

Select Active Flag From Supplier By Supplier ID
    [Arguments]     ${supplier_id}
    ${output}=  Query   SELECT Active FROM Supplier WHERE supplierID = ${supplier_id}
    [Return]    ${output} 

Verify Supplier Active Flag Store In DB Correctly
    [Arguments]     ${supplier_id}  ${expected_val}
    ${active_flag}      Select Active Flag From Supplier By Supplier ID     ${supplier_id}
    Should Be Equal As Strings  ${active_flag[0][0]}  ${expected_val}