*** Settings ***
Library     String
Library     Selenium2Library
Library     DatabaseLibrary
Library     DateTime
Library     openpyxl
Library     Collections
Library     RequestsLibrary
Resource    ${CURDIR}/../common/common.robot

*** Keywords ***
Count Total Room View
    ${output}=      Query   SELECT count(*) FROM RoomView
    [Return]        ${output[0][0]}

Select Room View Order By Name EN
    [Arguments]     ${sort_type}=ASC   
    ${output}=      Query   SELECT * FROM RoomView Order by NameEN ${sort_type}
    [Return]        ${output}
