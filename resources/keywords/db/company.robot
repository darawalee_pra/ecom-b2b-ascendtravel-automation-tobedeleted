*** Settings ***
Library     String
Library     Selenium2Library
Library     DatabaseLibrary
Library     DateTime
Library     openpyxl
Library     Collections
Library     RequestsLibrary
Resource    ${CURDIR}/../common/common.robot

*** Keywords ***
Count Total Company
    ${output}=      Query   SELECT count(*) FROM Company WHERE Active =1
    [Return]        ${output}

Select Company Name Order By Name
    [Arguments]     ${sort_type}=ASC    ${number}=50    
    ${output}=      Query   SELECT TOP ${number} NameEN FROM Company WHERE Active = 1 Order by NameEN ${sort_type}
    [Return]        ${output}