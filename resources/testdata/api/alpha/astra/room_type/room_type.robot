*** Variables ***
${product_data_file}            create_product_aey.xlsx
${hotel_id_1}                   21    
${hotel_name_th}                โรงแรมเมอคิวรี่สำหรับสร้างชนิดของห้อง
${hotel_name_en}                Mercury Hotel for create room type
${hotel_address_th}             ที่อยู่โรงแรมเมอคิวรี่
${hotel_address_en}             Mercury Hotel Address
${hotel_image}                  s3://hotel/h1       
${hotel_id_2}                   166    
${hotel_id_3}                   200
${hotel_name_th_long}           โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว โรงแรมเมอคิวรี่ชื่อยาว
${hotel_name_en_long}           Mercury Hotel for create room type long name Mercury Hotel for create room type long name Mercury Hotel for create room type long name Mercury Hotel for create room type long name Mercury Hotel for create room type long name
${hotel_address_th_long}        ที่อยู่โรงแรมเมอคิวรี่ ซอย เมอเคียว ถนน ดาวเคราะห์ อำเภอ เมือง จังหวัด กทม 12345 
${hotel_address_en_long}        555 Mercury Hotel Address Rd. AAABB test Bangkok Thailand   
@{roomview_name_en}             
@{roomview_name_th}             
@{bed_types_list}               Single Bed    Semi Double-bed   Double Bed  Queen Bed   King Bed    Super King Bed  Bunk Bed    Sofa Bed    Futon   Male Capsule    Female Capsule
&{supplier_dict_hotel_3}        252=Mercury supplier 1
&{supplier_dict_hotel_1}        252=Mercury supplier 1    261=Mercury supplier 10    262=Mercury supplier 11          253=Mercury supplier 2    254=Mercury supplier 3     259=Mercury supplier 8
...                             255=Mercury supplier 4    256=Mercury supplier 5     257=!@\'\" Mercury supplier 6    258=Mercury supplier 7    260=เมอคิวรี่ ซัพพรายเออร์ 9
...                             263=Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12 Mercury supplier 12       