*** Variables ***
# ################################## For Production Test ##################################
${credit_card_user}                                          thortestuser@gmail.com
${credit_card_pass}                                          12345678
${credit_term_user}                                          ascendthorteami@gmail.com
${credit_term_pass}                                          pZmgwqj0

### SCG ###
${scg_username}     ascendtr
${scg_password}     Passw0rd*1234
${scg_email}    ascendtr@scg.com
${scg_mobile_number}    0967671454
${scg_budget_limit_txt_en}     Please check your budget for book the hotel from your business unit.
${scg_budget_limit_txt_th}  โปรดตรวจสอบอัตราค่าที่พักสำหรับพนักงานจากต้นสังกัดของคุณ
${scg_booker_first_name}    SCG Booker FistName
${scg_booker_last_name}     SCG Booker LastName
${scg_cost_center}  9999-99999
${scg_cost_center_name}     test

${scg_tax_invoice_name_credit_terms}    Test
${scg_tax_invoice_id_credit_terms}  0000
${scg_tax_invoice_address_credit_terms}     Test

${scg_bill_to_name}     woramon thanasapjinda
${scg_bill_to_contact_number}   0878987878
${scg_bill_to_address}  AIA Tower


### Hotel Information ###
${work_purpose}                                              Work
${leisure_purpose}                                           Leisure
#- Direct Hotel -#
${seach_type_hotel}                                          Hotel
${hotel_direct_name}                                         zdvhml
${hotel_direct_room_type_pay_later}                          Test Charge Money Cancellation Room A3
${hotel_direct_room_type_free_cancellation}                  Test Free Cancellation Room A2 C1
${hotel_direct_address}                                      Bangkok

#- Hotel Bed -#
${hotel_bed_name}                                            Admiral Suites Sukhumvit by Compass Hospitality
${hotel_bed_change_name}                                     Motive Cottage Resort

### Destination Information ###
${seach_type_destination}                                    Destination
${destination_name}                                          zdvhml
${destination_hotel}                                         zdvhml

### POI Information ###
${seach_type_poi}                                            POI
${poi_name}                                                  Paragon
${poi_hotel_name}                                            Hip Hotel Bangkok

${change_poi_name}                                           Siam Commercial Bank
${change_poi_hotel_name}                                     Northgate Ratchayothin Hotel

### Booking Information ###
${booker_firstname}                                          Test Sanity Booker FirstName
${booker_lastname}                                           Test Sanity Booker LastName
${booker_mobile_no}                                          0123456789

${guest_room_one_firstname_credit_card}                      Sanity GuestOne FirstName Credit Card
${guest_room_one_lastname_credit_card}                       Sanity GuestOne LastName Credit Card
${guest_room_one_email_credit_card}                          thortestuser@gmail.com
${guest_room_one_nationality}                                Thai

${guest_room_two_firstname_credit_card}                      Sanity GuestTwo FirstName Credit Card
${guest_room_two_lastname_credit_card}                       Sanity GuestTwo LastName Credit Card
${guest_room_two_email_credit_card}                          thortestuser@gmail.com
${guest_room_two_nationality}                                Thai

${guest_room_one_firstname_credit_terms}                     Sanity GuestOne FirstName Credit Terms
${guest_room_one_lastname_credit_terms}                      Sanity GuestOne LastName Credit Terms
${guest_room_one_email_credit_terms}                         thortestuser@gmail.com
${guest_room_one_nationality}                                Thai

${guest_room_two_firstname_credit_terms}                     Sanity GuestTwo FirstName Credit Terms
${guest_room_two_lastname_credit_terms}                      Sanity GuestTwo LastName Credit Terms
${guest_room_two_email_credit_terms}                         thortestuser@gmail.com
${guest_room_two_nationality}                                Thai

#- Credit Card -#
${payment_method_credit_card}                                Visa Card/Master Card
${credit_card_number}                                        5270696498854501
${name_on_card}                                              T CHIANGVIVAT
${card_expiry_month}                                         08    
${card_expiry_year}                                          2022
${credit_card_cvv}                                           190

${tax_invoice_name}                                          Test Sanity Company
${tax_invoice_number}                                        1111111111111
${tax_invoice_address}                                       Test Sanity Address

#- Credit Term -#
${payment_method_credit_terms}                               Credit Terms
${cost_center}                                               12345678AA
${cost_center_name}                                          AscendTravel1
${company_reference_code}                                    76543210
${tax_invoice_name_credit_terms}                             Sanity company name 1 tran tax 20170920


### Thank you information ###
${booking_status_credit_card_paynow}                         Your payment is processed successfully.
${booking_status_credit_card_paylater}                       Your reservation is processed successfully.
${booking_status_credit_terms}                               Your reservation is processed successfully.

