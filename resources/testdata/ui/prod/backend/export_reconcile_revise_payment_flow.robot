*** Variables ***
${order_id_column}                                           B
${booking_id_column}                                         C
${payment_gateway_column}                                    E
${act_payment_id_column}                                     F
${payment_id_column}                                         G
${booking_date_column}                                       H
${booking_time_column}                                       I
${payment_date_column}                                       J
${payment_time_column}                                       K
${traveling_objective_column}                                L
${payment_method_column}                                     M
${card_number_column}                                        N
${name_on_card_column}                                       P
${company_name_column}                                       R
${contact_number_column}                                     T
${email_column}                                              U
${billing_name_column}                                       V
${billing_address_column}                                    W
${billing_province_column}                                   X
${receipt_tax_invoice_name_column}                           Y
${receipt_tax_invoice_address_column}                        Z
${tax_id_column}                                             AA
${branch_column}                                             AB
${payment_status_column}                                     AC
${financial_status_column}                                   AD
${unearned_date_column}                                      AE
${unearned_time_column}                                      AF
${revenue_date_column}                                       AG
${revenue_time_column}                                       AH
${timstamp_status_change_date_column}                        AI
${timstamp_status_change_time_column}                        AJ
${order_status_column}                                       AK
${cancel_status_column}                                      AL
${supplier_name_column}                                      AO
${sap_code_column}                                           AP
${destination_column}                                        AR
${product_name_column}                                       AS
${product_type_column}                                       AT
${inventory_column}                                          AU
${main_category_column}                                      AW
${main_traveler_name_column}                                 AY
${other_traveler_name_column}                                AZ
${traveler_type_column}                                      BA
${service_start_column}                                      BB
${service_end_column}                                        BC
${selling_ex_vat_column}                                     BD
${selling_vat_column}                                        BE
${total_selling_price_thb_column}                            BF
${cost_ex_vat_column}                                        BP
${cost_vat_column}                                           BQ
${total_cost_column}                                         BR
${cost_currency_column}                                      BS
${customer_currency_column}                                  BT
${profit_amount_column}                                      BU
${profit_percent_column}                                     BV
${excange_rate_column}                                       BW
${converted_cost_column}                                     BX
${exchange_source_column}                                    BY
${receipt_tax_invoice_ref_column}                            BZ
${invoice_ref_column}                                        CA
${tax_invoice_ref_cancellation_column}                       CB
${cancellation_policy_column}                                CC
${cancellation_date_column}                                  CD
${supplier_cancellation_charge_thb_ex_vat_column}            CE
${supplier_cancellation_charge_thb_vat_column}               CF
${supplier_cancellation_charge_thb_inc_vat_column}           CG
${customer_cancellation_charge_thb_ex_vat_column}            CH
${customer_cancellation_charge_thb_vat_column}               CI
${customer_cancellation_charge_thb_inc_vat_column}           CJ
${cn_ref_column}                                             CK
${cancellation_cost_ex_vat_column}                           CL
${cancellation_cost_vat_column}                              CM
${cancellation_cost_inc_vat_column}                          CN
${supplier_type_column}                                      CO
${supplier_credit_terms_column}                              CP
${pay_out_status_column}                                     CQ
${upload_pay_out_date_column}                                CR
${upload_pay_out_time_column}                                CS
${customer_cedit_terms_column}                               CT
${link_attachment_column}                                    CU
${bill_to_name_column}                                       CV
${contact_bill_to_column}                                    CW
${bill_to_address_column}                                    CX
${company_sap_code_column}                                   CY
${upload_pay_in_date_column}                                 CZ
${upload_pay_in_time_column}                                 DA
${note_column}                                               DB
${customer_credit_billing_no_column}                         DC
${pay_in_slip_column}                                        DD
${company_reference_code_column}                             DE
${cost_center_column}                                        DF
${cost_center_name_column}                                   DG
${po_number_column}                                          DH
${partner_name_column}                                       DI

${direct_hotel_search}                                       Robot All Room Type Hotel
${direct_room_non_refundable_th}                             Room Non Refundable ไทย
${direct_room_charge_money_th}                               Room Charge Money ไทย
${big_company_username}                                      foammm@grr.la
${big_company_password}                                      P@ssw0rd
${booker_first_name}                                         BookerFirstNameRobot
${booker_last_name}                                          BookerLastNameRobot
${booker_mobile_no}                                          0811111111
${guest_first_name}                                          GuestRobot
${guest_last_name}                                           GuestRobot
${guest_email}                                               foammm@grr.la
${guest_nationality}                                         TH
${credit_card_no}                                            4111111111111111
${credit_card_name}                                          Robot Card
${credit_card_cvv}                                           123
${address_company}                                           Robot Company
${address_tax_number}                                        1111111111111
${address_address}                                           Robot Address
${payment_method_credit_card}                                Credit/Debit Card

${directroom_charge_percent}                                 Room Charge Percent
${hbt_hotel_search}                                          Admiral Suites Sukhumvit by Compass Hospitality
${hbt_room_double_studio}                                    Double Superior Studio (Room Only)
${payment_method_credit_terms}                               Credit Terms
${big_company_username_credit_terms}                         e2e-creditterms@grr.la
${big_company_password_credit_terms}                         12345678