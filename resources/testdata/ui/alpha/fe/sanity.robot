*** Variables ***
# ################################## For Alpha Test ##################################
${credit_card_user}                                          foammm@grr.la
${credit_card_pass}                                          P@ssw0rd
${credit_term_user}                                          foammm@grr.la
${credit_term_pass}                                          P@ssw0rd
# ${credit_term_user}                                          darawalee.pra@ascendcorp.com
# ${credit_term_pass}                                          Bangkok123
# ${credit_term_user}                                          nonthapat.wat@ascendcorp.com
# ${credit_term_pass}                                          403390182

### Hotel Information ###
${work_purpose}                                              Work
${leisure_purpose}                                           Leisure
#- Direct Hotel -#
${seach_type_hotel}                                          Hotel
${hotel_direct_name}                                         Robot All Room Type Hotel For Robot Only
${hotel_direct_room_type_pay_later}                          Room Charge 100 Percent Daycancel Is 5 En
${hotel_direct_room_type_free_cancellation}                  Room Free Cancellation
${hotel_direct_address}                                      Robot All Room Type Hotel For Robot Only Address

#- Hotel Bed -#
${hotel_bed_name}                                            Admiral Suites Sukhumvit by Compass Hospitality
${hotel_bed_change_name}                                     Motive Cottage Resort

### Destination Information ###
${seach_type_destination}                                    Destination
${destination_name}                                          Robot Destination
${destination_hotel}                                         Robot All Room Type Hotel For Robot Only

### POI Information ###
${seach_type_poi}                                            POI
${poi_name}                                                  Paragon
${poi_hotel_name}                                            Hip Hotel Bangkok

${change_poi_name}                                           Siam Commercial Bank
${change_poi_hotel_name}                                     Northgate Ratchayothin Hotel

### Booking Information ###
${booker_firstname}                                          Test Sanity Booker FirstName
${booker_lastname}                                           Test Sanity Booker LastName
${booker_mobile_no}                                          0123456789

${guest_room_one_firstname_credit_card}                      Sanity GuestOne FirstName Credit Card
${guest_room_one_lastname_credit_card}                       Sanity GuestOne LastName Credit Card
${guest_room_one_email_credit_card}                          thortestuser@gmail.com
${guest_room_one_nationality}                                Thai

${guest_room_two_firstname_credit_card}                      Sanity GuestTwo FirstName Credit Card
${guest_room_two_lastname_credit_card}                       Sanity GuestTwo LastName Credit Card
${guest_room_two_email_credit_card}                          thortestuser@gmail.com
${guest_room_two_nationality}                                Thai

${guest_room_one_firstname_credit_terms}                     Sanity GuestOne FirstName Credit Terms
${guest_room_one_lastname_credit_terms}                      Sanity GuestOne LastName Credit Terms
${guest_room_one_email_credit_terms}                         thortestuser@gmail.com
${guest_room_one_nationality}                                Thai

${guest_room_two_firstname_credit_terms}                     Sanity GuestTwo FirstName Credit Terms
${guest_room_two_lastname_credit_terms}                      Sanity GuestTwo LastName Credit Terms
${guest_room_two_email_credit_terms}                         thortestuser@gmail.com
${guest_room_two_nationality}                                Thai

#- Credit Card -#
${payment_method_credit_card}                                Visa Card/Master Card
${credit_card_number}                                        4111111111111111
${name_on_card}                                              Test Sanity Card
${card_expiry_month}                                         08    
${card_expiry_year}                                          2022
${credit_card_cvv}                                           123

${tax_invoice_name}                                          Sanity Company
${tax_invoice_number}                                        1111111111111
${tax_invoice_address}                                       Test Sanity Address

#- Credit Term -#
${payment_method_credit_terms}                               Credit Terms
${cost_center}                                               12345678AA
${cost_center_name}                                          Ascend Corporation
${company_reference_code}                                    99998
${tax_invoice_name_credit_terms}                             Ascend Travel Trans 


### Thank you information ###
${booking_status_credit_card_paynow}                         Your payment is processed successfully.
${booking_status_credit_card_paylater}                       Your reservation is processed successfully.
${booking_status_credit_terms}                               Your reservation is processed successfully.

