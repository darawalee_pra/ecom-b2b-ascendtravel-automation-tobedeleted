*** Variables ***
${booker_first_name}           booker
${booker_last_name}            tester
${booker_email}                narospol@qlovr.com
${booker_mobile}               0343243434
${booker_nationality}          ${EMPTY}

${company_name}                Qlovr
${tax_number}                  4334343443444
${tax_address}                 test

${check_in_day_pay_later}         25
${check_out_day_pay_later}        26

${check_in_day_pay_now}         2
${check_out_day_pay_now}        3