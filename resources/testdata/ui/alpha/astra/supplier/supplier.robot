*** Variables ***
${supplier_data_file}       supplier.xlsx
${create_supplier_json}     create_supplier.json
${edit_supplier_json}       edit_supplier.json
${pdf_file}                 robot_pdf.pdf
${jpg_file}                 robot_jpg.jpg
${png_file}                 robot_png.png
${sheet_name}               suppliertestdata
${page_size_default}        50
${page_size_s}              100
${page_size_m}              150
${page_size_l}              200
${sort_asc}                 ASC
${sort_desc}                DESC
${active_status}            True
${inactive_status}          False
${main_sap_code_1}          100001
${main_sap_code_2}          100010
${main_sap_code_3}          100020
${main_sap_code_4}          300020
${main_sap_code_5}          123567
${alt_sap_code_1}           302890
${alt_sap_code_2}           417094
${alt_sap_code_3}           511073
${alt_currency_id}          3
${main_sap_code_flag}       0
${default_per_mark_up}      10.0
${sup_name_1}               001 asgrad supplier 
${sup_name_2}               Asgard star wars
${sup_name_3}               aSgard supplier 2
${sup_name_4}               @$gard., (300020)/073
${sup_name_5}               567 micro
${keyword_1}                30
${keyword_2}                asgard
${keyword_3}                @
# @{keyword_3}                @   !   $   %   ^   &   *   (   )   _   +   =   -   ?   <   >   /   \   ]   ;   :   .   ,   '
${keyword_4}                testnomatchedresult
${keyword_5}                Contrary to its name, this keyword accepts the alert by default (i.e. presses Ok). accept can be set to a false value to dismiss the alert (i.e. to press Cancel). Contrary to its name, this keyword accepts the alert by default (i.e. presses Ok). accept c

## Requests value
${def_type_id}              1
${def_type_title}           Direct Contact with Net Price
${def_country_id}           212
${def_country_title}        Thailand
${def_supplier_name}        Robot Supplier
${def_address}              Robot Supplier Address
${def_tax_name}             Robot Supplier
${def_tax_id}               9876543210321
${def_tax_address}          Robot Supplier Address
${def_not_follow_invoice}   true
@{def_email_reserve_list}   thortestuser@gmail.com
${def_mark_up}              999.99
${def_include_vat}          true
${def_status}               true
${def_payment_type_id}      1
${def_payment_credit}       true
${def_payment_title}        Petty Cash
${def_payment_days}         25
${def_payment_active}       true
${def_payment_type}         true
${def_account_name}         Robot Account
${def_account_number}       1234567890
${def_bank_id}              1
${def_bank_title}           KBank
${def_payment_note}         Robot Note
${def_is_main_sap_code}     true
${def_currency_id}          3
${def_currency_code}        EUR
${def_currency_title}       Euro
${def_contact_name}         Robot Contact
${def_contact_position}     Front Office
${def_contact_email}        front@adf.com
${def_contact_phone}        0123456789 
